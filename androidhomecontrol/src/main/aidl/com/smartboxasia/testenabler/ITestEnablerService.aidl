package com.smartboxasia.testenabler;

import java.util.List;

interface ITestEnablerService {
    boolean getFlagState(String appName, String flagName);
}