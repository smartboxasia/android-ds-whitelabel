package com.smartboxasia.testenabler;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class TestEnablerHelper {

    private static final String TAG = TestEnablerHelper.class.getSimpleName();

    private static final byte[] serviceSignature = new byte[] {
            48, -126, 3, 61, 48, -126, 2, 37, -96, 3, 2, 1, 2, 2, 4, 5, 41, -34, -60, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 11, 5, 0, 48, 79, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 67, 72, 49, 18, 48, 16, 6, 3, 85, 4, 7, 19, 9, 83, 99, 104, 108, 105, 101, 114, 101, 110, 49, 21, 48, 19, 6,
            3, 85, 4, 10, 19, 12, 100, 105, 103, 105, 116, 97, 108, 115, 116, 114, 111, 109, 49, 21, 48, 19, 6, 3, 85, 4, 3, 19, 12, 76, 97, 115, 115, 101, 32, 72, 97, 110, 115, 101, 110, 48, 30, 23, 13, 49, 51, 48, 55, 49, 49, 49, 53, 51, 53, 53, 48, 90, 23, 13, 52, 56, 48, 55, 48, 50, 49, 53, 51,
            53, 53, 48, 90, 48, 79, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 67, 72, 49, 18, 48, 16, 6, 3, 85, 4, 7, 19, 9, 83, 99, 104, 108, 105, 101, 114, 101, 110, 49, 21, 48, 19, 6, 3, 85, 4, 10, 19, 12, 100, 105, 103, 105, 116, 97, 108, 115, 116, 114, 111, 109, 49, 21, 48, 19, 6, 3, 85, 4, 3, 19,
            12, 76, 97, 115, 115, 101, 32, 72, 97, 110, 115, 101, 110, 48, -126, 1, 34, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 3, -126, 1, 15, 0, 48, -126, 1, 10, 2, -126, 1, 1, 0, -40, 11, -94, -27, -60, 66, -96, -25, -78, -107, 38, 1, 60, 95, 20, -56, -28, -54, 5, 58, -44, -68,
            -32, 6, 60, -54, 50, 112, -64, 33, 105, -34, -77, 20, -28, -17, -90, 81, 42, -38, -39, -117, 33, -110, -126, 73, -25, -127, -105, 109, -53, -37, 107, -1, -29, -26, 117, -98, 47, 77, 7, -4, -17, -10, 116, 83, -66, 85, 38, 105, 127, 101, -107, -60, -40, -51, -5, -110, 27, 98, -114, 34,
            -79, 35, -43, 76, -5, 93, 69, 7, -118, -53, -70, -1, -17, 44, -20, -97, -9, -64, 92, 74, -25, 13, -34, 97, -104, -8, 72, 2, 26, 60, 1, -103, 113, 61, 17, 86, -16, -20, 7, 94, 21, -91, -73, 97, -7, -27, -103, -88, -113, -121, 59, 62, 95, 79, 1, 91, 21, -36, -46, 58, 67, -55, -50, 15,
            -33, -51, 113, -71, -108, 27, -92, 107, 65, -25, -96, -43, -109, 44, 94, 103, 111, -42, -48, 110, -18, 1, -58, -48, -9, -46, -112, -70, -94, 126, 21, 112, -95, 101, -110, 21, 8, -11, -18, 36, 34, -2, 85, -34, 126, -32, -127, 118, -103, -83, -99, 115, 71, -59, -67, -116, -24, 19, -20,
            66, 42, -115, 24, -30, -42, -71, 33, -28, 7, 62, -68, -73, -26, -17, -88, 33, -107, -76, 111, -114, -82, -41, -43, -96, 58, 71, 26, -51, 76, 0, 119, 1, 61, 43, -61, -67, 19, -2, 115, 111, 27, -126, -10, 84, 69, 52, 22, 115, -76, 47, 2, 3, 1, 0, 1, -93, 33, 48, 31, 48, 29, 6, 3, 85, 29,
            14, 4, 22, 4, 20, -106, -122, -48, 40, 83, 56, -36, -1, 50, 87, 119, -118, 94, -32, -54, -78, 108, 78, -30, -56, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 11, 5, 0, 3, -126, 1, 1, 0, -99, -96, 97, 17, -112, 104, 36, -71, -45, -49, -23, -116, 73, -25, -56, -94, -103, 35, 39, -25,
            75, 103, -22, -88, 16, 41, -70, 65, 121, -19, 5, 99, 57, 68, -44, 25, 17, -91, 45, -120, 12, 60, 32, 7, 67, 73, -36, 64, 124, 112, 22, -22, -94, 50, 40, 88, 96, 94, -79, -79, -84, 70, 62, -102, -34, -39, 110, 124, -68, -12, 44, -30, -35, -3, 104, -86, -10, -76, 20, -10, -39, -56, -28,
            -33, 18, 46, 88, -31, 89, -72, 76, 24, 33, -18, -5, -123, -79, -70, 82, 20, 118, -73, -121, 40, -82, 77, 115, 60, 32, 114, 20, -45, 98, 106, 48, 68, -22, -79, 78, 88, -65, -2, 97, 70, 79, -5, 39, -28, 15, -15, 106, 53, -38, -94, 22, -10, 127, 102, 69, -66, 105, 5, -72, -43, -47, 120,
            -84, -70, -10, 19, -31, 71, 102, 62, 57, -59, -117, -83, 61, 58, -5, 59, 4, -112, 31, -91, -40, 22, 34, -17, 13, 84, 74, -58, 59, 87, 43, 15, 102, -73, 89, 104, 111, 86, -100, 49, -51, -38, -39, 82, -66, -29, -82, 102, 15, 21, -14, -103, -57, 112, -124, -29, 8, 41, 55, 43, 25, -65, -44,
            -38, -102, 123, 27, 113, 63, 114, -55, 77, -115, -79, 2, 59, 118, -27, 5, 61, -108, -37, -96, 0, 115, -15, -10, -106, 123, 5, -111, -83, -76, 102, -104, 36, -44, -91, -19, 106, -43, 40, -42, -87, -111, 101, -29, 105, -102, 99
    };

    private static final String TESTENABLER_SERVICE_PKG = "com.smartboxasia.testenabler";
    private static final String TESTENABLER_SERVICE_CLASS = "com.smartboxasia.testenabler.TestEnablerService";

    private static final String APPNAME_FLAG_DELIMITER = "#";

    private static final Map<String, Boolean> FLAGS = Maps.newHashMap();

    private static Context context;
    private static String appName;
    private static List<String> flagNames;

    private static ITestEnablerService mService;
    private static ServiceConnection mConnection = new ServiceConnection() {
        // Called when the connection with the service is established
        public void onServiceConnected(final ComponentName className, final IBinder service) {
            mService = ITestEnablerService.Stub.asInterface(service);
            FLAGS.clear();
            try {
                for (final String flagName : flagNames) {
                    FLAGS.put(flagName, mService.getFlagState(appName, flagName));
                }
            } catch (final RemoteException e) {
                // ignore
            } finally {
                context.unbindService(mConnection);
            }
        }

        // Called when the connection with the service disconnects unexpectedly
        public void onServiceDisconnected(final ComponentName className) {
            mService = null;
        }
    };

    private static Signature[] getPackageSignatures(final Context context, final String packageName) throws NameNotFoundException {
        return context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES).signatures;
    }

    /**
     * Initializes the TestEnablerHelper and tries to establish a connection to the TestEnabler Service. If a connection can be established all the flagNames stored in the manager are cached in this helper.
     * 
     * @param context The application context.
     * @param appName The name of the application whose flagNames should get requested. The name must not be null or empty, and it must not contain the character "#".
     */
    public static void checkForTestEnablerService(final Context context, final String appName, final List<String> flagNames) {
        if (!isNameValid(appName)) {
            Log.d(TAG, "Invalid app name: " + appName + ".");
            return;
        }
        for (final String flagName : flagNames) {
            if (!isNameValid(flagName)) {
                Log.d(TAG, "Invalid flag name: " + flagName + ".");
                return;
            }
        }

        TestEnablerHelper.context = context;
        TestEnablerHelper.appName = appName;
        TestEnablerHelper.flagNames = flagNames;

        if (!isTestEnablerServiceAvailable(context)) {
            return;
        }

        final Intent intent = new Intent();
        intent.setComponent(new ComponentName(TESTENABLER_SERVICE_PKG, TESTENABLER_SERVICE_CLASS));
        context.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    public static boolean isTestEnablerServiceAvailable(final Context context) {
        try {
            final Signature requiredSignatures = new Signature(serviceSignature);
            final Signature[] serviceSignatures = getPackageSignatures(context, TESTENABLER_SERVICE_PKG);
            final boolean disjoint = Collections.disjoint(Lists.newArrayList(requiredSignatures), Lists.newArrayList(serviceSignatures));
            if (disjoint) {
                Log.d(TAG, "This app and the service are not signed with the same key.");
                return false;
            }
        } catch (final NameNotFoundException e) {
            Log.d(TAG, "Could not get signatures of this app or the service.");
            return false;
        }
        return true;
    }

    /**
     * Returns the cached value of the current flag state.
     * 
     * @param flagName The name of the flag whose state should get returned. The flagName name must not be null or empty, and it must not contain the character "#".
     * @return The state of the requested flag or false if it is unknown.
     */
    public static boolean getFlagState(final String flagName) {
        if (!FLAGS.containsKey(flagName)) {
            Log.d(TAG, "Unknown flag " + flagName + ".");
            FLAGS.put(flagName, false);
        }
        return FLAGS.get(flagName);
    }

    private static boolean isNameValid(final String name) {
        if (Strings.isNullOrEmpty(name) || name.contains(APPNAME_FLAG_DELIMITER)) {
            return false;
        }
        return true;
    }
}
