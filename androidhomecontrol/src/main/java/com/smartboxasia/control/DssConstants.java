package com.smartboxasia.control;

public class DssConstants {

    public static class Rooms {

        public static final int GLOBAL = 0;
        public static final int USER_ACTION = -1;
        public static final int UNKNOWN = -2;

        public static boolean isGlobal(final int roomId) {
            return roomId <= 0;
        }

    }

    public static class Groups {

        public static final int ACTION = 0;
        public static final int LIGHT = 1;
        public static final int SHADE = 2;
        public static final int CLIMATE = 3;
        public static final int AUDIO = 4;
        public static final int VIDEO = 5;
        public static final int JOKER = 8;
        public static final int MAX_GROUP = 9;

    }

    public static class Scenes {

        public static final long USER_DEFINED_ACTIONS = 1024;

        public static final long OFF = 0; // off scene called from zone pushbutton (Aus)
        public static final long AREA_1_OFF = 1; // off scene called from area pushbutton 1 (Bereich 1 Aus)
        public static final long AREA_2_OFF = 2; // off scene called from area pushbutton 2 (Bereich 2 Aus)
        public static final long AREA_3_OFF = 3; // off scene called from area pushbutton 3 (Bereich 3 Aus)
        public static final long AREA_4_OFF = 4; // off scene called from area pushbutton 4 (Bereich 4 Aus)
        public static final long PRESET_1 = 5; // scene 1 called from zone pushbutton (Stimmung 1)
        public static final long AREA_1_ON = 6; // scene 1 called from area pushbutton 1 (Bereich 1 Ein)
        public static final long AREA_2_ON = 7; // scene 1 called from area pushbutton 2 (Bereich 2 Ein)
        public static final long AREA_3_ON = 8; // scene 1 called from area pushbutton 3 (Bereich 3 Ein)
        public static final long AREA_4_ON = 9; // scene 1 called from area pushbutton 4 (Bereich 4 Ein)

        public static final long CONT = 10; // dim continue (area pushbutton 1..4)
        public static final long STEP_DOWN = 11; // dim down one step (zone pushbutton)
        public static final long STEP_UP = 12; // dim up one step (zone pushbutton)
        public static final long MIN = 13; // minimal value of a device (light off, blinds down, marquee in, ...)
        public static final long MAX = 14; // maximal value of a device (light max. on, blinds up, marquee out, ...)
        public static final long STOP = 15; // stop (zone pushbutton)
        // 16 is reserved
        public static final long PRESET_2 = 17; // scene 2 called from zone pushbutton (Stimmung 2)
        public static final long PRESET_3 = 18; // scene 3 called from zone pushbutton (Stimmung 3)
        public static final long PRESET_4 = 19; // scene 4 called from zone pushbutton (Stimmung 4)

        public static final long PRESET_12 = 20; // scene 2 called from area pushbutton 1 (Stimmung 12)
        public static final long PRESET_13 = 21; // scene 3 called from area pushbutton 1 (Stimmung 13)
        public static final long PRESET_14 = 22; // scene 4 called from area pushbutton 1 (Stimmung 14)
        public static final long PRESET_22 = 23; // scene 2 called from area pushbutton 2 (Stimmung 22)
        public static final long PRESET_23 = 24; // scene 3 called from area pushbutton 2 (Stimmung 23)
        public static final long PRESET_24 = 25; // scene 4 called from area pushbutton 2 (Stimmung 24)
        public static final long PRESET_32 = 26; // scene 2 called from area pushbutton 3 (Stimmung 32)
        public static final long PRESET_33 = 27; // scene 3 called from area pushbutton 3 (Stimmung 33)
        public static final long PRESET_34 = 28; // scene 4 called from area pushbutton 3 (Stimmung 34)
        public static final long PRESET_42 = 29; // scene 2 called from area pushbutton 4 (Stimmung 42)

        public static final long PRESET_43 = 30; // scene 3 called from area pushbutton 4 (Stimmung 43)
        public static final long PRESET_44 = 31; // scene 4 called from area pushbutton 4 (Stimmung 44)
        public static final long PRESET_10 = 32; // off scene called from scene extender pushbutton 1 (Stimmung 10)
        public static final long PRESET_11 = 33; // on scene called from scene extender pushbutton 1 (Stimmung 11)
        public static final long PRESET_20 = 34; // off scene called from scene extender pushbutton 2 (Stimmung 20)
        public static final long PRESET_21 = 35; // on scene called from scene extender pushbutton 2 (Stimmung 21)
        public static final long PRESET_30 = 36; // off scene called from scene extender pushbutton 3 (Stimmung 30)
        public static final long PRESET_31 = 37; // on scene called from scene extender pushbutton 3 (Stimmung 31)
        public static final long PRESET_40 = 38; // off scene called from scene extender pushbutton 4 (Stimmung 40)
        public static final long PRESET_41 = 39; // on scene called from scene extender pushbutton 4 (Stimmung 41)

        public static final long AUTO_OFF = 40; // Slowly fade down to off value
        // 41 is reserved
        public static final long AREA_1_DOWN = 42; // dim down one step (area pushbutton 1), use scene 10 to continue
        public static final long AREA_1_UP = 43; // dim up one step (area pushbutton 1), use scene 10 to continue
        public static final long AREA_2_DOWN = 44; // dim down one step (area pushbutton 2), use scene 10 to continue
        public static final long AREA_2_UP = 45; // dim up one step (area pushbutton 2), use scene 10 to continue
        public static final long AREA_3_DOWN = 46; // dim down one step (area pushbutton 3), use scene 10 to continue
        public static final long AREA_3_UP = 47; // dim up one step (area pushbutton 3), use scene 10 to continue
        public static final long AREA_4_DOWN = 48; // dim down one step (area pushbutton 4), use scene 10 to continue
        public static final long AREA_4_UP = 49; // dim up one step (area pushbutton 4), use scene 10 to continue

        public static final long LOCAL_OFF = 50; // local off scene (called longernally from local pushbutton)
        public static final long LOCAL_ON = 51; // local on scene (called longernally from local pushbutton)
        public static final long AREA_1_STOP = 52; // stop (area pushbutton 1)
        public static final long AREA_2_STOP = 53; // stop (area pushbutton 2)
        public static final long AREA_3_STOP = 54; // stop (area pushbutton 3)
        public static final long AREA_4_STOP = 55; // stop (area pushbutton 4)
        // 56 is reserved
        // 57 is reserved
        // 58 is reserved
        // 59 is reserved

        // 60 is reserved
        // 61 is reserved
        // 62 is reserved
        // 63 is reserved
        public static final long AUTO_STANDBY = 64;
        public static final long PANIC = 65;
        public static final long ENERGY_OVERLOAD = 66;
        public static final long STANDBY = 67;
        public static final long DEEP_OFF = 68;
        public static final long SLEEPING = 69;

        public static final long WAKE_UP = 70;
        public static final long PRESENT = 71;
        public static final long ABSENT = 72;
        public static final long BELL_SINGAL = 73;
        public static final long ALARM_1 = 74;
        public static final long ROOM_WAKEUP = 75;
        public static final long FIRE = 76;
        // 77 is reserved
        // 78 is reserved
        // 79 is reserved

        // 80 is reserved
        // 81 is reserved
        // 82 is reserved
        public static final long ALARM_2 = 83;
        public static final long ALARM_3 = 84;
        public static final long ALARM_4 = 85;
        public static final long WIND = 86;
        public static final long NO_WIND = 87;
        public static final long RAIN = 88;
        public static final long NO_RAIN = 89;

        public static final long HAIL = 90;
        public static final long NO_HAIL = 91;
        // 92 is reserved
        // 93 is reserved
        // 94 is reserved
        // 95 is reserved
        // 96 is reserved
        // 97 is reserved
        // 98 is reserved
        // 99 is reserved

        // 100 is reserved
        // 101 is reserved
        // 102 is reserved
        // 103 is reserved
        // 104 is reserved
        // 105 is reserved
        // 106 is reserved
        // 107 is reserved
        // 108 is reserved
        // 109 is reserved

        // 110 is reserved
        // 111 is reserved
        // 112 free for individual use
        // 113 free for individual use
        // 114 free for individual use
        // 115 free for individual use
        // 116 free for individual use
        // 117 free for individual use
        // 118 free for individual use
        // 119 free for individual use

        // 120 free for individual use
        // 121 free for individual use
        // 122 free for individual use
        // 123 free for individual use
        // 124 free for individual use
        // 125 free for individual use
        // 126 free for individual use
        // 127 free for individual use

    }

    public static class OutputModes {

        public static final int DISABLED = 0; // No output or output disabled
        public static final int SWITCHED = 16; // Switched
        public static final int RMS_DIMMER = 17; // RMS (root mean square) dimmer
        public static final int RMS_DIMMER_WITH_CURVE = 18; // RMS dimmer with characteristic curve
        public static final int PHASE_CONTROL_DIMMER = 19; // Phase control dimmer
        public static final int PHASE_CONTROL_DIMMER_WITH_CURVE = 20; // Phase control dimmer with characteristic curve
        public static final int REVERSE_PHASE_CONTROL_DIMMER = 21; // Reverse phase control dimmer
        public static final int DIMMABLE = 22; // Reverse phase control dimmer with characteristic curve
        public static final int PWM_DIMMER = 23; // PWM (pulse width modulation)
        public static final int PWM_DIMMER_WITH_CURVE = 24; // PWM with characteristic curve
        public static final int POSITIONING_CONTROL = 33; // Positioning control
        public static final int RELAY_SWITCHED = 39; // Relay with switched mode scene table configuration
        public static final int RELAY_WIPED = 40; // Relay with wiped mode scene table configuration
        public static final int RELAY_SAVING = 41; // Relay with saving mode scene table configuration
        public static final int POSITIONING_CONTROL_UNCALIBRATED = 42; // Positioning control for uncalibrated shutter

    }

    public static class DeviceTypes {

        public static final int JALOUSIE = 3292;
        public static final int MARQUISE = 3282;

    }
}
