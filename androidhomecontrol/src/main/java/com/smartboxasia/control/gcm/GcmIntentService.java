/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.smartboxasia.control.gcm;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

import com.smartboxasia.control.R;
import com.smartboxasia.control.ui.overview.RoomOverview;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.common.collect.Lists;

/**
 * This {@code IntentService} does the actual handling of the GCM message. {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a partial wake lock for this service while the
 * service does its work. When the service is finished, it calls {@code completeWakefulIntent()} to release the wake lock.
 */
public class GcmIntentService extends IntentService {

    public static final String TAG = "SMARTBOX";

    private static final String PUSH_NOTIFICATION_HAIL_WARNING_TEST = "PUSH_NOTIFICATION_HAIL_WARNING_TEST";
    private static final String PUSH_NOTIFICATION_HAIL_WARNING = "PUSH_NOTIFICATION_HAIL_WARNING";
    private static final String PUSH_NOTIFICATION_HAIL_IS_OVER = "PUSH_NOTIFICATION_HAIL_IS_OVER";
    private static final String PUSH_NOTIFICATION_HAIL_EXECUTED = "PUSH_NOTIFICATION_HAIL_EXECUTED";

    private static final String PUSH_NOTIFICATION_DSS_EVENT_PUSH = "PUSH_NOTIFICATION_DSS_EVENT_PUSH";

    private static int PUSH_NOTIFICATION_ID = 1000;

    private List<String> pushIds = Lists.newArrayList(PUSH_NOTIFICATION_DSS_EVENT_PUSH);
    private List<String> hailWarningPushIds = Lists.newArrayList(PUSH_NOTIFICATION_HAIL_WARNING_TEST, PUSH_NOTIFICATION_HAIL_WARNING, PUSH_NOTIFICATION_HAIL_IS_OVER, PUSH_NOTIFICATION_HAIL_EXECUTED);

    private NotificationManager mNotificationManager;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @SuppressLint("Wakelock")
    @SuppressWarnings("deprecation")
    @Override
    protected void onHandleIntent(final Intent intent) {
        final Bundle extras = intent.getExtras();
        final GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        final String messageType = gcm.getMessageType(intent);

        if (extras != null && !extras.isEmpty()) { // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM will be extended in the future with new message types, just ignore any message types you're not interested in, or that you don't recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // Log notification of received message.
                Log.i(TAG, "Received: " + extras.toString());

                mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

                final String pushId = (String) extras.get("notificationId");
                // if hail warning
                if (hailWarningPushIds.contains(pushId)) {

                    // ignore hail warning push notifications

                } else if (pushIds.contains(pushId)) {

                    final String msg = extras.getString("body");
                    final Notification.Builder mBuilder = new Notification.Builder(this).setSmallIcon(R.drawable.icon).setContentTitle("SMARTBOX").setContentText(msg).setStyle(new Notification.BigTextStyle().bigText(msg))
                            .setDefaults(Notification.DEFAULT_ALL).setAutoCancel(true);

                    final PendingIntent contentIntent = createPendingIntent(RoomOverview.class);
                    mBuilder.setContentIntent(contentIntent);

                    // wake device
                    final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                    final WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "SMARTBOX");
                    wl.acquire(5000);

                    mNotificationManager.notify(PUSH_NOTIFICATION_ID, mBuilder.build());
                } else {
                    // ignore unknown messages
                }
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private PendingIntent createPendingIntent(final Class<?> className) {
        final Intent pendingIntent = new Intent(this, className);
        pendingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return PendingIntent.getActivity(this, 0, pendingIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
