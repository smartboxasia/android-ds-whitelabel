package com.smartboxasia.control.gcm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;

import com.smartboxasia.control.App;
import com.smartboxasia.control.BuildConfig;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.helper.SSLSocketFactoryCreator;
import com.smartboxasia.control.data.helper.UUIDHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.common.base.Strings;

public class PushNotificationServiceImpl {

    private static final String TAG = PushNotificationServiceImpl.class.getSimpleName();
    private static final String GCM_PREFS = "SMARTBOXpushNotificationId";
    private static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "app_version";
    private final String SENDER_ID;

    public static final String CLOUD_RETURN_CODE = "ReturnCode";
    public static final String CLOUD_RETURN_MESSAGE = "ReturnMessage";

    private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    private ConnectionData connectionData;

    private GoogleCloudMessaging gcm;

    private String regid;

    public PushNotificationServiceImpl(final Context context, final ConnectionData connectionData) {
        SENDER_ID = context.getString(R.string.google_sender_id);

        this.connectionData = connectionData;

        checkGcmRegistration(context);

        sendRegistrationIdToBackend();
    }

    /**
     * Sends HTTP Get request and returns result or empty string.
     * 
     * @param commandUrl the file part of the url to request
     * @param appendSessionToken if true the session token will be appended, if false it will not be appended
     * @return the result string or the empty string
     * @throws IOException in case of connection trouble or malformed url
     * @throws UnknownCertificateExcpetion
     */
    public String sendCommand(final String commandUrl) throws IOException {
        String result = null;
        try {
            result = getResultString(commandUrl);
        } catch (final Exception e) {
            throw new IOException(e);
        }

        return result;
    }

    private String getResultString(final String commandUrl) throws MalformedURLException, IOException, ProtocolException {
        final StringBuilder result = new StringBuilder();
        final String urlString = connectionData.getCloudUrl() + commandUrl;
        final URL url = new URL(urlString);
        if (BuildConfig.DEBUG) {
            Log.v(TAG, url.toString());
        }
        final HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setDoOutput(false);
        connection.setRequestMethod("POST");
        connection.setConnectTimeout(15000);
        connection.setReadTimeout(30000);
        connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        connection.setRequestProperty("Content-Length", "0");
        connection.setSSLSocketFactory(SSLSocketFactoryCreator.createSslSocketFactory());
        connection.setHostnameVerifier(new HostnameVerifier() {

            @Override
            public boolean verify(final String hostname, final SSLSession session) {
                return true;
            }
        });

        connection.connect();

        final InputStream content = connection.getInputStream();
        try {
            final BufferedReader reader = new BufferedReader(new InputStreamReader(content));
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
        } finally {
            content.close();
        }
        return result.toString();
    }

    // //////////////////////////////////////////////////////////////////
    // GCM registration implemented following the android developer guide
    // //////////////////////////////////////////////////////////////////

    private void checkGcmRegistration(final Context context) {
        // check if push notifications will work and register the device
        if (checkPlayServices(context)) {
            gcm = GoogleCloudMessaging.getInstance(context);
            regid = getPushNotificationId();

            if (regid.isEmpty()) {
                registerInBackground();
            }

        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
    }

    private boolean checkPlayServices(final Context context) {
        final int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                showPlayServiceWarning(resultCode);
            } else {
                Log.i(TAG, "This device is not supported by play services");
            }
            return false;
        }
        return true;
    }

    private void showPlayServiceWarning(final int resultCode) {
        final Intent intent = new Intent(App.getInstance(), PlayServiceWarningActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(PlayServiceWarningActivity.KEY_RESULT_CODE, resultCode);
        App.getInstance().startActivity(intent);
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's shared preferences.
     */
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(final Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(App.getInstance());
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    // Persist the regID - no need to register again.
                    PushNotificationServiceImpl.this.setPushNotificationId(regid);

                    // You should send the registration ID to your server over HTTP, so it
                    // can use GCM/HTTP or CCS to send messages to your app.
                    sendRegistrationIdToBackend();

                } catch (final IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(final String msg) {
                if (!Strings.isNullOrEmpty(msg)) {
                    Log.i(TAG, msg);
                }
            }
        }.execute(null, null, null);
    }

    private String getPushNotificationId() {
        final SharedPreferences prefs = App.getInstance().getSharedPreferences(GCM_PREFS, Context.MODE_PRIVATE);
        final String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID since the existing regID is
        // not guaranteed to work with the new app version.
        final int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        final int currentVersion = getAppVersion(App.getInstance());
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(final Context context) {
        try {
            final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (final NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void setPushNotificationId(final String regId) {
        final SharedPreferences prefs = App.getInstance().getSharedPreferences(GCM_PREFS, Context.MODE_PRIVATE);
        final int appVersion = getAppVersion(App.getInstance());
        Log.i(TAG, "Saving regId on app version " + appVersion);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send messages to your app.
     */
    private void sendRegistrationIdToBackend() {
        // if we are logged into a SMARTBOX account, update the registration id! (it might change on updating)
        if (connectionData.isCloudLogin()) {
            setupPushNotification();
        }
    }

    private void setupPushNotification() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                setupPushNotificationTask();
            }
        });
    }

    private void setupPushNotificationTask() {
        final String appToken = connectionData.getApplicationToken();

        if (Strings.isNullOrEmpty(appToken)) {
            return; // we are missing a valid app token
        }

        // get uuid
        final String uuid = UUIDHelper.getUuid();

        // get current language
        final String currentLanguage = Locale.getDefault().getLanguage();

        // get push notification id
        final String registrationId = getPushNotificationId();
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration ID not found while registering push notification");
            return;
        }

        final String request = "public/service/v1_0/NotificationServiceConfiguration/SetPushNotificationChannel?" + "appToken=" + appToken + "&mobileAppUUID=" + uuid + "&channelID=" + registrationId + "&channelType=GCM&language=" + currentLanguage;

        try {
            final String resultString = sendCommand(request);
            final JSONObject result = new JSONObject(resultString);
            if (result.getInt(CLOUD_RETURN_CODE) == 0) {
                Log.d(TAG, "Succesfully set up push notification");
            } else {
                Log.e(TAG, ("setting up push notification not ok: " + result.getString(CLOUD_RETURN_MESSAGE)));
            }
        } catch (final IOException e) {
            Log.e(TAG, ("setting up push notification not ok: " + e.getMessage()));
        } catch (final JSONException e) {
            Log.e(TAG, ("setting up push notification not ok: " + e.getMessage()));
        }
    }
}
