package com.smartboxasia.control;

import java.util.ArrayList;

import android.app.Application;
import android.os.AsyncTask;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.config.connection.ConnectionConfigService;
import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.domain.LoginResult;
import com.smartboxasia.control.events.LoginResultEvent;
import com.smartboxasia.control.gcm.PushNotificationServiceImpl;
import com.smartboxasia.testenabler.TestEnablerHelper;
import com.google.android.gms.analytics.Tracker;
import com.google.common.collect.Lists;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

public class App extends Application {

    public static final String DEBUG_MODE_FLAG = "debugMode";

    private static final String DEBUG_MODE_APPNAME = "dSHomeControl";
    private static final ArrayList<String> DEBUG_MODE_FLAGNAMES = Lists.newArrayList(DEBUG_MODE_FLAG);

    public static final String EXTRA_SHOW_PUSH_UP_DOWN_ANIMATION = "com.smartboxasia.sia.push_up_down_animation";

    public static final int REQUEST_CODE_BASE_HOMESCREEN = 4000;
    public static final int REQUEST_CODE_BASE_CONFIGURATION = 5000;

    public static final Object REQUEST_LOCK = new Object();

    public static final int DIMMING_DELAY = 500; // in milliseconds
    public static final int METER_POLL_DELAY = 1500; // in milliseconds
    public static final int DSS_ACTION_DELAY = 1000; // in milliseconds
    public static final int DSS_ACTION_THROTTLED_DELAY = 5000; // in milliseconds

    public static final int NUMBEROFACTIVITIES = 64;

    public static final EventBus eventBus = new EventBus();

    Tracker gmsTracker;

    private static App instance;

    public static boolean credentialsDialogShown;

    public App() {
        instance = this;
    }

    public static App getInstance() {
        return instance;
    }

    @Subscribe
    public void onEvent(final LoginResultEvent event) {
        final ConnectionData activeConnectionData = Connection.getActiveConnectionData();
        if (event.getResult() == LoginResult.LOGIN_RESULT_OK && activeConnectionData != null) {

            // remember the last successful connection
            ConnectionConfigService.updateLastUsedConnectionData(activeConnectionData.getUrl());

            // load room list settings
            RoomsStore.loadRoomSequenceFromPrefs();

            credentialsDialogShown = false;

            // initialize push notifications
            if (activeConnectionData.isCloudLogin()) {
                new PushNotificationServiceImpl(getApplicationContext(), activeConnectionData);
            }

            updateAnalyticsActivation();

            GAHelper.sendConnectionTypeEvent(activeConnectionData);
        }
    }

    private void updateAnalyticsActivation() {
        // do not send GA data if no cloud connection is set up
        for (final ConnectionData data : ConnectionConfigService.getAllConnectionData()) {
            if (data.isCloudLogin()) {
                GAHelper.setAppOptOut(false);
                return;
            }
        }
        GAHelper.setAppOptOut(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        GAHelper.createTracker(this);

        // Start async check if debugging for this app is on
        TestEnablerHelper.checkForTestEnablerService(this, DEBUG_MODE_APPNAME, DEBUG_MODE_FLAGNAMES);

        eventBus.register(this);

        final ConnectionData lastUsedConnectionData = ConnectionConfigService.getLastUsedConnectionData();

        Connection.reconnectConnectionService(lastUsedConnectionData, null, null, null, false);

        // Workaround for a crash in android 4.0.3 web views if there has not been
        // an AsyncTask started from a UI thread (http://stackoverflow.com/questions/6968744)
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(final Void... params) {
                return null;
            }
        }.execute();
    }

    public static void hideIME(final View... views) {
        // Hide the keyboard when dialog is closed
        final InputMethodManager inputManager = (InputMethodManager) instance.getSystemService(INPUT_METHOD_SERVICE);
        for (final View view : views) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
