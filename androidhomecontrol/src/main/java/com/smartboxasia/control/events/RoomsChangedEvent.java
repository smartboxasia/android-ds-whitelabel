package com.smartboxasia.control.events;

public class RoomsChangedEvent extends BaseEvent {

    public RoomsChangedEvent() {
    }

    public RoomsChangedEvent(final int errorCode) {
        super(errorCode);
    }

}
