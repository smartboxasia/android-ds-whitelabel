package com.smartboxasia.control.events;

public class DeviceBlinkEvent extends BaseEvent {

    public DeviceBlinkEvent() {
    }

    public DeviceBlinkEvent(final int errorCode) {
        super(errorCode);
    }

}
