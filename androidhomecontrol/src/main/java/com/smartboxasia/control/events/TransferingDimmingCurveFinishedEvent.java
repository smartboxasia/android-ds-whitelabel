package com.smartboxasia.control.events;

public class TransferingDimmingCurveFinishedEvent {

    public static final int ERROR_NO_CURVES = -1;
    public static final int ERROR_TRANSFER_IN_PROGRESS = 26;

    private final boolean success;
    private final int errorCode;

    public TransferingDimmingCurveFinishedEvent() {
        this.success = true;
        this.errorCode = 0;
    }

    public TransferingDimmingCurveFinishedEvent(int errorCode) {
        this.success = false;
        this.errorCode = errorCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public int getErrorCode() {
        return errorCode;
    }

}
