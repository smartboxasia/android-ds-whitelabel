package com.smartboxasia.control.events;

public class GotUserDefinedActionsEvent extends BaseEvent {

    public static final int ERROR_NO_UDA_DEFINED = 1;

    public GotUserDefinedActionsEvent() {
    }

    public GotUserDefinedActionsEvent(final int errorCode) {
        super(errorCode);
    }

}
