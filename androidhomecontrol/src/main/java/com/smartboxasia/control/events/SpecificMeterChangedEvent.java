package com.smartboxasia.control.events;

public class SpecificMeterChangedEvent extends BaseEvent {

    public static final int ERROR_JSON = 1;

    private final String meterId;
    private final int specificConsumption;

    public SpecificMeterChangedEvent(final String meterId, final int specificConsumption) {
        this.meterId = meterId;
        this.specificConsumption = specificConsumption;
    }

    public SpecificMeterChangedEvent(final int errorCode, final String meterId, final int specificConsumption) {
        super(errorCode);
        this.meterId = meterId;
        this.specificConsumption = specificConsumption;
    }

    public int getSpecificMeterConsumption() {
        return specificConsumption;
    }

    public String getMeterId() {
        return meterId;
    }
}
