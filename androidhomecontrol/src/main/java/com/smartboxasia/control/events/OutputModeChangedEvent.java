package com.smartboxasia.control.events;

import com.smartboxasia.control.domain.OutputMode;

public class OutputModeChangedEvent extends BaseEvent {
    private final OutputMode newMode;

    public OutputModeChangedEvent(final OutputMode newMode) {
        this.newMode = newMode;
    }

    public OutputModeChangedEvent(final int errorCode, final OutputMode newMode) {
        super(errorCode);
        this.newMode = newMode;
    }

    public OutputMode getNewOutputMode() {
        return newMode;
    }
}
