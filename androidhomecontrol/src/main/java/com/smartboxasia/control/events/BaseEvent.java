package com.smartboxasia.control.events;

import com.smartboxasia.control.data.helper.JSONCallbackBase;

public class BaseEvent {

    private int errorCode;

    public BaseEvent() {
        errorCode = JSONCallbackBase.ERROR_NONE;
    }

    public BaseEvent(final int errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorCode(final int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public boolean isSuccess() {
        return errorCode == JSONCallbackBase.ERROR_NONE;
    }

}
