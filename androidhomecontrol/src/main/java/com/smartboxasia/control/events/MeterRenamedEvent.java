package com.smartboxasia.control.events;

public class MeterRenamedEvent extends BaseEvent {

    private final String name;

    public MeterRenamedEvent(final String name) {
        this.name = name;
    }

    public MeterRenamedEvent(final int errorCode, final String name) {
        super(errorCode);
        this.name = name;
    }

    public String getNewMeterName() {
        return name;
    }
}
