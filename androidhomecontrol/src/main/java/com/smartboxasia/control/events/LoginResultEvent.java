package com.smartboxasia.control.events;

import com.smartboxasia.control.domain.LoginResult;

public class LoginResultEvent {

    private final LoginResult result;

    public LoginResultEvent(final LoginResult result) {
        this.result = result;
    }

    public LoginResult getResult() {
        return result;
    }
}
