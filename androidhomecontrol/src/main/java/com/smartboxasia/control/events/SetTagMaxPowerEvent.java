package com.smartboxasia.control.events;

public class SetTagMaxPowerEvent extends BaseEvent {

    public static final int ERROR_CLOUD = 1;

    public SetTagMaxPowerEvent() {
    }

    public SetTagMaxPowerEvent(int errorCode) {
        super(errorCode);
    }

}
