package com.smartboxasia.control.events;

public class SceneSavedEvent extends BaseEvent {

    public SceneSavedEvent() {
    }

    public SceneSavedEvent(final int errorCode) {
        super(errorCode);
    }
}
