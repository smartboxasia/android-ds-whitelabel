package com.smartboxasia.control.events;

import java.util.List;

import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsTag;

public class GetAllTagsReceivedEvent extends BaseEvent {

    private final DsDevice device;
    private final List<DsTag> dsTags;

    public GetAllTagsReceivedEvent(final DsDevice device, final List<DsTag> dsTags) {
        super();
        this.device = device;
        this.dsTags = dsTags;
    }

    public GetAllTagsReceivedEvent(final int errorCode, final DsDevice device) {
        super(errorCode);
        this.device = device;
        this.dsTags = null;
    }

    public DsDevice getDevice() {
        return device;
    }

    public List<DsTag> getDsTags() {
        return dsTags;
    }

}
