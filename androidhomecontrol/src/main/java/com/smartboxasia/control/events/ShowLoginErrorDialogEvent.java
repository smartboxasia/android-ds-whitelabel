package com.smartboxasia.control.events;

public class ShowLoginErrorDialogEvent {

    private final String title;
    private final String message;
    private final boolean showButton;

    public ShowLoginErrorDialogEvent(final String title, final String message, final boolean showButton) {
        this.title = title;
        this.message = message;
        this.showButton = showButton;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public boolean getShowButton() {
        return showButton;
    }

}
