package com.smartboxasia.control.events;

public class SetTagSocketTypeEvent extends BaseEvent {

    public static final int ERROR_CLOUD = 1;

    public SetTagSocketTypeEvent() {
    }

    public SetTagSocketTypeEvent(final int errorCode) {
        super(errorCode);
    }

}
