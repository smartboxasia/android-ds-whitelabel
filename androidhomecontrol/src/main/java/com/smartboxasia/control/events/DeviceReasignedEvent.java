package com.smartboxasia.control.events;

public class DeviceReasignedEvent extends BaseEvent {

    private final int newRoomId;

    public DeviceReasignedEvent(final int newRoomId) {
        this.newRoomId = newRoomId;
    }

    public DeviceReasignedEvent(final int errorCode, final int newRoomId) {
        super(errorCode);
        this.newRoomId = newRoomId;
    }

    public int getNewRoomId() {
        return newRoomId;
    }
}
