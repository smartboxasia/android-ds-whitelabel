package com.smartboxasia.control.events;

public class CheckIlluminantResultEvent extends BaseEvent {

    public static final int ERROR = 1;

    public final String deviceDimmableResult;
    public final String fittingCheckResult;
    public final String illuminantDimmingResult;
    public final boolean isTransferDimmingCurveDialog;

    public CheckIlluminantResultEvent(final String deviceDimmableResult, final String fittingCheckResult, final String illuminantDimmingResult, final boolean isTransferDimmingCurveDialog) {
        this.deviceDimmableResult = deviceDimmableResult;
        this.fittingCheckResult = fittingCheckResult;
        this.illuminantDimmingResult = illuminantDimmingResult;
        this.isTransferDimmingCurveDialog = isTransferDimmingCurveDialog;
    }

    public CheckIlluminantResultEvent(final int errorCode) {
        super(errorCode);
        this.deviceDimmableResult = "";
        this.fittingCheckResult = "";
        this.illuminantDimmingResult = "";
        this.isTransferDimmingCurveDialog = false;
    }

}
