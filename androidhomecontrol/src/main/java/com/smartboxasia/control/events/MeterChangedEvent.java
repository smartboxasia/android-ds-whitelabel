package com.smartboxasia.control.events;

public class MeterChangedEvent extends BaseEvent {

    public MeterChangedEvent() {
    }

    public MeterChangedEvent(final int errorCode) {
        super(errorCode);
    }

}
