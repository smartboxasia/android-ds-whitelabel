package com.smartboxasia.control.events;

public class AllMetersConsumptionsChangedEvent extends BaseEvent {

    public AllMetersConsumptionsChangedEvent() {
    }

    public AllMetersConsumptionsChangedEvent(final int errorCode) {
        super(errorCode);
    }

}
