package com.smartboxasia.control.events;

public class DeviceValueChangedEvent extends BaseEvent {

    public DeviceValueChangedEvent() {
    }

    public DeviceValueChangedEvent(final int errorCode) {
        super(errorCode);
    }

}
