package com.smartboxasia.control.events;

public class RoomRenamedEvent extends BaseEvent {

    public RoomRenamedEvent() {

    }

    public RoomRenamedEvent(final int errorCode) {
        super(errorCode);
    }

}
