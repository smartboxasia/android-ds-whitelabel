package com.smartboxasia.control.events;

public class DeviceStateChangedEvent extends BaseEvent {

    public DeviceStateChangedEvent() {
    }

    public DeviceStateChangedEvent(final int errorCode) {
        super(errorCode);
    }
}
