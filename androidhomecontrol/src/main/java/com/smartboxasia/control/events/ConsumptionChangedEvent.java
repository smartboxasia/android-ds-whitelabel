package com.smartboxasia.control.events;

public class ConsumptionChangedEvent extends BaseEvent {

    public ConsumptionChangedEvent() {
    }

    public ConsumptionChangedEvent(final int errorCode) {
        super(errorCode);
    }

}
