package com.smartboxasia.control.events;

public class SceneRenamedEvent extends BaseEvent {

    private final String name;

    public SceneRenamedEvent(final String name) {
        this.name = name;
    }

    public SceneRenamedEvent(final int errorCode, final String name) {
        super(errorCode);
        this.name = name;
    }

    public String getNewName() {
        return name;
    }
}
