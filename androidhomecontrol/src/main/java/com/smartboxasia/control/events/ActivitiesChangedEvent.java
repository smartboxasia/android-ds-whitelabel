package com.smartboxasia.control.events;

public class ActivitiesChangedEvent extends BaseEvent {

    public ActivitiesChangedEvent() {
    }

    public ActivitiesChangedEvent(final int errorCode) {
        super(errorCode);
    }
}
