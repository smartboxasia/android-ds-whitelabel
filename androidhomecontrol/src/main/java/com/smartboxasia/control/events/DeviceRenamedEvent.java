package com.smartboxasia.control.events;

public class DeviceRenamedEvent extends BaseEvent {

    private final String newDeviceName;

    public DeviceRenamedEvent(final String newDeviceName) {
        this.newDeviceName = newDeviceName;
    }

    public DeviceRenamedEvent(final int errorCode, final String newDeviceName) {
        super(errorCode);
        this.newDeviceName = newDeviceName;
    }

    public String getNewDeviceName() {
        return newDeviceName;
    }
}
