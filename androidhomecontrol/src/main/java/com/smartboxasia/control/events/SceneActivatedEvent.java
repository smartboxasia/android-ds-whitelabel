package com.smartboxasia.control.events;

public class SceneActivatedEvent extends BaseEvent {

    public SceneActivatedEvent() {
    }

    public SceneActivatedEvent(final int errorCode) {
        super(errorCode);
    }

}
