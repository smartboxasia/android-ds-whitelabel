package com.smartboxasia.control.ui.overview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.events.ActivitiesChangedEvent;
import com.smartboxasia.control.events.DeviceStateChangedEvent;
import com.smartboxasia.control.events.RoomsChangedEvent;
import com.smartboxasia.control.events.SceneActivatedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.smartboxasia.control.ui.helper.ConsumptionHelper;
import com.smartboxasia.control.ui.helper.ConsumptionViewUpdater;
import com.smartboxasia.control.ui.helper.MeteringViewUpdater;
import com.smartboxasia.control.ui.helper.ShakeEventListener;
import com.smartboxasia.control.ui.settings.ConfigurationScreen;
import com.google.common.base.Strings;
import com.google.common.eventbus.Subscribe;

public class AdvancedButton extends ConnectivityMonitoringFragmentActivity {

    private static final int REQEUST_CODE_BASE_ADVANCE_BUTTON = 300;

    public static final int REQUEST_CODE_CONFIGURATION_SCREEN = App.REQUEST_CODE_BASE_HOMESCREEN + REQEUST_CODE_BASE_ADVANCE_BUTTON;

    public static final String EXTRA_CURRENT_NAME = "com.smartboxasia.sia.currentName";
    public static final String EXTRA_TYPE_NAME = "com.smartboxasia.sia.typeName";
    public static final String EXTRA_IS_DEVICE = "com.smartboxasia.sia.isDevice";
    public static final String EXTRA_SCENE_ID = "com.smartboxasia.sia.sceneId";
    public static final String EXTRA_DEVICE_ID = "com.smartboxasia.sia.deviceId";
    public static final String EXTRA_ROOM_ID = "com.smartboxasia.sia.roomId";

    // Flag if receiver is registered
    private boolean isDevice;
    private boolean isTilting = false;
    private boolean isTouching = false;
    private int roomId;
    private String deviceId;
    private long sceneId;
    private ImageButton toggleButton;
    private View toggleBackground;
    private View plusButton;
    private View minusButton;

    private OrientationEventListener myOrientationEventListener;
    private SensorManager mSensorManager;
    private ShakeEventListener mSensorListener;

    private FavoritesBarFragment favoritesBarFragment;

    private final ConsumptionHelper consumption = new ConsumptionHelper(this, R.id.ConsumptionTextView, R.id.ConsumptionColor);

    private boolean buttonIsActive;

    @Subscribe
    public void onEvent(final SceneActivatedEvent event) {
        markButton();
        favoritesBarFragment.updateFavoriteList();
    }

    @Subscribe
    public void onEvent(final ActivitiesChangedEvent event) {
        markButton();
    }

    @Subscribe
    public void onEvent(final RoomsChangedEvent event) {
        markButton();
    }

    @Subscribe
    public void onEvent(final DeviceStateChangedEvent event) {
        markButton();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // resume listening for shakes
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);

        // update favorites
        favoritesBarFragment.updateFavoriteList();

        // Register for events
        App.eventBus.register(this);

        // resume polling the consumption (this also gets a new session token, in case it timed out)
        consumption.onResume();

        // update rooms if device, else update activites
        if (isDevice) {
            DssService.updateRooms(getApplicationContext());
        } else {
            DssService.updateActivties(getApplicationContext());
        }

        GAHelper.sendScreenViewEvent("Overview Advanced");
    }

    @Override
    protected void onPause() {
        super.onPause();

        consumption.onPause();

        // Unregister for events
        App.eventBus.unregister(this);

        // stop listening for shakes
        mSensorManager.unregisterListener(mSensorListener);

        // stop listening for orientation
        myOrientationEventListener.disable();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.overview_control_button_light);

        favoritesBarFragment = (FavoritesBarFragment) getSupportFragmentManager().findFragmentById(R.id.FavoritesBar);

        mSensorListener = new ShakeEventListener();
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);

        mSensorListener.setOnShakeListener(new ShakeEventListener.OnShakeListener() {
            Handler handler = new android.os.Handler();
            boolean sleep = false;

            public void onShake() {
                if (!sleep) {
                    toggleButtonClick(null);
                    sleep = true;
                    // prevent "double shakes" by waiting a second to reenable
                    waitForPeriod(App.DSS_ACTION_DELAY);
                }
            }

            // changes sleep boolean after "period" ms
            public void waitForPeriod(final int period) {
                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        sleep = false;

                    }
                }, period);
            }
        });

        final String currentName = getIntent().getExtras().getString(EXTRA_CURRENT_NAME);
        final String typeName = getIntent().getExtras().getString(EXTRA_TYPE_NAME);
        isDevice = getIntent().getExtras().getBoolean(EXTRA_IS_DEVICE, false);
        roomId = getIntent().getExtras().getInt(EXTRA_ROOM_ID);
        deviceId = getIntent().getExtras().getString(EXTRA_DEVICE_ID);
        sceneId = getIntent().getExtras().getLong(EXTRA_SCENE_ID);

        final TextView tv = (TextView) findViewById(R.id.currentTextView);
        tv.setText(currentName);
        final TextView tv2 = (TextView) findViewById(R.id.TitleTextView);
        tv2.setText(typeName);

        toggleButton = (ImageButton) findViewById(R.id.toggleButton);
        toggleBackground = findViewById(R.id.advancedButtonLayout);

        // set a repeatlistener for 250 ms interval
        plusButton = findViewById(R.id.plusButton);
        minusButton = findViewById(R.id.minusButton);
        plusButton.setOnTouchListener(new RepeatListener(App.DIMMING_DELAY, true));
        minusButton.setOnTouchListener(new RepeatListener(App.DIMMING_DELAY, false));

        myOrientationEventListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_GAME) {
            // private int period = 250;
            Handler handler = new android.os.Handler();
            boolean sleep = false;

            @Override
            public void onOrientationChanged(final int arg0) {
                if (!isTouching) {
                    final DsRoom room = RoomsStore.get_room_by_id(roomId);
                    final DsDevice device = room.get_device_by_id(deviceId);
                    // increment or decrement depending on tilt
                    if (arg0 > 250 && arg0 < 320 && !sleep) {
                        final boolean firstCall = !isTilting;
                        isTilting = true;
                        if (isDevice) {
                            DssService.callSceneOnDevice(getApplicationContext(), device, DssConstants.Scenes.STEP_DOWN);
                        } else {
                            DssService.callActivity(getApplicationContext(), room.get_id(), DssConstants.Groups.LIGHT, getSceneIdToCall(firstCall, false), false);
                        }

                        minusButton.setPressed(true);
                        plusButton.setPressed(false);

                        // wait for downstream
                        sleep = true;
                        waitForPeriod(App.DIMMING_DELAY);
                    } else if (arg0 > 40 && arg0 < 120 && !sleep) {
                        final boolean firstCall = !isTilting;
                        isTilting = true;
                        if (isDevice) {
                            DssService.callSceneOnDevice(getApplicationContext(), device, DssConstants.Scenes.STEP_UP);
                        } else {
                            DssService.callActivity(getApplicationContext(), room.get_id(), DssConstants.Groups.LIGHT, getSceneIdToCall(firstCall, true), false);
                        }

                        plusButton.setPressed(true);
                        minusButton.setPressed(false);

                        // wait for downstream
                        sleep = true;
                        waitForPeriod(App.DIMMING_DELAY);
                    } else if (!sleep) {
                        minusButton.setPressed(false);
                        plusButton.setPressed(false);
                        isTilting = false;
                    }
                }
            }

            // changes sleep boolean after "period" ms
            public void waitForPeriod(final int period) {
                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        sleep = false;
                    }
                }, period);
            }
        };

        if (!Strings.isNullOrEmpty(deviceId)) {
            final View layout = findViewById(R.id.ConsumptionValues);
            layout.setVisibility(View.VISIBLE);

            final TextView deviceConsumption = (TextView) findViewById(R.id.device_consumption);
            deviceConsumption.setText(getString(R.string.watts_pattern, getString(R.string.dash)));
            final TextView deviceMetering = (TextView) findViewById(R.id.device_metering);
            deviceMetering.setText(getString(R.string.kilowatthours_pattern, getString(R.string.dash)));
        }

        consumption.update();

        initMenuButton();
    }

    private void initMenuButton() {
        final View menuButton = findViewById(R.id.menuButton);
        menuButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                startActivityForResult(new Intent(AdvancedButton.this, ConfigurationScreen.class), REQUEST_CODE_CONFIGURATION_SCREEN);
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }
        });
    }

    // Make sure the layout is not recreated on keyboard hidden events (for hardware keyboard phones)
    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.overview_control_button_light);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
    }

    // callback defined in xml
    public void goBackClick(final View V) {
        onBackPressed();
    }

    // callback defined in xml
    public void toggleButtonClick(final View V) {
        toggleButton.setImageResource(R.drawable.yellow_dot_pressed);

        // toggle device or scene
        final DsRoom dssRoom = RoomsStore.get_room_by_id(roomId);
        if (isDevice) {
            final DsDevice device = dssRoom.get_device_by_id(deviceId);
            if (device.is_on()) {
                DssService.turnOffDevice(getApplicationContext(), roomId, device);
            } else {
                DssService.turnOnDevice(getApplicationContext(), roomId, device);
            }
        } else {
            if (dssRoom.get_lastCalledScene(DssConstants.Groups.LIGHT) == sceneId) {
                DssService.callActivity(getApplicationContext(), roomId, DssConstants.Groups.LIGHT, DssConstants.Scenes.OFF, false);
            } else {
                DssService.callActivity(getApplicationContext(), roomId, DssConstants.Groups.LIGHT, sceneId, false);
            }
        }
        clearDeviceConsumptionValue();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        } else if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    private void markButton() {
        toggleButton.setImageResource(R.drawable.yellow_dot);
        if (isDevice) {
            buttonIsActive = RoomsStore.get_room_by_id(roomId).get_device_by_id(deviceId).is_on();
        } else {
            final long lastCalledScene = RoomsStore.get_room_by_id(roomId).get_lastCalledScene(DssConstants.Groups.LIGHT);
            if (lastCalledScene == sceneId) {
                buttonIsActive = true;
            }
            if (lastCalledScene == DssConstants.Scenes.OFF) {
                buttonIsActive = false;
            }
        }
        if (buttonIsActive) {
            toggleBackground.setBackgroundResource(R.drawable.advanced_active);
            myOrientationEventListener.enable();
        } else {
            toggleBackground.setBackgroundResource(R.drawable.advanced_notactive);
            myOrientationEventListener.disable();
            minusButton.setPressed(false);
            plusButton.setPressed(false);
        }
    }

    private class RepeatListener implements OnTouchListener, Runnable {

        private int period;
        private boolean increment;
        Handler handler = new android.os.Handler();

        public RepeatListener(final int period, final boolean increment) {
            this.period = period;
            this.increment = increment;
        }

        @SuppressLint("ClickableViewAccessibility")
        public boolean onTouch(final View view, final MotionEvent motionEvent) {
            final int action = motionEvent.getAction();
            if (!isTilting) {
                if (action == MotionEvent.ACTION_DOWN) {
                    isTouching = true;
                    Log.i("Repeat", "ACTION_DOWN");

                    doCallActivity(true);

                    handler.removeCallbacks(this);
                    handler.postDelayed(this, period);
                } else if (action == MotionEvent.ACTION_UP) {
                    isTouching = false;
                    Log.i("Repeat", "ACTION_UP");
                    handler.removeCallbacks(this);
                }
            }
            return false;
        }

        public void run() {
            doCallActivity(false);
            handler.postDelayed(this, period);
        }

        private void doCallActivity(final boolean firstCall) {
            final DsRoom dssRoom = RoomsStore.get_room_by_id(roomId);
            final DsDevice device = dssRoom.get_device_by_id(deviceId);
            if (isDevice) {
                if (increment) {
                    DssService.callSceneOnDevice(getApplicationContext(), device, DssConstants.Scenes.STEP_UP);
                } else {
                    DssService.callSceneOnDevice(getApplicationContext(), device, DssConstants.Scenes.STEP_DOWN);
                }
            } else {
                final long sceneToCall = getSceneIdToCall(firstCall, increment);
                DssService.callActivity(getApplicationContext(), roomId, DssConstants.Groups.LIGHT, sceneToCall, false);
            }
            clearDeviceConsumptionValue();
        }
    }

    private long getSceneIdToCall(final boolean firstCall, final boolean up) {
        if (firstCall) {
            if (sceneId == DssConstants.Scenes.AREA_1_ON || sceneId == DssConstants.Scenes.AREA_1_OFF) {
                return up ? DssConstants.Scenes.AREA_1_UP : DssConstants.Scenes.AREA_1_DOWN;
            } else if (sceneId == DssConstants.Scenes.AREA_2_ON || sceneId == DssConstants.Scenes.AREA_2_OFF) {
                return up ? DssConstants.Scenes.AREA_2_UP : DssConstants.Scenes.AREA_2_DOWN;
            } else if (sceneId == DssConstants.Scenes.AREA_3_ON || sceneId == DssConstants.Scenes.AREA_3_OFF) {
                return up ? DssConstants.Scenes.AREA_3_UP : DssConstants.Scenes.AREA_3_DOWN;
            } else if (sceneId == DssConstants.Scenes.AREA_4_ON || sceneId == DssConstants.Scenes.AREA_4_OFF) {
                return up ? DssConstants.Scenes.AREA_4_UP : DssConstants.Scenes.AREA_4_DOWN;
            } else {
                return up ? DssConstants.Scenes.STEP_UP : DssConstants.Scenes.STEP_DOWN;
            }
        } else {
            if (sceneId == DssConstants.Scenes.AREA_1_ON || sceneId == DssConstants.Scenes.AREA_1_OFF
                    || sceneId == DssConstants.Scenes.AREA_2_ON || sceneId == DssConstants.Scenes.AREA_2_OFF
                    || sceneId == DssConstants.Scenes.AREA_3_ON || sceneId == DssConstants.Scenes.AREA_3_OFF
                    || sceneId == DssConstants.Scenes.AREA_4_ON || sceneId == DssConstants.Scenes.AREA_4_OFF) {
                return DssConstants.Scenes.CONT;
            } else {
                return up ? DssConstants.Scenes.STEP_UP : DssConstants.Scenes.STEP_DOWN;
            }
        }
    }

    // callback defined in xml
    public void refreshClicked(final View view) {
        if (!Strings.isNullOrEmpty(deviceId)) {
            updateConsumptionValues();
            GAHelper.sendrefreshConsumptionClicked();
        }
    }

    private void clearDeviceConsumptionValue() {
        ((TextView) findViewById(R.id.device_consumption)).setText(R.string.dash);
    }

    private void updateConsumptionValues() {
        final DsDevice device = RoomsStore.get_room_by_id(roomId).get_device_by_id(deviceId);

        final TextView deviceConsumption = (TextView) findViewById(R.id.device_consumption);
        if (deviceConsumption.getTag() != null) {
            if (deviceConsumption.getTag() instanceof AsyncTask) {
                ((AsyncTask<?, ?, ?>) deviceConsumption.getTag()).cancel(true);
            }
        }
        final View consumptionProgress = findViewById(R.id.device_consumption_progress);
        final AsyncTask<Void, Void, Integer> consumptionViewUpdater = new ConsumptionViewUpdater(this, device, deviceConsumption, consumptionProgress);
        deviceConsumption.setTag(consumptionViewUpdater);

        final TextView deviceMetering = (TextView) findViewById(R.id.device_metering);
        if (deviceMetering.getTag() != null) {
            if (deviceMetering.getTag() instanceof AsyncTask) {
                ((AsyncTask<?, ?, ?>) deviceMetering.getTag()).cancel(true);
            }
        }
        final View meteringProgress = findViewById(R.id.device_metering_progress);
        meteringProgress.setVisibility(View.VISIBLE);
        final AsyncTask<Void, Void, Integer> meteringViewUpdater = new MeteringViewUpdater(this, device, deviceMetering, meteringProgress);
        deviceMetering.setTag(consumptionViewUpdater);

        consumptionViewUpdater.execute();
        meteringViewUpdater.execute();
    }
}
