package com.smartboxasia.control.ui.settings;

import java.io.IOException;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.ApplicationConfigService;
import com.smartboxasia.control.data.config.connection.ConnectionConfigService;
import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.connection.direct_1_17_0.LocalNetworkProbe;
import com.smartboxasia.control.data.connection.direct_1_17_0.LocalNetworkProbe.UpdateListener;
import com.smartboxasia.control.domain.LoginResult;
import com.smartboxasia.control.events.LoginResultEvent;
import com.smartboxasia.control.ui.dialog.LoginDialogActivity;
import com.smartboxasia.control.ui.helper.OnClickListenerScaffold;
import com.smartboxasia.testenabler.TestEnablerHelper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import com.google.common.primitives.Ints;

public class ServerSettings extends FragmentActivity implements UpdateListener<ConnectionData> {

    private static final String TAG = ServerSettings.class.getSimpleName();

    private static final int REQUEST_CODE_LOGIN = 1;
    public static final String EXTRA_RELOGIN = "com.smartboxasia.sia.relogin";

    public final static int LOCALPORT = 8080;
    static final String HOSTNAME = "smartboxasia";

    protected ServerSettingsListAdapter adapter;
    protected ListView list;

    private String newServerUrl;
    private String newServerName;
    private boolean push_down_animation = true;

    public final List<ConnectionData> customServers = Lists.newArrayList();

    private LocalNetworkProbe probe;

    @Subscribe
    public void onEvent(final LoginResultEvent event) {
        if (event.getResult() == LoginResult.LOGIN_RESULT_OK) {

            customServers.clear();
            customServers.addAll(ConnectionConfigService.getAllConnectionData());

            setResultOkAndFinish();
        }
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        final AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
        // only do something if it's a custom server
        if (info.id == -1) {
            final int footerCount = list.getFooterViewsCount();
            final int totalCount = list.getCount();
            final int customServerIndex = info.position - (totalCount - footerCount);

            // create an edit dialog
            final AlertDialog.Builder alert = new AlertDialog.Builder(this);

            alert.setTitle(R.string.manual_input_edit);

            final LinearLayout dialogLayout = new LinearLayout(this);

            // Set an EditText view to edit user name input
            final EditText inputName = new EditText(this);
            inputName.setText(customServers.get(customServerIndex).getName());

            // Set an EditText view to edit user url input
            final EditText inputUrl = new EditText(this);
            final String tempUrl = customServers.get(customServerIndex).getUrl();
            inputUrl.setText(tempUrl);

            dialogLayout.setOrientation(LinearLayout.VERTICAL);
            dialogLayout.addView(inputName);
            dialogLayout.addView(inputUrl);

            alert.setView(dialogLayout);

            alert.setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, final int whichButton) {

                    App.hideIME(inputUrl, inputName);

                    String url = getInputString(inputUrl);
                    if (url.length() == 0) {
                        url = getString(R.string.not_available);
                    } else {
                        url = url.replace(" ", "");
                    }

                    String name = getInputString(inputName);
                    if (name.length() == 0) {
                        name = getString(R.string.not_available);
                    }

                    // update the connection data stored
                    final ConnectionData data = customServers.get(customServerIndex);
                    if (data != null) {
                        data.setName(name);
                        final String oldUrl = data.getUrl();
                        if (!oldUrl.equals(url)) {
                            // the url was edited, make sure we remove the old entry and add a new one. The url is the key.
                            ConnectionConfigService.deleteConnectionData(oldUrl);
                            data.setUrl(url);
                            ConnectionConfigService.addConnectionData(data);
                        }
                    }

                    customServers.clear();
                    customServers.addAll(ConnectionConfigService.getAllConnectionData());

                    // refresh
                    refreshServers();
                }
            });

            alert.setNeutralButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, final int whichButton) {

                    final ConnectionData data = customServers.get(customServerIndex);
                    final String serverName = Strings.nullToEmpty(data.getName());
                    final String serverUrl = Strings.nullToEmpty(data.getUrl());
                    final ConnectionData lastUsedConnectionData = ConnectionConfigService.getLastUsedConnectionData();
                    if (lastUsedConnectionData != null && serverName.equals(lastUsedConnectionData.getName()) && serverUrl.equals(lastUsedConnectionData.getUrl())) {
                        showDeleteConfirmation(customServerIndex);
                    } else {
                        deleteServerSetting(customServerIndex);
                    }
                }
            });

            alert.setNegativeButton(getString(R.string.cancel), new OnClickListenerScaffold());

            alert.show();
        }
    }

    private void showDeleteConfirmation(final int customServerNumber) {
        final AlertDialog.Builder builder = new Builder(this);
        builder.setTitle(R.string.confirm_delete);
        builder.setNegativeButton(R.string.cancel, new OnClickListenerScaffold());
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                ConnectionConfigService.deleteLastUsedConnectionData();
                deleteServerSetting(customServerNumber);
                ApplicationConfigService.clearAllCaches();
                Connection.reconnectConnectionService(null, null, null, null, true);
            }
        });
        builder.create().show();
    }

    private void deleteServerSetting(final int customServerNumber) {
        ConnectionConfigService.deleteConnectionData(customServers.get(customServerNumber).getUrl());
        customServers.clear();
        customServers.addAll(ConnectionConfigService.getAllConnectionData());
        refreshServers();
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_servers);

        ((TextView) findViewById(R.id.titleTextView)).setText(R.string.settings_title_servers);

        if (getIntent().hasExtra(ConfigurationScreen.EXTRA_SHOW_ANIMATION)) {
            push_down_animation = getIntent().getExtras().getBoolean(ConfigurationScreen.EXTRA_SHOW_ANIMATION, true);
        }

        // relogin case
        if (getIntent().getBooleanExtra(EXTRA_RELOGIN, false)) {
            final ConnectionData data = Connection.getActiveConnectionData();
            if (data.isCloudLogin()) {
                showCloudCredentialsDialog();
            } else {
                newServerUrl = data.getUrl();
                newServerName = data.getName();
                getDirectApplicationToken();
            }
        }

        list = (ListView) this.findViewById(R.id.dssList);

        registerForContextMenu(list);

        // load custom servers
        customServers.clear();
        customServers.addAll(ConnectionConfigService.getAllConnectionData());

        addCustomServers();

        // add padding
        final TextView padding = new TextView(this);
        padding.setHeight(getResources().getDimensionPixelSize(R.dimen.settings_border_margin));
        list.addFooterView(padding, null, false);

        addCloudConnectButton();

        addManualConnectButton();

        if (TestEnablerHelper.getFlagState(App.DEBUG_MODE_FLAG)) {
            setupDebugCloudChooser();
        }

        addOnClickListener();

        adapter = new ServerSettingsListAdapter(this, customServers);
        list.setAdapter(adapter); // add Adapter after all footers have been added to be faster

        addRefreshButton();
    }

    private void addCustomServers() {
        for (final ConnectionData data : customServers) {
            final View customServerView = getLayoutInflater().inflate(R.layout.settings_servers_item_custom, list, false);

            final String title = data.getName();
            ((TextView) customServerView.findViewById(R.id.customServerName)).setText(title);

            final String addr = data.getUrl();
            ((TextView) customServerView.findViewById(R.id.customServerUrl)).setText(addr);

            // mark the current server, if it's set
            final ConnectionData lastUsedConnectionData = ConnectionConfigService.getLastUsedConnectionData();
            if (lastUsedConnectionData != null && !Strings.isNullOrEmpty(lastUsedConnectionData.getUrl())) {
                if (addr.compareTo(lastUsedConnectionData.getUrl()) == 0) {
                    // orange
                    customServerView.setBackgroundResource(R.color.orange_selection);
                    customServerView.setDrawingCacheEnabled(false);
                }
            }

            list.addFooterView(customServerView, null, true);
        }
    }

    private void addCloudConnectButton() {
        final View smartboxasiaInputView = getLayoutInflater().inflate(R.layout.settings_servers_input_dialog_cloud, list, false);
        smartboxasiaInputView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                showCloudCredentialsDialog();
            }
        });
        list.addFooterView(smartboxasiaInputView, null, false);
    }

    private void addManualConnectButton() {
        final View manualInputView = getLayoutInflater().inflate(R.layout.settings_servers_input_dialog_manual, list, false);
        manualInputView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                showDirectServerSettingsDialog();
            }
        });
        list.addFooterView(manualInputView, null, false);
    }

    private void addOnClickListener() {
        list.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
                newServerName = ((TextView) view.findViewById(android.R.id.text1)).getText().toString();
                newServerUrl = ((TextView) view.findViewById(android.R.id.text2)).getText().toString();

                connectToServer();
            }
        });
    }

    private void addRefreshButton() {
        final ImageView refreshButton = new ImageView(this);
        final LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        refreshButton.setLayoutParams(params);
        refreshButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                refreshServers();
            }
        });
        refreshButton.setImageResource(R.drawable.orange_ribbon_refresh);
        ((LinearLayout) findViewById(R.id.title)).addView(refreshButton);
    }

    @Override
    protected void onResume() {
        super.onResume();

        probe = new LocalNetworkProbe(getApplicationContext(), this);
        try {
            probe.startProbe();
        } catch (final IOException e) {
            // Could not start probing the network
            Log.e(TAG, "could not start Network Probe");
            // TODO let the user know
        }

        GAHelper.sendScreenViewEvent("Settings Servers");
    }

    @Override
    protected void onPause() {
        super.onPause();

        probe.stopProbeAsync();

        if (push_down_animation) {
            overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Register for events. Do this in onStart so we also get the events if a DialogActivity is showing.
        App.eventBus.register(this);

    }

    @Override
    protected void onStop() {
        super.onStop();

        // Unregister for events
        App.eventBus.unregister(this);
    }

    private void refreshServers() {
        final Intent myIntent = new Intent(this, ServerSettings.class);

        // make sure the activity result reaches the ConfigurationScreen
        myIntent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);

        myIntent.putExtra(ConfigurationScreen.EXTRA_SHOW_ANIMATION, push_down_animation);
        push_down_animation = false;
        startActivity(myIntent);
        finish();
        overridePendingTransition(0, 0);
    }

    private void setResultOkAndFinish() {
        setResult(ConfigurationScreen.SERVERS_CHANGED);
        finish();
    }

    // callback defined in xml
    public void onCustomServerClick(final View V) {
        newServerUrl = ((TextView) V.findViewById(R.id.customServerUrl)).getText().toString();
        newServerName = ((TextView) V.findViewById(R.id.customServerName)).getText().toString();

        connectToServer();
    }

    private void connectToServer() {
        // Check if application token exists
        final ConnectionData data = ConnectionConfigService.getConnectionData(newServerUrl);

        // if no applicationToken is saved, request one
        if (data == null || Strings.isNullOrEmpty(data.getApplicationToken())) {
            if (ConnectionData.isCloudLogin(newServerUrl)) {
                showCloudCredentialsDialog();
            } else {
                getDirectApplicationToken();
            }
        } else {
            // else set the token and url, and finish
            ConnectionConfigService.deleteLastUsedConnectionData();
            Connection.reconnectConnectionService(data, null, null, null, false);

            setResultOkAndFinish();
        }
    }

    private void showCloudCredentialsDialog() {

        final ConnectionData data = new ConnectionData(ConnectionData.CONNECTION_TYPE_CLOUD);
        data.setUrl(ConnectionConfigService.getGlobalCloudUrl());
        data.setCloudUrl(ConnectionConfigService.getGlobalCloudUrl());
        final Intent intent = new Intent(ServerSettings.this, LoginDialogActivity.class);
        intent.putExtra(LoginDialogActivity.EXTRA_DATA, data);
        startActivityForResult(intent, REQUEST_CODE_LOGIN);

    }

    private static String getInputString(final EditText editText) {
        return editText.getText().toString().trim();
    }

    private void showDirectServerSettingsDialog() {

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle(R.string.manual_input);

        final LinearLayout dialogLayout = new LinearLayout(this);

        // Set an EditText view to get user name input
        final EditText inputName = new EditText(this);
        inputName.setHint(R.string.name_hint);

        // Set an EditText view to get user url input
        final EditText inputUrl = new EditText(this);
        inputUrl.setHint(R.string.url_hint);

        // Set an EditText view to get port number input
        final EditText inputPort = new EditText(this);
        inputPort.setInputType(InputType.TYPE_CLASS_NUMBER);
        inputPort.setHint(R.string.port_hint);

        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        dialogLayout.addView(inputName);
        dialogLayout.addView(inputUrl);
        dialogLayout.addView(inputPort);

        alert.setView(dialogLayout);

        alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int whichButton) {
                App.hideIME(inputName, inputUrl);

                String url = getInputString(inputUrl);
                if (url.length() == 0) {
                    url = getString(R.string.not_available);
                } else {
                    url = url.replace(" ", "");
                }

                String name = inputName.getText().toString();
                if (name.length() == 0) {
                    name = getString(R.string.not_available);
                }

                Integer port = Ints.tryParse(getInputString(inputPort));
                if (port == null || port <= 0) {
                    port = LOCALPORT;
                }

                url = "https://" + url + ":" + port;

                newServerName = name;
                newServerUrl = url;

                getDirectApplicationToken();
            }
        });

        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int whichButton) {
                // Canceled.
            }
        });

        alert.show();
    }

    private void getDirectApplicationToken() {
        final ConnectionData data = new ConnectionData(ConnectionData.isCloudLogin(newServerUrl) ? ConnectionData.CONNECTION_TYPE_CLOUD : ConnectionData.CONNECTION_TYPE_MANUAL);
        data.setName(newServerName);
        data.setUrl(newServerUrl);
        data.setCloudUrl(ConnectionConfigService.getGlobalCloudUrl());
        final Intent intent = new Intent(ServerSettings.this, LoginDialogActivity.class);
        intent.putExtra(LoginDialogActivity.EXTRA_DATA, data);
        startActivityForResult(intent, REQUEST_CODE_LOGIN);
    }

    // callback defined in xml
    public void goBackClick(final View V) {
        onBackPressed();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == REQUEST_CODE_LOGIN) {
            if (resultCode == RESULT_OK) {
                setResultOkAndFinish();
            }
        }
    }

    @Override
    public void updateReceived(final ConnectionData newConnection) {
        adapter.notifyFound(newConnection);
    }

    private void setupDebugCloudChooser() {
        final Spinner chooser = (Spinner) getLayoutInflater().inflate(R.layout.settings_cloud_url_chooser, list, false);

        final TypedArray stringIds = getResources().obtainTypedArray(R.array.debug_cloud_urls);
        for (int i = 0; i < stringIds.length(); i++) {
            final int resourceId = stringIds.getResourceId(i, R.string.cloud_url_production);
            if (getString(resourceId).equals(ConnectionConfigService.getGlobalCloudUrl())) {
                chooser.setSelection(i);
                break;
            }
        }
        stringIds.recycle();

        chooser.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                final TypedArray stringIds = getResources().obtainTypedArray(R.array.debug_cloud_urls);
                final int resourceId = stringIds.getResourceId(position, R.string.cloud_url_production);
                stringIds.recycle();
                final String cloudUrl = getString(resourceId);
                ConnectionConfigService.setDebugCloudLoginUrl(cloudUrl);
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {
                final String cloudUrl = getString(R.string.cloud_url_production);
                ConnectionConfigService.setDebugCloudLoginUrl(cloudUrl);
            }
        });

        list.addFooterView(chooser);
    }
}
