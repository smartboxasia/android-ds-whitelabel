package com.smartboxasia.control.ui.helper;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import com.smartboxasia.control.App;
import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsRoom;

public final class SwitchOffOnClickListener implements DialogInterface.OnClickListener {
    private Thread switchOffTask;
    private final int roomId;
    private final DsRoom room;
    private final String roomName;

    public SwitchOffOnClickListener(final int roomId, final DsRoom room, final String roomName) {
        this.roomId = roomId;
        this.room = room;
        this.roomName = roomName;
    }

    public void onClick(final DialogInterface dialog, final int item) {
        final Context context = App.getInstance();
        switch (item) {
            case 0:
                // force switch off this room (lights)
                DssService.callActivity(context, room.get_id(), DssConstants.Groups.LIGHT, DssConstants.Scenes.OFF, true);
                Toast.makeText(context, context.getString(R.string.switched_off) + " " + roomName, Toast.LENGTH_SHORT).show();
                break;
            case 1:
                // Start a background service that shuts off all other rooms
                startSwitchOffTask(roomId);
                // notify user
                Toast.makeText(context, context.getString(R.string.switched_off_everything) + " " + roomName, Toast.LENGTH_SHORT).show();
                break;
            case 2:
                // cancel
                break;
            default:
                break;
        }
    }

    private void startSwitchOffTask(final int roomId) {
        if (switchOffTask != null) {
            switchOffTask.interrupt();
        }
        // Do this in a new thread
        switchOffTask = new OffSwitcherThread(App.getInstance(), roomId);
        switchOffTask.start();
    }
}
