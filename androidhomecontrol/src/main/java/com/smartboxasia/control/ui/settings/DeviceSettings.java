package com.smartboxasia.control.ui.settings;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.DssConstants.Groups;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DsDeviceDefaultBehavior;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.events.RoomsChangedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;

public class DeviceSettings extends ConnectivityMonitoringFragmentActivity {

    private static final int SHOW_ROOM_DETAILS = 1;

    private final List<DsDevice> content = Lists.newArrayList();
    private final List<Integer> roomIds = Lists.newArrayList();
    private ArrayAdapter<DsDevice> adapter;
    private boolean changed = false;

    private ListView listView;

    @Subscribe
    public void onEvent(final RoomsChangedEvent event) {
        if (event.isSuccess()) {
            // if all rooms are received, update list
            updateList();
        }
        DssService.updateActivties(getApplicationContext());
    }

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_list);

        ((TextView) findViewById(R.id.titleTextView)).setText(R.string.settings_title_devices);

        adapter = new ArrayAdapter<DsDevice>(this, R.layout.settings_list_item, content) {
            @Override
            public View getView(final int position, View convertView, final ViewGroup parent) {

                // if content is a room, create a thin bar. Else create a proper bar for a device
                DsDevice device = null;
                String deviceName;
                int deviceGroup;

                synchronized (content) {
                    device = content.get(position);
                }
                if (device == null) {
                    deviceName = "";
                    deviceGroup = Groups.MAX_GROUP;
                } else {
                    deviceName = device.get_name();
                    deviceGroup = device.get_groupNumber();
                }

                if (roomIds.get(position) == -1) {
                    convertView = getLayoutInflater().inflate(R.layout.settings_list_divider, parent, false);
                    ((TextView) convertView.findViewById(R.id.settingsTextView)).setText(deviceName);
                } else {
                    convertView = getLayoutInflater().inflate(R.layout.settings_list_item, parent, false);
                    ((TextView) convertView.findViewById(R.id.settingsTextView)).setText(deviceName);
                    if (position % 2 == 0) {
                        ((LinearLayout) convertView.findViewById(R.id.settingsItem)).setBackgroundResource(R.drawable.conf_list_white);
                    }

                    // set icon
                    if (deviceGroup == 1) {
                        ((ImageView) convertView.findViewById(R.id.icon)).setImageResource(R.drawable.lightbulb);
                    } else if (deviceGroup == 2) {
                        ((ImageView) convertView.findViewById(R.id.icon)).setImageResource(R.drawable.shade);
                    } else if (deviceGroup == 3) {
                        ((ImageView) convertView.findViewById(R.id.icon)).setImageResource(R.drawable.temp_active);
                    }
                }

                return convertView;
            }
        };

        listView = (ListView) findViewById(android.R.id.list);
        listView.setAdapter(adapter);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Unregister for events
        App.eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register for events
        App.eventBus.register(this);

        // update rooms
        DssService.updateRooms(this);
        updateList();

        GAHelper.sendScreenViewEvent("Settings Devices");
    }

    @Override
    public void onBackPressed() {
        if (changed) {
            setResult(ConfigurationScreen.DEVICES_CHANGED);
        }
        finish();

    }

    private void updateList() {
        synchronized (content) {
            // clear and update content
            content.clear();
            roomIds.clear();

            // add room names and their devices
            for (final DsRoom room : RoomsStore.get_roomsSorted()) {
                // empty device used to hold room name
                final DsDevice dummyDevice = new DsDevice(new DsDeviceDefaultBehavior(""));
                dummyDevice.set_name(room.get_name());
                content.add(dummyDevice);
                roomIds.add(-1); // not used, filler
                for (final DsDevice device : room.get_devices()) {
                    content.add(device);
                    roomIds.add(room.get_id());
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    // callback defined in xml
    public void goBackClick(final View V) {
        onBackPressed();
    }

    // callback defined in xml
    public void selectButtonClick(final View V) {
        if (V == null) {
            return;
        }
        final int position = listView.getPositionForView(V);
        final Intent myIntent = new Intent(this, DeviceSettingsDetails.class);
        synchronized (content) {
            final int roomId = roomIds.get(position);
            final String deviceNumber = content.get(position).get_id();
            myIntent.putExtra(DeviceSettingsDetails.EXTRA_ROOM_ID, roomId);
            myIntent.putExtra(DeviceSettingsDetails.EXTRA_DEVICE_ID, deviceNumber);
        }
        this.startActivityForResult(myIntent, SHOW_ROOM_DETAILS);

    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == SHOW_ROOM_DETAILS) {
            if (resultCode == RESULT_OK) {
                changed = true;
            }
        }
    }
}
