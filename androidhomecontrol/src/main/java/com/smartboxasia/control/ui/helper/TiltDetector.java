package com.smartboxasia.control.ui.helper;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

public class TiltDetector {

    private OrientationChangeEventListener orientationChangeEventListener;
    private SensorManager sensorManager;
    private Sensor accelerometer;

    public void init(final Activity activity) {
        sensorManager = (SensorManager) activity.getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        orientationChangeEventListener = new OrientationChangeEventListener(activity);
    }

    public void onResume() {
        sensorManager.registerListener(orientationChangeEventListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void onPause() {
        sensorManager.unregisterListener(orientationChangeEventListener);
    }

    public void disable() {
        orientationChangeEventListener.disable();
    }

    public void enable() {
        orientationChangeEventListener.enable();
    }
}
