package com.smartboxasia.control.ui.overview;

import java.util.List;
import java.util.Set;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.events.ActivitiesChangedEvent;
import com.smartboxasia.control.events.SceneActivatedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.smartboxasia.control.ui.helper.ConsumptionHelper;
import com.smartboxasia.control.ui.helper.TiltDetector;
import com.smartboxasia.control.ui.settings.ConfigurationScreen;
import com.google.common.collect.Sets;
import com.google.common.eventbus.Subscribe;
import com.viewpagerindicator.IconPageIndicator;

public class ActivityOverview extends ConnectivityMonitoringFragmentActivity {

    private static final String TAG = ActivityOverview.class.getSimpleName();

    public static final String EXTRA_ROOM_ID = "com.smartboxasia.sia.roomId";

    private static final int REQEUST_CODE_BASE_ACTIVITIES = 100;

    public static final int REQEUST_CODE_ADVANCED_BUTTON = App.REQUEST_CODE_BASE_HOMESCREEN + REQEUST_CODE_BASE_ACTIVITIES;
    private static final int REQUEST_CODE_CONFIGURATION_SCREEN = REQEUST_CODE_ADVANCED_BUTTON + 1;
    private static final int REQUEST_CODE_DEVICE_OVERVIEW = REQUEST_CODE_CONFIGURATION_SCREEN + 1;

    private int roomId;

    private TiltDetector tiltDetector = new TiltDetector();

    private ViewPager viewPager;
    private ActivityOverviewPagerAdapter adapter;
    private IconPageIndicator indicator;
    private int currentPage = 0;

    private FavoritesBarFragment favoritesBarFragment;

    private final ConsumptionHelper consumption = new ConsumptionHelper(this, R.id.ConsumptionTextView, R.id.ConsumptionColor);

    private TextView roomNameTv;

    @Subscribe
    public void onEvent(final SceneActivatedEvent event) {
        markLastCalledScene();
        favoritesBarFragment.updateFavoriteList();
    }

    @Subscribe
    public void onEvent(final ActivitiesChangedEvent event) {
        markLastCalledScene();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // enable orientation listener
        tiltDetector.onResume();

        // update favorites
        favoritesBarFragment.updateFavoriteList();

        // Register for events
        App.eventBus.register(this);

        // resume polling the consumption (this also gets a new session token, in case it timed out)
        consumption.onResume();

        // Update activities
        DssService.updateActivties(getApplicationContext());

        roomNameTv.setText(RoomsStore.get_room_by_id(roomId).get_name());

        GAHelper.sendScreenViewEvent("Overview Activities");
    }

    @Override
    protected void onPause() {
        super.onPause();

        consumption.onPause();

        // Unregister from events
        App.eventBus.unregister(this);

        // stop listening for orientation
        tiltDetector.onPause();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.overview_activities);

        favoritesBarFragment = (FavoritesBarFragment) getSupportFragmentManager().findFragmentById(R.id.FavoritesBar);

        tiltDetector.init(this);
        tiltDetector.enable();

        // get roomNumber from the intent
        roomId = getIntent().getExtras().getInt(EXTRA_ROOM_ID);
        final DsRoom room = RoomsStore.get_room_by_id(roomId);

        // check what colour groups are available in this roomNr
        final List<DsDevice> devices = room.get_devices();

        final Set<Integer> avaiableGroups = Sets.newHashSet();
        for (final DsDevice device : devices) {
            avaiableGroups.add(device.get_groupNumber());
        }

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        indicator = (IconPageIndicator) findViewById(R.id.indicator);
        indicator.setOnPageChangeListener(new SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(final int position) {
                currentPage = position;
                indicator.notifyDataSetChanged();
            }
        });

        adapter = new ActivityOverviewPagerAdapter(this, avaiableGroups, roomId);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);

        roomNameTv = (TextView) findViewById(R.id.currentRoomTextView);

        consumption.update();

        initMenuButton();
    }

    private void initMenuButton() {
        final View menuButton = findViewById(R.id.menuButton);
        menuButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                startActivityForResult(new Intent(ActivityOverview.this, ConfigurationScreen.class), REQUEST_CODE_CONFIGURATION_SCREEN);
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }
        });
    }

    // Make sure the layout is not recreated on keyboard hidden events (for hardware keyboard phones)
    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.overview_activities);
    }

    public void goBackClick(final View V) {
        finish();
    }

    // callback defined in xml
    public void goToDevicesClick(final View V) {
        final Intent myIntent = new Intent(this, DeviceOverview.class);
        myIntent.putExtra(DeviceOverview.EXTRA_ROOM_ID, roomId);
        myIntent.putExtra(DeviceOverview.EXTRA_GROUP_NUMBER, currentPage);
        this.startActivityForResult(myIntent, REQUEST_CODE_DEVICE_OVERVIEW);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case REQEUST_CODE_ADVANCED_BUTTON: {
                if (resultCode == RESULT_OK) {
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            }
            case REQUEST_CODE_CONFIGURATION_SCREEN: {
                if (resultCode == RESULT_OK) {
                    setResult(RESULT_OK);
                    finish();
                } else if (resultCode == ConfigurationScreen.SERVERS_CHANGED) {
                    setResult(ConfigurationScreen.SERVERS_CHANGED);
                    finish();
                }
                break;
            }
            case REQUEST_CODE_DEVICE_OVERVIEW: {
                if (resultCode == RESULT_OK) {
                    final int groupNumber = data.getIntExtra(DeviceOverview.EXTRA_GROUP_NUMBER, 0);
                    currentPage = groupNumber;
                    viewPager.setCurrentItem(groupNumber);
                    indicator.notifyDataSetChanged();
                } else if (resultCode == ConfigurationScreen.SERVERS_CHANGED) {
                    setResult(ConfigurationScreen.SERVERS_CHANGED);
                    finish();
                }
                break;
            }
            default: {
                Log.w(TAG, "Unknown request code returned: " + requestCode);
                break;
            }
        }
    }

    private void markLastCalledScene() {
        // Visually mark the last called scene, by notifying all list adapters that data changed
        adapter.notifyDataSetChanged();

    }

}
