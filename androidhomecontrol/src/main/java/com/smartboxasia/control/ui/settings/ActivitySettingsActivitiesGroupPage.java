package com.smartboxasia.control.ui.settings;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.smartboxasia.control.App;
import com.smartboxasia.control.R;
import com.smartboxasia.control.dto.DsScene;
import com.smartboxasia.control.events.FilteredScenesChangedEvent;
import com.smartboxasia.control.ui.helper.SceneHelper;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;

public class ActivitySettingsActivitiesGroupPage extends ListFragment {
    public static final String GROUP_NUMBER = "groupNumber";
    public static final String ROOM_ID = "roomId";

    private int roomId;
    private int groupNumber;
    private final List<DsScene> scenes = Lists.newArrayList();

    @Subscribe
    public void onEvent(final FilteredScenesChangedEvent event) {
        updateList();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        roomId = getArguments().getInt(ROOM_ID);
        groupNumber = getArguments().getInt(GROUP_NUMBER);

        return inflater.inflate(R.layout.settings_activities_group_page, container, false);

    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final BaseAdapter adapter = new ActivitySettingsActivitiesListAdapter((ActivitySettingsActivities) getActivity(), scenes);
        setListAdapter(adapter);

        updateList();
    }

    private void updateList() {
        scenes.clear();
        final boolean showAll = ((ActivitySettingsActivities) getActivity()).isShowAll();

        scenes.addAll(SceneHelper.createScenesList(roomId, groupNumber, showAll));

        ((BaseAdapter) getListAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();

        App.eventBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        App.eventBus.unregister(this);
    }
}
