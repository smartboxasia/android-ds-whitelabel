package com.smartboxasia.control.ui.settings;

import static com.smartboxasia.control.ui.settings.ConfigurationScreen.EXTRA_SHOW_ANIMATION;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.FavoriteActivitiesStore;
import com.smartboxasia.control.dto.DsScene;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mobeta.android.dslv.DragSortListView;
import com.mobeta.android.dslv.SimpleDragSortCursorAdapter;

public class FavoriteSettings extends Activity {

    private static final int REQUEST_NEW_FAVORITE = 70;
    private static final String COLUMN_NAME = "Name";

    private boolean hasChanged = false;
    private SimpleDragSortCursorAdapter adapter;
    private boolean push_down_animation = true;
    private boolean _gettingNewFavorite = false;
    private DragSortListView listView;

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_favorites);

        ((TextView) findViewById(R.id.titleTextView)).setText(R.string.settings_title_favorites);

        addPlusButton();

        if (getIntent().hasExtra(EXTRA_SHOW_ANIMATION)) {
            push_down_animation = getIntent().getExtras().getBoolean(EXTRA_SHOW_ANIMATION, true);
        }

        listView = (DragSortListView) findViewById(android.R.id.list);

        updateListViewAdapter();
    }

    private void addPlusButton() {
        final ImageView plusButton = new ImageView(this);
        final LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        plusButton.setLayoutParams(params);
        plusButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                addFavorite();
            }
        });
        plusButton.setImageResource(R.drawable.orange_ribbon_plus);
        ((LinearLayout) findViewById(R.id.title)).addView(plusButton);
    }

    private void addFavorite() {
        final Intent myIntent = new Intent(this, ActivitySettingsRooms.class);
        myIntent.putExtra(ActivitySettingsRooms.EXTRA_NEW_FAVORITE, true);
        _gettingNewFavorite = true;
        this.startActivityForResult(myIntent, REQUEST_NEW_FAVORITE);
    }

    private void updateListViewAdapter() {
        final MatrixCursor cursor = new MatrixCursor(new String[] {
                BaseColumns._ID, COLUMN_NAME
        });
        final List<DsScene> favoriteActivitiesList = Lists.newArrayList(FavoriteActivitiesStore.get_favoriteActivities());
        // Add items to the favorite list.
        for (int i = 0; i < favoriteActivitiesList.size(); i++) {
            cursor.addRow(new Object[] {
                    i, favoriteActivitiesList.get(i).get_name()
            });
        }
        adapter = new SimpleDragSortCursorAdapter(this, R.layout.settings_favorites_item, cursor, new String[] {
                COLUMN_NAME
        }, new int[] {
                R.id.favoriteSettingsTextView
        }, 0) {
            @Override
            public void drop(final int from, final int to) {
                super.drop(from, to);
                Log.i("drop", "from:" + from + " to:" + to);
                // change the sequence accordingly
                final DsScene temp = favoriteActivitiesList.remove(from);
                favoriteActivitiesList.add(to, temp);
                FavoriteActivitiesStore.set_favoriteActivities(Sets.newLinkedHashSet(favoriteActivitiesList));

                hasChanged = true;
            }
        };
        listView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        GAHelper.sendScreenViewEvent("Settings Favorites");
    }

    @Override
    public void onPause() {
        super.onPause();

        if (push_down_animation && !_gettingNewFavorite) {
            overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        }
    }

    @Override
    public void onBackPressed() {
        if (hasChanged) {
            setResult(ConfigurationScreen.FAVORITES_CHANGED);
        }
        finish();
    }

    // callback defined in xml
    public void goBackClick(final View V) {
        onBackPressed();
    }

    // callback defined in xml
    public void removeFavoriteButtonClick(final View V) {
        final int position = listView.getPositionForView((View) V.getParent());

        // remove the chosen activity from the favorite list and update
        synchronized (FavoriteActivitiesStore.class) {
            final List<DsScene> favScenes = Lists.newArrayList(FavoriteActivitiesStore.get_favoriteActivities());
            favScenes.remove(favScenes.get(position));
            FavoriteActivitiesStore.set_favoriteActivities(Sets.newLinkedHashSet(favScenes));
        }

        updateListViewAdapter();
        hasChanged = true;
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == REQUEST_NEW_FAVORITE) {
            // if something is changed, update
            if (resultCode == ConfigurationScreen.FAVORITES_CHANGED) {
                hasChanged = true;
                updateListViewAdapter();
            }
            _gettingNewFavorite = false;
        }
    }
}
