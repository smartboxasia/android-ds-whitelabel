package com.smartboxasia.control.ui.helper;

import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.R;
import com.smartboxasia.control.dto.DsScene;

public class SceneIconHelper {

    public static int getIcon(final DsScene scene) {
        // set icon
        if (scene.get_groupNumber() == DssConstants.Groups.LIGHT) {
            if (scene.get_sceneId() == DssConstants.Scenes.OFF) {
                return R.drawable.lightbulb_off;
            } else {
                return R.drawable.lightbulb;
            }
        } else if (scene.get_groupNumber() == DssConstants.Groups.SHADE) {
            if (scene.get_sceneId() == DssConstants.Scenes.OFF) {
                return R.drawable.shade_deact_full;
            } else if (scene.get_sceneId() == DssConstants.Scenes.PRESET_1) {
                return R.drawable.shade_open;
            } else {
                return R.drawable.shade;
            }
        } else if (scene.get_groupNumber() == DssConstants.Groups.CLIMATE) {
            if (scene.get_sceneId() == DssConstants.Scenes.OFF) {
                return R.drawable.temp_deact;
            } else {
                return R.drawable.temp_active;
            }
        } else if (scene.get_groupNumber() == DssConstants.Groups.AUDIO) {
            if (scene.get_sceneId() == DssConstants.Scenes.OFF) {
                return R.drawable.music_off;
            } else {
                return R.drawable.music;
            }
        } else if (scene.get_groupNumber() == DssConstants.Groups.VIDEO) {
            if (scene.get_sceneId() == DssConstants.Scenes.OFF) {
                return R.drawable.television_off;
            } else {
                return R.drawable.television;
            }
        }
        return -1;
    }
}
