package com.smartboxasia.control.ui.overview;

import static com.smartboxasia.control.ui.settings.ConfigurationScreen.EXTRA_SHOW_ANIMATION;

import java.util.List;
import java.util.Set;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.DssConstants.Rooms;
import com.smartboxasia.control.DssConstants.Scenes;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.FavoriteActivitiesStore;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.dto.DsScene;
import com.smartboxasia.control.ui.helper.OnClickListenerScaffold;
import com.smartboxasia.control.ui.settings.FavoriteSettings;
import com.google.common.collect.Lists;

public class FavoritesBarFragment extends Fragment {

    private float scale;
    private int width;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.overview_favorites_bar, container, false);
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateFavoriteList();
    }

    public void updateFavoriteList() {
        final LinearLayout layout = (LinearLayout) getView().findViewById(R.id.FavoriteList);
        layout.removeAllViews(); // clear favorites list

        if (!Connection.isLoggedIn) {
            return;
        }

        scale = getActivity().getResources().getDisplayMetrics().density;
        width = (int) (80 * scale + 0.5f);

        final Set<DsScene> favoriteActivities = FavoriteActivitiesStore.get_favoriteActivities();

        for (final DsScene favScene : favoriteActivities) {
            final Button button = createSceneButton(favScene);
            if (button != null) {
                layout.addView(button);
            }
        }
        final ImageButton addButton = createPlusButton();
        layout.addView(addButton);
    }

    private Button createSceneButton(final DsScene favoriteScene) {

        final Button button = initFavButton(favoriteScene);

        if (button == null) {
            return null;
        }

        // check if it's a last called scene, and bold if so
        checkLastCalledScene(favoriteScene, button);

        // set action of clicking a favorite activity
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View V) {
                final long sceneId = favoriteScene.get_sceneId();
                final int roomId = favoriteScene.get_roomId();
                final int groupId = favoriteScene.get_groupNumber();

                // if "leave home" ask for confirmation
                if (sceneId == DssConstants.Scenes.ABSENT) {
                    final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(R.string.confirm_leavehome);
                    alert.setPositiveButton(getActivity().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, final int whichButton) {
                            button.setTextColor(getResources().getColor(android.R.color.black));
                            DssService.callActivity(getActivity(), roomId, DssConstants.Groups.ACTION, sceneId, false);
                            GAHelper.sendFavoriteCalledEvent("leave");
                        }
                    });
                    alert.setNegativeButton(getActivity().getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, final int whichButton) {
                            // do nothing
                        }
                    });
                    alert.show();
                } else if (sceneId == DssConstants.Scenes.PANIC) {
                    // if "panic" ask for confirmation and keep it in panic mode
                    final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(R.string.confirm_panic);
                    alert.setPositiveButton(getActivity().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, final int whichButton) {
                            // call panic and wait for deactivation
                            DssService.callActivity(getActivity(), roomId, DssConstants.Groups.ACTION, sceneId, false);
                            final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(R.string.panic_activated);
                            alert.setCancelable(false);
                            alert.setPositiveButton(getActivity().getString(R.string.deactivate), new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog, final int whichButton) {
                                    DssService.undoScene(getActivity(), roomId, DssConstants.Groups.ACTION, sceneId);
                                }
                            });
                            alert.show();
                            GAHelper.sendFavoriteCalledEvent("panic");
                        }
                    });
                    alert.setNegativeButton(getActivity().getString(R.string.no), new OnClickListenerScaffold());
                    alert.show();
                } else if (sceneId == DssConstants.Scenes.PRESENT || sceneId == DssConstants.Scenes.BELL_SINGAL) {
                    // if "come home" or "door bell", don't do different behaviour on bold up
                    button.setTextColor(getResources().getColor(android.R.color.black));
                    DssService.callActivity(getActivity(), roomId, groupId, sceneId, false);
                    GAHelper.sendFavoriteCalledEvent("scene");
                } else {
                    button.setTextColor(getResources().getColor(android.R.color.black));
                    if (button.getTypeface() != null) {
                        // if bolded, call undo
                        DssService.undoScene(getActivity(), roomId, groupId, sceneId);
                    } else {
                        // else call the scene
                        DssService.callActivity(getActivity(), roomId, groupId, sceneId, false);
                    }
                    final boolean isUDA = sceneId >= Scenes.USER_DEFINED_ACTIONS;
                    GAHelper.sendFavoriteCalledEvent(isUDA ? "uda" : "scene");
                }
            }
        });
        return button;
    }

    private void checkLastCalledScene(final DsScene favoriteScene, final Button button) {
        for (final DsRoom room : RoomsStore.get_roomsSorted()) {
            final int groupNumber = favoriteScene.get_groupNumber();
            final boolean isSameSceneId = favoriteScene.get_sceneId() == room.get_lastCalledScene(groupNumber);
            final boolean isSameRoomId = favoriteScene.get_roomId() == room.get_id();
            if (isSameSceneId && isSameRoomId) {
                button.setTypeface(Typeface.DEFAULT_BOLD);
            }
        }
    }

    private Button initFavButton(final DsScene favoriteScene) {
        final List<DsScene> knownScenes = Lists.newArrayList();

        if (!Rooms.isGlobal(favoriteScene.get_roomId())) {
            final DsRoom room = RoomsStore.get_room_by_id(favoriteScene.get_roomId());
            if (room == null) {
                return null;
            }
            knownScenes.addAll(room.get_scenes());
        }

        final String text;
        if (knownScenes.contains(favoriteScene)) {
            final int idx = knownScenes.indexOf(favoriteScene);
            text = knownScenes.get(idx).get_name();
        } else {
            text = favoriteScene.get_name();
        }

        final Button button = new Button(getActivity());
        button.setBackgroundResource(R.drawable.hs_favoritebutton);
        button.setTextAppearance(getActivity(), R.style.ButtonFontStyle);
        button.setTextColor(Color.WHITE);
        button.setText(text);

        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, LayoutParams.MATCH_PARENT);
        params.leftMargin = (int) (3 * scale + 0.5f);
        params.rightMargin = (int) (3 * scale + 0.5f);
        params.topMargin = (int) (8 * scale + 0.5f);
        button.setPadding((int) (1 * scale + 0.5f), 0, (int) (1 * scale + 0.5f), 0);
        button.setLayoutParams(params);
        button.setGravity(Gravity.CENTER);
        return button;
    }

    private ImageButton createPlusButton() {
        // add the "+" button
        final ImageButton addButton = new ImageButton(getActivity());
        addButton.setBackgroundResource(R.drawable.hs_favoritebutton);
        addButton.setImageResource(R.drawable.hs_add);

        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, LayoutParams.MATCH_PARENT);
        params.leftMargin = (int) (3 * scale + 0.5f);
        params.rightMargin = (int) (3 * scale + 0.5f);
        params.topMargin = (int) (8 * scale + 0.5f);
        addButton.setPadding((int) (1 * scale + 0.5f), 0, (int) (1 * scale + 0.5f), 0);
        addButton.setLayoutParams(params);

        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View V) {
                final Intent intent = new Intent(getActivity(), FavoriteSettings.class);
                intent.putExtra(EXTRA_SHOW_ANIMATION, true);
                getActivity().startActivityForResult(intent, RoomOverview.REQUEST_CODE_CONFIGURATION_SCREEN);
                getActivity().overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }
        });
        return addButton;
    }
}
