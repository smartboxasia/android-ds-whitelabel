package com.smartboxasia.control.ui.overview;

import java.util.List;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.MetersCache;
import com.smartboxasia.control.dto.DsMeter;
import com.smartboxasia.control.ui.helper.PieChartView;
import com.google.common.collect.Lists;

final class PieChartAdapter extends BaseAdapter {

    private Activity activity;
    private List<DsMeter> meters;

    public PieChartAdapter(final Activity activity) {
        this.activity = activity;
        updateMeterList();
    }

    private void updateMeterList() {
        meters = Lists.newArrayList(MetersCache.get_meters(false));
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(App.getInstance()).inflate(R.layout.pie_chart_item, parent, false);
        }
        final TextView dummyTv = (TextView) convertView.findViewById(R.id.dummyTextView);
        if (position > 0) {
            dummyTv.setVisibility(View.GONE);
        } else {
            dummyTv.setVisibility(View.VISIBLE);
        }

        final TextView meterNameTv = (TextView) convertView.findViewById(R.id.meterNameTextView);
        meterNameTv.setText(getMeterEntry(position));

        final FrameLayout colorView = ((FrameLayout) convertView.findViewById(R.id.colorView));

        final View legendColorPointView = createLegendColorPointView(position);
        colorView.removeAllViews();
        colorView.addView(legendColorPointView);
        return convertView;
    }

    private CharSequence getMeterEntry(final int position) {
        return meters.get(position).get_name() + ": " + meters.get(position).get_consumption() + "W";
    }

    private View createLegendColorPointView(final int position) {
        final Paint paint = new Paint();
        paint.setColor(PieChartView.getColor(position));
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);

        final Rect bgPadding = new Rect();

        final View view = new View(activity) {
            @Override
            protected void onDraw(final Canvas canvas) {
                final Drawable bg = getBackground();
                bg.getPadding(bgPadding);
                final int height = bg.getIntrinsicHeight() - bgPadding.top - bgPadding.bottom;
                final int width = bg.getIntrinsicWidth() - bgPadding.left - bgPadding.right;

                canvas.drawCircle(getRight() - width / 2, getBottom() - height / 2, height / 4, paint);
            };
        };
        view.setBackgroundResource(R.drawable.visual_label_left_inactive);
        return view;
    }

    @Override
    public int getCount() {
        return meters.size();
    }

    @Override
    public Object getItem(final int position) {
        return meters.get(position);
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public void notifyDataSetChanged() {
        updateMeterList();
        super.notifyDataSetChanged();
    }
}
