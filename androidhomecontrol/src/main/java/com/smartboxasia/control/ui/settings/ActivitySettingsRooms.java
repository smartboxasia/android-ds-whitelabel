package com.smartboxasia.control.ui.settings;

import static com.smartboxasia.control.intentservice.Service_smartboxasia.ACTION_IDENTIFIER;
import static com.smartboxasia.control.intentservice.Service_smartboxasia.CALLING_PACKAGE;

import java.util.List;
import java.util.Set;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.smartboxasia.control.App;
import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.config.app.UserDefinedActionsCache;
import com.smartboxasia.control.data.config.connection.ConnectionConfigService;
import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.dto.DsScene;
import com.smartboxasia.control.events.GotUserDefinedActionsEvent;
import com.smartboxasia.control.events.LoginResultEvent;
import com.smartboxasia.control.events.RoomsChangedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;

public class ActivitySettingsRooms extends ConnectivityMonitoringFragmentActivity {

    public static final int RESULT_FAVORITE_ADDED = 11;
    public static final String EXTRA_NEW_FAVORITE = "com.smartboxasia.sia.activityNewFavorite";

    private static final int REQUEST_ACTIVITY_LIST = 80;

    private List<DsRoom> rooms = Lists.newArrayList();
    private ArrayAdapter<DsRoom> adapter;
    private boolean changed = false;
    private boolean isLookingForFavorite = false;

    // Api function
    private String apiCallingActionIdentifier;
    private String apiActionIdentifier;
    private String apiCallerIdentifier;
    private ProgressDialog progressDialog;
    private boolean roomsUpdated;
    private boolean udaUpdated;
    private ListView listView;

    private static final Handler handler = new Handler() {
        @Override
        public void handleMessage(final Message msg) {
            Connection.refreshSession();
        };
    };

    @Subscribe
    public void onEvent(final RoomsChangedEvent event) {
        roomsUpdated = true;
        if (udaUpdated) {
            updateListFromThread();
        }
    }

    @Subscribe
    public void onEvent(final GotUserDefinedActionsEvent event) {
        udaUpdated = true;
        if (roomsUpdated) {
            updateListFromThread();
        }
    }

    private void updateListFromThread() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                updateList();
                if (apiCallingActionIdentifier != null) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    @Subscribe
    public void onEvent(final LoginResultEvent event) {
        if (apiCallingActionIdentifier != null) {
            switch (event.getResult()) {
                case LOGIN_RESULT_OK: {
                    DssService.updateRooms(getApplicationContext());
                    DssService.updateUserDefinedActions(getApplicationContext());
                    break;
                }
                case LOGIN_RESULT_NOT_OK: {
                    progressDialog.setMessage(getString(R.string.connection_problem));
                    // throttle retry
                    handler.sendEmptyMessageDelayed(0, App.DSS_ACTION_DELAY);
                    break;
                }
                default:
                    // TODO really ignore?
            }
        }
    }

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_list);

        ((TextView) findViewById(R.id.titleTextView)).setText(R.string.settings_title_activities);

        // api scenario
        try {
            apiActionIdentifier = getIntent().getExtras().getString(ACTION_IDENTIFIER);
            apiCallerIdentifier = getIntent().getExtras().getString(CALLING_PACKAGE);

            // create a unique action id from calling package + their supplied id
            if (apiCallerIdentifier != null || apiActionIdentifier != null) {
                apiCallingActionIdentifier = apiCallerIdentifier + apiActionIdentifier;
            }
        } catch (final Exception e) {
            Log.i("action!!!", "Failed to get calling action intent exception: " + e.getLocalizedMessage());
        }

        try {
            isLookingForFavorite = getIntent().getExtras().getBoolean(EXTRA_NEW_FAVORITE, false);
        } catch (final Exception e) {
            // not important
        }

        // set title to "choose activity"
        if (isLookingForFavorite || apiCallingActionIdentifier != null) {
            final TextView titleTv = (TextView) findViewById(R.id.titleTextView);
            titleTv.setText(R.string.activities_choose);
        }

        // do startup routine if opened from another app
        if (apiCallingActionIdentifier != null) {
            initConnection();
        }

        adapter = new ArrayAdapter<DsRoom>(this, R.layout.settings_list_item, rooms) {
            @Override
            public View getView(final int position, View convertView, final ViewGroup parent) {
                if (convertView == null) {
                    convertView = getLayoutInflater().inflate(R.layout.settings_list_item, parent, false);
                    convertView.findViewById(R.id.icon).setVisibility(View.GONE);
                    final View v = convertView.findViewById(R.id.settingsTextView);
                    final int paddingLeft = getResources().getDimensionPixelSize(R.dimen.settings_text_margin);
                    v.setPadding(paddingLeft, v.getPaddingTop(), v.getPaddingRight(), v.getPaddingBottom());
                }

                ((TextView) convertView.findViewById(R.id.settingsTextView)).setText(rooms.get(position).get_name());

                if (position % 2 == 0) {
                    convertView.setBackgroundResource(R.drawable.conf_list_grey);
                } else {
                    convertView.setBackgroundResource(R.drawable.conf_list_white);
                }

                return convertView;
            }
        };

        listView = (ListView) findViewById(android.R.id.list);
        listView.setAdapter(adapter);

        updateList();
    }

    private void initConnection() {

        final ConnectionData lastUsedConnectionData = ConnectionConfigService.getLastUsedConnectionData();
        if (lastUsedConnectionData != null && !Strings.isNullOrEmpty(lastUsedConnectionData.getUrl()) && lastUsedConnectionData.getApplicationToken() != null) {

            progressDialog = ProgressDialog.show(this, getString(R.string.connecting), getString(R.string.updating_rooms) + ": " + lastUsedConnectionData.getName(), true, true);
            progressDialog.setCanceledOnTouchOutside(true);
            progressDialog.show();

            // get session token and resume polling the consumption (and reload rooms if needed)
            Connection.getConnectionService().refreshSession(lastUsedConnectionData, false);
        } else {
            Toast.makeText(this, R.string.select_server, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // Unregister for events
        App.eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register for events
        App.eventBus.register(this);

        // update rooms if we're not called from another app
        if (apiCallingActionIdentifier != null) {
            // get session token
            Connection.refreshSession();
        } else {
            // update rooms
            DssService.updateRooms(getApplicationContext());
        }

        GAHelper.sendScreenViewEvent("Settings Activities Select Room");
    }

    @Override
    public void onBackPressed() {
        if (changed) {
            setResult(RESULT_OK);
        }
        finish();
    }

    // callback defined in xml
    public void goBackClick(final View V) {
        onBackPressed();
    }

    private void updateList() {
        // clear and update content
        rooms.clear();

        // add global activities
        rooms.add(new DsRoom(DssConstants.Rooms.GLOBAL, getString(R.string.global_activities), null));

        // add user defined actions
        final Set<DsScene> userDefinedActions = UserDefinedActionsCache.get_userDefinedActions();
        if (userDefinedActions != null && userDefinedActions.size() > 0) {
            rooms.add(new DsRoom(DssConstants.Rooms.USER_ACTION, getString(R.string.user_defined_actions), null));
        }

        // add all real rooms
        rooms.addAll(RoomsStore.get_roomsSorted());

        adapter.notifyDataSetChanged();
    }

    // callback defined in xml
    public void selectButtonClick(final View V) {
        final int position = listView.getPositionForView(V);

        final DsRoom room = rooms.get(position);
        final int selectedRoomId = room.get_id();

        Intent intent;
        if (selectedRoomId == DssConstants.Rooms.GLOBAL) {
            intent = new Intent(this, ActivitySettingsGlobalActions.class);
        } else if (selectedRoomId == DssConstants.Rooms.USER_ACTION) {
            intent = new Intent(this, ActivitySettingsUserActions.class);
        } else {
            intent = new Intent(this, ActivitySettingsActivities.class);
            intent.putExtra(ActivitySettingsActivities.EXTRA_ROOM_ID, selectedRoomId);
        }

        intent.putExtra(ActivitySettingsActivities.EXTRA_NEW_FAVORITE, isLookingForFavorite);

        if (apiCallingActionIdentifier != null) {
            intent.putExtra(ACTION_IDENTIFIER, apiActionIdentifier);
            intent.putExtra(CALLING_PACKAGE, apiCallerIdentifier);
            this.startActivityForResult(intent, REQUEST_ACTIVITY_LIST);
        } else if (isLookingForFavorite) {
            this.startActivityForResult(intent, REQUEST_ACTIVITY_LIST);
        } else {
            this.startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ACTIVITY_LIST) {
            if (resultCode == RESULT_OK) {
                if (apiCallingActionIdentifier != null) {
                    // If we were called by another app, we tell it that the call finished
                    setResult(RESULT_OK, data);
                    finish();
                } else {
                    changed = true;
                }
            } else if (resultCode == RESULT_FAVORITE_ADDED) {
                // In case we selected a favorite to add, we skip the rooms list
                // and go back directly to the FavoriteSettings
                setResult(ConfigurationScreen.FAVORITES_CHANGED);
                finish();
            }
        }
    }
}
