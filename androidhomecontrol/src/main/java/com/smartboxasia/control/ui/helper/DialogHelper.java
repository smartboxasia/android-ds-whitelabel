package com.smartboxasia.control.ui.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;

import com.smartboxasia.control.R;

public class DialogHelper {

    /**
     * Shows an information dialog for the user.
     * @param activity
     * @param title the Title of the dialog
     * @param message the message shown in the dialog
     * @param showButton if true a cancel button is shown
     */
    public static void showInformationDialog(final Activity activity, final String title, final String message, final boolean showButton) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(activity).setTitle(title).setMessage(message).setPositiveButton(R.string.cancel, new CancelClickListener()).create().show();
            }
        });
    }

    /**
     * Shows an information dialog for the user.
     * @param activity
     * @param title the Title of the dialog
     * @param message the message shown in the dialog
     */
    public static void showInformationDialog(final Activity activity, final String title, final String message, final View view, final boolean showButton) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(activity).setTitle(title).setMessage(message).setView(view).setPositiveButton(R.string.cancel, new CancelClickListener()).create().show();
            }
        });
    }

    /**
     * Shows an information dialog for the user.
     * @param activity
     * @param title the Title of the dialog
     * @param message the message shown in the dialog
     */
    public static void showInformationDialog(final Activity activity, final int titleResId, final int messageResId, final boolean showButton) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(activity).setTitle(titleResId).setMessage(messageResId).setPositiveButton(R.string.cancel, new CancelClickListener()).create().show();
            }
        });
    }

    /**
     * Shows an information dialog for the user.
     * @param activity
     * @param title the Title of the dialog
     * @param message the message shown in the dialog
     */
    public static void showInformationDialog(final Activity activity, final int titleResId, final int messageResId, final int imageResId, final boolean showButton) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final ImageView imageView = new ImageView(activity);
                imageView.setImageResource(imageResId);
                new AlertDialog.Builder(activity).setTitle(titleResId).setMessage(messageResId).setView(imageView).setPositiveButton(R.string.cancel, new CancelClickListener()).create().show();
            }
        });
    }
}
