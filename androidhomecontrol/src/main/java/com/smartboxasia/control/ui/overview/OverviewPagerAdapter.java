package com.smartboxasia.control.ui.overview;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.smartboxasia.control.ui.helper.GroupIconHelper;
import com.google.common.collect.Lists;
import com.viewpagerindicator.IconPagerAdapter;

public abstract class OverviewPagerAdapter extends PagerAdapter implements IconPagerAdapter {

    protected static GroupIconHelper iconHelper = new GroupIconHelper();
    protected Activity activity;
    protected List<Integer> availableGroups;
    protected int roomId;

    public OverviewPagerAdapter(final Activity activity, final Set<Integer> avaiableGroups, final int roomId) {
        super();

        this.activity = activity;

        this.availableGroups = Lists.newArrayList(iconHelper.getValidGroups(avaiableGroups));
        Collections.sort(this.availableGroups);

        this.roomId = roomId;
    }

    @Override
    public int getCount() {
        return availableGroups.size();
    }

    /**
     * Create the page for the given position. The adapter is responsible for adding the view to the container given here, although it only must ensure this is done by the time it returns from
     * {@link #finishUpdate()}.
     * 
     * @param container The containing View in which the page will be shown.
     * @param position The page position to be instantiated.
     * @return Returns an Object representing the new page. This does not need to be a View, but can be some other container of the page.
     */
    @Override
    public Object instantiateItem(final View collection, final int position) {

        final ListView newList = new ListView(activity);

        newList.setBackgroundColor(Color.TRANSPARENT);
        newList.setCacheColorHint(Color.TRANSPARENT);
        newList.setDivider(null);

        // Create dummy textview for the bottom of the list, to make it look proper
        final TextView dummyView = new TextView(activity);
        dummyView.setClickable(false);
        newList.addFooterView(dummyView);

        updateItemList(newList, availableGroups.get(position));
        ((ViewPager) collection).addView(newList, 0);

        return newList;
    }

    /**
     * Remove a page for the given position. The adapter is responsible for removing the view from its container, although it only must ensure this is done by the time it returns from
     * {@link #finishUpdate()}.
     * 
     * @param container The containing View from which the page will be removed.
     * @param position The page position to be removed.
     * @param object The same object that was returned by {@link #instantiateItem(View, int)}.
     */
    @Override
    public void destroyItem(final View collection, final int position, final Object view) {
        ((ViewPager) collection).removeView((ListView) view);
    }

    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(final Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getIconResId(final int index) {
        return iconHelper.getIconForGroup(availableGroups.get(index));
    }

    abstract void updateItemList(final ListView theList, final int groupNumber);
}
