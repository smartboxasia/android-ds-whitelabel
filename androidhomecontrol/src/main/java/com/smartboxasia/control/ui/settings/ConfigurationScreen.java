package com.smartboxasia.control.ui.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView.LayoutParams;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;

public class ConfigurationScreen extends Activity {

    public static final String EXTRA_SHOW_ANIMATION = "com.smartboxasia.sia.push_down_animation";

    public static final int REQUEST_CODE_CONFIGURATION = App.REQUEST_CODE_BASE_CONFIGURATION;
    public static final int REQUEST_CODE_SERVERS = REQUEST_CODE_CONFIGURATION + 100;
    public static final int REQUEST_CODE_ROOMS = REQUEST_CODE_SERVERS + 100;
    public static final int REQUEST_CODE_ACTIVITIES = REQUEST_CODE_ROOMS + 100;
    public static final int REQUEST_CODE_DEVICES = REQUEST_CODE_ACTIVITIES + 100;
    public static final int REQUEST_CODE_METERS = REQUEST_CODE_DEVICES + 100;
    public static final int REQUEST_CODE_FAVORITES = REQUEST_CODE_METERS + 100;
    public static final int REQUEST_CODE_INFO = REQUEST_CODE_FAVORITES + 100;

    // resultcodes
    public static int SERVERS_CHANGED = 10;
    public static int ROOMS_CHANGED = 11;
    public static int ACTIVITIES_CHANGED = 12;
    public static int FAVORITES_CHANGED = 13;
    public static int METERS_CHANGED = 14;
    public static int DEVICES_CHANGED = 15;

    private GridView grid;

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_settings);

        ((ImageView) findViewById(R.id.backButton)).setImageResource(R.drawable.orange_ribbon_overview);
        ((TextView) findViewById(R.id.titleTextView)).setText(R.string.configuration_title);

        grid = (GridView) findViewById(R.id.settings_grid);
        final ConfigurationScreenGridAdapter adapter = new ConfigurationScreenGridAdapter(this);
        grid.setAdapter(adapter);
    }

    private static int measureCellWidth(final Context context, final View cell) {

        // We need a fake parent
        final FrameLayout buffer = new FrameLayout(context);
        final android.widget.AbsListView.LayoutParams layoutParams = new android.widget.AbsListView.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        buffer.addView(cell, layoutParams);

        cell.forceLayout();
        cell.measure(10000, 10000);

        final int width = cell.getMeasuredWidth();

        buffer.removeAllViews();

        return width;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Measure the size of a cell and use this to dynamically initialize the grid view row width
        final int columnWidth = measureCellWidth(this, grid.getAdapter().getView(0, null, null));
        grid.setColumnWidth(columnWidth);

        ((ConfigurationScreenGridAdapter) grid.getAdapter()).update();

        GAHelper.sendScreenViewEvent("Settings");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
    }

    // callback defined in xml
    public void goBackClick(final View V) {
        onBackPressed();
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == REQUEST_CODE_SERVERS) {
            // if server is changed, set result and finish()
            if (resultCode == SERVERS_CHANGED) {
                setResult(SERVERS_CHANGED);
                finish();
            }
        } else if (requestCode == REQUEST_CODE_ROOMS) {
            // if rooms are changed, set result
            if (resultCode == ROOMS_CHANGED) {
                setResult(ROOMS_CHANGED);
            }
        } else if (requestCode == REQUEST_CODE_ACTIVITIES) {
            // activites are changed, set result
            if (resultCode == ACTIVITIES_CHANGED) {
                setResult(ACTIVITIES_CHANGED);
            }
        } else if (requestCode == REQUEST_CODE_DEVICES) {
            // devices are changed, set result
            if (resultCode == DEVICES_CHANGED) {
                setResult(DEVICES_CHANGED);
            }
        } else if (requestCode == REQUEST_CODE_METERS) {
            // meters are changed, set result
            if (resultCode == METERS_CHANGED) {
                setResult(METERS_CHANGED);
            }
        } else if (requestCode == REQUEST_CODE_FAVORITES) {
            // favorites are changed, set result
            if (resultCode == FAVORITES_CHANGED) {
                setResult(FAVORITES_CHANGED);
            }
        }
    }
}
