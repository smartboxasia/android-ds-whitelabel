package com.smartboxasia.control.ui.overview;

import android.content.Intent;
import android.content.res.Configuration;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.events.SceneActivatedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.smartboxasia.control.ui.helper.ConsumptionHelper;
import com.smartboxasia.control.ui.settings.ConfigurationScreen;
import com.google.common.eventbus.Subscribe;

public class AdvancedButtonShade extends ConnectivityMonitoringFragmentActivity {

    private static final int REQEUST_CODE_BASE_ADVANCE_BUTTON_SHADE = 400;

    public static final int REQUEST_CODE_CONFIGURATION_SCREEN = App.REQUEST_CODE_BASE_HOMESCREEN + REQEUST_CODE_BASE_ADVANCE_BUTTON_SHADE;

    public static final String EXTRA_CURRENT_NAME = "com.smartboxasia.sia.currentName";
    public static final String EXTRA_TYPE_NAME = "com.smartboxasia.sia.typeName";
    public static final String EXTRA_IS_DEVICE = "com.smartboxasia.sia.isDevice";
    public static final String EXTRA_DEVICE_ID = "com.smartboxasia.sia.deviceId";
    public static final String EXTRA_ROOM_ID = "com.smartboxasia.sia.roomId";

    // Flag if receiver is registered
    private boolean isDevice;
    private boolean isTilting = false;
    private boolean isTouching = false;
    private int roomId;
    private String deviceId;
    private View minButton;
    private View maxButton;

    private OrientationEventListener myOrientationEventListener;

    private FavoritesBarFragment favoritesBarFragment;

    private final ConsumptionHelper consumption = new ConsumptionHelper(this, R.id.ConsumptionTextView, R.id.ConsumptionColor);

    @Subscribe
    public void onEvent(final SceneActivatedEvent event) {
        favoritesBarFragment.updateFavoriteList();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // listen for orientation changes
        myOrientationEventListener.enable();

        // update favorites
        favoritesBarFragment.updateFavoriteList();

        // Register for events
        App.eventBus.register(this);

        // resume polling the consumption (this also gets a new session token, in case it timed out)
        consumption.onResume();

        // update rooms if device, else update activites
        if (isDevice) {
            DssService.updateRooms(getApplicationContext());
        } else {
            DssService.updateActivties(getApplicationContext());
        }

        GAHelper.sendScreenViewEvent("Overview Advanced Shade");
    }

    @Override
    protected void onPause() {
        super.onPause();

        consumption.onPause();

        // Unregister for events
        App.eventBus.unregister(this);

        // stop listening for orientation
        myOrientationEventListener.disable();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.overview_control_button_shade);

        favoritesBarFragment = (FavoritesBarFragment) getSupportFragmentManager().findFragmentById(R.id.FavoritesBar);

        final String currentName = getIntent().getExtras().getString(EXTRA_CURRENT_NAME);
        final String typeName = getIntent().getExtras().getString(EXTRA_TYPE_NAME);
        isDevice = getIntent().getExtras().getBoolean(EXTRA_IS_DEVICE, false);
        roomId = getIntent().getExtras().getInt(EXTRA_ROOM_ID);
        deviceId = getIntent().getExtras().getString(EXTRA_DEVICE_ID);

        final TextView tv = (TextView) findViewById(R.id.currentTextView);
        tv.setText(currentName);
        final TextView tv2 = (TextView) findViewById(R.id.TitleTextView);
        tv2.setText(typeName);

        final OnTouchListener listener = new OnTouchListener() {

            public boolean onTouch(final View view, final MotionEvent motionEvent) {
                final int action = motionEvent.getAction();
                if (!isTilting) {
                    if (action == MotionEvent.ACTION_DOWN) {
                        isTouching = true;
                    } else if (action == MotionEvent.ACTION_UP) {
                        isTouching = false;
                    }
                }
                return false;
            }
        };
        minButton = findViewById(R.id.fullDownButton);
        maxButton = findViewById(R.id.fullUpButton);
        minButton.setOnTouchListener(listener);
        maxButton.setOnTouchListener(listener);
        findViewById(R.id.singleDownButton).setOnTouchListener(listener);
        findViewById(R.id.singleUpButton).setOnTouchListener(listener);

        myOrientationEventListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {

            boolean tiltLeft = false;
            boolean tiltRight = false;
            boolean stop = false;

            @Override
            public void onOrientationChanged(final int arg0) {
                if (!isTouching) {
                    final DsRoom room = RoomsStore.get_room_by_id(roomId);
                    final DsDevice device = room.get_device_by_id(deviceId);
                    // call
                    if (arg0 > 250 && arg0 < 320) {
                        if (!tiltLeft) {
                            isTilting = true;
                            // on left tilt, call MIN scene
                            if (isDevice) {
                                DssService.callSceneOnDevice(getApplicationContext(), device, DssConstants.Scenes.MIN);
                            } else {
                                DssService.callActivity(getApplicationContext(), roomId, DssConstants.Groups.SHADE, DssConstants.Scenes.MIN, false);
                            }

                            tiltRight = false;
                            tiltLeft = true;
                            stop = true;

                            minButton.setPressed(true);
                            maxButton.setPressed(false);
                        }
                    } else if (arg0 > 40 && arg0 < 120) {
                        if (!tiltRight) {
                            isTilting = true;
                            // on right tilt, call MAX scene
                            if (isDevice) {
                                DssService.callSceneOnDevice(getApplicationContext(), device, DssConstants.Scenes.MAX);
                            } else {
                                DssService.callActivity(getApplicationContext(), roomId, DssConstants.Groups.SHADE, DssConstants.Scenes.MAX, false);
                            }

                            tiltLeft = false;
                            tiltRight = true;
                            stop = true;

                            minButton.setPressed(false);
                            maxButton.setPressed(true);
                        }
                    }

                    if (!(arg0 > 250 && arg0 < 320) && !(arg0 > 40 && arg0 < 120) && stop) {
                        // if tilting and not in a tilting position, call STOP scene
                        if (isDevice) {
                            DssService.callSceneOnDevice(getApplicationContext(), device, DssConstants.Scenes.STOP);
                        } else {
                            DssService.callActivity(getApplicationContext(), roomId, DssConstants.Groups.SHADE, DssConstants.Scenes.STOP, false);
                        }

                        tiltLeft = false;
                        tiltRight = false;
                        stop = false;
                        isTilting = false;

                        minButton.setPressed(false);
                        maxButton.setPressed(false);
                    }
                }
            }
        };

        consumption.update();

        initMenuButton();
    }

    private void initMenuButton() {
        final View menuButton = findViewById(R.id.menuButton);
        menuButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                startActivityForResult(new Intent(AdvancedButtonShade.this, ConfigurationScreen.class), REQUEST_CODE_CONFIGURATION_SCREEN);
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }
        });
    }

    // Make sure the layout is not recreated on keyboard hidden events (for hardware keyboard phones)
    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.overview_control_button_shade);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
    }

    // callback defined in xml
    public void goBackClick(final View V) {
        onBackPressed();
    }

    // callback defined in xml
    public void stopClick(final View V) {
        // send STOP scene
        doCallScene(DssConstants.Scenes.STOP, DssConstants.Groups.SHADE);
    }

    // callback defined in xml
    public void singleUpButtonClick(final View V) {
        if (!isTilting) {
            doCallScene(DssConstants.Scenes.STEP_UP, DssConstants.Groups.SHADE);
        }
    }

    // callback defined in xml
    public void fullUpButtonClick(final View V) {
        if (!isTilting) {
            doCallScene(DssConstants.Scenes.MAX, DssConstants.Groups.SHADE);
        }
    }

    // callback defined in xml
    public void singleDownButtonClick(final View V) {
        if (!isTilting) {
            doCallScene(DssConstants.Scenes.STEP_DOWN, DssConstants.Groups.SHADE);
        }
    }

    // callback defined in xml
    public void fullDownButtonClick(final View V) {
        if (!isTilting) {
            doCallScene(DssConstants.Scenes.MIN, DssConstants.Groups.SHADE);
        }
    }

    private void doCallScene(final long sceneId, final int groupNumber) {
        if (isDevice) {
            final DsRoom room = RoomsStore.get_room_by_id(roomId);
            final DsDevice device = room.get_device_by_id(deviceId);
            DssService.callSceneOnDevice(getApplicationContext(), device, sceneId);
        } else {
            DssService.callActivity(getApplicationContext(), roomId, groupNumber, sceneId, false);
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }
}
