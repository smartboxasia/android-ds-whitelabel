package com.smartboxasia.control.ui.helper;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;

public class MobileSettingsClickListener implements OnClickListener {

    private Activity ativity;

    public MobileSettingsClickListener(final Activity ativity) {
        this.ativity = ativity;
    }

    @Override
    public void onClick(final DialogInterface dialog, final int which) {
        ativity.startActivity(new Intent(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS));
        dialog.dismiss();
    }

}
