package com.smartboxasia.control.ui.settings;

import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.UserDefinedActionsCache;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsScene;
import com.google.common.collect.Lists;

public class ActivitySettingsUserActions extends ActivitySettingsActivitiesBase {

    List<DsScene> activities = Lists.newArrayList();
    private ListView listView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_list);

        ((TextView) findViewById(R.id.titleTextView)).setText(R.string.user_defined_actions);

        listView = (ListView) findViewById(android.R.id.list);
        listView.setAdapter(getAdapter());
    }

    @Override
    protected BaseAdapter creatAdapter() {
        return new BaseAdapter() {

            @Override
            public int getCount() {
                return activities.size();
            }

            @Override
            public Object getItem(final int position) {
                return activities.get(position);
            }

            @Override
            public long getItemId(final int position) {
                return position;
            }

            @Override
            public View getView(final int position, View convertView, final ViewGroup parent) {
                if (convertView == null) {
                    convertView = LayoutInflater.from(ActivitySettingsUserActions.this).inflate(R.layout.settings_list_item, parent, false);
                }
                final DsScene scene = activities.get(position);
                ((TextView) convertView.findViewById(R.id.settingsTextView)).setText(scene.get_name());

                if (apiCallingActionIdentifier != null || isLookingForFavorite) {
                    convertView.findViewById(R.id.selectButton).setVisibility(View.INVISIBLE);
                }
                return convertView;
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();

        DssService.updateUserDefinedActions(getApplicationContext());

        GAHelper.sendScreenViewEvent("Settings Activities Select UDA");
    }

    @Override
    public void updateList() {
        // clear and update content
        activities.clear();

        // add user defined actions
        if (UserDefinedActionsCache.get_userDefinedActions() != null) {
            activities.addAll(UserDefinedActionsCache.get_userDefinedActions());
        }

        getAdapter().notifyDataSetChanged();
    }

    // directly called from button as defined in xml
    public void selectButtonClick(final View view) {
        final int position = listView.getPositionForView(view);
        final DsScene theScene = activities.get(position);

        sceneSelected(theScene);
    }
}
