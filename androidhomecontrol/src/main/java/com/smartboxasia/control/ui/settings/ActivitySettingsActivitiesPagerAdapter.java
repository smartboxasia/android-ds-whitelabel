package com.smartboxasia.control.ui.settings;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.ui.helper.GroupIconHelper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.viewpagerindicator.IconPagerAdapter;

public class ActivitySettingsActivitiesPagerAdapter extends FragmentStatePagerAdapter implements IconPagerAdapter {

    private int roomId = DssConstants.Rooms.UNKNOWN;

    private static GroupIconHelper iconHelper = new GroupIconHelper();
    private List<Integer> avaiableGroups;

    ActivitySettingsActivitiesPagerAdapter(final FragmentManager fm, final int roomId) {
        super(fm);
        this.roomId = roomId;

        // check what colour groups are available in this roomNr
        final List<DsDevice> devices = RoomsStore.get_room_by_id(roomId).get_devices();

        final Set<Integer> groups = Sets.newHashSet();
        for (final DsDevice device : devices) {
            groups.add(device.get_groupNumber());
        }

        avaiableGroups = Lists.newArrayList(iconHelper.getValidGroups(groups));
        Collections.sort(this.avaiableGroups);
    }

    // workaround, that causes all views to be redrawn when notifyDataSetChanged is called on the adapter
    @Override
    public int getItemPosition(final Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return avaiableGroups.size();
    }

    @Override
    public Fragment getItem(final int position) {

        final Bundle args = new Bundle();
        args.putInt(ActivitySettingsActivitiesGroupPage.GROUP_NUMBER, avaiableGroups.get(position));
        args.putInt(ActivitySettingsActivitiesGroupPage.ROOM_ID, roomId);
        final Fragment page = new ActivitySettingsActivitiesGroupPage();
        page.setArguments(args);

        return page;
    }

    @Override
    public int getIconResId(final int index) {
        return iconHelper.getIconForGroup(avaiableGroups.get(index));
    }
}
