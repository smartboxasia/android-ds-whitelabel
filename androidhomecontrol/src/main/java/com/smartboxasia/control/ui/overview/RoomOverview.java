package com.smartboxasia.control.ui.overview;

import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.ApplicationConfigService;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.config.connection.ConnectionConfigService;
import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.events.ActivitiesChangedEvent;
import com.smartboxasia.control.events.GotUserDefinedActionsEvent;
import com.smartboxasia.control.events.LoginResultEvent;
import com.smartboxasia.control.events.RoomsChangedEvent;
import com.smartboxasia.control.events.SceneActivatedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.smartboxasia.control.ui.WelcomeActivity;
import com.smartboxasia.control.ui.helper.ConsumptionHelper;
import com.smartboxasia.control.ui.helper.SwitchOffOnClickListener;
import com.smartboxasia.control.ui.helper.TiltDetector;
import com.smartboxasia.control.ui.settings.ConfigurationScreen;
import com.smartboxasia.control.ui.settings.ServerSettings;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;

public class RoomOverview extends ConnectivityMonitoringFragmentActivity {

    private static final int REQUEST_CODE_LOGIN = App.REQUEST_CODE_BASE_HOMESCREEN;
    /* pk */static final int REQUEST_CODE_CONFIGURATION_SCREEN = REQUEST_CODE_LOGIN + 1;
    private static final int REQUEST_CODE_ACTIVITY_OVERVIEW = REQUEST_CODE_CONFIGURATION_SCREEN + 1;
    public static final int REQUEST_CODE_CONNECTIVITY_CHANGED = REQUEST_CODE_ACTIVITY_OVERVIEW + 1;
    public static final int REQUEST_CODE_WELCOME = REQUEST_CODE_CONNECTIVITY_CHANGED + 1;

    private static final Handler handler = new Handler() {
        @Override
        public void handleMessage(final Message msg) {
            Connection.refreshSessionNoWarnings();
        };
    };

    private ProgressDialog _progressDialog;
    private ListView roomList;
    private BaseAdapter adapter;
    private List<DsRoom> rooms = Lists.newArrayList();

    private TiltDetector tiltDetector = new TiltDetector();
    private boolean eventBusRegistered;

    private FavoritesBarFragment favoritesBarFragment;

    private final ConsumptionHelper consumption = new ConsumptionHelper(this, R.id.ConsumptionTextView, R.id.ConsumptionColor);

    @Subscribe
    public void onEvent(final SceneActivatedEvent event) {
        favoritesBarFragment.updateFavoriteList();
    }

    @Subscribe
    public void onEvent(final RoomsChangedEvent event) {
        if (event.isSuccess()) {
            updateRoomList();
            DssService.updateUserDefinedActions(getApplicationContext());
            DssService.updateActivties(getApplicationContext());
            _progressDialog.dismiss();
            tiltDetector.enable();
        }
    }

    @Subscribe
    public void onEvent(final ActivitiesChangedEvent event) {
        favoritesBarFragment.updateFavoriteList();
    }

    @Subscribe
    public void onEvent(final GotUserDefinedActionsEvent event) {
        favoritesBarFragment.updateFavoriteList();
    }

    @Subscribe
    public void onEvent(final LoginResultEvent event) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                switch (event.getResult()) {
                    case LOGIN_RESULT_OK: {
                        consumption.onPause();
                        consumption.onResume();
                        favoritesBarFragment.updateFavoriteList();

                        // refresh values
                        DssService.updateRooms(App.getInstance());
                        DssService.updateMeters(App.getInstance());
                        break;
                    }
                    case LOGIN_RESULT_NOT_OK: {
                        _progressDialog.setMessage(getString(R.string.connection_problem));
                        // throttle retry
                        handler.sendEmptyMessageDelayed(0, App.DSS_ACTION_THROTTLED_DELAY);
                        break;
                    }
                    case LOGIN_RESULT_NEED_CREDENTIALS: {
                        _progressDialog.dismiss();
                    }
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!eventBusRegistered) {
            eventBusRegistered = true;
            // Register for events
            App.eventBus.register(this);
        }

        updateRoomList();

        // update favorites
        favoritesBarFragment.updateFavoriteList();

        if (Connection.isLoggedIn) {
            consumption.onResume();
        } else {
            consumption.update(-1);
        }
        DssService.updateRooms(App.getInstance());

        // enable orientation listener again if coming back
        tiltDetector.onResume();

        GAHelper.sendScreenViewEvent("Overview Rooms");
    }

    @Override
    protected void onPause() {
        super.onPause();

        // stop listening for orientation
        tiltDetector.onPause();

        consumption.onPause();

        if (eventBusRegistered) {
            // Unregister for events
            App.eventBus.unregister(this);
            eventBusRegistered = false;
        }
    }

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.overview_rooms);

        favoritesBarFragment = (FavoritesBarFragment) getSupportFragmentManager().findFragmentById(R.id.FavoritesBar);

        tiltDetector.init(this);

        initMenuButton();

        initProgressDialog();

        initRoomList();

        if (ConnectionConfigService.getAllConnectionData().size() == 0) {
            startActivityForResult(new Intent(this, WelcomeActivity.class), REQUEST_CODE_WELCOME);
        } else {
            initConnection();
        }
    }

    private void initConnection() {
        final ConnectionData lastUsedConnectionData = ConnectionConfigService.getLastUsedConnectionData();
        if (lastUsedConnectionData == null || Strings.isNullOrEmpty(lastUsedConnectionData.getUrl())) {
            startActivityForResult(new Intent(this, ServerSettings.class), REQUEST_CODE_LOGIN);
            return;
        } else {
            refreshSession(lastUsedConnectionData);
        }
    }

    private void refreshSession() {
        final ConnectionData data = Connection.getActiveConnectionData();
        refreshSession(data);
    }

    private void refreshSession(final ConnectionData data) {
        tiltDetector.disable();
        _progressDialog.show();
        _progressDialog.setMessage(getString(R.string.updating_rooms) + ": " + data.getName());

        if (!eventBusRegistered) {
            eventBusRegistered = true;
            // on startup we need to register for events a bit earlier or we are going to miss early events.
            App.eventBus.register(this);
        }

        // get session token and resume polling the consumption (and reload rooms if needed)
        Connection.getConnectionService().refreshSession(data, false);
    }

    private void initProgressDialog() {
        _progressDialog = new ProgressDialog(this);
        _progressDialog.setTitle(R.string.connecting);
        final ConnectionData activeConnectionData = Connection.getActiveConnectionData();
        String name = "";
        if (activeConnectionData != null) {
            name = activeConnectionData.getName();
        }
        _progressDialog.setMessage(getString(R.string.updating_rooms) + ": " + name);
        _progressDialog.setIndeterminate(true);
        _progressDialog.setCancelable(true);
    }

    private void initRoomList() {

        // get the room list
        roomList = (ListView) findViewById(android.R.id.list);

        // register for long press
        registerForContextMenu(roomList);

        // Create dummy textview for the header and bottom of the list, to make it look proper
        final TextView dummyView = new TextView(this);
        roomList.addHeaderView(dummyView);
        roomList.addFooterView(dummyView);
        roomList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
                if (view.equals(dummyView)) {
                    return;
                }
                final Intent myIntent = new Intent(RoomOverview.this, ActivityOverview.class);
                myIntent.putExtra(ActivityOverview.EXTRA_ROOM_ID, rooms.get(position - roomList.getHeaderViewsCount()).get_id());
                startActivityForResult(myIntent, REQUEST_CODE_ACTIVITY_OVERVIEW);
            }
        });
        roomList.setOnItemLongClickListener(new OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> parent, final View view, final int position, final long id) {
                final DsRoom room = rooms.get(position - roomList.getHeaderViewsCount());
                final String roomName = room.get_name();
                final int roomId = room.get_id();

                final AlertDialog.Builder builder = new AlertDialog.Builder(RoomOverview.this);
                builder.setTitle(roomName);
                builder.setItems(getResources().getStringArray(R.array.switch_off_array), new SwitchOffOnClickListener(roomId, room, roomName));
                builder.create().show();
                return true;
            }
        });

        adapter = new RoomOverviewAdapter(this, rooms);
        roomList.setAdapter(adapter);
    }

    private void initMenuButton() {
        final View menuButton = findViewById(R.id.menuButton);
        menuButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                startActivityForResult(new Intent(RoomOverview.this, ConfigurationScreen.class), REQUEST_CODE_CONFIGURATION_SCREEN);
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }
        });
    }

    // Make sure the layout is not recreated on keyboard hidden events (for hardware keyboard phones)
    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        setContentView(R.layout.overview_rooms);
        super.onConfigurationChanged(newConfig);
    }

    // in case the server is changed, update.
    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        switch (requestCode) {
            case REQUEST_CODE_LOGIN: {
                if (resultCode == ConfigurationScreen.SERVERS_CHANGED) {
                    reconnect();
                }
                break;
            }
            case REQUEST_CODE_CONFIGURATION_SCREEN: {
                // if configuration changed the server
                if (resultCode == ConfigurationScreen.SERVERS_CHANGED) {
                    reconnect();
                }
                // if configuration changed the rooms
                if (resultCode == ConfigurationScreen.ROOMS_CHANGED) {
                    updateRoomList();
                }
                // if configuration changed the activites
                if (resultCode == ConfigurationScreen.ACTIVITIES_CHANGED) {
                    favoritesBarFragment.updateFavoriteList();
                }
                // if configuration changed the favorites
                if (resultCode == ConfigurationScreen.FAVORITES_CHANGED) {
                    favoritesBarFragment.updateFavoriteList();
                }
                break;
            }
            case REQUEST_CODE_ACTIVITY_OVERVIEW: {
                // if configuration changed the server
                if (resultCode == ConfigurationScreen.SERVERS_CHANGED) {
                    reconnect();
                }
                break;
            }
            case REQUEST_CODE_CONNECTIVITY_CHANGED:
                refreshSession();
                break;
            case REQUEST_CODE_WELCOME:
                if (resultCode == RESULT_OK) {
                    initConnection();
                } else {
                    finish();
                }
                break;
        }

        checkServerSelected();
    }

    private void reconnect() {
        tiltDetector.disable();
        _progressDialog.show();
        ApplicationConfigService.clearAllCaches();
        consumption.update(-1);
        refreshSession();
    }

    private void checkServerSelected() {
        if (Connection.getActiveConnectionData() == null || Connection.getActiveConnectionData().getUrl() == null) {
            Toast.makeText(this, R.string.select_server, Toast.LENGTH_LONG).show();
            _progressDialog.dismiss();
        }
    }

    private void updateRoomList() {
        rooms.clear();
        rooms.addAll(RoomsStore.get_roomsSorted());
        adapter.notifyDataSetChanged();
    }
}
