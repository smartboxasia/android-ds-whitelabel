package com.smartboxasia.control.ui.overview;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.DssConstants.Rooms;
import com.smartboxasia.control.DssConstants.Scenes;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.dto.DsScene;
import com.smartboxasia.control.ui.helper.SceneHelper;
import com.smartboxasia.control.ui.helper.SceneIconHelper;

final class ActivityOverviewAdapter extends BaseAdapter {
    private Activity activity;
    final List<DsScene> visibleScenes;
    private int roomId;

    ActivityOverviewAdapter(final Activity activity, final int roomId, final int groupNumber) {
        this.activity = activity;
        this.roomId = roomId;

        visibleScenes = SceneHelper.createScenesList(roomId, groupNumber, false);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final DsRoom room = RoomsStore.get_room_by_id(roomId);
        final DsScene scene = visibleScenes.get(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.overview_activities_item, parent, false);
        }
        final ImageView configButton = (ImageView) convertView.findViewById(R.id.configButton);

        ActivityOverviewPagerAdapter.updateBackground(convertView, room, scene);

        configButton.setImageResource(SceneIconHelper.getIcon(scene));

        // set click listener on the configButton (any scene except OFF)
        if (scene.get_sceneId() != DssConstants.Scenes.OFF) {
            configButton.setOnClickListener(new OnClickListener() {
                public void onClick(final View v) {
                    // do the business
                    advancedButtonClick(scene);
                }
            });
        }

        // set click listener on the activityClick
        final TextView activityTV = (TextView) convertView.findViewById(R.id.activityTextView);
        activityTV.setOnClickListener(new OnClickListener() {
            public void onClick(final View v) {
                // set the pressed color
                activityTV.setTextColor(activity.getResources().getColor(android.R.color.black));
                // do the activity click
                activityClick(scene);
            }
        });

        activityTV.setTextColor(activity.getResources().getColorStateList(R.color.color_selector));
        activityTV.setText(scene.get_name());

        return convertView;
    }

    @Override
    public int getCount() {
        return visibleScenes.size();
    }

    @Override
    public Object getItem(final int position) {
        return visibleScenes.get(position);
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    private void advancedButtonClick(final DsScene scene) {
        Intent myIntent = null;

        if (scene.get_groupNumber() == DssConstants.Groups.LIGHT) {
            myIntent = new Intent(activity, AdvancedButton.class);
            myIntent.putExtra(AdvancedButton.EXTRA_SCENE_ID, scene.get_sceneId());
            myIntent.putExtra(AdvancedButton.EXTRA_CURRENT_NAME, scene.get_name());
            myIntent.putExtra(AdvancedButton.EXTRA_ROOM_ID, roomId);
            myIntent.putExtra(AdvancedButton.EXTRA_TYPE_NAME, activity.getString(R.string.activities_title));
        } else if (scene.get_groupNumber() == DssConstants.Groups.SHADE) {
            myIntent = new Intent(activity, AdvancedButtonShade.class);
            // send roomId name to 5-way button, because scenes doesn't make sense there
            myIntent.putExtra(AdvancedButtonShade.EXTRA_CURRENT_NAME, RoomsStore.get_room_by_id(roomId).get_name());
            myIntent.putExtra(AdvancedButtonShade.EXTRA_ROOM_ID, roomId);
            myIntent.putExtra(AdvancedButtonShade.EXTRA_TYPE_NAME, activity.getString(R.string.activities_title));
        } else {
            // no advanced button for other groups , just do a normal click
            activityClick(scene);
            return;
        }

        activity.startActivityForResult(myIntent, ActivityOverview.REQEUST_CODE_ADVANCED_BUTTON);
        activity.overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
    }

    private void activityClick(final DsScene scene) {
        final DsRoom room = RoomsStore.get_room_by_id(roomId);

        // If already selected, turn off lights (call scene 0)
        if (scene.get_groupNumber() == DssConstants.Groups.LIGHT && room.get_lastCalledScene(DssConstants.Groups.LIGHT) == scene.get_sceneId()) {
            DssService.callActivity(activity, room.get_id(), DssConstants.Groups.LIGHT, DssConstants.Scenes.OFF, false);
        }
        // use force=true if scene 0 is explicitly chosen on lights
        else if (scene.get_groupNumber() == DssConstants.Groups.LIGHT && scene.get_sceneId() == DssConstants.Scenes.OFF) {
            DssService.callActivity(activity, room.get_id(), DssConstants.Groups.LIGHT, DssConstants.Scenes.OFF, true);
            Toast.makeText(activity, activity.getString(R.string.force_off), Toast.LENGTH_SHORT).show();
        }
        // if already selected (group 2) call scene STOP (15)
        else if (scene.get_groupNumber() == DssConstants.Groups.SHADE && room.get_lastCalledScene(DssConstants.Groups.SHADE) == scene.get_sceneId()) {
            DssService.callActivity(activity, room.get_id(), DssConstants.Groups.SHADE, DssConstants.Scenes.STOP, false);
        }
        // if already selected (group 3) call scene 0
        else if (scene.get_groupNumber() == DssConstants.Groups.CLIMATE && room.get_lastCalledScene(DssConstants.Groups.CLIMATE) == scene.get_sceneId()) {
            DssService.callActivity(activity, room.get_id(), DssConstants.Groups.CLIMATE, DssConstants.Scenes.OFF, false);
        }
        // else call chosen scene on the desired groupNumber
        else {
            DssService.callActivity(activity, scene.get_roomId(), scene.get_groupNumber(), scene.get_sceneId(), false);
        }

        final boolean isGlobal = Rooms.isGlobal(scene.get_roomId());
        final boolean isUAD = Scenes.USER_DEFINED_ACTIONS >= scene.get_sceneId();
        GAHelper.sendSceneCalledEvent(isGlobal ? isUAD ? "uad" : "global" : "scene");
    }
}
