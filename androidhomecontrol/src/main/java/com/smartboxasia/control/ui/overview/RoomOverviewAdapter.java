package com.smartboxasia.control.ui.overview;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smartboxasia.control.R;
import com.smartboxasia.control.dto.DsRoom;

final class RoomOverviewAdapter extends BaseAdapter {

    private Context context;
    private List<DsRoom> rooms;

    public RoomOverviewAdapter(final Context context, final List<DsRoom> rooms) {
        this.context = context;
        this.rooms = rooms;
    }

    @Override
    public int getCount() {
        return rooms.size();
    }

    @Override
    public Object getItem(final int position) {
        return rooms.get(position);
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.overview_rooms_item, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.roomTextView)).setText(rooms.get(position).get_name());
        return convertView;
    }
}