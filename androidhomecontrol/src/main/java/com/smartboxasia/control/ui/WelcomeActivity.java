package com.smartboxasia.control.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.connection.ConnectionConfigService;
import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.ui.dialog.LoginDialogActivity;
import com.smartboxasia.control.ui.helper.BackgroundGradientDrawable;
import com.smartboxasia.control.ui.settings.ConfigurationScreen;
import com.smartboxasia.control.ui.settings.ServerSettings;

public class WelcomeActivity extends Activity {

    private static final int REQUEST_CODE_LOGIN = 1;
    private static final int REQUEST_CODE_SERVER_SETTINGS = 2;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        findViewById(R.id.welcome).setBackgroundDrawable(new BackgroundGradientDrawable());

        final TextView emailTextView = (TextView) findViewById(R.id.welcome_email_input);
        final TextView passTextView = (TextView) findViewById(R.id.welcome_password_input);

        final Button loginButton = (Button) findViewById(R.id.welcome_login_button);
        loginButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                final String email = emailTextView.getText().toString();
                final String pass = passTextView.getText().toString();
                prepareCloudLogin(email, pass);
            }
        });
        final Button localButton = (Button) findViewById(R.id.welcome_local_button);
        localButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                final Intent intent = new Intent(WelcomeActivity.this, ServerSettings.class);
                startActivityForResult(intent, REQUEST_CODE_SERVER_SETTINGS);
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        GAHelper.sendScreenViewEvent("Login");
    }

    private void prepareCloudLogin(final String email, final String pass) {
        final ConnectionData data = new ConnectionData(ConnectionData.CONNECTION_TYPE_CLOUD);
        data.setUser(email);
        data.setName(email);

        final String url = ConnectionConfigService.getGlobalCloudUrl();
        data.setUrl(url); // our first calls go to the cloud
        data.setCloudUrl(url);

        final Intent intent = new Intent(this, LoginDialogActivity.class);
        intent.putExtra(LoginDialogActivity.EXTRA_DATA, data);
        intent.putExtra(LoginDialogActivity.EXTRA_PASS, pass);
        startActivityForResult(intent, REQUEST_CODE_LOGIN);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == REQUEST_CODE_LOGIN) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
                overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
            }
        } else if (requestCode == REQUEST_CODE_SERVER_SETTINGS) {
            if (resultCode == ConfigurationScreen.SERVERS_CHANGED) {
                setResult(RESULT_OK);
                finish();
                overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
            }
        }
    }

}
