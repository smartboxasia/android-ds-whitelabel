package com.smartboxasia.control.ui.helper;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.provider.Settings;
import android.view.Surface;

import com.smartboxasia.control.data.config.app.MetersCache;
import com.smartboxasia.control.ui.overview.PieChart;

final class OrientationChangeEventListener implements SensorEventListener {

    private static final int TILTING_THRESHOLD = 6;

    private final Activity activity;

    private boolean isEnabled = false;
    private boolean currentIsOverThreshold = false;

    /**
     * @param touchHandlerLayout
     */
    OrientationChangeEventListener(final Activity activity) {
        this.activity = activity;
    }

    private boolean isAutoRotateOn() {
        return Settings.System.getInt(activity.getContentResolver(),
                Settings.System.ACCELEROMETER_ROTATION, 0) == 1;
    }

    @Override
    public void onSensorChanged(final SensorEvent event) {

        // Handle the events for which we registered
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float val;
            final int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
            if (OrientationHelper.naturalOrientationIsPortrait(activity, rotation)) {
                switch (rotation) {
                    case Surface.ROTATION_0:
                        val = event.values[0]; // use x in portrait mode
                        break;
                    case Surface.ROTATION_90:
                        val = -event.values[1]; // use -x in reverse portrait mode
                        break;
                    case Surface.ROTATION_180:
                        val = -event.values[0]; // use -y in landscape mode
                        break;
                    case Surface.ROTATION_270:
                        val = event.values[1]; // use y in reverse landscape mode
                        break;
                    default:
                        val = 0;
                        break;
                }
            } else {
                switch (rotation) {
                    case Surface.ROTATION_0:
                        val = event.values[0]; // use -y in portrait mode
                        break;
                    case Surface.ROTATION_90:
                        val = -event.values[1]; // use y in reverse portrait mode
                        break;
                    case Surface.ROTATION_180:
                        val = -event.values[0]; // use -x in landscape mode
                        break;
                    case Surface.ROTATION_270:
                        val = event.values[1]; // use x in reverse landscape mode
                        break;
                    default:
                        val = 0;
                        break;
                }
            }
            final boolean newIsOverThreshold = val > (-TILTING_THRESHOLD) && val < (TILTING_THRESHOLD);
            if (!newIsOverThreshold && currentIsOverThreshold) {
                if (isAutoRotateOn() && isEnabled && !MetersCache.get_meters(false).isEmpty()) {
                    // only start the landscape visualization if e.g. the progress dialog is not shown
                    final Intent intent = new Intent(activity, PieChart.class);
                    activity.startActivity(intent);
                }
            }
            currentIsOverThreshold = newIsOverThreshold;
        }
    }

    @Override
    public void onAccuracyChanged(final Sensor sensor, final int accuracy) {
    }

    public void enable() {
        isEnabled = true;
    }

    public void disable() {
        isEnabled = false;
    }
}
