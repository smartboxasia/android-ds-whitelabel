package com.smartboxasia.control.ui.settings;

import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.smartboxasia.control.DssConstants.Groups;
import com.smartboxasia.control.R;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.ui.helper.ActivitySettingsConfigViewInitializer;
import com.smartboxasia.control.ui.helper.ActivitySettingsConfigViewInitializerShade;
import com.google.common.collect.Lists;

public final class ActivitySettingsConfigAdapter extends BaseAdapter {

    public static class Holder {
        public DsDevice device;
        public AsyncTask<Void, Void, List<Integer>> initializer;
    }

    private final Activity activity;
    private List<DsDevice> devices = Lists.newArrayList();
    private List<AsyncTask<Void, Void, List<Integer>>> initializers = Lists.newArrayList();

    public ActivitySettingsConfigAdapter(final Activity activity, final List<DsDevice> devices) {
        this.activity = activity;
        this.devices.addAll(devices);
        Collections.sort(this.devices, DsDevice.COMPARATOR_ALPHABETICAL);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final int groupNumber = devices.get(position).get_groupNumber();

        if (convertView == null) {
            convertView = createListItem(groupNumber, parent);
        } else {
            final ActivitySettingsConfigAdapter.Holder oldHolder = (ActivitySettingsConfigAdapter.Holder) convertView.getTag();
            if (oldHolder.device.get_groupNumber() != groupNumber) {
                convertView = createListItem(groupNumber, parent);
            }
        }
        attachInitializer(position, convertView);

        if (position % 2 == 0) {
            convertView.findViewById(R.id.config_item).setBackgroundResource(R.drawable.conf_list_item_grey_selector);
        }

        return convertView;
    }

    private View createListItem(final int groupNumber, final ViewGroup parent) {
        switch (groupNumber) {
            case Groups.SHADE: {
                final View view = LayoutInflater.from(activity).inflate(R.layout.settings_activities_config_item_shade, parent, false);
                return view;
            }
            default: {
                final View view = LayoutInflater.from(activity).inflate(R.layout.settings_activities_config_item, parent, false);
                return view;
            }
        }
    }

    private void attachInitializer(final int position, final View view) {
        Holder holder = (Holder) view.getTag();
        if (holder == null) {
            holder = new ActivitySettingsConfigAdapter.Holder();
            holder.device = devices.get(position);
        }
        if (initializers.size() <= position) {
            switch (holder.device.get_groupNumber()) {
                case Groups.SHADE: {
                    holder.initializer = new ActivitySettingsConfigViewInitializerShade(activity, holder.device, view);
                    holder.initializer.execute();
                    initializers.add(holder.initializer);
                    break;
                }
                default: {
                    holder.initializer = new ActivitySettingsConfigViewInitializer(activity, holder.device, view);
                    holder.initializer.execute();
                    initializers.add(holder.initializer);
                    break;
                }
            }
        } else {
            holder.initializer = initializers.get(position);
            switch (holder.device.get_groupNumber()) {
                case Groups.SHADE: {
                    final ActivitySettingsConfigViewInitializerShade activitySettingsConfigViewInitializerShade = (ActivitySettingsConfigViewInitializerShade) holder.initializer;
                    activitySettingsConfigViewInitializerShade.attachView(view);
                    break;
                }
                default: {
                    final ActivitySettingsConfigViewInitializer activitySettingsConfigViewInitializer = (ActivitySettingsConfigViewInitializer) holder.initializer;
                    activitySettingsConfigViewInitializer.attachToView(view);
                    break;
                }
            }
        }

        view.setTag(holder);
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public Object getItem(final int position) {
        return devices.get(position);
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    public List<AsyncTask<Void, Void, List<Integer>>> getInitializers() {
        return initializers;
    }
}
