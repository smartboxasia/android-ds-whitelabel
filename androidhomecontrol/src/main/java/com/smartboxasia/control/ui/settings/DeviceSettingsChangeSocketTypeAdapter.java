package com.smartboxasia.control.ui.settings;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smartboxasia.control.R;
import com.smartboxasia.control.data.helper.Mappings;
import com.smartboxasia.control.data.tagging.cloud_1_0.json.TaggingMappings;
import com.smartboxasia.control.domain.SocketType;

public class DeviceSettingsChangeSocketTypeAdapter extends BaseAdapter {

    private final Context context;
    private final SocketType currentSocketType;

    public DeviceSettingsChangeSocketTypeAdapter(final Context context, final SocketType currentSocketType) {
        this.context = context;
        this.currentSocketType = currentSocketType;
    }

    @Override
    public int getCount() {
        return SocketType.values().length + 1;
    }

    @Override
    public SocketType getItem(final int position) {
        if (position >= SocketType.values().length + 1) {
            return null;
        }
        return SocketType.values()[position];
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.settings_device_details_change_socket_type_item, parent, false);
        }
        final TextView typeNameTv = (TextView) convertView.findViewById(R.id.typeName);

        final SocketType socketType = position == 0 ? null : SocketType.values()[position - 1];
        typeNameTv.setText(Mappings.findValue(TaggingMappings.SOCKET_TYPES, socketType, ""));

        final boolean checked = socketType == currentSocketType;
        convertView.findViewById(R.id.check).setVisibility(checked ? View.VISIBLE : View.INVISIBLE);

        if (position % 2 == 0) {
            convertView.setBackgroundResource(R.drawable.conf_list_grey);
        } else {
            convertView.setBackgroundResource(R.drawable.conf_list_white);
        }

        return convertView;
    }
}
