package com.smartboxasia.control.ui.helper;

import java.text.DecimalFormat;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.domain.OutputMode;
import com.smartboxasia.control.dto.DsDevice;

public class MeteringViewUpdater extends AsyncTask<Void, Void, Integer> {

    private DecimalFormat format = new DecimalFormat("0.00");
    private Context context;
    private DsDevice device;
    private TextView view;
    private View progress;

    public MeteringViewUpdater(final Context context, final DsDevice device, final TextView view, final View progress) {
        this.context = context;
        this.device = device;
        this.view = view;
        this.progress = progress;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progress.setVisibility(View.VISIBLE);
        view.setVisibility(View.GONE);
    }

    @Override
    protected Integer doInBackground(final Void... params) {
        Integer result = -1;
        synchronized (App.REQUEST_LOCK) {
            if (device.get_outputMode() != OutputMode.DISABLED) {
                result = DssService.getDeviceMeteringValueSync(device);
            }
        }
        return result;
    }

    @Override
    protected void onPostExecute(final Integer result) {
        progress.setVisibility(View.GONE);
        view.setVisibility(View.VISIBLE);
        if (result == -1) {
            view.setText(context.getString(R.string.kilowatthours_pattern, context.getString(R.string.dash)));
        } else {
            view.setText(context.getString(R.string.kilowatthours_pattern, format.format(result / 100f)));
        }
    }
}
