package com.smartboxasia.control.ui.overview;

import java.util.Set;

import android.app.Activity;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.smartboxasia.control.R;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.dto.DsScene;

public class ActivityOverviewPagerAdapter extends OverviewPagerAdapter {

    public ActivityOverviewPagerAdapter(final Activity activity, final Set<Integer> avaiableGroups, final int roomId) {
        super(activity, avaiableGroups, roomId);
    }

    void updateItemList(final ListView theList, final int groupNumber) {

        final BaseAdapter myAdapter = new ActivityOverviewAdapter(activity, roomId, groupNumber);

        theList.setAdapter(myAdapter);

    }

    static void updateBackground(final View convertView, final DsRoom room, final DsScene scene) {
        if (room.get_lastCalledScene(scene.get_groupNumber()) == scene.get_sceneId()) {
            convertView.findViewById(R.id.configButton).setBackgroundResource(R.drawable.visual_label_left_active);
            convertView.findViewById(R.id.activityTextView).setBackgroundResource(R.drawable.visual_label_right_active);
        } else {
            convertView.findViewById(R.id.configButton).setBackgroundResource(R.drawable.visual_label_left_inactive);
            convertView.findViewById(R.id.activityTextView).setBackgroundResource(R.drawable.visual_label_right_inactive);
        }
    }

}
