package com.smartboxasia.control.ui.helper;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.data.dss.direct_1_17_0.json.JSONConstants.DeviceTypes;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.ui.settings.ActivitySettingsConfigAdapter.Holder;
import com.google.common.collect.Lists;

public class ActivitySettingsConfigViewInitializerShade extends AsyncTask<Void, Void, List<Integer>> {

    private Context context;
    private DsDevice device;
    private View convertView;
    private final boolean hasShadeAngle;
    private int posValue;
    private int angValue;
    private int shadeType;

    private boolean finished = false;

    public ActivitySettingsConfigViewInitializerShade(final Context context, final DsDevice device, final View convertView) {
        this.context = context;
        this.device = device;
        this.convertView = convertView;
        hasShadeAngle = device.get_type() == DeviceTypes.JALOUSIE;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        updateView();
    }

    @Override
    protected List<Integer> doInBackground(final Void... params) {
        final List<Integer> result = Lists.newArrayList();
        synchronized (App.REQUEST_LOCK) {
            final int deviceOuputValue = decodeDssPositionValue(DssService.getDeviceOuputValueSync(device));
            result.add(deviceOuputValue);
            if (hasShadeAngle) {
                final int shadeAngle = decodeDssAngleValue(DssService.getShadeAngleSync(device));
                result.add(shadeAngle);
            }
        }
        return result;
    }

    @Override
    protected void onPostExecute(final List<Integer> result) {

        // Check if we're still attached to the view. If not, finish quietly.
        final Holder holder = (Holder) convertView.getTag();
        if (holder.initializer != this) {
            return;
        }

        shadeType = result.size();
        if (shadeType == 1) {
            posValue = result.get(0);
        } else if (shadeType == 2) {
            posValue = result.get(0);
            angValue = result.get(1);
        }

        finished = true;

        updateView();
    }

    public void attachView(final View view) {
        convertView = view;
        updateView();
    }

    private void updateView() {

        final TextView deviceName = (TextView) convertView.findViewById(R.id.device_name);
        final ProgressBar progress = (ProgressBar) convertView.findViewById(R.id.progress);
        final TextView positionValue = (TextView) convertView.findViewById(R.id.position_value);
        final SeekBar positionSeekbar = (SeekBar) convertView.findViewById(R.id.position_seekbar);
        final TextView angleValue = (TextView) convertView.findViewById(R.id.angle_value);
        final SeekBar angleSeekbar = (SeekBar) convertView.findViewById(R.id.angle_seekbar);

        if (!finished) {

            deviceName.setVisibility(View.VISIBLE);
            deviceName.setText(device.get_name());

            positionValue.setVisibility(View.GONE);
            positionSeekbar.setVisibility(View.GONE);
            angleValue.setVisibility(View.GONE);
            angleSeekbar.setVisibility(View.GONE);

            progress.setVisibility(View.VISIBLE);
        } else {
            if (shadeType == 1) {
                positionSeekbar.setVisibility(View.VISIBLE);
                positionValue.setVisibility(View.VISIBLE);

                positionSeekbar.setProgress(posValue);
                positionValue.setText(context.getString(R.string.percent_pattern, posValue));
            } else if (shadeType == 2) {
                positionSeekbar.setVisibility(View.VISIBLE);
                positionValue.setVisibility(View.VISIBLE);
                angleSeekbar.setVisibility(View.VISIBLE);
                angleValue.setVisibility(View.VISIBLE);

                positionSeekbar.setProgress(posValue);
                positionValue.setText(context.getString(R.string.percent_pattern, posValue));

                angleSeekbar.setProgress(angValue);
                angleValue.setText(context.getString(R.string.percent_pattern, angValue));
            }

            // register change listeners after the values have been changed
            positionSeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                @Override
                public void onStopTrackingTouch(final SeekBar seekBar) {
                    posValue = encodeDssPositionValue(seekBar.getProgress());
                    DssService.setDeviceOutputValue(device, posValue);
                }

                @Override
                public void onStartTrackingTouch(final SeekBar seekBar) {

                }

                @Override
                public void onProgressChanged(final SeekBar seekBar, final int progress, final boolean fromUser) {
                    positionValue.setText(context.getString(R.string.percent_pattern, progress));
                }
            });

            // register change listeners after the values have been changed
            angleSeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                @Override
                public void onStopTrackingTouch(final SeekBar seekBar) {
                    // inversed because of visual logic
                    angValue = encodeDssAngleValue(seekBar.getProgress());
                    DssService.setShadeAngle(device, angValue);
                }

                @Override
                public void onStartTrackingTouch(final SeekBar seekBar) {

                }

                @Override
                public void onProgressChanged(final SeekBar seekBar, final int progress, final boolean fromUser) {
                    angleValue.setText(context.getString(R.string.percent_pattern, progress));
                }
            });

            progress.setVisibility(View.GONE);
        }
    }

    static int encodeDssPositionValue(final int value) {
        return Math.round(value * 655.35f); // transform it from seekbar range (0-100) into dss range (0-65535)
    }

    static int decodeDssPositionValue(final int dssValue) {
        final int value = dssValue; // the received value in dss range (0-65535)
        return Math.round(value / 655.35f); // transform it into seekbar range (0-100)
    }

    static int encodeDssAngleValue(final int value) {
        return Math.round(value * 2.55f); // transform it from seekbar range (0-100) into dss range (0-255)
    }

    static int decodeDssAngleValue(final int dssValue) {
        final int value = dssValue; // the received value in dss range (0-255)
        return Math.round(value / 2.55f); // transform it into seekbar range (0-100)
    }
}
