package com.smartboxasia.control.ui.dialog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.connection.ConnectionConfigService;
import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.events.LoginResultEvent;
import com.smartboxasia.control.events.ShowLoginErrorDialogEvent;
import com.smartboxasia.control.ui.helper.DialogHelper;
import com.google.common.base.Strings;
import com.google.common.eventbus.Subscribe;

public class LoginDialogActivity extends Activity {

    private static final String TAG = LoginDialogActivity.class.getSimpleName();

    private static final String EXTRA_IS_LOGGING_IN = "isLoggingIn";

    public static final String EXTRA_DATA = "data";
    public static final String EXTRA_PASS = "pass";
    public static final String EXTRA_USER = "user";

    private boolean isLoggingIn;
    private String user;
    private String pass;
    private ConnectionData data;

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;

    @Subscribe
    public void onEvent(final ShowLoginErrorDialogEvent event) {
        DialogHelper.showInformationDialog(this, event.getTitle(), event.getMessage(), event.getShowButton());
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getAttributes().windowAnimations = R.style.Animation_Translucent_Fade;

        setTitle(R.string.logging_in);
        setContentView(R.layout.activity_login);

        App.eventBus.register(this);

        if (savedInstanceState != null) {

            // dialog is being restored after configuration change

            user = savedInstanceState.getString(EXTRA_USER);
            pass = savedInstanceState.getString(EXTRA_PASS);
            data = (ConnectionData) savedInstanceState.getSerializable(EXTRA_DATA);
            isLoggingIn = savedInstanceState.getBoolean(EXTRA_IS_LOGGING_IN);

            initViews();

            if (isLoggingIn) {
                showProgress();
            } else {
                showCredentialsForm();
            }
        } else {

            // dialog has been started

            final Intent intent = getIntent();
            pass = intent.getStringExtra(EXTRA_PASS);
            data = (ConnectionData) intent.getSerializableExtra(EXTRA_DATA);
            user = data.getUser();

            initViews();

            // only log in if credentials got passed in or if we already have a valid app token
            if ((!Strings.isNullOrEmpty(user) && !Strings.isNullOrEmpty(pass)) || data.hasValidAppToken()) {
                doLogin();
            } else {
                showCredentialsForm();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        GAHelper.sendScreenViewEvent("Credentials Dialog");
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        App.credentialsDialogShown = false;
        App.eventBus.unregister(this);
    }

    private void initViews() {
        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);

        mPasswordView.setHint(R.string.prompt_password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView textView, final int id, final KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    doLogin();
                    return true;
                }
                return false;
            }
        });

        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View view) {
                user = mEmailView.getText().toString();
                pass = mPasswordView.getText().toString();
                doLogin();
            }
        });
    }

    private void doLogin() {
        App.hideIME(mEmailView, mPasswordView);
        isLoggingIn = true;
        showProgress();
        ConnectionConfigService.deleteLastUsedConnectionData();
        Connection.reconnectConnectionService(data, null, user, pass, true);
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(EXTRA_USER, user);
        outState.putString(EXTRA_PASS, pass);
        outState.putSerializable(EXTRA_DATA, data);
        outState.putBoolean(EXTRA_IS_LOGGING_IN, isLoggingIn);
    }

    private void showProgress() {
        findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        findViewById(R.id.login_form).setVisibility(View.INVISIBLE);
    }

    private void showCredentialsForm() {
        isLoggingIn = false;

        if (data.getType() == ConnectionData.CONNECTION_TYPE_CLOUD || data.getType() == ConnectionData.CONNECTION_TYPE_CLOUD_ONLY) {
            mEmailView.setHint(R.string.prompt_email);
        } else {
            mEmailView.setHint(R.string.prompt_user);
        }

        // for direct login we might not have a username to prepopulate. Use the default
        if (data.getType() != ConnectionData.CONNECTION_TYPE_CLOUD && data.getType() != ConnectionData.CONNECTION_TYPE_CLOUD_ONLY && Strings.isNullOrEmpty(user)) {
            user = getString(R.string.ds_default_user);
        }
        mEmailView.setText(user);

        if (!Strings.isNullOrEmpty(user)) {
            mPasswordView.requestFocus();
        }

        findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);
        findViewById(R.id.login_form).setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onEvent(final LoginResultEvent event) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                switch (event.getResult()) {
                    case LOGIN_RESULT_OK: {
                        Log.d(TAG, "log in succeeded, closing dialog");
                        setResult(Activity.RESULT_OK, getIntent());
                        finish();
                        break;
                    }
                    case LOGIN_RESULT_NOT_OK: {
                        Log.d(TAG, "could not log in, reopening credentials");
                        showCredentialsForm();
                        break;
                    }
                    case LOGIN_RESULT_NEED_CREDENTIALS: {
                        Log.d(TAG, "credentials needed");
                        showCredentialsForm();
                        break;
                    }
                    default: {
                        Log.d(TAG, "unknown result returned from login");
                        setResult(Activity.RESULT_CANCELED);
                        finish();
                        break;
                    }
                }
            }
        });
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        return true; // no touch outside to dismiss
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0, 0);
    }
}
