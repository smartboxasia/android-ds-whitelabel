package com.smartboxasia.control.ui.settings;

import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.events.RoomRenamedEvent;
import com.smartboxasia.control.events.RoomsChangedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.google.common.eventbus.Subscribe;
import com.mobeta.android.dslv.DragSortListView;
import com.mobeta.android.dslv.SimpleDragSortCursorAdapter;

public class RoomSettings extends ConnectivityMonitoringFragmentActivity {

    private static final String COLUMN_NAME = "RoomName";
    private boolean changedName = false;
    private RoomListCursorAdapter adapter;
    private DragSortListView listView;

    private final class RoomListCursorAdapter extends SimpleDragSortCursorAdapter {
        private boolean changedSequence = false;

        private RoomListCursorAdapter(final Context context, final int layout, final Cursor c, final String[] from, final int[] to, final int flags) {
            super(context, layout, c, from, to, flags);
        }

        @Override
        public void drop(final int from, final int to) {
            super.drop(from, to);
            Log.i("drop", "from:" + from + " to:" + to);

            synchronized (RoomsStore.class) {

                final List<DsRoom> sortedRooms = RoomsStore.get_roomsSorted();

                final DsRoom room = sortedRooms.remove(from);
                sortedRooms.add(to, room);

                RoomsStore.set_roomsSorted(sortedRooms);
            }
            changedSequence = true;
        }

        public boolean isChangedSequence() {
            return changedSequence;
        }
    }

    @Subscribe
    public void onEvent(final RoomRenamedEvent event) {
        if (event.isSuccess()) {
            Toast.makeText(RoomSettings.this, getString(R.string.rename_success), Toast.LENGTH_SHORT).show();
            DssService.updateRooms(getApplicationContext());
        } else {
            Toast.makeText(RoomSettings.this, getString(R.string.rename_fail), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onEvent(final RoomsChangedEvent event) {
        // if all rooms are received, update list
        updateListViewAdapter();
        DssService.updateActivties(getApplicationContext());
    }

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_rooms);

        ((TextView) findViewById(R.id.titleTextView)).setText(R.string.settings_title_rooms);

        listView = (DragSortListView) findViewById(android.R.id.list);

        updateListViewAdapter();

        registerForContextMenu(listView);
    }

    private void updateListViewAdapter() {
        final MatrixCursor cursor = createMatrixCursor();
        adapter = new RoomListCursorAdapter(this, R.layout.settings_rooms_item, cursor, new String[] {
                COLUMN_NAME
        }, new int[] {
                R.id.roomSettingsTextView
        }, 0);
        listView.setAdapter(adapter);
    }

    private MatrixCursor createMatrixCursor() {
        final MatrixCursor cursor = new MatrixCursor(new String[] {
                BaseColumns._ID, COLUMN_NAME
        });
        // Add items to the roomlist.
        int index = 0;
        for (final DsRoom room : RoomsStore.get_roomsSorted()) {
            cursor.addRow(new Object[] {
                    index++, room.get_name()
            });
        }
        return cursor;
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        final AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
        final String currentName = (String) ((TextView) info.targetView.findViewById(R.id.roomSettingsTextView)).getText();
        editName(currentName, info.position);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Unregister for events
        App.eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register for events
        App.eventBus.register(this);

        // update rooms
        DssService.updateRooms(this);

        GAHelper.sendScreenViewEvent("Settings Rooms");
    }

    @Override
    public void onBackPressed() {
        if (changedName || adapter.isChangedSequence()) {
            setResult(ConfigurationScreen.ROOMS_CHANGED);
        }
        finish();

    }

    // callback defined in xml
    public void goBackClick(final View V) {
        onBackPressed();
    }

    // callback defined in xml
    public void editNameButtonClick(final View V) {

        final TextView currentName = (TextView) (((View) V.getParent()).findViewById(R.id.roomSettingsTextView));
        final int position = listView.getPositionForView((View) V.getParent());
        editName((String) currentName.getText(), position);
    }

    private void editName(final String currentName, final int position) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle(R.string.edit_name);

        final LinearLayout dialogLayout = new LinearLayout(this);

        // Set an EditText view to get username input
        final EditText inputName = new EditText(this);
        inputName.setText(currentName);
        inputName.setSelection(inputName.getText().length());

        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        dialogLayout.addView(inputName);

        alert.setView(dialogLayout);

        alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int whichButton) {

                App.hideIME(inputName);

                final String newRoomName = inputName.getText().toString().trim();

                // get room id
                final int roomId = RoomsStore.get_roomsSorted().get(position).get_id();

                // change name
                Log.i("roomSettings", "changing roomId:" + roomId + " to name:" + newRoomName);
                DssService.changeRoomName(RoomSettings.this, roomId, newRoomName);
                changedName = true;
            }
        });

        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int whichButton) {
                // do nothing
            }
        });

        alert.show();

    }
}
