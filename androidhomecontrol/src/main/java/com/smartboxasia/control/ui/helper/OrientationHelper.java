package com.smartboxasia.control.ui.helper;

import static android.content.res.Configuration.SCREENLAYOUT_SIZE_MASK;
import static android.content.res.Configuration.SCREENLAYOUT_SIZE_NORMAL;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.util.DisplayMetrics;
import android.view.Surface;

class OrientationHelper {

    /**
     * Sets the passed in activity to portrait mode only if we are on a device with a screen smaller than large
     */
    public static void setOrientationMode(final Activity activity) {
        final int screenLayout = activity.getResources().getConfiguration().screenLayout & SCREENLAYOUT_SIZE_MASK;
        if (screenLayout <= SCREENLAYOUT_SIZE_NORMAL) {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    public static boolean naturalOrientationIsPortrait(final Activity activity, final int rotation) {
        final DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        final int screenWidth = dm.widthPixels;
        final int screenHeight = dm.heightPixels;

        return ((rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) && screenHeight > screenWidth) || ((rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) && screenWidth > screenHeight);
    }

    public static int getCurrentScreenOrientation(final Activity activity) {
        int result = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        final int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();

        if (naturalOrientationIsPortrait(activity, rotation)) {
            switch (rotation) {
                case Surface.ROTATION_0:
                    result = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_90:
                    result = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_180:
                    result = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                case Surface.ROTATION_270:
                    result = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                default:
                    result = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
            }
        } else {
            switch (rotation) {
                case Surface.ROTATION_0:
                    result = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_90:
                    result = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                case Surface.ROTATION_180:
                    result = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                case Surface.ROTATION_270:
                    result = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                default:
                    result = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
            }
        }

        return result;
    }
}
