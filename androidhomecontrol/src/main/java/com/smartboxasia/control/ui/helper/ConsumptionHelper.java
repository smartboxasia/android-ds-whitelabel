package com.smartboxasia.control.ui.helper;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.ConsumptionCache;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.events.ConsumptionChangedEvent;
import com.google.common.eventbus.Subscribe;

public class ConsumptionHelper {

    private static final int POWER_PER_DEVICE = 50; // TODO This is copied from the iOS version. Is there no better way?

    private final Activity activity;
    private final int consumptionTextViewId;
    private final int consumptionViewId;

    private volatile boolean running;

    private static final Handler handler = new Handler(Looper.getMainLooper()) {

        @Override
        public void handleMessage(final Message msg) {
            DssService.updateConsumption();
        };
    };

    public ConsumptionHelper(final Activity activity, final int consumptionTextViewId, final int consumptionViewId) {
        this.activity = activity;
        this.consumptionTextViewId = consumptionTextViewId;
        this.consumptionViewId = consumptionViewId;

        App.eventBus.register(this);
    }

    public void update() {
        update(ConsumptionCache.get_consumption());
    }

    public void update(final int value) {
        String valueString = "" + value;
        if (value < 0) {
            valueString = activity.getString(R.string.double_dash);
        }
        final String consumptionText = activity.getString(R.string.watts_pattern, valueString);
        ((TextView) activity.findViewById(consumptionTextViewId)).setText(consumptionText);

        int numOfDevices = 0;
        for (final DsRoom room : RoomsStore.get_roomsSorted()) {
            numOfDevices += room.get_devices().size();
        }

        if (numOfDevices == 0) {
            ((ConsumptionView) activity.findViewById(consumptionViewId)).setCurrentConsumption(-1);
        } else {
            final int maxValue = numOfDevices * POWER_PER_DEVICE;
            final float percentOfMax = value * 1f / maxValue;
            final float valueInPercent = Math.min(percentOfMax, 1);
            ((ConsumptionView) activity.findViewById(consumptionViewId)).setCurrentConsumption(valueInPercent);
        }
    }

    @Subscribe
    public void onEvent(final ConsumptionChangedEvent event) {
        update();
        if (running) {
            handler.removeMessages(0);
            handler.sendEmptyMessageDelayed(0, App.METER_POLL_DELAY);
        }
    }

    public synchronized void onPause() {
        running = false;
        handler.removeMessages(0);
    }

    public synchronized void onResume() {
        running = true;
        handler.removeMessages(0);
        handler.sendEmptyMessage(0);
    }
}
