package com.smartboxasia.control.ui.settings;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.MetersCache;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsMeter;
import com.smartboxasia.control.events.MeterChangedEvent;
import com.smartboxasia.control.events.SpecificMeterChangedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;

public class MetersSettings extends ConnectivityMonitoringFragmentActivity {

    private static final int SHOW_METER_DETAILS = 1;

    private final List<String> content = Lists.newArrayList();
    private BaseAdapter adapter;
    private boolean changed = false;

    private ListView listView;

    @Subscribe
    public void onEvent(final MeterChangedEvent event) {
        updateList();
    }

    @Subscribe
    public void onEvent(final SpecificMeterChangedEvent event) {
        updateList();
    }

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_list);

        ((TextView) findViewById(R.id.titleTextView)).setText(R.string.settings_title_meters);

        adapter = new BaseAdapter() {
            @Override
            public View getView(final int position, View convertView, final ViewGroup parent) {
                if (convertView == null) {
                    convertView = getLayoutInflater().inflate(R.layout.settings_list_item, parent, false);
                }
                synchronized (content) {
                    final String meterName = content.get(position);
                    ((TextView) convertView.findViewById(R.id.settingsTextView)).setText(Strings.nullToEmpty(meterName));
                }
                if (position % 2 == 1) {
                    ((LinearLayout) convertView.findViewById(R.id.settingsItem)).setBackgroundResource(R.drawable.conf_list_white);
                }

                return convertView;
            }

            @Override
            public int getCount() {
                synchronized (content) {
                    return content.size();
                }
            }

            @Override
            public Object getItem(final int position) {
                synchronized (content) {
                    return content.get(position);
                }
            }

            @Override
            public long getItemId(final int position) {
                return position;
            }
        };

        listView = (ListView) findViewById(android.R.id.list);
        listView.setAdapter(adapter);

        updateList();
    }

    @Override
    public void onPause() {
        super.onPause();

        // Unregister from events
        App.eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register for events
        App.eventBus.register(this);

        // update meters
        DssService.updateMeters(this);

        GAHelper.sendScreenViewEvent("Settings Meters");
    }

    public void updateList() {
        synchronized (content) {
            // clear and update content
            content.clear();

            // add room names and their activities
            for (final DsMeter meter : MetersCache.get_meters(true)) {
                content.add(meter.get_name());
            }
            adapter.notifyDataSetChanged();
        }
    }

    // callback defined in xml
    public void goBackClick(final View V) {
        if (changed) {
            setResult(ConfigurationScreen.METERS_CHANGED);
        }
        finish();
    }

    // callback defined in xml
    public void selectButtonClick(final View V) {
        final int position = listView.getPositionForView(V);
        final Intent myIntent = new Intent(this, MetersSettingsDetails.class);
        myIntent.putExtra(MetersSettingsDetails.EXTRA_METER_NUMBER, position);
        this.startActivityForResult(myIntent, SHOW_METER_DETAILS);
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == SHOW_METER_DETAILS) {
            if (resultCode == RESULT_OK) {
                changed = true;
            }
        }
    }
}
