package com.smartboxasia.control.ui.settings;

import static com.smartboxasia.control.ui.settings.ConfigurationScreen.EXTRA_SHOW_ANIMATION;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartboxasia.control.R;
import com.smartboxasia.control.data.connection.Connection;

public class ConfigurationScreenGridAdapter extends BaseAdapter {

    private static class SettingCategory {
        private int titleResId;
        private int iconResId;
        private int requestCode;
        private Class<? extends Activity> activityClass;

        public SettingCategory(final int titleResId, final int iconResId, final int requestCode, final Class<? extends Activity> activityClass) {
            this.titleResId = titleResId;
            this.iconResId = iconResId;
            this.requestCode = requestCode;
            this.activityClass = activityClass;
        }
    }

    private static final SettingCategory[] CATEGORIES_UNCONNECTED = new SettingCategory[] {
            new SettingCategory(R.string.settings_title_servers, R.drawable.config_option1, ConfigurationScreen.REQUEST_CODE_SERVERS, ServerSettings.class),
            new SettingCategory(R.string.settings_title_info, R.drawable.config_option7, ConfigurationScreen.REQUEST_CODE_INFO, InfoSettings.class),
    };

    private static final SettingCategory[] CATEGORIES_CONNECTED = new SettingCategory[] {
            new SettingCategory(R.string.settings_title_servers, R.drawable.config_option1, ConfigurationScreen.REQUEST_CODE_SERVERS, ServerSettings.class),
            new SettingCategory(R.string.settings_title_rooms, R.drawable.config_option2, ConfigurationScreen.REQUEST_CODE_ROOMS, RoomSettings.class),
            new SettingCategory(R.string.settings_title_activities, R.drawable.config_option3, ConfigurationScreen.REQUEST_CODE_ACTIVITIES, ActivitySettingsRooms.class),
            new SettingCategory(R.string.settings_title_devices, R.drawable.config_option4, ConfigurationScreen.REQUEST_CODE_DEVICES, DeviceSettings.class),
            new SettingCategory(R.string.settings_title_meters, R.drawable.config_option5, ConfigurationScreen.REQUEST_CODE_METERS, MetersSettings.class),
            new SettingCategory(R.string.settings_title_favorites, R.drawable.config_option6, ConfigurationScreen.REQUEST_CODE_FAVORITES, FavoriteSettings.class),
            new SettingCategory(R.string.settings_title_info, R.drawable.config_option7, ConfigurationScreen.REQUEST_CODE_INFO, InfoSettings.class),
    };

    private SettingCategory[] settings;

    private ConfigurationScreen activity;

    public ConfigurationScreenGridAdapter(final ConfigurationScreen activity) {
        this.activity = activity;
        this.settings = CATEGORIES_UNCONNECTED;
    }

    @Override
    public int getCount() {
        return settings.length;
    }

    @Override
    public Object getItem(final int position) {
        return settings[position];
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.screen_settings_item, parent, false);
        }
        final SettingCategory category = settings[position];
        final TextView title = (TextView) convertView.findViewById(R.id.setting_category_title);
        final ImageView icon = (ImageView) convertView.findViewById(R.id.setting_category_icon);

        title.setText(category.titleResId);
        icon.setImageResource(category.iconResId);

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                final Intent intent = new Intent(activity, category.activityClass);
                intent.putExtra(EXTRA_SHOW_ANIMATION, false);
                activity.startActivityForResult(intent, category.requestCode);
            }
        });
        return convertView;
    }

    public void update() {
        if (Connection.isLoggedIn) {
            settings = CATEGORIES_CONNECTED;
        } else {
            settings = CATEGORIES_UNCONNECTED;
        }
        notifyDataSetChanged();
    }
}
