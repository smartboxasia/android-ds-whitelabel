package com.smartboxasia.control.ui.settings;

import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.dto.DsScene;
import com.google.common.collect.Lists;

public class ActivitySettingsGlobalActions extends ActivitySettingsActivitiesBase {

    List<DsScene> activities = Lists.newArrayList();
    private ListView listView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_list);

        ((TextView) findViewById(R.id.titleTextView)).setText(R.string.global_activities);

        listView = (ListView) findViewById(android.R.id.list);
        listView.setAdapter(getAdapter());
    }

    @Override
    protected void onResume() {
        super.onResume();

        GAHelper.sendScreenViewEvent("Settings Activities Select Global");
    }

    @Override
    public void updateList() {
        // clear and update content
        activities.clear();

        // add global activities
        activities.add(new DsScene(getString(R.string.leave_home), DssConstants.Scenes.ABSENT, DssConstants.Groups.ACTION, DssConstants.Rooms.GLOBAL));
        activities.add(new DsScene(getString(R.string.come_home), DssConstants.Scenes.PRESENT, DssConstants.Groups.ACTION, DssConstants.Rooms.GLOBAL));
        activities.add(new DsScene(getString(R.string.door_bell), DssConstants.Scenes.BELL_SINGAL, DssConstants.Groups.ACTION, DssConstants.Rooms.GLOBAL));
        activities.add(new DsScene(getString(R.string.panic), DssConstants.Scenes.PANIC, DssConstants.Groups.ACTION, DssConstants.Rooms.GLOBAL));

        getAdapter().notifyDataSetChanged();
    }

    // directly called from button as defined in xml
    public void selectButtonClick(final View view) {
        final int position = listView.getPositionForView(view);
        final DsScene theScene = activities.get(position);

        sceneSelected(theScene);
    }

    @Override
    protected BaseAdapter creatAdapter() {
        return new BaseAdapter() {

            @Override
            public int getCount() {
                return activities.size();
            }

            @Override
            public Object getItem(final int position) {
                return activities.get(position);
            }

            @Override
            public long getItemId(final int position) {
                return position;
            }

            @Override
            public View getView(final int position, View convertView, final ViewGroup parent) {
                if (convertView == null) {
                    convertView = LayoutInflater.from(ActivitySettingsGlobalActions.this).inflate(R.layout.settings_list_item, parent, false);
                }
                final DsScene scene = activities.get(position);
                ((TextView) convertView.findViewById(R.id.settingsTextView)).setText(scene.get_name());

                if (apiCallingActionIdentifier != null || isLookingForFavorite) {
                    convertView.findViewById(R.id.selectButton).setVisibility(View.INVISIBLE);
                }
                return convertView;
            }
        };
    }
}
