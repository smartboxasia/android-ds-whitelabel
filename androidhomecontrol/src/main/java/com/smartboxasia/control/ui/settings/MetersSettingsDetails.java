package com.smartboxasia.control.ui.settings;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.MetersCache;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.domain.Capability;
import com.smartboxasia.control.dto.DsMeter;
import com.smartboxasia.control.events.MeterChangedEvent;
import com.smartboxasia.control.events.MeterRenamedEvent;
import com.smartboxasia.control.events.SpecificMeterChangedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.google.common.eventbus.Subscribe;

public class MetersSettingsDetails extends ConnectivityMonitoringFragmentActivity {

    private static final String TAG = MetersSettingsDetails.class.getSimpleName();

    public static final String EXTRA_METER_NUMBER = "com.smartboxasia.sia.meterNumber";

    private DsMeter meter;

    private int meterNumber;

    private boolean hasChanged = false;

    private boolean running;
    private static final Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(final Message msg) {
            final DsMeter meter = (DsMeter) msg.obj;
            DssService.updateMeterConsumption(App.getInstance(), meter);
        };
    };

    @Subscribe
    public void onEvent(final SpecificMeterChangedEvent event) {
        if (event.isSuccess()) {
            if (meter.get_dsuid().equals(event.getMeterId()) || meter.get_dsid().equals(event.getMeterId())) {
                updateConsumptionText(event.getSpecificMeterConsumption());
            }
        }
        if (running && meter.hasCapability(Capability.METERING)) {
            pollConsumption();
        }
    }

    private void pollConsumption() {
        final Message msg = handler.obtainMessage(0);
        msg.obj = meter;
        handler.sendMessageDelayed(msg, App.METER_POLL_DELAY);
    }

    @Subscribe
    public void onEvent(final MeterRenamedEvent event) {
        if (event.isSuccess()) {
            hasChanged = true;

            final String newMeterName = event.getNewMeterName();
            updateMeterName(newMeterName);
            MetersCache.get_meters(true).get(meterNumber).set_name(newMeterName);

            Toast.makeText(getApplicationContext(), getString(R.string.rename_success), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.rename_fail), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onEvent(final MeterChangedEvent event) {
        updateMeterName(meter.get_name());
    }

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_meters_details);

        if (!getIntent().hasExtra(EXTRA_METER_NUMBER)) {
            Log.e(TAG, "Meter number as intent extra necessary for this activity");
            finish();
        }

        meterNumber = getIntent().getExtras().getInt(EXTRA_METER_NUMBER);

        meter = MetersCache.get_meters(true).get(meterNumber);

        updateMeterName(meter.get_name());

        updateMeterValues();
    }

    private void updateMeterName(final String newMeterName) {

        final TextView titleTv = (TextView) findViewById(R.id.titleTextView);
        titleTv.setText("5. " + newMeterName);

        final TextView meterTv = (TextView) findViewById(R.id.meterName);
        meterTv.setText(newMeterName);
    }

    private void updateMeterValues() {
        final TextView idTv = (TextView) findViewById(R.id.meterId);
        idTv.setText(meter.get_displayId());

        final TextView meterReadingTv = (TextView) findViewById(R.id.meterReading);
        final String value = String.valueOf(meter.get_meterReading());
        meterReadingTv.setText(getString(R.string.watthours_pattern, value));

        updateConsumptionText(-1); // initialize with no valid value
    }

    @Override
    public void onPause() {
        super.onPause();

        running = false;
        handler.removeMessages(0);

        // Unregister from events
        App.eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register for events
        App.eventBus.register(this);

        if (meter.hasCapability(Capability.METERING)) {
            running = true;
            handler.removeMessages(0);
            final Message msg = handler.obtainMessage(0);
            msg.obj = meter;
            handler.sendMessage(msg);
        }

        GAHelper.sendScreenViewEvent("Settings Meters Details");
    }

    // callback defined in xml
    public void goBackClick(final View V) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (hasChanged) {
            setResult(RESULT_OK);
        }
        finish();
    }

    private void updateConsumptionText(final int consumption) {
        final TextView consumptionTv = (TextView) findViewById(R.id.consumption);
        final String value;
        if (consumption < 0) {
            value = getString(R.string.dash);
        } else {
            value = String.valueOf(consumption);
        }
        consumptionTv.setText(getString(R.string.watts_pattern, value));
    }

    // callback defined in xml
    public void changeMeterNameClick(final View V) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.edit_name);

        final LinearLayout dialogLayout = new LinearLayout(this);

        // Set an EditText view to get username input
        final EditText inputName = new EditText(this);
        inputName.setText(meter.get_name());
        inputName.setSelection(inputName.getText().length());

        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        dialogLayout.addView(inputName);

        alert.setView(dialogLayout);

        alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int whichButton) {

                App.hideIME(inputName);

                final String tempNewMeterName = inputName.getText().toString().trim();

                // change name
                Log.i("specificMeter", "changing meter: " + meter.get_name() + " to name:" + tempNewMeterName);
                DssService.changeMeterName(getApplicationContext(), meter, tempNewMeterName);
            }
        });

        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int whichButton) {
                // do nothing
            }
        });
        alert.show();
    }
}
