package com.smartboxasia.control.ui.helper;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;

import com.smartboxasia.control.App;
import com.smartboxasia.control.R;

public class BackgroundGradientDrawable extends Drawable {

    private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final int centerColor;
    private final int outerColor;

    public BackgroundGradientDrawable() {
        centerColor = App.getInstance().getResources().getColor(R.color.gradient_start);
        outerColor = App.getInstance().getResources().getColor(R.color.gradient_end);
    }

    @Override
    protected void onBoundsChange(final Rect bounds) {
        super.onBoundsChange(bounds);

        final int width = bounds.width();
        final int height = bounds.height();

        final Shader gradient = new RadialGradient(width * 0.2f, height * 1.0f, height * 0.7f, centerColor, outerColor, TileMode.CLAMP);
        paint.setShader(gradient);
    }

    @Override
    public void draw(final Canvas canvas) {
        canvas.drawRect(getBounds(), paint);
    }

    @Override
    public void setAlpha(final int alpha) {

    }

    @Override
    public void setColorFilter(final ColorFilter cf) {

    }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }

}
