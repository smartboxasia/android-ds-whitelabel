package com.smartboxasia.control.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.data.dss.direct_1_17_0.json.DssMappings;
import com.smartboxasia.control.domain.OutputMode;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.events.OutputModeChangedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.google.common.base.Strings;
import com.google.common.eventbus.Subscribe;

public class DeviceSettingsChangeOutputMode extends ConnectivityMonitoringFragmentActivity {

    private static final String TAG = DeviceSettingsChangeOutputMode.class.getSimpleName();

    public static final String EXTRA_DEVICE_ID = "com.smartboxasia.sia.deviceId";
    public static final String EXTRA_ROOM_NUMBER = "com.smartboxasia.sia.roomNumber";

    public static final String RESULT_EXTRA_OUTPUT_MODE = "com.smartboxasia.sia.outputMode";

    private DsDevice device;

    @Subscribe
    public void onEvent(final OutputModeChangedEvent event) {
        if (event.isSuccess()) {
            final Intent data = new Intent();
            data.putExtra(RESULT_EXTRA_OUTPUT_MODE, event.getNewOutputMode());
            setResult(RESULT_OK, data);
            Toast.makeText(getApplicationContext(), getString(R.string.settings_device_details_change_output_mode_success), Toast.LENGTH_SHORT).show();
        } else {
            setResult(RESULT_CANCELED);
            Toast.makeText(getApplicationContext(), getString(R.string.settings_device_details_change_output_mode_fail), Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_device_details_change_output_mode);

        ((TextView) findViewById(R.id.titleTextView)).setText(R.string.settings_device_change_output_mode_title);

        String deviceId = "";
        if (getIntent().hasExtra(EXTRA_DEVICE_ID)) {
            deviceId = getIntent().getStringExtra(EXTRA_DEVICE_ID);
        }
        int roomId = -1;
        if (getIntent().hasExtra(EXTRA_ROOM_NUMBER)) {
            roomId = getIntent().getIntExtra(EXTRA_ROOM_NUMBER, -1);
        }

        if (roomId < 0 || Strings.isNullOrEmpty(deviceId)) {
            Log.e(TAG, "Unknown room or device defined");
            // TODO let the user know about this?
            finish();
        }
        final DsRoom room = RoomsStore.get_room_by_id(roomId);
        device = room.get_device_by_id(deviceId);

        final ViewGroup list = (ViewGroup) findViewById(android.R.id.list);
        for (final OutputMode mode : device.getAvailableOutputModes()) {
            createView(list, mode);
        }
        updateCheckedMode(list, device.get_outputMode());

        updateEntryBackgrounds();
    }

    private void updateEntryBackgrounds() {
        boolean toggle = false;
        final ViewGroup viewGroup = (ViewGroup) findViewById(android.R.id.list);
        // skip spacer, start at second child
        for (int i = 1; i < viewGroup.getChildCount(); i++) {
            final View entry = viewGroup.getChildAt(i);
            if (entry != null && entry.getVisibility() == View.VISIBLE) {
                if (toggle) {
                    entry.setBackgroundResource(R.drawable.conf_list_white);
                } else {
                    entry.setBackgroundResource(R.drawable.conf_list_grey);
                }
                toggle = !toggle;
            }
        }
    }

    private void createView(final ViewGroup parent, final OutputMode mode) {
        final View view = LayoutInflater.from(this).inflate(R.layout.settings_device_details_change_output_mode_item, parent, false);
        final Integer outputModeNameResId = DssMappings.OUTPUT_MODES_TO_NAME_RES.get(mode);
        ((TextView) view.findViewById(R.id.output_mode_name)).setText(outputModeNameResId);
        view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                view.findViewById(R.id.output_mode_check).setVisibility(View.GONE);
                view.findViewById(R.id.output_mode_progress).setVisibility(View.VISIBLE);
                DssService.setDeviceOutputMode(device, mode);
            }
        });
        view.setTag(mode);
        parent.addView(view);
    }

    private void updateCheckedMode(final ViewGroup list, final OutputMode mode) {
        for (int i = 1; i < list.getChildCount(); i++) {
            final View view = list.getChildAt(i);
            final OutputMode tagMode = (OutputMode) view.getTag();
            view.findViewById(R.id.output_mode_check).setVisibility(tagMode.equals(mode) ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // Unregister for events
        App.eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register for events
        App.eventBus.register(this);

        GAHelper.sendScreenViewEvent("Settings Devices Output Mode");
    }

    public void goBackClick(final View view) {
        onBackPressed();
    }
}
