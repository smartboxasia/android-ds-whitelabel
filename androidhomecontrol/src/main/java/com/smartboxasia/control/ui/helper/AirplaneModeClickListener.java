package com.smartboxasia.control.ui.helper;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.provider.Settings;

public class AirplaneModeClickListener implements OnClickListener {

    private Activity activity;

    public AirplaneModeClickListener(final Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(final DialogInterface dialog, final int which) {
        if (ConnectionChecker.isIntentAvailable(activity, android.provider.Settings.ACTION_AIRPLANE_MODE_SETTINGS)) {
            activity.startActivity(new Intent(android.provider.Settings.ACTION_AIRPLANE_MODE_SETTINGS));
        } else {
            activity.startActivity(new Intent(Settings.ACTION_SETTINGS));
        }
        dialog.dismiss();
    }
}
