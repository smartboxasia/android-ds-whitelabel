package com.smartboxasia.control.ui.overview;

import java.util.List;
import java.util.Set;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.events.DeviceStateChangedEvent;
import com.smartboxasia.control.events.RoomsChangedEvent;
import com.smartboxasia.control.events.SceneActivatedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.smartboxasia.control.ui.helper.ConsumptionHelper;
import com.smartboxasia.control.ui.helper.TiltDetector;
import com.smartboxasia.control.ui.settings.ConfigurationScreen;
import com.google.common.collect.Sets;
import com.google.common.eventbus.Subscribe;
import com.viewpagerindicator.IconPageIndicator;

public class DeviceOverview extends ConnectivityMonitoringFragmentActivity {

    public static final String EXTRA_GROUP_NUMBER = "com.smartboxasia.sia.groupNumber";
    public static final String EXTRA_ROOM_ID = "com.smartboxasia.sia.roomId";

    private static final int REQEUST_CODE_BASE_DEVICES = 200;

    /* pk */static final int REQEUST_CODE_ADVANCE_BUTTON = App.REQUEST_CODE_BASE_HOMESCREEN + REQEUST_CODE_BASE_DEVICES;
    private static final int REQUEST_CODE_CONFIGURATION_SCREEN = REQEUST_CODE_ADVANCE_BUTTON + 1;

    // private ListView deviceList;
    private int roomId;

    private TiltDetector tiltDetector = new TiltDetector();

    private ViewPager viewPager;
    private DeviceOverviewPagerAdapter adapter;
    private IconPageIndicator indicator;
    private int currentPage = 0;

    private FavoritesBarFragment favoritesBarFragment;

    private final ConsumptionHelper consumption = new ConsumptionHelper(this, R.id.ConsumptionTextView, R.id.ConsumptionColor);
    private TextView roomNameTv;

    @Subscribe
    public void onEvent(final SceneActivatedEvent event) {
        // if scene was activated, update UI
        favoritesBarFragment.updateFavoriteList();
        markActiveDevices();
    }

    @Subscribe
    public void onEvent(final DeviceStateChangedEvent event) {
        // If a device state changed update the UI
        markActiveDevices();
    }

    @Subscribe
    public void onEvent(final RoomsChangedEvent event) {
        // if all rooms are received, notify room list and update the activities list
        markActiveDevices();
        DssService.updateActivties(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();
        // enable orientation listener
        tiltDetector.onResume();

        // update favorites
        favoritesBarFragment.updateFavoriteList();

        // Register for events
        App.eventBus.register(this);

        // resume polling the consumption (this also gets a new session token, in case it timed out)
        consumption.onResume();

        // update devices(rooms)
        DssService.updateRooms(this);

        roomNameTv.setText(RoomsStore.get_room_by_id(roomId).get_name());

        GAHelper.sendScreenViewEvent("Overview Devices");
    }

    @Override
    protected void onPause() {
        super.onPause();

        consumption.onPause();

        // Unregister for events
        App.eventBus.unregister(this);

        // stop listening for orientation
        tiltDetector.onPause();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.overview_devices);

        favoritesBarFragment = (FavoritesBarFragment) getSupportFragmentManager().findFragmentById(R.id.FavoritesBar);

        tiltDetector.init(this);
        tiltDetector.enable();

        // get roomNumber from the intent
        roomId = getIntent().getExtras().getInt(EXTRA_ROOM_ID);

        // check what colour groups are available in this room
        final List<DsDevice> devices = RoomsStore.get_room_by_id(roomId).get_devices();

        final Set<Integer> avaiableGroups = Sets.newHashSet();
        for (final DsDevice device : devices) {
            avaiableGroups.add(device.get_groupNumber());
        }

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        indicator = (IconPageIndicator) findViewById(R.id.indicator);

        adapter = new DeviceOverviewPagerAdapter(this, avaiableGroups, roomId);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);

        indicator.setOnPageChangeListener(new SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(final int position) {
                currentPage = position;
                indicator.notifyDataSetChanged();
            }
        });

        roomNameTv = (TextView) findViewById(R.id.currentRoomTextView);

        final int groupNumber = getIntent().getIntExtra(EXTRA_GROUP_NUMBER, 0);
        currentPage = groupNumber;
        viewPager.setCurrentItem(groupNumber);
        indicator.notifyDataSetChanged();

        consumption.update();

        initMenuButton();
    }

    private void initMenuButton() {
        final View menuButton = findViewById(R.id.menuButton);
        menuButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                startActivityForResult(new Intent(DeviceOverview.this, ConfigurationScreen.class), REQUEST_CODE_CONFIGURATION_SCREEN);
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }
        });
    }

    // Make sure the layout is not recreated on keyboard hidden events (for hardware keyboard phones)
    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.overview_devices);
    }

    // callback defined in xml
    public void goBackClick(final View V) {
        final Intent myIntent = new Intent();
        myIntent.putExtra(EXTRA_GROUP_NUMBER, currentPage);
        setResult(RESULT_OK, myIntent);
        finish();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case REQEUST_CODE_ADVANCE_BUTTON: {
                if (resultCode == RESULT_OK) {
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            }
            case REQUEST_CODE_CONFIGURATION_SCREEN: {
                if (resultCode == RESULT_OK) {
                    setResult(RESULT_OK);
                    finish();
                } else if (resultCode == ConfigurationScreen.SERVERS_CHANGED) {
                    setResult(ConfigurationScreen.SERVERS_CHANGED);
                    finish();
                }
                break;
            }
        }
    }

    private void markActiveDevices() {
        // Visually mark the last called scene, by notifying all listadapters that data changed
        adapter.notifyDataSetChanged();
    }

}
