package com.smartboxasia.control.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.helper.NopConnectionService;
import com.smartboxasia.control.domain.LoginResult;
import com.smartboxasia.control.events.LoginResultEvent;
import com.smartboxasia.control.events.NoConnectivityEvent;
import com.smartboxasia.control.events.ShowLoginErrorDialogEvent;
import com.smartboxasia.control.ui.helper.AirplaneModeClickListener;
import com.smartboxasia.control.ui.helper.BackgroundGradientDrawable;
import com.smartboxasia.control.ui.helper.CancelClickListener;
import com.smartboxasia.control.ui.helper.ConnectionChecker;
import com.smartboxasia.control.ui.helper.DialogHelper;
import com.smartboxasia.control.ui.helper.MobileSettingsClickListener;
import com.smartboxasia.control.ui.helper.WifiSettingsClickListener;
import com.smartboxasia.control.ui.settings.ServerSettings;
import com.google.common.eventbus.Subscribe;

public class NoConnectivityActivity extends Activity {

    protected static final int REQUEST_CODE_SETTINGS = 1;
    private View progressBar;
    private View retry;
    private View settings;
    private boolean showNextError = false;

    @Subscribe
    public void onEvent(final ShowLoginErrorDialogEvent event) {
        if (showNextError) {
            showNextError = false;
            DialogHelper.showInformationDialog(this, event.getTitle(), event.getMessage(), event.getShowButton());
        }
    }

    @Subscribe
    public void onEvent(final NoConnectivityEvent event) {
        clearProgressBar();
        showNextError = false;
    }

    @Subscribe
    public void onEvent(final LoginResultEvent event) {
        clearProgressBar();
        // do not reset the showNextError variable as the login process sends ShowLoginErrorDialogEvent with detailed info
        if (event.getResult() == LoginResult.LOGIN_RESULT_OK) {
            finish();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initLooks();
        initProgressBar();
        initRetryLink();
        initSettingsLink();
    }

    @Override
    public void onBackPressed() {
        // send our app to the background on back
        moveTaskToBack(true);
    }

    @Override
    protected void onStop() {
        App.eventBus.unregister(this);
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        App.eventBus.register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkConnection();

        GAHelper.sendScreenViewEvent("Connection error");
    }

    private void checkConnection() {

        if (Connection.getConnectionService() instanceof NopConnectionService) {
            finish();
        }
        progressBar.setVisibility(View.VISIBLE);
        if (ConnectionChecker.isAirplaneModeOn(getApplicationContext())) {
            showAirplaneModeDialog();
        } else if (!ConnectionChecker.isDataEnabled(getApplicationContext())) {
            showDataEnabledDialog();
        } else {
            Connection.refreshSession();
        }
    }

    private void showDataEnabledDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_title_warning);
        builder.setMessage(R.string.dialog_message_no_wifi_and_data);
        builder.setNegativeButton(android.R.string.cancel, new CancelClickListener());
        builder.setNeutralButton(R.string.dialog_button_mobile_settings, new MobileSettingsClickListener(this));
        builder.setPositiveButton(R.string.dialog_button_wifi_settings, new WifiSettingsClickListener(this));
        final AlertDialog dialog = builder.create();
        dialog.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss(final DialogInterface dialog) {
                progressBar.setVisibility(View.GONE);
            }
        });
        dialog.show();
    }

    private void showAirplaneModeDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_title_warning);
        builder.setMessage(R.string.dialog_message_airplane_mode);
        builder.setNegativeButton(android.R.string.cancel, new CancelClickListener());
        builder.setPositiveButton(R.string.dialog_button_airplane_mode_settings, new AirplaneModeClickListener(this));
        final AlertDialog dialog = builder.create();
        dialog.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss(final DialogInterface dialog) {
                progressBar.setVisibility(View.GONE);
            }
        });
        dialog.show();
    }

    private void clearProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @SuppressWarnings("deprecation")
    private void initLooks() {
        setContentView(R.layout.activity_no_connection);
        findViewById(R.id.container).setBackgroundDrawable(new BackgroundGradientDrawable());
    }

    private void initProgressBar() {
        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
    }

    private void initRetryLink() {
        retry = findViewById(R.id.retry);
        retry.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                showNextError = true;
                checkConnection();
            }
        });
    }

    private void initSettingsLink() {
        settings = findViewById(R.id.server_settings);
        settings.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                startActivityForResult(new Intent(NoConnectivityActivity.this, ServerSettings.class), REQUEST_CODE_SETTINGS);
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }
        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SETTINGS) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }
    }
}
