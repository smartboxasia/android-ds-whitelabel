package com.smartboxasia.control.ui.settings;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.tagging.cloud_1_0.TaggingService;
import com.smartboxasia.control.domain.SocketType;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.dto.DsTag;
import com.smartboxasia.control.events.SetTagSocketTypeEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.google.common.base.Strings;
import com.google.common.eventbus.Subscribe;

public class DeviceSettingsChangeSocketType extends ConnectivityMonitoringFragmentActivity {

    private static final String TAG = DeviceSettingsChangeSocketType.class.getSimpleName();

    public static final String EXTRA_DEVICE_ID = "com.smartboxasia.sia.deviceId";
    public static final String EXTRA_ROOM_NUMBER = "com.smartboxasia.sia.roomNumber";
    public static final String EXTRA_SOCKET_TYPE = "com.smartboxasia.sia.socketType";
    public static final String EXTRA_TAG = "com.smartboxasia.sia.tag";

    private DsDevice device;
    private DsTag tag;

    private boolean canClick = true;

    @Subscribe
    public void onEvent(final SetTagSocketTypeEvent event) {
        if (event.isSuccess()) {
            setResult(RESULT_OK);
            Toast.makeText(getApplicationContext(), getString(R.string.settings_device_details_change_socket_type_success), Toast.LENGTH_SHORT).show();
        } else {
            setResult(RESULT_CANCELED);
            Toast.makeText(getApplicationContext(), getString(R.string.settings_device_details_change_socket_type_fail), Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_device_details_change_socket_type);

        ((TextView) findViewById(R.id.titleTextView)).setText(R.string.settings_device_change_socket_type_title);

        String deviceId = "";
        if (getIntent().hasExtra(EXTRA_DEVICE_ID)) {
            deviceId = getIntent().getStringExtra(EXTRA_DEVICE_ID);
        }
        int roomId = -1;
        if (getIntent().hasExtra(EXTRA_ROOM_NUMBER)) {
            roomId = getIntent().getIntExtra(EXTRA_ROOM_NUMBER, -1);
        }
        SocketType currentSocketType = null;
        if (getIntent().hasExtra(EXTRA_SOCKET_TYPE)) {
            currentSocketType = (SocketType) getIntent().getSerializableExtra(EXTRA_SOCKET_TYPE);
        }
        if (getIntent().hasExtra(EXTRA_TAG)) {
            tag = (DsTag) getIntent().getSerializableExtra(EXTRA_TAG);
        }

        if (roomId < 0 || Strings.isNullOrEmpty(deviceId)) {
            Log.e(TAG, "Unknown room or device defined");
            // TODO let the user know about this? Would be a programming error
            finish();
        }

        final DsRoom room = RoomsStore.get_room_by_id(roomId);
        device = room.get_device_by_id(deviceId);

        final BaseAdapter adapter = new DeviceSettingsChangeSocketTypeAdapter(this, currentSocketType);
        final ListView list = (ListView) findViewById(android.R.id.list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> addapterView, final View view, final int position, final long id) {
                if (canClick && position >= 0 && position < SocketType.values().length + 1) {
                    view.findViewById(R.id.check).setVisibility(View.INVISIBLE);
                    view.findViewById(R.id.progress).setVisibility(View.VISIBLE);
                    if (position == 0) {
                        // delete tag
                        TaggingService.deleteTagSocketType(device, tag);
                    } else {
                        TaggingService.setTagSocketType(SocketType.values()[position - 1], device); // always use dSID for cloud services
                    }
                }
            }
        });

        addInfoButton();
    }

    private void addInfoButton() {
        final ImageView refreshButton = new ImageView(this);
        final LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        refreshButton.setLayoutParams(params);
        refreshButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                showInfoPage();
            }
        });
        refreshButton.setImageResource(R.drawable.orange_ribbon_info);
        ((LinearLayout) findViewById(R.id.title)).addView(refreshButton);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Unregister for events
        App.eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register for events
        App.eventBus.register(this);

        GAHelper.sendScreenViewEvent("Settings Devices Socket Type");
    }

    public void goBackClick(final View view) {
        onBackPressed();
    }

    private void showInfoPage() {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.socket_type_info_page_url)));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
