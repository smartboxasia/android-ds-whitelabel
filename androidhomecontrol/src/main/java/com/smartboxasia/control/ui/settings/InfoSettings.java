package com.smartboxasia.control.ui.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;

public class InfoSettings extends Activity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_info);

        ((TextView) findViewById(R.id.titleTextView)).setText(R.string.settings_title_info);

        final TextView appNameTv = (TextView) findViewById(R.id.settingsAppNameTextView);
        appNameTv.setText(getString(R.string.ds_app_name));
        final TextView versionTv = (TextView) findViewById(R.id.settingsVersionTextView);
        try {
            final PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionTv.setText(getString(R.string.version, packageInfo.versionName + " (" + packageInfo.versionCode + ")"));
        } catch (final NameNotFoundException e) {
            // ignore, show no version
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        GAHelper.sendScreenViewEvent("Settings Info");
    }

    // callback defined in xml
    public void licensesClick(final View V) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.title_licenses);

        final WebView wv = new WebView(this);
        wv.loadUrl(getString(R.string.licenses_url));
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
                view.loadUrl(url);
                return true;
            }
        });

        alert.setView(wv);
        alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int id) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    // callback defined in xml
    public void goBackClick(final View V) {
        finish();
    }
}
