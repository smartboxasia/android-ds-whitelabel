package com.smartboxasia.control.ui.settings;

import java.util.List;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.domain.OutputMode;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.events.DeviceValueChangedEvent;
import com.smartboxasia.control.events.SceneSavedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;

public class ActivitySettingsConfig extends ConnectivityMonitoringFragmentActivity {

    public static final String EXTRA_ROOM_ID = "com.smartboxasia.sia.roomId";
    public static final String EXTRA_SCENE_ID = "com.smartboxasia.sia.sceneId";
    public static final String EXTRA_GROUP_NUMBER = "com.smartboxasia.sia.groupNumber";

    private List<DsDevice> devices = Lists.newArrayList();
    private int roomId;
    private int groupNumber;
    private long sceneId;

    private ListView list;
    private ActivitySettingsConfigAdapter adapter;

    @Subscribe
    public void onEvent(final DeviceValueChangedEvent event) {
        if (event.isSuccess()) {
            Toast.makeText(getApplicationContext(), getString(R.string.setting_value_success), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.setting_value_failed), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onEvent(final SceneSavedEvent event) {
        if (event.isSuccess()) {
            Toast.makeText(getApplicationContext(), getString(R.string.save_success), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.save_failed), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activities_config);

        roomId = getIntent().getExtras().getInt(EXTRA_ROOM_ID);
        sceneId = getIntent().getExtras().getLong(EXTRA_SCENE_ID);
        groupNumber = getIntent().getExtras().getInt(EXTRA_GROUP_NUMBER);

        final DsRoom room = RoomsStore.get_room_by_id(roomId);
        ((TextView) findViewById(R.id.titleTextView)).setText(room.get_name());

        for (final DsDevice device : room.get_devices()) {
            if (device.get_outputMode() != OutputMode.DISABLED && device.get_groupNumber() == groupNumber) {
                devices.add(device);
            }
        }

        list = (ListView) findViewById(android.R.id.list);
        adapter = new ActivitySettingsConfigAdapter(this, devices);
        list.setAdapter(adapter);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Unregister from events
        App.eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register for events
        App.eventBus.register(this);

        GAHelper.sendScreenViewEvent("Settings Activities Edit Config");
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        cancelValueLoading();
        finish();
    }

    private void cancelValueLoading() {
        for (final AsyncTask<Void, Void, List<Integer>> initializer : adapter.getInitializers()) {
            initializer.cancel(true);
        }
    }

    // callback defined in xml
    public void goBackClick(final View v) {
        onBackPressed();
    }

    // callback defined in xml
    public void callActivityClick(final View v) {
        DssService.callActivity(getApplicationContext(), roomId, groupNumber, sceneId, false);
        cancelValueLoading();
        adapter = new ActivitySettingsConfigAdapter(this, devices);
        list.setAdapter(adapter);
    }

    // callback defined in xml
    public void saveSceneClick(final View v) {
        DssService.saveScene(sceneId, groupNumber);
        GAHelper.sendSaveSceneEvent();
    }
}
