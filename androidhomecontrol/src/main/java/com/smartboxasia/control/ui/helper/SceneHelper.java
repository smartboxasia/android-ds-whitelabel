package com.smartboxasia.control.ui.helper;

import java.util.List;
import java.util.Map;

import android.content.Context;

import com.smartboxasia.control.App;
import com.smartboxasia.control.DssConstants.Groups;
import com.smartboxasia.control.DssConstants.Scenes;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.config.app.ShownScenesStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.json.DssMappings;
import com.smartboxasia.control.dto.DsScene;
import com.google.common.collect.Lists;

public class SceneHelper {

    public static String getDefaultSceneName(final Context context, final long sceneId, final int groupNumber) {
        switch (groupNumber) {
            case Groups.SHADE:
                return context.getString(DssMappings.DEFAULT_SCENE_NAMES_SHADE.get(sceneId));
            default:
                return context.getString(DssMappings.DEFAULT_SCENE_NAMES.get(sceneId));
        }
    }

    public static final List<Long> defaultShownSceneIds = Lists.newArrayList(Scenes.OFF, Scenes.PRESET_1, Scenes.PRESET_2, Scenes.PRESET_3, Scenes.PRESET_4);
    public static final List<String> oldDefaultSceneNames = Lists.newArrayList("T0_S0", "T0_S1", "T0_S2", "T0_S3", "T0_S4");

    public static final List<Long> defaultSceneIds = Lists.newArrayList();
    static {
        defaultSceneIds.add(Scenes.PRESET_1);
        defaultSceneIds.add(Scenes.PRESET_2);
        defaultSceneIds.add(Scenes.PRESET_3);
        defaultSceneIds.add(Scenes.PRESET_4);
        defaultSceneIds.add(Scenes.OFF);
        defaultSceneIds.add(Scenes.AREA_1_ON);
        defaultSceneIds.add(Scenes.AREA_2_ON);
        defaultSceneIds.add(Scenes.AREA_3_ON);
        defaultSceneIds.add(Scenes.AREA_4_ON);
        defaultSceneIds.add(Scenes.AREA_1_OFF);
        defaultSceneIds.add(Scenes.AREA_2_OFF);
        defaultSceneIds.add(Scenes.AREA_3_OFF);
        defaultSceneIds.add(Scenes.AREA_4_OFF);
        defaultSceneIds.add(Scenes.PRESET_11);
        defaultSceneIds.add(Scenes.PRESET_12);
        defaultSceneIds.add(Scenes.PRESET_13);
        defaultSceneIds.add(Scenes.PRESET_14);
        defaultSceneIds.add(Scenes.PRESET_21);
        defaultSceneIds.add(Scenes.PRESET_22);
        defaultSceneIds.add(Scenes.PRESET_23);
        defaultSceneIds.add(Scenes.PRESET_24);
        defaultSceneIds.add(Scenes.PRESET_31);
        defaultSceneIds.add(Scenes.PRESET_32);
        defaultSceneIds.add(Scenes.PRESET_33);
        defaultSceneIds.add(Scenes.PRESET_34);
        defaultSceneIds.add(Scenes.PRESET_41);
        defaultSceneIds.add(Scenes.PRESET_42);
        defaultSceneIds.add(Scenes.PRESET_43);
        defaultSceneIds.add(Scenes.PRESET_44);
    }

    public static List<DsScene> createScenesList(final int roomId, final int groupNumber, final boolean showAll) {
        final Context context = App.getInstance();

        final Map<Long, Integer> defaultSceneNamesList;
        if (groupNumber == Groups.SHADE) {
            defaultSceneNamesList = DssMappings.DEFAULT_SCENE_NAMES_SHADE;
        } else {
            defaultSceneNamesList = DssMappings.DEFAULT_SCENE_NAMES;
        }

        final List<DsScene> defaultScenesList = Lists.newArrayList();
        for (final Long seneId : SceneHelper.defaultSceneIds) {
            final int sceneNameResId = defaultSceneNamesList.get(seneId);
            defaultScenesList.add(new DsScene(context.getString(sceneNameResId), seneId, groupNumber, roomId));
        }

        final List<DsScene> knownScenes = RoomsStore.get_room_by_id(roomId).get_scenes();
        final List<DsScene> shownScenes = ShownScenesStore.get_shownScenesForRoomAndGroup(roomId, groupNumber);
        final List<DsScene> result = Lists.newArrayList();

        for (final DsScene scene : defaultScenesList) {
            // use known scene if available
            if (showAll || shownScenes.contains(scene)) {
                if (knownScenes.contains(scene)) {
                    final int knownSceneIdx = knownScenes.indexOf(scene);
                    result.add(knownScenes.get(knownSceneIdx));
                } else {
                    result.add(scene);
                }
            }
        }
        return result;
    }
}
