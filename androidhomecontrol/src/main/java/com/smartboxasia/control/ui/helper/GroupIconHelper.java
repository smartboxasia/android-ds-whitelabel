package com.smartboxasia.control.ui.helper;

import java.util.Map;
import java.util.Set;

import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.R;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class GroupIconHelper {

    private static final Map<Integer, Integer> ICONS = Maps.newHashMap();
    {
        ICONS.put(DssConstants.Groups.LIGHT, R.drawable.circle_yellow);
        ICONS.put(DssConstants.Groups.SHADE, R.drawable.circle_grey);
        ICONS.put(DssConstants.Groups.CLIMATE, R.drawable.circle_blue);
        ICONS.put(DssConstants.Groups.AUDIO, R.drawable.circle_cyan);
        ICONS.put(DssConstants.Groups.VIDEO, R.drawable.circle_magenta);
    };

    public int getIconForGroup(final int index) {
        if (ICONS.containsKey(index)) {
            return ICONS.get(index);
        } else {
            return R.drawable.circle_white;
        }
    }

    public Set<Integer> getValidGroups(final Set<Integer> avaiableGroups) {
        return Sets.intersection(ICONS.keySet(), (avaiableGroups));
    }
}
