package com.smartboxasia.control.ui.overview;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.domain.OutputMode;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsRoom;
import com.google.common.collect.Lists;

final class DeviceOverviewAdapter extends BaseAdapter {
    private final List<DsDevice> devices = Lists.newArrayList();
    private final int groupNumber;
    private Activity activity;
    private DsRoom room;
    private int roomId;

    DeviceOverviewAdapter(final Activity activity, final int roomId, final int groupNumber) {
        this.activity = activity;
        this.roomId = roomId;
        this.groupNumber = groupNumber;

        room = RoomsStore.get_room_by_id(roomId);
        for (final DsDevice device : room.get_devices()) {
            if (device.get_groupNumber() == groupNumber && device.get_outputMode() != OutputMode.DISABLED) {
                devices.add(device);
            }
        }
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.overview_devices_item, parent, false);
        }

        final DsDevice device = devices.get(position);

        if (groupNumber == DssConstants.Groups.LIGHT) {
            // if device is on, give it the "active" label
            ((ImageView) convertView.findViewById(R.id.configButton)).setImageResource(R.drawable.lightbulb);

            if (device.is_on()) {
                convertView.findViewById(R.id.configButton).setBackgroundResource(R.drawable.visual_label_left_active);
                convertView.findViewById(R.id.deviceTextView).setBackgroundResource(R.drawable.visual_label_right_active);
            } else {
                convertView.findViewById(R.id.configButton).setBackgroundResource(R.drawable.visual_label_left_inactive);
                convertView.findViewById(R.id.deviceTextView).setBackgroundResource(R.drawable.visual_label_right_inactive);
            }
        } else if (groupNumber == DssConstants.Groups.SHADE) {
            // only one color for shades, as on/off doesn't make sense
            ((ImageView) convertView.findViewById(R.id.configButton)).setImageResource(R.drawable.shade);
            convertView.findViewById(R.id.configButton).setBackgroundResource(R.drawable.visual_label_left_inactive);
            convertView.findViewById(R.id.deviceTextView).setBackgroundResource(R.drawable.visual_label_right_inactive);

        } else if (groupNumber == DssConstants.Groups.CLIMATE) {
            // if device is on, give it the "active" label TODO:change to blue klemme graphics
            ((ImageView) convertView.findViewById(R.id.configButton)).setImageResource(R.drawable.temp_active);
            if (device.is_on()) {
                convertView.findViewById(R.id.configButton).setBackgroundResource(R.drawable.visual_label_left_active);
                convertView.findViewById(R.id.deviceTextView).setBackgroundResource(R.drawable.visual_label_right_active);
            } else {
                convertView.findViewById(R.id.configButton).setBackgroundResource(R.drawable.visual_label_left_inactive);
                convertView.findViewById(R.id.deviceTextView).setBackgroundResource(R.drawable.visual_label_right_inactive);
            }
        } else {
            // if device is on, give it the "active" label
            ((ImageView) convertView.findViewById(R.id.configButton)).setImageResource(R.drawable.as_configurebutton);
            if (device.is_on()) {
                convertView.findViewById(R.id.configButton).setBackgroundResource(R.drawable.visual_label_left_active);
                convertView.findViewById(R.id.deviceTextView).setBackgroundResource(R.drawable.visual_label_right_active);
            } else {
                convertView.findViewById(R.id.configButton).setBackgroundResource(R.drawable.visual_label_left_inactive);
                convertView.findViewById(R.id.deviceTextView).setBackgroundResource(R.drawable.visual_label_right_inactive);
            }
        }

        // set click listener on the config button
        final ImageView configButton = (ImageView) convertView.findViewById(R.id.configButton);
        configButton.setOnClickListener(new OnClickListener() {
            public void onClick(final View v) {
                // do the business
                advancedButtonClick(devices.get(position), devices.get(position).get_name(), groupNumber);
            }
        });

        // set click listener on the device click
        final TextView deviceTV = (TextView) convertView.findViewById(R.id.deviceTextView);
        deviceTV.setOnClickListener(new OnClickListener() {
            public void onClick(final View v) {
                // set the pressed color
                deviceTV.setTextColor(activity.getResources().getColor(android.R.color.black));
                // do the activity click
                if (groupNumber == DssConstants.Groups.SHADE) {
                    // if shade, just go to advanced button screen
                    advancedButtonClick(devices.get(position), devices.get(position).get_name(), groupNumber);
                } else {
                    deviceClick(device, groupNumber);
                }
            }
        });

        deviceTV.setText(devices.get(position).get_name());
        deviceTV.setTextColor(activity.getResources().getColorStateList(R.color.color_selector));

        return convertView;
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public Object getItem(final int position) {
        return devices.get(position);
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    private void advancedButtonClick(final DsDevice device, final String name, final int groupNumber) {
        Intent myIntent = null;

        if (groupNumber == DssConstants.Groups.LIGHT) {
            myIntent = new Intent(activity, AdvancedButton.class);
            myIntent.putExtra(AdvancedButton.EXTRA_ROOM_ID, roomId);
            myIntent.putExtra(AdvancedButton.EXTRA_CURRENT_NAME, name);
            myIntent.putExtra(AdvancedButton.EXTRA_TYPE_NAME, activity.getString(R.string.devices_title));
            myIntent.putExtra(AdvancedButton.EXTRA_IS_DEVICE, true);
            myIntent.putExtra(AdvancedButton.EXTRA_DEVICE_ID, device.get_id());
        } else if (groupNumber == DssConstants.Groups.SHADE) {
            myIntent = new Intent(activity, AdvancedButtonShade.class);
            myIntent.putExtra(AdvancedButtonShade.EXTRA_ROOM_ID, roomId);
            myIntent.putExtra(AdvancedButtonShade.EXTRA_CURRENT_NAME, name);
            myIntent.putExtra(AdvancedButtonShade.EXTRA_TYPE_NAME, activity.getString(R.string.devices_title));
            myIntent.putExtra(AdvancedButtonShade.EXTRA_IS_DEVICE, true);
            myIntent.putExtra(AdvancedButtonShade.EXTRA_DEVICE_ID, device.get_id());
        } else {
            // just click normally if another group is clicked
            deviceClick(device, groupNumber);
            return;
        }

        activity.startActivityForResult(myIntent, DeviceOverview.REQEUST_CODE_ADVANCE_BUTTON);
        activity.overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
    }

    private void deviceClick(final DsDevice device, final int groupNumber) {
        // Toggle device
        if (!device.is_on()) {
            DssService.turnOnDevice(activity, roomId, device);
        } else {
            DssService.turnOffDevice(activity, roomId, device);
        }
        GAHelper.sendSceneCalledEvent("device");
    }
}
