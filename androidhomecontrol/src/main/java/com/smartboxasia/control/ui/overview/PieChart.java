package com.smartboxasia.control.ui.overview;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.MetersCache;
import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.events.AllMetersConsumptionsChangedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.smartboxasia.control.ui.helper.ConsumptionHelper;
import com.smartboxasia.control.ui.helper.PieChartView;
import com.google.common.eventbus.Subscribe;

public class PieChart extends ConnectivityMonitoringFragmentActivity {

    private int pieChartSize;
    private ListView meterList;
    private BaseAdapter meterListAdapter;

    private final ConsumptionHelper consumption = new ConsumptionHelper(this, R.id.ConsumptionTextView, R.id.ConsumptionColor);

    private boolean running;
    private static final Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(final Message msg) {
            DssService.updateAllMeterConsumptions();
        }
    };

    @Subscribe
    public void onEvent(final AllMetersConsumptionsChangedEvent event) {
        updatePieChart();
        meterListAdapter.notifyDataSetChanged();
        if (running) {
            handler.sendEmptyMessageDelayed(0, App.METER_POLL_DELAY);
        }
    }

    private void updatePieChart() {

        final Bitmap mBackgroundImage = Bitmap.createBitmap(pieChartSize, pieChartSize, Bitmap.Config.ARGB_8888);
        PieChartView pieChartView = new PieChartView(this);
        pieChartView.setLayoutParams(new LayoutParams(pieChartSize, pieChartSize));
        pieChartView.setGeometry(pieChartSize, pieChartSize, 2, 2, 2, 2);
        pieChartView.setData(MetersCache.get_meters(false));
        pieChartView.invalidate();
        pieChartView.draw(new Canvas(mBackgroundImage));
        pieChartView = null;

        final ImageView _imageView = (ImageView) findViewById(R.id.pieChart);
        _imageView.setImageBitmap(mBackgroundImage);
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pie_chart);
        final TextView titleTv = (TextView) findViewById(R.id.serverNameTextView);
        final ConnectionData activeConnectionData = Connection.getActiveConnectionData();
        if (activeConnectionData == null) {
            Toast.makeText(getApplicationContext(), R.string.select_server, Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        titleTv.setText(activeConnectionData.getName());

        final Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        final int height = display.getHeight();
        pieChartSize = (int) (height * 0.7);

        meterList = (ListView) findViewById(R.id.meterListView);
        meterListAdapter = new PieChartAdapter(this);
        meterList.setAdapter(meterListAdapter);

        consumption.update();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Unregister from events
        App.eventBus.unregister(this);

        consumption.onPause();

        running = false;
        handler.removeMessages(0);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register for events
        App.eventBus.register(this);

        // resume polling the consumption (this also gets a new session token, in case it timed out)
        consumption.onResume();

        running = true;
        handler.sendEmptyMessage(0);

        GAHelper.sendScreenViewEvent("Overview Pie Chart");
    }

    // callback defined in xml
    public void goBackClick(final View V) {
        finish();
    }
}
