package com.smartboxasia.control.ui;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.smartboxasia.control.App;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.helper.NopConnectionService;
import com.smartboxasia.control.domain.LoginResult;
import com.smartboxasia.control.events.LoginResultEvent;
import com.smartboxasia.control.events.NoConnectivityEvent;
import com.smartboxasia.control.events.ShowLoginErrorDialogEvent;
import com.smartboxasia.control.ui.helper.DialogHelper;
import com.smartboxasia.control.ui.settings.ServerSettings;
import com.google.common.eventbus.Subscribe;

public class ConnectivityMonitoringFragmentActivity extends FragmentActivity {

    private static final String TAG = ConnectivityMonitoringFragmentActivity.class.getSimpleName();

    private ConnectivityManager connectivityManager;
    private volatile boolean showingAlert;

    @Subscribe
    public synchronized void onEvent(final ShowLoginErrorDialogEvent event) {
        DialogHelper.showInformationDialog(this, event.getTitle(), event.getMessage(), event.getShowButton());
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Override
    protected void onPause() {
        App.eventBus.unregister(monitor);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showingAlert = false;
        App.eventBus.register(monitor);

        final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        final boolean dummyConnectionServiceActive = (Connection.getConnectionService() instanceof NopConnectionService);
        final boolean connectivityAvailable = networkInfo != null && networkInfo.isConnected();
        if (!dummyConnectionServiceActive && !connectivityAvailable) {
            showAlert();
        }
    }

    private final Object monitor = new Object() {
        @Subscribe
        public void onEvent(final NoConnectivityEvent e) {
            showAlert();
        }

        @Subscribe
        public void onEvent(final LoginResultEvent event) {
            if (!App.credentialsDialogShown && event.getResult() == LoginResult.LOGIN_RESULT_NEED_CREDENTIALS) {
                App.credentialsDialogShown = true;
                // open the server page for handling login
                final Intent reloginIntent = new Intent(ConnectivityMonitoringFragmentActivity.this, ServerSettings.class);
                reloginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                reloginIntent.putExtra(ServerSettings.EXTRA_RELOGIN, true);
                startActivity(reloginIntent);
            }
        }
    };

    private void showAlert() {
        if (!showingAlert) {
            showingAlert = true;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "Starting NoConnectivtiyActivity");
                    final Intent intent = new Intent(ConnectivityMonitoringFragmentActivity.this, NoConnectivityActivity.class);
                    startActivity(intent);
                }
            });
        }
    }
}
