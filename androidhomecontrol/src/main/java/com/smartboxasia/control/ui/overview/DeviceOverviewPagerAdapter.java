package com.smartboxasia.control.ui.overview;

import java.util.Set;

import android.app.Activity;
import android.widget.BaseAdapter;
import android.widget.ListView;

public class DeviceOverviewPagerAdapter extends OverviewPagerAdapter {

    public DeviceOverviewPagerAdapter(final Activity activity, final Set<Integer> avaiableGroups, final int roomId) {
        super(activity, avaiableGroups, roomId);
    }

    void updateItemList(final ListView theList, final int groupNumber) {

        final BaseAdapter adapter = new DeviceOverviewAdapter(activity, roomId, groupNumber);

        theList.setAdapter(adapter);

    }
}
