package com.smartboxasia.control.ui.settings;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.events.FilteredScenesChangedEvent;
import com.viewpagerindicator.IconPageIndicator;

public class ActivitySettingsActivities extends ActivitySettingsActivitiesBase {

    private ActivitySettingsActivitiesPagerAdapter activitiesSettingsPagerAdapter;
    private boolean showAll;

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.settings_activities);

        final DsRoom room = RoomsStore.get_room_by_id(roomId);
        ((TextView) findViewById(R.id.titleTextView)).setText(room.get_name());

        activitiesSettingsPagerAdapter = new ActivitySettingsActivitiesPagerAdapter(getSupportFragmentManager(), roomId);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.activities_pager);
        viewPager.setAdapter(activitiesSettingsPagerAdapter);
        final IconPageIndicator indicator = ((IconPageIndicator) findViewById(R.id.indicator));
        indicator.setViewPager(viewPager);

        final View showActivitiesButton = findViewById(R.id.show_activities);
        final View showAllButton = findViewById(R.id.show_all);

        showActivitiesButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                setShowAll(false);
                updateFilterButtons();
            }
        });
        showAllButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                setShowAll(true);
                updateFilterButtons();
            }
        });

        setShowAll(false);
    }

    @Override
    protected BaseAdapter creatAdapter() {
        return null;
    }

    @Override
    public void updateList() {
        activitiesSettingsPagerAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateFilterButtons();

        DssService.updateActivties(getApplicationContext());

        GAHelper.sendScreenViewEvent("Settings Activities Select Scene");
    }

    private void updateFilterButtons() {
        findViewById(R.id.show_activities).setSelected(!isShowAll());
        findViewById(R.id.show_all).setSelected(isShowAll());
    }

    boolean isShowAll() {
        return showAll;
    }

    private void setShowAll(final boolean showAll) {
        this.showAll = showAll;
        App.eventBus.post(new FilteredScenesChangedEvent());
    }
}
