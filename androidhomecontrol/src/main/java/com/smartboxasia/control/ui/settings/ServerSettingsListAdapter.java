package com.smartboxasia.control.ui.settings;

import java.net.URL;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.connection.Connection;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

@SuppressWarnings("deprecation")
public class ServerSettingsListAdapter extends BaseAdapter {

    private static final String TAG = ServerSettingsListAdapter.class.getSimpleName();

    private final Context context;

    private final List<ConnectionData> customServers = Lists.newArrayList();
    private final List<ConnectionData> knownServers = Lists.newArrayList();

    public ServerSettingsListAdapter(final Context context, final List<ConnectionData> customServers) {
        this.context = context;
        this.customServers.addAll(customServers);
    }

    public void notifyFound(final ConnectionData newConnection) {

        // check if we already have this dss
        for (final ConnectionData data : customServers) {
            if (newConnection.getUrl().equals(data.getUrl())) {
                Log.w(TAG, "Already have server address loaded = " + newConnection.getUrl());
                return;
            }
        }
        for (final ConnectionData data : knownServers) {
            if (newConnection.getName().equalsIgnoreCase(data.getName())) {
                Log.w(TAG, "Already have DatabaseId loaded = " + newConnection.getName());
                return;
            }
        }

        if (!knownServers.contains(newConnection)) {
            knownServers.add(newConnection);
            notifyDataSetChanged();
        }

    }

    public Object getItem(final int position) {
        return knownServers.get(position);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    public int getCount() {
        return knownServers.size();
    }

    public long getItemId(final int position) {
        return position;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.settings_servers_item_dss, parent, false);
        }

        try {
            final ConnectionData data = knownServers.get(position);

            final String title = data.getName();
            final String addr = data.getUrl();

            ((TextView) convertView.findViewById(android.R.id.text1)).setText(title);
            ((TextView) convertView.findViewById(android.R.id.text2)).setText(addr);

            // different background for every second
            if (position % 2 == 0) {
                ((TwoLineListItem) convertView.findViewById(R.id.itemContainer)).setBackgroundResource(R.drawable.conf_list_item_grey_selector);
            } else {
                ((TwoLineListItem) convertView.findViewById(R.id.itemContainer)).setBackgroundResource(R.drawable.conf_list_item_white_selector);
            }

            // mark the current server, if it's set
            final ConnectionData activeConnectionData = Connection.getActiveConnectionData();
            if (activeConnectionData != null && !Strings.isNullOrEmpty(activeConnectionData.getUrl())) {
                final URL currentUrl = new URL(activeConnectionData.getUrl());

                if (addr.compareTo(currentUrl.getHost()) == 0) {
                    // orange
                    ((TwoLineListItem) convertView.findViewById(R.id.itemContainer)).setBackgroundResource(R.color.orange_selection);
                    ((TwoLineListItem) convertView.findViewById(R.id.itemContainer)).setDrawingCacheEnabled(false);
                }
            }

        } catch (final Exception e) {
            Log.d(TAG, String.format("Problem getting ZeroConf information %s", e.getMessage()));
            final String unknown = context.getString(R.string.unknown);
            ((TextView) convertView.findViewById(android.R.id.text1)).setText(unknown);
            ((TextView) convertView.findViewById(android.R.id.text2)).setText(unknown);
        }

        return convertView;
    }
}
