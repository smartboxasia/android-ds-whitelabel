package com.smartboxasia.control.ui.helper;

import java.util.Map;
import java.util.Set;

import android.content.Context;
import android.util.Log;

import com.smartboxasia.control.App;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsRoom;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

final class OffSwitcherThread extends Thread {
    private static final String TAG = OffSwitcherThread.class.getSimpleName();

    private final Context context;
    private final int roomId;

    OffSwitcherThread(final Context context, final int roomId) {
        this.context = context;
        this.roomId = roomId;
    }

    @Override
    public void run() {
        // force switch everything else off (lights)
        // every time we encounter a dsm that have already been called, we wait a second, to ensure downstream isn't blocked
        final Map<String, String> metersAlreadyWorking = Maps.newHashMap();
        final Set<String> metersInCurrentRoom = Sets.newHashSet();

        boolean willSleep = false;

        for (final DsRoom room : RoomsStore.get_roomsSorted()) {

            // if there's a new switch off task to be started, we can stop now. TODO: really? Should we tell the user?
            if (isInterrupted()) {
                Log.i(TAG, "switching off all other rooms has been interrupted by a new switch off task");
                return;
            }

            if (room.get_id() != roomId) {
                metersInCurrentRoom.clear();

                for (final DsDevice device : room.get_devices()) {
                    metersInCurrentRoom.add(device.get_dsmId());
                }

                for (final String meter : metersInCurrentRoom) {
                    if (metersAlreadyWorking.containsKey(meter)) {
                        willSleep = true;
                    }
                }

                if (willSleep) {
                    try {
                        // clear the working meters and sleep a second
                        metersAlreadyWorking.clear();
                        willSleep = false;
                        Thread.sleep(App.DSS_ACTION_DELAY);
                    } catch (final InterruptedException e) {
                        interrupt();
                        continue;
                    }
                }

                for (final String meter : metersInCurrentRoom) {
                    metersAlreadyWorking.put(meter, null);
                }

                DssService.callActivity(context, room.get_id(), 1, 0, true);
            }
        }
        Log.i(TAG, "done switching off all other rooms");
    }
}
