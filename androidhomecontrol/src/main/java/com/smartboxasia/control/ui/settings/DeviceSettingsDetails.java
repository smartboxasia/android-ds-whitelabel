package com.smartboxasia.control.ui.settings;

import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.smartboxasia.control.App;
import com.smartboxasia.control.DssConstants.Groups;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.MetersCache;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.dimwizard.cloud_1_1.DimWizardService;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.data.dss.direct_1_17_0.json.DssMappings;
import com.smartboxasia.control.data.helper.Mappings;
import com.smartboxasia.control.data.materialmaster.cloud_1_0.MaterialMasterService;
import com.smartboxasia.control.data.materialmaster.cloud_1_0.json.JSONConstants;
import com.smartboxasia.control.data.tagging.cloud_1_0.TaggingService;
import com.smartboxasia.control.data.tagging.cloud_1_0.json.TaggingMappings;
import com.smartboxasia.control.domain.OutputMode;
import com.smartboxasia.control.domain.SocketType;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsMeter;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.dto.DsTag;
import com.smartboxasia.control.events.CheckIlluminantResultEvent;
import com.smartboxasia.control.events.DeviceBlinkEvent;
import com.smartboxasia.control.events.DeviceRenamedEvent;
import com.smartboxasia.control.events.GetAllTagsReceivedEvent;
import com.smartboxasia.control.events.SetTagMaxPowerEvent;
import com.smartboxasia.control.events.TransferingDimmingCurveFinishedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.smartboxasia.control.ui.helper.ConsumptionViewUpdater;
import com.smartboxasia.control.ui.helper.DimmingViewUpdater;
import com.smartboxasia.control.ui.helper.MeteringViewUpdater;
import com.smartboxasia.control.ui.helper.ShadeViewUpdater;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.eventbus.Subscribe;
import com.google.common.primitives.Ints;
import com.google.zxing.client.android.Intents.Scan;

public class DeviceSettingsDetails extends ConnectivityMonitoringFragmentActivity {

    private static final String TAG = DeviceSettingsDetails.class.getSimpleName();

    static class Holder<V> {
        static final List<Integer> itemsToCancel = Lists.newArrayList();
        AsyncTask<Void, Void, V> initializer;
    }

    private static final int REQUEST_ROOM_ASSIGNMENT = ConfigurationScreen.REQUEST_CODE_DEVICES + 10;
    private static final int REQUEST_CHANGE_OUTPUT_MODE = REQUEST_ROOM_ASSIGNMENT + 10;
    private static final int REQUEST_CHANGE_SOCKET_TYPE = REQUEST_CHANGE_OUTPUT_MODE + 10;
    private static final int REQUEST_CHECK_ILLUMINANT = REQUEST_CHANGE_SOCKET_TYPE + 10;
    private static final int REQUEST_TRANSFER_DIMMING_CURVE = REQUEST_CHECK_ILLUMINANT + 10;

    public static final String EXTRA_ROOM_ID = "com.smartboxasia.sia.roomId";
    public static final String EXTRA_DEVICE_ID = "com.smartboxasia.sia.deviceNumber";

    private int roomId;
    private String deviceId;
    private String deviceName;
    private Integer maxPower;
    private SocketType socketType;

    private boolean hasChanged = false;

    private final Map<String, DsTag> tags = Maps.newHashMap();
    private AlertDialog dialog;

    @Subscribe
    public void onEvent(final DeviceRenamedEvent event) {
        if (event.isSuccess()) {
            hasChanged = true;

            final String newDeviceName = event.getNewDeviceName();
            updateDeviceName(newDeviceName);
            RoomsStore.get_room_by_id(roomId).get_device_by_id(deviceId).set_name(newDeviceName);

            Toast.makeText(getApplicationContext(), getString(R.string.rename_success), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.rename_fail), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onEvent(final DeviceBlinkEvent event) {
        if (event.isSuccess()) {
            Toast.makeText(getApplicationContext(), getString(R.string.settings_device_details_blink_success), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.settings_device_details_blink_fail), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onEvent(final SetTagMaxPowerEvent event) {
        if (event.isSuccess()) {
            hasChanged = true;
            updateTags(RoomsStore.get_room_by_id(roomId).get_device_by_id(deviceId));
            Toast.makeText(getApplicationContext(), getString(R.string.settings_device_details_change_max_power_success), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.settings_device_details_change_max_power_fail), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onEvent(final TransferingDimmingCurveFinishedEvent event) {
        if (dialog == null) {
            return;
        }
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                final Button button1 = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                final Button button2 = dialog.getButton(DialogInterface.BUTTON_NEUTRAL);
                final TextView message = (TextView) dialog.findViewById(R.id.ean_message);
                final View progress = dialog.findViewById(R.id.check_result_progress);
                final ImageView indicator = (ImageView) dialog.findViewById(R.id.check_result_indicator);
                if (event.isSuccess()) {
                    dialog.setTitle(R.string.settings_device_details_transfer_dimming_curve_success_title);
                    message.setText(R.string.settings_device_details_transfer_dimming_curve_success_message);
                    indicator.setImageResource(R.drawable.check_ok);
                } else if (event.getErrorCode() == TransferingDimmingCurveFinishedEvent.ERROR_TRANSFER_IN_PROGRESS) {
                    dialog.setTitle(R.string.settings_device_details_transfer_dimming_curve_failed_title);
                    message.setText(R.string.settings_device_details_transfer_in_progress_message);
                    indicator.setImageResource(R.drawable.check_fail);
                } else {
                    dialog.setTitle(R.string.settings_device_details_transfer_dimming_curve_failed_title);
                    message.setText(R.string.settings_device_details_transfer_dimming_curve_failed_message);
                    indicator.setImageResource(R.drawable.check_fail);
                }
                progress.setVisibility(View.INVISIBLE);
                indicator.setVisibility(View.VISIBLE);
                button1.setText(android.R.string.ok);
                button1.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(final View v) {
                        dialog.dismiss();
                    }
                });
                button2.setVisibility(View.GONE);
                dialog.setCancelable(true);
            }
        });
    }

    @Subscribe
    public void onEvent(final GetAllTagsReceivedEvent event) {
        setTags(event.getDsTags());
        updateMaxPower(event.getDevice());
        updateSocketType(event.getDevice());
        updateFooter(event.getDevice());
    }

    private boolean isDsDevice(final DsDevice device) {
        return !(device.get_hasDsuid() && Strings.isNullOrEmpty(device.get_dsid()));
    }

    private boolean isLightDevice(final DsDevice device) {
        return device.get_groupNumber() == Groups.LIGHT;
    }

    private boolean isNotDisabled(final DsDevice device) {
        return device.get_outputMode() != OutputMode.DISABLED;
    }

    private boolean isDimmable(final DsDevice device) {
        return device.get_outputMode() == OutputMode.DIMMABLE;
    }

    private boolean hasOutputModes(final DsDevice device) {
        final List<OutputMode> modes = device.getAvailableOutputModes();
        return modes != null && !modes.isEmpty();
    }

    private DsDevice getDevice() {
        return RoomsStore.get_room_by_id(roomId).get_device_by_id(deviceId);
    }

    private boolean isCloudLogin() {
        return Connection.getActiveConnectionData().isCloudLogin();
    }

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_device_details);

        roomId = getIntent().getExtras().getInt(EXTRA_ROOM_ID);
        deviceId = getIntent().getExtras().getString(EXTRA_DEVICE_ID);

        reloadFields();

        updateEntryBackgrounds();

        updateFooter(getDevice());

        addRefreshButton();
    }

    private void addRefreshButton() {
        final ImageView refreshButton = new ImageView(this);
        final LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        refreshButton.setLayoutParams(params);
        refreshButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                reloadFields();
            }
        });
        refreshButton.setImageResource(R.drawable.orange_ribbon_refresh);
        ((LinearLayout) findViewById(R.id.title)).addView(refreshButton);
    }

    private void updateFooter(final DsDevice device) {
        final boolean isCloudLogin = isCloudLogin();
        findViewById(R.id.footer_divider).setVisibility(View.GONE);
        findViewById(R.id.footer_with_two_buttons).setVisibility(View.GONE);
        findViewById(R.id.footer_with_four_buttons).setVisibility(View.GONE);

        if (isLightDevice(device) && isDsDevice(device) && hasOutputModes(device)) {
            findViewById(R.id.footer_divider).setVisibility(View.VISIBLE);
            findViewById(R.id.footer_with_four_buttons).setVisibility(View.VISIBLE);
            findViewById(R.id.four_button_footer_button_1).setEnabled(hasTag(DsTag.TAG_INFO_PAGE_URL) && isCloudLogin);
            findViewById(R.id.four_button_footer_button_2).setEnabled(hasTag(DsTag.TAG_SERVICE_PAGE_URL) && isCloudLogin);
            findViewById(R.id.four_button_footer_button_3).setEnabled(isNotDisabled(device) && hasTag(DsTag.TAG_MAX_POWER) && hasTag(DsTag.TAG_SOCKET_TYPE) && isCloudLogin);
            findViewById(R.id.four_button_footer_button_4).setEnabled(isDimmable(device) && hasTag(DsTag.TAG_MAX_POWER) && hasTag(DsTag.TAG_SOCKET_TYPE) && isCloudLogin);
        } else {
            findViewById(R.id.footer_divider).setVisibility(View.VISIBLE);
            findViewById(R.id.footer_with_two_buttons).setVisibility(View.VISIBLE);
            findViewById(R.id.two_button_footer_button_1).setEnabled(hasTag(DsTag.TAG_INFO_PAGE_URL) && isCloudLogin);
            findViewById(R.id.two_button_footer_button_2).setEnabled(hasTag(DsTag.TAG_SERVICE_PAGE_URL) && isCloudLogin);
        }
    }

    private void reloadFields() {
        final DsRoom room = RoomsStore.get_room_by_id(roomId);
        if (room == null) {
            return;
        }
        final DsDevice device = room.get_device_by_id(deviceId);
        updateMeterName(device);
        updateDeviceName(device.get_name());
        updateRoomName(room);
        updateOuputMode(device);
        updateDeviceId(device);
        updateMaxPower(device);
        updateSocketType(device);
        updateTags(device);
        updateDeviceValue(device);
        updateConsumption(device);
        updateMeterReading(device);
    }

    private void updateTags(final DsDevice device) {

        final View socketTypeLayout = findViewById(R.id.socketTypeLayout);
        final View socketTypeProgress = findViewById(R.id.socketTypeProgress);
        final TextView socketTypeTv = (TextView) findViewById(R.id.socketType);
        final View maxPowerLayout = findViewById(R.id.maxPowerLayout);
        final View maxPowerProgress = findViewById(R.id.maxPowerProgress);
        final TextView maxPowerTv = (TextView) findViewById(R.id.maxPower);

        if (isLightDevice(device) && isDsDevice(device) && isCloudLogin()) {

            socketTypeLayout.setVisibility(View.VISIBLE);
            socketTypeLayout.setEnabled(false);
            socketTypeProgress.setVisibility(View.VISIBLE);
            socketTypeTv.setVisibility(View.INVISIBLE);

            maxPowerLayout.setVisibility(View.VISIBLE);
            maxPowerLayout.setEnabled(false);
            maxPowerProgress.setVisibility(View.VISIBLE);
            maxPowerTv.setVisibility(View.INVISIBLE);

        } else {

            // Do not show SocketType and MaxPower for direct connections
            socketTypeLayout.setVisibility(View.GONE);
            maxPowerLayout.setVisibility(View.GONE);

        }

        TaggingService.getAllTags(device);
    }

    private void updateSocketType(final DsDevice device) {
        final View socketTypeLayout = findViewById(R.id.socketTypeLayout);
        socketTypeLayout.setVisibility(View.GONE);

        if (isLightDevice(device) && isDsDevice(device) && isCloudLogin()) {
            final TextView socketTypeTv = (TextView) findViewById(R.id.socketType);
            final View socketTypeProgress = findViewById(R.id.socketTypeProgress);
            socketTypeTv.setVisibility(View.VISIBLE);
            socketTypeProgress.setVisibility(View.GONE);
            socketTypeLayout.setEnabled(isCloudLogin());

            if (getDevice().isOemDevice()) {
                findViewById(R.id.socketTypeEditButton).setVisibility(View.GONE);
            }

            final DsTag tag = getTag(DsTag.TAG_SOCKET_TYPE);
            if (tag != null) {
                final String value = tag.valueShortName;
                socketType = Mappings.findKey(TaggingMappings.SOCKET_TYPES, value);
                if (socketType == null) {
                    socketTypeTv.setText(R.string.dash);
                } else {
                    socketTypeTv.setText(value);
                }
            } else {
                socketType = null;
                socketTypeTv.setText(R.string.dash);
            }

            socketTypeLayout.setVisibility(View.VISIBLE);
        }
    }

    private void updateMaxPower(final DsDevice device) {
        final View maxPowerLayout = findViewById(R.id.maxPowerLayout);
        maxPowerLayout.setVisibility(View.GONE);

        if (isLightDevice(device) && isDsDevice(device) && isCloudLogin()) {
            final TextView maxPowerTv = (TextView) findViewById(R.id.maxPower);
            final View maxPowerProgress = findViewById(R.id.maxPowerProgress);
            maxPowerTv.setVisibility(View.VISIBLE);
            maxPowerProgress.setVisibility(View.GONE);
            maxPowerLayout.setEnabled(isCloudLogin());

            if (getDevice().isOemDevice()) {
                findViewById(R.id.maxPowerEditButton).setVisibility(View.GONE);
            }

            final DsTag tag = getTag(DsTag.TAG_MAX_POWER);
            if (tag != null) {
                maxPower = Ints.tryParse(tag.value);
                if (maxPower != null && maxPower > 0) {
                    maxPowerTv.setText(getString(R.string.watts_pattern, "" + maxPower));
                } else {
                    maxPowerTv.setText(getString(R.string.watts_pattern, getString(R.string.dash)));
                }
            } else {
                maxPower = null;
                maxPowerTv.setText(getString(R.string.watts_pattern, getString(R.string.dash)));
            }

            maxPowerLayout.setVisibility(View.VISIBLE);
        }
    }

    private void updateMeterReading(final DsDevice device) {
        final Holder<Integer> holder = new Holder<Integer>();
        final TextView view = (TextView) findViewById(R.id.meterReading);
        final View progress = findViewById(R.id.meterReadingProgress);
        if (view != null) {
            holder.initializer = new MeteringViewUpdater(this, device, view, progress);
            holder.initializer.execute();
            view.setTag(holder);
            Holder.itemsToCancel.add(R.id.meterReading);
        }
    }

    private void updateConsumption(final DsDevice device) {
        final Holder<Integer> holder = new Holder<Integer>();
        final TextView view = (TextView) findViewById(R.id.consumption);
        final View progress = findViewById(R.id.consumptionProgress);
        if (view != null) {
            holder.initializer = new ConsumptionViewUpdater(this, device, view, progress);
            holder.initializer.execute();
            view.setTag(holder);
            Holder.itemsToCancel.add(R.id.consumption);
        }
    }

    private void updateDeviceValue(final DsDevice device) {
        final Holder<Integer> holder = new Holder<Integer>();
        final TextView view = (TextView) findViewById(R.id.deviceValue);
        final View progress = findViewById(R.id.deviceValueProgress);
        if (view != null) {
            if (device.get_groupNumber() == Groups.SHADE) {
                holder.initializer = new ShadeViewUpdater(this, device, view, progress);
            } else {
                holder.initializer = new DimmingViewUpdater(this, device, view, progress);
            }
            holder.initializer.execute();
            view.setTag(holder);
            Holder.itemsToCancel.add(R.id.deviceValue);
        }
    }

    private void updateRoomName(final DsRoom room) {
        final TextView roomTv = (TextView) findViewById(R.id.roomName);
        roomTv.setText(room.get_name());
    }

    private void updateOuputMode(final DsDevice device) {
        if (device.get_groupNumber() == Groups.LIGHT && hasOutputModes(device)) {
            findViewById(R.id.outputModeLayout).setVisibility(View.VISIBLE);
            final TextView roomTv = (TextView) findViewById(R.id.outputMode);
            final OutputMode mode = device.get_outputMode();
            if (mode != null) {
                roomTv.setText(DssMappings.OUTPUT_MODES_TO_NAME_RES.get(mode));
            }
            if (!device.canModifyOutputMode()) {
                findViewById(R.id.outputModeChangeButton).setVisibility(View.INVISIBLE);
            }
        }
    }

    private void updateDeviceName(final String newDeviceName) {
        deviceName = newDeviceName;

        final TextView deviceTv = (TextView) findViewById(R.id.deviceName);
        deviceTv.setText(newDeviceName);

        final TextView titleTv = (TextView) findViewById(R.id.titleTextView);
        titleTv.setText("4. " + newDeviceName);
    }

    private void updateDeviceId(final DsDevice device) {
        final TextView deviceIdTv = (TextView) findViewById(R.id.deviceId);
        deviceIdTv.setText(device.get_displayId());
    }

    private void updateMeterName(final DsDevice device) {
        // get the associated meter name
        String meterName = getString(R.string.not_available);
        for (final DsMeter meter : MetersCache.get_meters(true)) {
            if (meter.get_dsid().equals(device.get_dsmId()) || meter.get_dsuid().equals(device.get_dsmUid())) {
                meterName = meter.get_name();
            }
        }
        final TextView meterNameTv = (TextView) findViewById(R.id.meterText);
        meterNameTv.setText(getString(R.string.meter) + " " + meterName);
    }

    private void updateEntryBackgrounds() {
        boolean toggle = false;
        final ViewGroup viewGroup = (ViewGroup) findViewById(android.R.id.list);
        // skip title, start at second child
        for (int i = 1; i < viewGroup.getChildCount(); i++) {
            final View entry = viewGroup.getChildAt(i);
            if (entry != null && entry.getVisibility() == View.VISIBLE) {
                if (toggle) {
                    entry.setBackgroundResource(R.drawable.conf_list_white);
                } else {
                    entry.setBackgroundResource(R.drawable.conf_list_grey);
                }
                toggle = !toggle;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // Unregister for events
        App.eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register for events
        App.eventBus.register(this);

        GAHelper.sendScreenViewEvent("Settings Devices Details");
    }

    @Override
    public void onBackPressed() {
        if (hasChanged) {
            setResult(RESULT_OK);
        }
        cancelValueLoading();
        finish();

    }

    private void cancelValueLoading() {
        for (final int viewId : Holder.itemsToCancel) {
            final View view = findViewById(viewId);
            if (view == null) {
                continue;
            }
            final Holder<?> holder = (Holder<?>) view.getTag();
            if (holder == null) {
                continue;
            }
            holder.initializer.cancel(true);
        }
    }

    // callback defined in xml
    public void goBackClick(final View V) {
        onBackPressed();
    }

    // callback defined in xml
    public void blinkDeviceClick(final View V) {
        final DsDevice device = getDevice();
        DssService.blinkDevice(getApplicationContext(), device);
    }

    // callback defined in xml
    public void assignRoomClick(final View V) {
        final Intent myIntent = new Intent(this, DeviceSettingsAssignRoom.class);
        myIntent.putExtra(DeviceSettingsAssignRoom.EXTRA_DEVICE_ID, deviceId);
        myIntent.putExtra(DeviceSettingsAssignRoom.EXTRA_ROOM_NUMBER, roomId);
        this.startActivityForResult(myIntent, REQUEST_ROOM_ASSIGNMENT);
    }

    // callback defined in xml
    public void changeDeviceNameClick(final View V) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.edit_name);

        final LinearLayout dialogLayout = new LinearLayout(this);

        // Set an EditText view to get username input
        final EditText inputName = new EditText(this);
        inputName.setText(deviceName);
        inputName.setSelection(inputName.getText().length());

        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        dialogLayout.addView(inputName);

        alert.setView(dialogLayout);

        alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int whichButton) {

                App.hideIME(inputName);

                final String tempNewDeviceName = inputName.getText().toString().trim();

                // change name
                Log.i("specificDevice", "changing device " + deviceName + " to name: " + tempNewDeviceName);
                final DsDevice device = getDevice();
                DssService.changeDeviceName(getApplicationContext(), device, tempNewDeviceName);
            }
        });

        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int whichButton) {
                // do nothing
            }
        });

        alert.show();
    }

    // callback defined in xml
    public void changeOutputModeClick(final View V) {
        if (!getDevice().canModifyOutputMode()) {
            return;
        }
        final Intent myIntent = new Intent(this, DeviceSettingsChangeOutputMode.class);
        myIntent.putExtra(DeviceSettingsAssignRoom.EXTRA_DEVICE_ID, deviceId);
        myIntent.putExtra(DeviceSettingsAssignRoom.EXTRA_ROOM_NUMBER, roomId);
        this.startActivityForResult(myIntent, REQUEST_CHANGE_OUTPUT_MODE);
    }

    // callback defined in xml
    public void changeMaxPowerClick(final View V) {
        if (getDevice().isOemDevice()) {
            return;
        }
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.edit_max_power);

        final LinearLayout dialogLayout = new LinearLayout(this);

        // Set an EditText view to get max power input
        final EditText inputName = new EditText(this);
        inputName.setText("" + (maxPower != null ? maxPower : ""));
        inputName.setSelection(inputName.getText().length());

        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        dialogLayout.addView(inputName);

        alert.setView(dialogLayout);

        alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int whichButton) {

                App.hideIME(inputName);

                final String tempNewMaxPower = inputName.getText().toString().trim();

                // change name
                Log.i("specificDevice", "changing max power to: " + tempNewMaxPower);
                final DsDevice device = getDevice();
                if (Strings.isNullOrEmpty(tempNewMaxPower)) {
                    // delete tag
                    TaggingService.deleteTagMaxPower(device, getTag(DsTag.TAG_MAX_POWER));
                } else {
                    TaggingService.setTagMaxPower(tempNewMaxPower, device); // use dsid for cloud calls
                }
            }
        });

        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int whichButton) {
                // do nothing
            }
        });

        alert.show();
    }

    // callback defined in xml
    public void changeSocketTypeClick(final View V) {
        if (getDevice().isOemDevice()) {
            return;
        }
        final Intent intent = new Intent(this, DeviceSettingsChangeSocketType.class);
        intent.putExtra(DeviceSettingsChangeSocketType.EXTRA_DEVICE_ID, deviceId);
        intent.putExtra(DeviceSettingsChangeSocketType.EXTRA_ROOM_NUMBER, roomId);
        intent.putExtra(DeviceSettingsChangeSocketType.EXTRA_SOCKET_TYPE, socketType);
        intent.putExtra(DeviceSettingsChangeSocketType.EXTRA_TAG, getTag(DsTag.TAG_SOCKET_TYPE));
        this.startActivityForResult(intent, REQUEST_CHANGE_SOCKET_TYPE);
    }

    // callback defined in xml
    public void deviceInfoClick(final View V) {
        final DsTag tag = getTag(DsTag.TAG_INFO_PAGE_URL);
        if (tag != null) {
            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tag.value));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            GAHelper.sendTaggingButtonClicked("device_info");
        }
    }

    // callback defined in xml
    public void serviceInfoClick(final View V) {
        final DsTag tag = getTag(DsTag.TAG_SERVICE_PAGE_URL);
        if (tag != null) {
            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tag.value));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            GAHelper.sendTaggingButtonClicked("service_info");
        }
    }

    // callback defined in xml
    public void illuminantCheckClick(final View V) {
        startIlluminantCheck(false);
        GAHelper.sendTaggingButtonClicked("illuminant_check");
    }

    // callback defined in xml
    public void transferDimmingCurveClick(final View V) {
        startIlluminantCheck(true);
        GAHelper.sendTaggingButtonClicked("transfer_dimming_curve");
    }

    private void startIlluminantCheck(final boolean isTransferDimmingCurveDialog) {
        final Intent intent = new Intent(Scan.ACTION);
        intent.putExtra(Scan.MODE, Scan.PRODUCT_MODE);
        intent.putExtra(Scan.PROMPT_MESSAGE, getString(R.string.illuminant_check));
        if (isTransferDimmingCurveDialog) {
            startActivityForResult(intent, REQUEST_TRANSFER_DIMMING_CURVE);
        } else {
            startActivityForResult(intent, REQUEST_CHECK_ILLUMINANT);
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case REQUEST_ROOM_ASSIGNMENT: {
                if (resultCode == RESULT_OK) {
                    hasChanged = true;

                    // update assigned room
                    final DsRoom oldRoom = RoomsStore.get_room_by_id(roomId);
                    roomId = data.getIntExtra(EXTRA_ROOM_ID, roomId);
                    final DsRoom room = RoomsStore.get_room_by_id(roomId);
                    updateRoomName(room);

                    // manually move the device in the cached list until it gets updated asynchronously
                    final DsDevice device = oldRoom.get_device_by_id(deviceId);
                    oldRoom.get_devices().remove(device);
                    room.get_devices().add(device);
                }
                break;
            }
            case REQUEST_CHANGE_OUTPUT_MODE: {
                if (resultCode == RESULT_OK) {
                    hasChanged = true;

                    // update output mode
                    final OutputMode newMode = (OutputMode) data.getSerializableExtra(DeviceSettingsChangeOutputMode.RESULT_EXTRA_OUTPUT_MODE);
                    final TextView roomTv = (TextView) findViewById(R.id.outputMode);
                    if (newMode != null) {
                        roomTv.setText(DssMappings.OUTPUT_MODES_TO_NAME_RES.get(newMode));
                    }

                    final DsDevice device = getDevice();
                    device.set_outputMode(newMode);

                    reloadFields();
                }
                break;
            }
            case REQUEST_CHANGE_SOCKET_TYPE: {
                if (resultCode == RESULT_OK) {
                    hasChanged = true;
                    updateTags(getDevice());
                }
                break;
            }
            case REQUEST_CHECK_ILLUMINANT: {
                if (resultCode == RESULT_OK) {
                    final String contents = data.getStringExtra("SCAN_RESULT");
                    final String format = data.getStringExtra("SCAN_RESULT_FORMAT");
                    Log.d(TAG, "scanned ean: format: " + format + ", content: " + contents);
                    startCheckIlluminantDialog(deviceId, contents, false);
                    GAHelper.sendBarcodeScanned();
                }
                break;
            }
            case REQUEST_TRANSFER_DIMMING_CURVE: {
                if (resultCode == RESULT_OK) {
                    final String contents = data.getStringExtra("SCAN_RESULT");
                    final String format = data.getStringExtra("SCAN_RESULT_FORMAT");
                    Log.d(TAG, "scanned ean: format: " + format + ", content: " + contents);
                    startCheckIlluminantDialog(deviceId, contents, true);
                }
                break;
            }

        }
    }

    @SuppressLint("InflateParams")
    private void startCheckIlluminantDialog(final String deviceId, final String ean, final boolean isTransferDimmingCurveDialog) {
        final DsDevice device = getDevice();
        final View view = LayoutInflater.from(this).inflate(R.layout.settings_device_details_scanning_ean, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.settings_device_check_illuminant_title);
        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.settings_device_details_button_scan_again, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                startIlluminantCheck(isTransferDimmingCurveDialog);
                dialog.dismiss();
            }
        });
        builder.setNeutralButton(R.string.settings_device_details_button_transfer_dimming_curve, null);
        dialog = builder.create();
        dialog.show();

        final View progress = dialog.findViewById(R.id.check_result_progress);
        final View indicator = dialog.findViewById(R.id.check_result_indicator);
        final TextView message = (TextView) dialog.findViewById(R.id.ean_message);
        final Button button = dialog.getButton(DialogInterface.BUTTON_NEUTRAL);

        progress.setVisibility(View.VISIBLE);
        indicator.setVisibility(View.INVISIBLE);

        // Override default click listener as it would always close the dialog on click
        dialog.getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                dialog.setCancelable(false);
                DimWizardService.transferDimmingCurve(device, ean);
                progress.setVisibility(View.VISIBLE);
                indicator.setVisibility(View.INVISIBLE);
                dialog.setTitle(R.string.settings_device_details_initializing_dimming_curve_transfer_title);
                message.setText(R.string.settings_device_details_initializing_dimming_curve_transfer_message);
            }
        });
        dialog.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
                dialog = null;
            }
        });

        message.setText(R.string.settings_device_details_searching);
        button.setVisibility(View.GONE);

        MaterialMasterService.checkIlluminant(device, ean, isTransferDimmingCurveDialog);
    }

    @Subscribe
    public void onEvent(final CheckIlluminantResultEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Button button = dialog.getButton(DialogInterface.BUTTON_NEUTRAL);
                final TextView message = (TextView) dialog.findViewById(R.id.ean_message);
                final View progress = dialog.findViewById(R.id.check_result_progress);
                final ImageView indicator = (ImageView) dialog.findViewById(R.id.check_result_indicator);

                if (event.isSuccess()) {
                    if (JSONConstants.FITTING_CHECK_FITS.equals(event.fittingCheckResult)) {
                        dialog.setTitle(R.string.settings_deivce_details_illuminant_ok_title);
                        message.setText(R.string.settings_deivce_details_illuminant_ok_message);
                    } else if (JSONConstants.FITTING_CHECK_NO_FIT_SOCKET_TYPE.equals(event.fittingCheckResult)) {
                        dialog.setTitle(R.string.settings_deivce_details_illuminant_fail_wrong_socket_title);
                        message.setText(R.string.settings_deivce_details_illuminant_fail_wrong_socket_message);
                    } else if (JSONConstants.FITTING_CHECK_NO_FIT_MAX_POWER.equals(event.fittingCheckResult)) {
                        dialog.setTitle(R.string.settings_deivce_details_illuminant_fail_wrong_power_title);
                        message.setText(R.string.settings_deivce_details_illuminant_fail_wrong_power_message);
                    } else if (JSONConstants.FITTING_CHECK_NO_FIT_MAX_POWER_SOCKET_TYPE.equals(event.fittingCheckResult)) {
                        dialog.setTitle(R.string.settings_deivce_details_illuminant_fail_wrong_power_title);
                        message.setText(R.string.settings_deivce_details_illuminant_fail_wrong_power_message);
                    } else if (JSONConstants.FITTING_CHECK_NO_CHECK_MISSING_DATA.equals(event.fittingCheckResult)) {
                        dialog.setTitle(R.string.settings_deivce_details_illuminant_device_not_found_title);
                        message.setText(R.string.settings_deivce_details_illuminant_device_not_found_message);
                    } else if (JSONConstants.FITTING_CHECK_NO_CHECK_MISSING_ILLUMINANT_DATA.equals(event.fittingCheckResult)) {
                        dialog.setTitle(R.string.settings_deivce_details_illuminant_illuminant_not_found_title);
                        message.setText(R.string.settings_deivce_details_illuminant_illuminant_not_found_message);
                    }

                    if (event.illuminantDimmingResult != null) {
                        if (event.isTransferDimmingCurveDialog && JSONConstants.FITTING_CHECK_FITS.equals(event.fittingCheckResult)
                                && (JSONConstants.ILLUMINANT_DIMMING_RESULT_GREEN.equals(event.illuminantDimmingResult) || JSONConstants.ILLUMINANT_DIMMING_RESULT_YELLOW.equals(event.illuminantDimmingResult))) {
                            button.setVisibility(View.VISIBLE);
                        } else {
                            button.setVisibility(View.GONE);
                        }
                        if (JSONConstants.ILLUMINANT_DIMMING_RESULT_GREEN.equals(event.illuminantDimmingResult)) {
                            indicator.setImageResource(R.drawable.check_ok);
                        } else if (JSONConstants.ILLUMINANT_DIMMING_RESULT_YELLOW.equals(event.illuminantDimmingResult)) {
                            indicator.setImageResource(R.drawable.check_notok);
                        } else if (JSONConstants.ILLUMINANT_DIMMING_RESULT_RED.equals(event.illuminantDimmingResult)) {
                            indicator.setImageResource(R.drawable.check_fail);
                        } else {
                            indicator.setImageResource(R.drawable.check_fail);
                        }
                    }
                } else {
                    dialog.setTitle(R.string.settings_deivce_details_illuminant_general_error_title);
                    message.setText(R.string.settings_deivce_details_illuminant_general_error_message);
                    indicator.setImageResource(R.drawable.check_fail);
                }

                progress.setVisibility(View.INVISIBLE);
                indicator.setVisibility(View.VISIBLE);
                dialog.setCancelable(true);
            }
        });

    }

    public void setTags(final List<DsTag> newTags) {
        tags.clear();
        if (newTags == null) {
            return;
        }
        for (final DsTag tag : newTags) {
            tags.put(tag.propertyShortName, tag);
        }
    }

    public DsTag getTag(final String key) {
        return tags.get(key);
    }

    public boolean hasTag(final String key) {
        return tags.get(key) != null;
    }
}
