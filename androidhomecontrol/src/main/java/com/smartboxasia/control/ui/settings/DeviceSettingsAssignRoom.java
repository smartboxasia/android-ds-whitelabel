package com.smartboxasia.control.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.smartboxasia.control.App;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.events.DeviceReasignedEvent;
import com.smartboxasia.control.events.RoomsChangedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.google.common.base.Strings;
import com.google.common.eventbus.Subscribe;

public class DeviceSettingsAssignRoom extends ConnectivityMonitoringFragmentActivity {

    private static final String TAG = DeviceSettingsAssignRoom.class.getSimpleName();

    public static final String EXTRA_DEVICE_ID = "com.smartboxasia.sia.deviceId";
    public static final String EXTRA_ROOM_NUMBER = "com.smartboxasia.sia.roomNumber";

    private DsRoom currentRoom;
    private String currentDeviceId;

    @Subscribe
    public void onEvent(final RoomsChangedEvent event) {
        DssService.updateActivties(getApplicationContext());
        DssService.updateRooms(getApplicationContext());
    }

    @Subscribe
    public void onEvent(final DeviceReasignedEvent event) {
        if (event.isSuccess()) {
            final Intent data = new Intent();
            data.putExtra(DeviceSettingsDetails.EXTRA_ROOM_ID, event.getNewRoomId());
            setResult(RESULT_OK, data);
            Toast.makeText(getApplicationContext(), getString(R.string.settings_device_details_reassign_success), Toast.LENGTH_SHORT).show();
        } else {
            setResult(RESULT_CANCELED);
            Toast.makeText(getApplicationContext(), getString(R.string.settings_device_details_reassign_fail), Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_device_details_assign_room);

        ((TextView) findViewById(R.id.titleTextView)).setText(R.string.settings_device_assign_room_title);

        if (getIntent().hasExtra(EXTRA_DEVICE_ID)) {
            currentDeviceId = getIntent().getStringExtra(EXTRA_DEVICE_ID);
        }
        int roomId = -1;
        if (getIntent().hasExtra(EXTRA_ROOM_NUMBER)) {
            roomId = getIntent().getIntExtra(EXTRA_ROOM_NUMBER, -1);
        }

        if (roomId < 0 || Strings.isNullOrEmpty(currentDeviceId)) {
            Log.e(TAG, "Unknown room or device defined");
            // TODO let the user know about this.
            finish();
        }
        currentRoom = RoomsStore.get_room_by_id(roomId);
        final int currentRoomId = currentRoom.get_id();

        final ViewGroup list = (ViewGroup) findViewById(android.R.id.list);
        for (final DsRoom room : RoomsStore.get_roomsSorted()) {
            createView(list, room);
        }
        updateCheckedMode(list, currentRoomId);

        updateEntryBackgrounds();
    }

    private void updateEntryBackgrounds() {
        boolean toggle = false;
        final ViewGroup viewGroup = (ViewGroup) findViewById(android.R.id.list);
        // skip spacer, start at second child
        for (int i = 1; i < viewGroup.getChildCount(); i++) {
            final View entry = viewGroup.getChildAt(i);
            if (entry != null && entry.getVisibility() == View.VISIBLE) {
                if (toggle) {
                    entry.setBackgroundResource(R.drawable.conf_list_white);
                } else {
                    entry.setBackgroundResource(R.drawable.conf_list_grey);
                }
                toggle = !toggle;
            }
        }
    }

    private void createView(final ViewGroup parent, final DsRoom room) {
        final View view = LayoutInflater.from(this).inflate(R.layout.settings_device_details_assign_room_item, parent, false);
        ((TextView) view.findViewById(R.id.output_mode_name)).setText(room.get_name());
        view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                view.findViewById(R.id.output_mode_check).setVisibility(View.GONE);
                view.findViewById(R.id.output_mode_progress).setVisibility(View.VISIBLE);
                final DsDevice device = currentRoom.get_device_by_id(currentDeviceId);
                DssService.assignDeviceToZone(getApplicationContext(), device, room.get_id());
            }
        });
        view.setTag(room.get_id());
        parent.addView(view);
    }

    private void updateCheckedMode(final ViewGroup list, final int currentRoomId) {
        for (int i = 1; i < list.getChildCount(); i++) {
            final View view = list.getChildAt(i);
            final Integer roomId = (Integer) view.getTag();
            view.findViewById(R.id.output_mode_check).setVisibility(roomId == currentRoomId ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // Unregister for events
        App.eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register for events
        App.eventBus.register(this);

        GAHelper.sendScreenViewEvent("Settings Devices Assign Room");
    }

    public void goBackClick(final View view) {
        onBackPressed();
    }
}
