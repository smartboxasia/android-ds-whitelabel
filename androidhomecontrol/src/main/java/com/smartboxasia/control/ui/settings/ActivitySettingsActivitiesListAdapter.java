package com.smartboxasia.control.ui.settings;

import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartboxasia.control.R;
import com.smartboxasia.control.dto.DsScene;
import com.smartboxasia.control.ui.helper.SceneIconHelper;

final class ActivitySettingsActivitiesListAdapter extends BaseAdapter {

    private ActivitySettingsActivities context;
    private List<DsScene> scenes;

    public ActivitySettingsActivitiesListAdapter(final ActivitySettingsActivities context, final List<DsScene> scenes) {
        this.context = context;
        this.scenes = scenes;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.settings_list_item, parent, false);
        }

        final DsScene scene = scenes.get(position);

        ((TextView) convertView.findViewById(R.id.settingsTextView)).setText(scene.get_name());

        if (position % 2 == 0) {
            convertView.setBackgroundResource(R.drawable.conf_list_grey);
        } else {
            convertView.setBackgroundResource(R.drawable.conf_list_white);
        }

        final ImageView iconView = (ImageView) convertView.findViewById(R.id.icon);
        iconView.setImageResource(SceneIconHelper.getIcon(scene));

        if (context.apiCallingActionIdentifier != null || context.isLookingForFavorite) {
            convertView.findViewById(R.id.selectButton).setVisibility(View.INVISIBLE);
        }

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                context.sceneSelected(scene);
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        return scenes.size();
    }

    @Override
    public Object getItem(final int position) {
        return scenes.get(position);
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }
}
