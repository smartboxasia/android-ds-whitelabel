package com.smartboxasia.control.ui.helper;

import java.text.DecimalFormat;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.smartboxasia.control.dto.DsMeter;

public class PieChartView extends View {
    private static final int WAIT = 0;
    private static final int IS_READY_TO_DRAW = 1;
    private static final int IS_DRAW = 2;
    private static final float START_INC = 30;
    private Paint mBgPaints = new Paint();
    private Paint mLinePaints = new Paint();
    private Paint mTextPaints = new Paint();
    private int mWidth;
    private int mHeight;
    private int mGapLeft;
    private int mGapRight;
    private int mGapTop;
    private int mGapBottom;
    private int mState = WAIT;
    private float mStart;
    private float mSweep;
    private int mMaxConnection;
    private List<DsMeter> mDataArray;

    // instantiate Objects used in draw loop
    private RectF mOval = new RectF(0, 0, 1, 1);
    private DecimalFormat floatFormatter = new DecimalFormat("0.# %");
    private Rect bounds = new Rect();

    public static int getColor(final int i) {
        return 0xff000000 + 256 * 256 * (((i + 1) * 97) % 256) + 256 * (((i + 1) * 141) % 256) + (((i + 1) * 67) % 256);
    }

    public PieChartView(final Context context) {
        super(context);
        init();
    }

    public PieChartView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mBgPaints.setAntiAlias(true);
        mBgPaints.setStyle(Paint.Style.FILL);
        mBgPaints.setColor(0x88FF0000);
        mBgPaints.setStrokeWidth(0.5f);
        mLinePaints.setAntiAlias(true);
        mLinePaints.setStyle(Paint.Style.STROKE);
        mLinePaints.setColor(0xFF000000);
        mLinePaints.setStrokeWidth(0.5f);
        mTextPaints.setAntiAlias(true);
        mTextPaints.setColor(0xFF000000);
        mTextPaints.setStrokeWidth(0.8f);
        mTextPaints.setTextSize(20);
    }

    // --------------------------------------------------------------------------------------
    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        // ------------------------------------------------------
        if (mState != IS_READY_TO_DRAW) {
            return;
        }
        canvas.drawColor(0, PorterDuff.Mode.CLEAR);

        // ------------------------------------------------------
        mStart = START_INC;
        for (final DsMeter meter : mDataArray) {
            mBgPaints.setColor(getColor(mDataArray.indexOf(meter)));
            mSweep = (mMaxConnection == 0 ? 360 : 360 * ((float) meter.get_consumption() / mMaxConnection));
            canvas.drawArc(mOval, mStart, mSweep, true, mBgPaints);
            canvas.drawArc(mOval, mStart, mSweep, true, mLinePaints);
            mStart += mSweep;
        }
        // ------------------------------------------------------------
        // Draw Legend on Pie
        // ------------------------------------------------------------
        mStart = START_INC;
        float lblX;
        float lblY;
        String LblPercent;
        float Percent;
        final float CenterOffset = (mWidth / 2); // Pie Center from Top-Left origin
        final float Conv = (float) (2 * Math.PI / 360); // Constant for convert Degree to rad.
        final float Radius = 3 * (mWidth / 2) / 4; // Radius of the circle will be drawn the legend.
        for (final DsMeter meter : mDataArray) {
            Percent = (mMaxConnection == 0 ? 1 : (float) meter.get_consumption() / (float) mMaxConnection);
            mSweep = 360 * Percent;
            // Format Label
            LblPercent = floatFormatter.format(Percent);
            // Get Label width and height in pixels
            mTextPaints.getTextBounds(LblPercent, 0, LblPercent.length(), bounds);
            // Claculate final coords for Label
            lblX = (float) (CenterOffset + Radius * Math.cos(Conv * (mStart + mSweep / 2))) - bounds.width() / 2;
            lblY = (float) (CenterOffset + Radius * Math.sin(Conv * (mStart + mSweep / 2))) + bounds.height() / 2;
            // Dwraw Label on Canvas
            mTextPaints.setStyle(Paint.Style.FILL_AND_STROKE);
            if (Percent >= 0.04) {
                canvas.drawText(LblPercent, lblX, lblY, mTextPaints);
            }
            mStart += mSweep;
        }

        // ------------------------------------------------------
        mState = IS_DRAW;
    }

    // --------------------------------------------------------------------------------------
    public void setGeometry(final int width, final int height, final int GapLeft, final int GapRight, final int GapTop, final int GapBottom) {
        mWidth = width;
        mHeight = height;
        mGapLeft = GapLeft;
        mGapRight = GapRight;
        mGapTop = GapTop;
        mGapBottom = GapBottom;

        // update piechart bounds
        mOval.set(mGapLeft, mGapTop, mWidth - mGapRight, mHeight - mGapBottom);
    }

    // --------------------------------------------------------------------------------------

    // --------------------------------------------------------------------------------------
    public void setData(final List<DsMeter> data) {
        mDataArray = data;
        mMaxConnection = 0;
        for (final DsMeter item : data) {
            mMaxConnection += item.get_consumption();
        }
        mState = IS_READY_TO_DRAW;
    }

    // --------------------------------------------------------------------------------------
    public void setState(final int State) {
        mState = State;
    }
}
