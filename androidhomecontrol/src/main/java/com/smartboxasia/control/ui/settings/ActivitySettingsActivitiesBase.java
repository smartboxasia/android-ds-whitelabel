package com.smartboxasia.control.ui.settings;

import static com.smartboxasia.control.intentservice.Service_smartboxasia.ACTION_IDENTIFIER;
import static com.smartboxasia.control.intentservice.Service_smartboxasia.CALLING_PACKAGE;
import static com.smartboxasia.control.intentservice.Service_smartboxasia.RESULT_ACTION;

import java.util.Set;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.smartboxasia.control.App;
import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.FavoriteActivitiesStore;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.domain.LoginResult;
import com.smartboxasia.control.dto.DsScene;
import com.smartboxasia.control.events.ActivitiesChangedEvent;
import com.smartboxasia.control.events.GotUserDefinedActionsEvent;
import com.smartboxasia.control.events.LoginResultEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.google.common.eventbus.Subscribe;

public abstract class ActivitySettingsActivitiesBase extends ConnectivityMonitoringFragmentActivity {

    private static final int REQUEST_ACTIVITIES_DETAILS = ConfigurationScreen.REQUEST_CODE_ACTIVITIES + 10;

    public static final String EXTRA_ROOM_ID = "com.smartboxasia.sia.roomId";
    public static final String EXTRA_NEW_FAVORITE = "com.smartboxasia.sia.activityNewFavorite";

    protected int roomId;

    protected boolean changed = false;

    protected boolean isLookingForFavorite = false;
    protected String apiCallingActionIdentifier = null;
    protected ProgressDialog progressDialog;

    private BaseAdapter adapter;

    public ActivitySettingsActivitiesBase() {
        super();
    }

    @Subscribe
    public void onEvent(final ActivitiesChangedEvent event) {
        updateList();
        if (apiCallingActionIdentifier != null) {
            progressDialog.dismiss();
        }
    }

    @Subscribe
    public void onEvent(final GotUserDefinedActionsEvent event) {
        updateList();
        if (apiCallingActionIdentifier != null) {
            progressDialog.dismiss();
        }
    }

    @Subscribe
    public void onEvent(final LoginResultEvent event) {
        if (event.getResult() == LoginResult.LOGIN_RESULT_OK) {
            DssService.updateActivties(getApplicationContext());
            DssService.updateUserDefinedActions(getApplicationContext());
        }
    }

    protected abstract BaseAdapter creatAdapter();

    protected BaseAdapter getAdapter() {
        return adapter;
    }

    protected abstract void updateList();

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // api scenario
        try {
            apiCallingActionIdentifier = getIntent().getExtras().getString(ACTION_IDENTIFIER);
            final String _apiCallerIdentifier = getIntent().getExtras().getString(CALLING_PACKAGE);

            // create a unique action id from calling package + their supplied id
            if (_apiCallerIdentifier != null || apiCallingActionIdentifier != null) {
                apiCallingActionIdentifier = _apiCallerIdentifier + apiCallingActionIdentifier;
            }
        } catch (final Exception e) {
            Log.i("action!!!", "Failed to get calling action intent exception: " + e.getLocalizedMessage());
        }

        try {
            isLookingForFavorite = getIntent().getExtras().getBoolean(EXTRA_NEW_FAVORITE, false);
            roomId = getIntent().getExtras().getInt(EXTRA_ROOM_ID, DssConstants.Rooms.UNKNOWN);
        } catch (final Exception e) {
            // not important
        }

        adapter = creatAdapter();
    }

    @Override
    public void onPause() {
        super.onPause();

        // Unregister for events
        App.eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register for events
        App.eventBus.register(this);

        // set title to "choose activity"
        if (isLookingForFavorite || apiCallingActionIdentifier != null) {
            final TextView titleTv = (TextView) findViewById(R.id.titleTextView);
            titleTv.setText(R.string.activities_choose);
        }

        updateList();
    }

    @Override
    public void onBackPressed() {
        if (changed) {
            setResult(RESULT_OK);
        }
        finish();
    }

    // callback defined in xml
    public void goBackClick(final View V) {
        onBackPressed();
    }

    protected void sceneSelected(final DsScene scene) {
        try {

            if (apiCallingActionIdentifier != null) {
                // another app is looking for a ds action

                String tokenizedScene = "";
                tokenizedScene += Connection.getActiveConnectionData().getUrl();
                tokenizedScene += "||";
                tokenizedScene += Connection.getActiveConnectionData().getApplicationToken();
                tokenizedScene += "||";
                tokenizedScene += scene.get_roomId();
                tokenizedScene += "||";
                tokenizedScene += scene.get_sceneId();
                tokenizedScene += "||";
                tokenizedScene += scene.get_groupNumber();

                Log.i("SceneData", tokenizedScene);

                // persist it in sharedPreferences
                final SharedPreferences preferences = getSharedPreferences("ExternalAppActions", MODE_PRIVATE);
                final SharedPreferences.Editor editor1 = preferences.edit();
                editor1.putString(apiCallingActionIdentifier, tokenizedScene);
                editor1.commit();

                // finish with OK
                final Intent resultIntent = new Intent();
                resultIntent.putExtra(RESULT_ACTION, scene.get_name());
                setResult(RESULT_OK, resultIntent);
                finish();
            } else {
                if (isLookingForFavorite) {

                    // We're adding a new favorite. Update the favorite list and finish.
                    final Set<DsScene> favoriteActivities = FavoriteActivitiesStore.get_favoriteActivities();
                    favoriteActivities.add(scene);
                    FavoriteActivitiesStore.set_favoriteActivities(favoriteActivities);

                    setResult(ActivitySettingsRooms.RESULT_FAVORITE_ADDED);
                    finish();
                } else {
                    showDetailsActivity(scene);
                }
            }

        } catch (final Exception e) {
            // we got a null pointer... just make them try again (ie do nothing)
            e.printStackTrace();
        }
    }

    private void showDetailsActivity(final DsScene scene) {

        final Intent intent = new Intent(this, ActivitySettingsDetails.class);
        intent.putExtra(ActivitySettingsDetails.EXTRA_ROOM_ID, scene.get_roomId());
        intent.putExtra(ActivitySettingsDetails.EXTRA_GROUP_NUMBER, scene.get_groupNumber());
        intent.putExtra(ActivitySettingsDetails.EXTRA_SCENE_ID, scene.get_sceneId());
        intent.putExtra(ActivitySettingsDetails.EXTRA_SCENE_NAME, scene.get_name());

        this.startActivityForResult(intent, REQUEST_ACTIVITIES_DETAILS);
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == REQUEST_ACTIVITIES_DETAILS) {
            if (resultCode == RESULT_OK) {
                changed = true;
                updateList();
            }
        }
    }
}
