package com.smartboxasia.control.ui.helper;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class CancelClickListener implements OnClickListener {

    @Override
    public void onClick(final DialogInterface dialog, final int which) {
        dialog.dismiss();
    }

}
