package com.smartboxasia.control.ui.settings;

import java.util.List;
import java.util.Set;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smartboxasia.control.App;
import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.DssConstants.Scenes;
import com.smartboxasia.control.GAHelper;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.FavoriteActivitiesStore;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.config.app.ShownScenesStore;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.dto.DsScene;
import com.smartboxasia.control.events.SceneRenamedEvent;
import com.smartboxasia.control.ui.ConnectivityMonitoringFragmentActivity;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.eventbus.Subscribe;

public class ActivitySettingsDetails extends ConnectivityMonitoringFragmentActivity {

    public static final String EXTRA_ROOM_ID = "com.smartboxasia.sia.roomId";
    public static final String EXTRA_GROUP_NUMBER = "com.smartboxasia.sia.groupNumber";
    public static final String EXTRA_SCENE_ID = "com.smartboxasia.sia.sceneId";
    public static final String EXTRA_SCENE_NAME = "com.smartboxasia.sia.sceneName";

    private static final int REQUEST_ACTIVITIES_CONFIGURATION = ConfigurationScreen.REQUEST_CODE_ACTIVITIES + 20;

    private boolean hasChangedSceneName = false;
    private CheckBox showInFavoritesCheckBox;
    private CheckBox showInActivitiesCheckBox;

    private DsScene scene;

    @Subscribe
    public void onEvent(final SceneRenamedEvent event) {
        if (event.isSuccess()) {
            hasChangedSceneName = true;
            scene.set_name(event.getNewName());

            // add scene to known scenes store if not yet present
            if (!RoomsStore.get_room_by_id(scene.get_roomId()).get_scenes().contains(scene)) {
                RoomsStore.get_room_by_id(scene.get_roomId()).get_scenes().add(scene);
            }

            // update the name used in the favorite list, if it's in there
            if (showInFavoritesCheckBox.isChecked()) {
                synchronized (FavoriteActivitiesStore.class) {
                    final Set<DsScene> favoriteActivities = FavoriteActivitiesStore.get_favoriteActivities();
                    for (final DsScene favScene : favoriteActivities) {
                        if (favScene.equals(scene)) {
                            favScene.set_name(scene.get_name());
                        }
                    }
                    FavoriteActivitiesStore.set_favoriteActivities(favoriteActivities);
                }
            }

            // update the name in the shownActivities list, if it's in there
            if (showInActivitiesCheckBox.isChecked()) {
                for (final DsScene shownScene : ShownScenesStore.get_shownScenesForRoomAndGroup(scene.get_roomId(), scene.get_groupNumber())) {
                    if (shownScene.equals(scene)) {
                        shownScene.set_name(scene.get_name());
                    }
                }
            }

            // update gui
            updateSceneName();
            Toast.makeText(getApplicationContext(), getString(R.string.rename_success), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.rename_fail), Toast.LENGTH_SHORT).show();
        }
    }

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activities_details);

        final int roomId = getIntent().getExtras().getInt(EXTRA_ROOM_ID);
        final long sceneId = getIntent().getExtras().getLong(EXTRA_SCENE_ID);
        final String sceneName = getIntent().getExtras().getString(EXTRA_SCENE_NAME);
        final int groupNumber = getIntent().getExtras().getInt(EXTRA_GROUP_NUMBER);

        scene = new DsScene(sceneName, sceneId, groupNumber, roomId);

        updateRoomName();

        updateSceneName();

        updateShowInFavoritesCheckBox();

        updateShowInScenesCheckBox();

        updateEditSceneButton();
    }

    private void updateRoomName() {
        String roomName;

        // can't edit name if it's a global activity
        if (DssConstants.Rooms.isGlobal(scene.get_roomId())) {
            final ImageView iv = (ImageView) findViewById(R.id.sceneNameEditButton);
            iv.setVisibility(View.GONE);
            final RelativeLayout rl = (RelativeLayout) findViewById(R.id.sceneNameLayout);
            rl.setClickable(false);
            roomName = getString(R.string.global);
        } else {
            roomName = RoomsStore.get_room_by_id(scene.get_roomId()).get_name();
        }

        final TextView roomTv = (TextView) findViewById(R.id.roomName);
        roomTv.setText(roomName);
    }

    private void updateSceneName() {
        final TextView titleTv = (TextView) findViewById(R.id.titleTextView);
        titleTv.setText("3. " + scene.get_name());

        final TextView activityTv = (TextView) findViewById(R.id.sceneNameText);
        activityTv.setText(scene.get_name());
    }

    private void updateShowInFavoritesCheckBox() {
        showInFavoritesCheckBox = (CheckBox) findViewById(R.id.showInFavoritesCheckBox);

        // check if present in the favorites
        if (FavoriteActivitiesStore.get_favoriteActivities().contains(scene)) {
            showInFavoritesCheckBox.setChecked(true);
        }
    }

    private void updateShowInScenesCheckBox() {
        if (DssConstants.Rooms.isGlobal(scene.get_roomId()) || scene.get_sceneId() == Scenes.OFF) {
            findViewById(R.id.showInActivitiesLayout).setVisibility(View.GONE);
        } else {
            showInActivitiesCheckBox = (CheckBox) findViewById(R.id.showInActivitiesCheckBox);
            final List<DsScene> shownActivities = ShownScenesStore.get_shownScenesForRoomAndGroup(scene.get_roomId(), scene.get_groupNumber());
            showInActivitiesCheckBox.setChecked(shownActivities.contains(scene));
        }
    }

    private void updateEditSceneButton() {
        if (DssConstants.Rooms.isGlobal(scene.get_roomId()) || scene.get_sceneId() == Scenes.OFF) {
            final View editSceneLayout = findViewById(R.id.editSceneLayout);
            editSceneLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // Unregister from events
        App.eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register for events
        App.eventBus.register(this);

        GAHelper.sendScreenViewEvent("Settings Activities Edit Details");
    }

    @Override
    public void onBackPressed() {
        if (hasChangedSceneName) {
            setResult(RESULT_OK);
        }
        finish();
    }

    // callback defined in xml
    public void goBackClick(final View v) {
        onBackPressed();
    }

    // callback defined in xml
    public void showInFavoritesCheckBoxClick(final View V) {
        // opposite because of marking toggled when clicked
        synchronized (FavoriteActivitiesStore.class) {
            final List<DsScene> favoriteActivities = Lists.newArrayList(FavoriteActivitiesStore.get_favoriteActivities());
            if (!showInFavoritesCheckBox.isChecked()) {
                // remove the activity from the favorite list
                favoriteActivities.remove(scene);
            } else {
                // add new favorite
                favoriteActivities.add(scene);
            }
            FavoriteActivitiesStore.set_favoriteActivities(Sets.newLinkedHashSet(favoriteActivities));
        }
    }

    // callback defined in xml
    public void showInActivitiesCheckBoxClick(final View v) {
        synchronized (ShownScenesStore.class) {
            final Set<DsScene> scenes = ShownScenesStore.get_shownScenes();
            if (!showInActivitiesCheckBox.isChecked()) {
                scenes.remove(scene);
            } else {
                scenes.add(scene);
            }
            ShownScenesStore.set_shownScenes(scenes);
        }
    }

    // callback defined in xml
    public void changeActivityNameClick(final View v) {

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.edit_name);

        final LinearLayout dialogLayout = new LinearLayout(this);

        // Set an EditText view to get username input
        final EditText inputName = new EditText(this);
        inputName.setText(scene.get_name());
        inputName.setSelection(inputName.getText().length());

        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        dialogLayout.addView(inputName);

        alert.setView(dialogLayout);

        alert.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int whichButton) {

                App.hideIME(inputName);

                final String tempNewSceneName = inputName.getText().toString().trim();

                // change name
                Log.i("specificActivity", "changing activity " + scene.get_name() + " to name:" + tempNewSceneName);
                DssService.changeActivityName(getApplicationContext(), scene.get_roomId(), scene.get_groupNumber(), scene.get_sceneId(), tempNewSceneName);
            }
        });

        alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int whichButton) {
                // do nothing
            }
        });

        alert.show();
    }

    // callback defined in xml
    public void editSceneClick(final View v) {

        final Intent intent = new Intent(this, ActivitySettingsConfig.class);
        intent.putExtra(ActivitySettingsConfig.EXTRA_ROOM_ID, scene.get_roomId());
        intent.putExtra(ActivitySettingsConfig.EXTRA_SCENE_ID, scene.get_sceneId());
        intent.putExtra(ActivitySettingsConfig.EXTRA_GROUP_NUMBER, scene.get_groupNumber());

        this.startActivityForResult(intent, REQUEST_ACTIVITIES_CONFIGURATION);
    }

    // callback defined in xml
    public void callActivityClick(final View v) {
        DssService.callActivity(getApplicationContext(), scene.get_roomId(), scene.get_groupNumber(), scene.get_sceneId(), false);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == REQUEST_ACTIVITIES_CONFIGURATION) {

        }
    }
}
