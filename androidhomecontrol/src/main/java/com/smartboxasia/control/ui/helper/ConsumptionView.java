package com.smartboxasia.control.ui.helper;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;

public class ConsumptionView extends View {

    private static final int H_MIN = 0; // bad
    private static final int H_MAX = 113; // good
    private static final int H_ERROR = 203; // error
    private static final float S = 1; // saturation 100%
    private static final float V = 0.87f; // VALUE

    private int a = 0;
    private int r = 0;
    private int g = 0;
    private int b = 0;

    public ConsumptionView(final Context context) {
        super(context, null, 0);
    }

    public ConsumptionView(final Context context, final AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public ConsumptionView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setCurrentConsumption(final float consumptionInPersent) {
        float color = H_ERROR; // disabled
        if (consumptionInPersent >= 0) {
            final float inverted = 1 - consumptionInPersent; // much consumption is bad, less is better
            color = (H_MAX - H_MIN) * inverted; // map color range on 0% - 100%
        }

        final float[] hsv = new float[] {
                color, S, V
        };
        final int argb = Color.HSVToColor(0xff, hsv);
        a = Color.alpha(argb);
        r = Color.red(argb);
        g = Color.green(argb);
        b = Color.blue(argb);

        invalidate();
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawARGB(a, r, g, b);
    }
}
