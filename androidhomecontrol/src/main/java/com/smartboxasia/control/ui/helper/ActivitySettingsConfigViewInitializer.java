package com.smartboxasia.control.ui.helper;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.smartboxasia.control.App;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.dss.direct_1_17_0.DssService;
import com.smartboxasia.control.domain.OutputMode;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.ui.settings.ActivitySettingsConfigAdapter.Holder;
import com.google.common.collect.Lists;

public class ActivitySettingsConfigViewInitializer extends AsyncTask<Void, Void, List<Integer>> {

    private Context context;
    private DsDevice device;
    private View convertView;
    private boolean finished = false;

    private int value = Integer.MIN_VALUE;

    public ActivitySettingsConfigViewInitializer(final Context context, final DsDevice device, final View convertView) {
        this.context = context;
        this.device = device;
        this.convertView = convertView;
    }

    @Override
    protected void onPreExecute() {
        final TextView deviceName = (TextView) convertView.findViewById(R.id.device_name);
        final ProgressBar progress = (ProgressBar) convertView.findViewById(R.id.progress);
        final TextView deviceValue = (TextView) convertView.findViewById(R.id.device_value);
        final SeekBar seekbar = (SeekBar) convertView.findViewById(R.id.seekbar);
        final ToggleButton toggle = (ToggleButton) convertView.findViewById(R.id.toggle);

        deviceName.setVisibility(View.VISIBLE);
        deviceName.setText(device.get_name());

        deviceValue.setVisibility(View.GONE);
        seekbar.setVisibility(View.GONE);
        toggle.setVisibility(View.GONE);

        progress.setVisibility(View.VISIBLE);
    }

    @Override
    protected List<Integer> doInBackground(final Void... params) {
        final List<Integer> result = Lists.newArrayList();
        synchronized (App.REQUEST_LOCK) {
            result.add(DssService.getDeviceOuputValueSync(device));
        }
        return result;
    }

    @Override
    protected void onPostExecute(final List<Integer> result) {

        // Check if we're still attached to the view. If not, finish quietly.
        final Holder holder = (Holder) convertView.getTag();
        if (holder.initializer != this) {
            return;
        }

        value = result.get(0);

        finished = true;

        updateView();
    }

    public void attachToView(final View view) {
        convertView = view;
        updateView();
    }

    private void updateView() {

        final TextView deviceName = (TextView) convertView.findViewById(R.id.device_name);
        final ProgressBar progress = (ProgressBar) convertView.findViewById(R.id.progress);
        final TextView deviceValue = (TextView) convertView.findViewById(R.id.device_value);
        final SeekBar seekbar = (SeekBar) convertView.findViewById(R.id.seekbar);
        final ToggleButton toggle = (ToggleButton) convertView.findViewById(R.id.toggle);

        if (!finished) {

            deviceName.setVisibility(View.VISIBLE);
            deviceName.setText(device.get_name());

            deviceValue.setVisibility(View.GONE);
            seekbar.setVisibility(View.GONE);
            toggle.setVisibility(View.GONE);

            progress.setVisibility(View.VISIBLE);
        } else {

            deviceName.setText(device.get_name());

            final OutputMode outputMode = device.get_outputMode();
            if (outputMode == OutputMode.DISABLED) {
                // no output
            } else if (outputMode == OutputMode.DIMMABLE) {
                // dimmable device
                seekbar.setVisibility(View.VISIBLE);
                deviceValue.setVisibility(View.VISIBLE);

                final int val = Math.round(value / 2.55f); // 255 is 100%
                seekbar.setProgress(val);
                deviceValue.setText(context.getString(R.string.percent_pattern, val));
            } else {
                // switchable device
                toggle.setVisibility(View.VISIBLE);
                toggle.setChecked(value > 127); // if value is >127 the value is true
            }

            // register change listeners after the values have been changed
            seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                @Override
                public void onStopTrackingTouch(final SeekBar seekBar) {
                    value = Math.round(seekBar.getProgress() * 2.55f);
                    DssService.setDeviceOutputValue(device, value);
                }

                @Override
                public void onStartTrackingTouch(final SeekBar seekBar) {

                }

                @Override
                public void onProgressChanged(final SeekBar seekBar, final int progress, final boolean fromUser) {
                    deviceValue.setText(context.getString(R.string.percent_pattern, progress));
                }
            });

            toggle.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
                    DssService.setDeviceOutputValue(device, (isChecked ? 255 : 0));
                }
            });

            progress.setVisibility(View.GONE);
        }
    }
}
