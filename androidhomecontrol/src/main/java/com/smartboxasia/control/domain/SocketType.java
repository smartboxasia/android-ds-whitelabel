package com.smartboxasia.control.domain;

public enum SocketType {
    E14,
    E27,
    G4,
    G53,
    G9,
    GU10,
    B15d,
    B22d,
    E10,
    E12,
    E40,
    Fa4,
    G635,
    GU53,
    GX53,
    GY635,
    S14d,
    S14s,
    R7s
}
