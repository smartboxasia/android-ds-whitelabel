package com.smartboxasia.control.domain;

public enum BusMemberType {
    UNKNOWN,
    DSM11,
    DSM12,
    VDSM,
    VDC;
}
