package com.smartboxasia.control.domain;

public enum Capability {
    DEVICES,
    METERING,
    TEMPERATURE_CONTROL
}
