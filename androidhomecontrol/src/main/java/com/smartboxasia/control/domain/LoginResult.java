package com.smartboxasia.control.domain;

public enum LoginResult {
    LOGIN_RESULT_OK,
    LOGIN_RESULT_NOT_OK,
    LOGIN_RESULT_NEED_CREDENTIALS
}
