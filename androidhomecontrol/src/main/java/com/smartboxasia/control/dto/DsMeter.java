package com.smartboxasia.control.dto;

import java.util.Collection;
import java.util.Set;

import com.smartboxasia.control.domain.BusMemberType;
import com.smartboxasia.control.domain.Capability;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;

public class DsMeter {
    private String _dsuid;
    private String _dsid;
    private String displayId;
    private String _name;
    private int _meterReading;
    private int _consumption;
    private boolean _hasDsUid;
    private final Set<Capability> capabilities = Sets.newHashSet();
    private BusMemberType _busMemberType;

    public void set_name(final String name) {
        _name = Strings.nullToEmpty(name);
    }

    public String get_name() {
        return _name;
    }

    public String get_id() {
        return (_hasDsUid ? _dsuid : _dsid);
    }

    public void set_dsuid(final String dsuid) {
        _dsuid = Strings.nullToEmpty(dsuid);
    }

    public String get_dsuid() {
        return _dsuid;
    }

    public void set_dsid(final String dsid) {
        _dsid = Strings.nullToEmpty(dsid);
    }

    public String get_dsid() {
        return _dsid;
    }

    public void set_meterReading(final int meterReading) {
        _meterReading = meterReading;
    }

    public int get_meterReading() {
        return _meterReading;
    }

    public void set_consumption(final int consumption) {
        _consumption = consumption;
    }

    public int get_consumption() {
        return _consumption;
    }

    public void set_capabilities(final Collection<Capability> capabilities) {
        this.capabilities.clear();
        this.capabilities.addAll(capabilities);
    }

    public boolean hasCapability(final Capability capability) {
        return capabilities.contains(capability);
    }

    public boolean get_hasDsuid() {
        return _hasDsUid;
    }

    public void set_hasDsuid(final boolean _hasDsUid) {
        this._hasDsUid = _hasDsUid;
    }

    public BusMemberType get_busMemberType() {
        return _busMemberType;
    }

    public void set_busMemberType(final BusMemberType _busMemberType) {
        this._busMemberType = _busMemberType;
    }

    public void set_displayId(final String displayId) {
        this.displayId = displayId;
    }

    public String get_displayId() {
        return displayId;
    }
}
