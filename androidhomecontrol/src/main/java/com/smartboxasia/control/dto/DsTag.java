package com.smartboxasia.control.dto;

import java.io.Serializable;

public class DsTag implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String TAG_MAX_POWER = "MAX_POWER";
    public static final String TAG_SOCKET_TYPE = "SOCKET_TYPE";
    public static final String TAG_INFO_PAGE_URL = "INFO_PAGE_URL";
    public static final String TAG_SERVICE_PAGE_URL = "SERVICE_PAGE_URL";

    public String irdiPR;
    public String irdiUN;
    public String irdiVA;
    public String propertyPreferredName;
    public String propertyShortName;
    public String unitPreferredName;
    public String unitShortName;
    public String value;
    public String valuePreferredName;
    public String valueShortName;
    public String tagMetaData;

    public DsTag withIrdiPR(final String irdiPR) {
        this.irdiPR = irdiPR;
        return this;
    }

    public DsTag withIrdiUN(final String irdiUN) {
        this.irdiUN = irdiUN;
        return this;
    }

    public DsTag withIrdiVA(final String irdiVA) {
        this.irdiVA = irdiVA;
        return this;
    }

    public DsTag withPropertyPreferredName(final String propertyPreferredName) {
        this.propertyPreferredName = propertyPreferredName;
        return this;
    }

    public DsTag withPropertyShortName(final String propertyShortName) {
        this.propertyShortName = propertyShortName;
        return this;
    }

    public DsTag withUnitPreferredName(final String unitPreferredName) {
        this.unitPreferredName = unitPreferredName;
        return this;
    }

    public DsTag withUnitShortName(final String unitShortName) {
        this.unitShortName = unitShortName;
        return this;
    }

    public DsTag withValue(final String value) {
        this.value = value;
        return this;
    }

    public DsTag withValuePreferredName(final String valuePreferredName) {
        this.valuePreferredName = valuePreferredName;
        return this;
    }

    public DsTag withValueShortName(final String valueShortName) {
        this.valueShortName = valueShortName;
        return this;
    }

    public DsTag withTagMetaData(final String tagMetaData) {
        this.tagMetaData = tagMetaData;
        return this;
    }
}
