package com.smartboxasia.control.dto;

import java.util.Comparator;

import com.google.common.base.Strings;

public class DsScene {

    public static final Comparator<? super DsScene> COMPARATOR_ID = new Comparator<DsScene>() {

        @Override
        public int compare(final DsScene sceneA, final DsScene sceneB) {
            return Long.signum(sceneA.get_sceneId() - sceneB.get_sceneId());
        }

    };

    private String _name; // not part of hashCode or equals
    private long _sceneId;
    private int _groupNumber;
    private int _roomId;

    public DsScene(final String name, final long sceneId, final int groupNumber, final int roomId) {
        set_name(name);
        set_sceneId(sceneId);
        set_groupNumber(groupNumber);
        set_roomId(roomId);
    }

    public void set_name(final String name) {
        _name = Strings.nullToEmpty(name);
    }

    public String get_name() {
        return _name;
    }

    public void set_sceneId(final long sceneId) {
        _sceneId = sceneId;
    }

    public long get_sceneId() {
        return _sceneId;
    }

    public void set_groupNumber(final int groupNumber) {
        _groupNumber = groupNumber;
    }

    public int get_groupNumber() {
        return _groupNumber;
    }

    public void set_roomId(final int roomId) {
        _roomId = roomId;
    }

    public int get_roomId() {
        return _roomId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + _groupNumber;
        result = prime * result + _roomId;
        result = prime * result + (int) (_sceneId ^ (_sceneId >>> 32));
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof DsScene)) {
            return false;
        }
        final DsScene other = (DsScene) obj;
        if (_groupNumber != other._groupNumber) {
            return false;
        }
        if (_roomId != other._roomId) {
            return false;
        }
        if (_sceneId != other._sceneId) {
            return false;
        }
        return true;
    }
}
