package com.smartboxasia.control.dto;

import java.util.Comparator;
import java.util.List;

import com.smartboxasia.control.data.dss.direct_1_17_0.DsDeviceBehavior;
import com.smartboxasia.control.domain.OutputMode;
import com.google.common.base.Strings;

public class DsDevice {

    public static final Comparator<DsDevice> COMPARATOR_ALPHABETICAL = new Comparator<DsDevice>() {
        public int compare(final DsDevice lhs, final DsDevice rhs) {
            return lhs.get_name().compareToIgnoreCase(rhs.get_name());
        };
    };

    public static final Comparator<? super DsDevice> COMPARATOR_GROUP_ALPHABETICAL = new Comparator<DsDevice>() {
        public int compare(final DsDevice lhs, final DsDevice rhs) {
            if (lhs.get_groupNumber() != rhs.get_groupNumber()) {
                return lhs.get_groupNumber() - rhs.get_groupNumber();
            } else {
                return lhs.get_name().compareToIgnoreCase(rhs.get_name());
            }
        };
    };

    private String _dsuid;
    private String _dsid;
    private String _displayId;
    private String _name;
    private boolean _on;
    private OutputMode _outputMode;
    private int _type;
    private String _dsmUid;
    private String _dsmId;
    private int _groupNumber;
    private boolean _hasDsUid;

    private DsDeviceBehavior behavior;

    public DsDevice(final DsDeviceBehavior behavior) {
        this.behavior = behavior;
    }

    public void set_name(final String name) {
        _name = Strings.nullToEmpty(name);
    }

    public String get_name() {
        return _name;
    }

    public String get_id() {
        return (_hasDsUid ? _dsuid : _dsid);
    }

    public void set_dsid(final String dsid) {
        _dsid = dsid;
    }

    public String get_dsid() {
        return _dsid;
    }

    public void set_dsuid(final String dsuid) {
        _dsuid = dsuid;
    }

    public String get_dsuid() {
        return _dsuid;
    }

    public void set_on(final boolean on) {
        _on = on;
    }

    public boolean is_on() {
        return _on;
    }

    public void set_outputMode(final OutputMode outputMode) {
        _outputMode = outputMode;
    }

    public OutputMode get_outputMode() {
        return _outputMode;
    }

    public void set_dsmId(final String dsmId) {
        _dsmId = Strings.nullToEmpty(dsmId);
    }

    public String get_dsmId() {
        return _dsmId;
    }

    public void set_dsmUid(final String dsmUid) {
        _dsmUid = Strings.nullToEmpty(dsmUid);
    }

    public String get_dsmUid() {
        return _dsmUid;
    }

    public void set_groupNumber(final int groupNumber) {
        _groupNumber = groupNumber;
    }

    public int get_groupNumber() {
        return _groupNumber;
    }

    public int get_type() {
        return _type;
    }

    public void set_type(final int _type) {
        this._type = _type;
    }

    public boolean get_hasDsuid() {
        return _hasDsUid;
    }

    public void set_hasDsuid(final boolean _hasDsUid) {
        this._hasDsUid = _hasDsUid;
    }

    public String get_displayId() {
        return _displayId;
    }

    public void set_displayId(final String _displayId) {
        this._displayId = _displayId;
    }

    public List<OutputMode> getAvailableOutputModes() {
        return behavior.getAvailableOutputModes();
    }

    public boolean isOemDevice() {
        return behavior.isOemDevice();
    }

    public boolean canModifyOutputMode() {
        return behavior.canModifyOutputMode();
    }
}
