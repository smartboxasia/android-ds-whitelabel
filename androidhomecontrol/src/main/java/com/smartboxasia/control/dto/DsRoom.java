package com.smartboxasia.control.dto;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class DsRoom {

    private static final long DEFAULT_LAST_CALLED_SCENE = 0l;

    public static final Comparator<? super DsRoom> COMPARATOR_ALPHABETIC = new Comparator<DsRoom>() {
        @Override
        public int compare(final DsRoom lhs, final DsRoom rhs) {
            return lhs.get_name().compareTo(rhs.get_name());
        }
    };

    private int _id;
    private String _name;
    private List<DsDevice> _devices = Lists.newArrayList();
    private final Map<Integer, Long> _lastCalledScenes = Maps.newLinkedHashMap();
    private List<DsScene> _scenes = Lists.newArrayList();

    public DsRoom(final int id, final String name, final List<DsDevice> devices) {
        set_id(id);
        set_name(name);
        set_devices(devices);
    }

    public void set_name(final String name) {
        _name = Strings.nullToEmpty(name);
    }

    public String get_name() {
        return _name;
    }

    public void set_scenes(final List<DsScene> allScenesInRoom) {
        if (allScenesInRoom == null) {
            _scenes = Lists.newArrayList();
        } else {
            _scenes = Lists.newArrayList(allScenesInRoom);
        }
    }

    public List<DsScene> get_scenes() {
        return _scenes;
    }

    public void set_id(final int id) {
        _id = id;
    }

    public int get_id() {
        return _id;
    }

    public void set_devices(final List<DsDevice> devices) {
        if (devices == null) {
            _devices = Lists.newArrayList();
        } else {
            _devices = devices;
        }
        Collections.sort(_devices, DsDevice.COMPARATOR_GROUP_ALPHABETICAL);
    }

    public List<DsDevice> get_devices() {
        return _devices;
    }

    public void set_lastCalledScene(final int group, final long lastCalledScene) {
        if (_lastCalledScenes.containsKey(group)) {
            _lastCalledScenes.put(group, lastCalledScene);
        }
    }

    public long get_lastCalledScene(final int group) {
        if (!_lastCalledScenes.containsKey(group)) {
            _lastCalledScenes.put(group, DEFAULT_LAST_CALLED_SCENE);
        }
        return _lastCalledScenes.get(group);
    }

    public DsDevice get_device_by_id(final String deviceId) {
        for (final DsDevice device : _devices) {
            if (device.get_id().equals(deviceId) || device.get_dsuid().equals(deviceId)) {
                return device;
            }
        }
        return null;
    }

    public DsScene get_scene(final long sceneId, final int groupNumber) {
        for (final DsScene scene : _scenes) {
            if (scene.get_sceneId() == sceneId && scene.get_groupNumber() == groupNumber) {
                return scene;
            }
        }
        return null;
    }
}
