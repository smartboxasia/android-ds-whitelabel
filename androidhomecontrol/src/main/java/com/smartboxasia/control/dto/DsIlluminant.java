package com.smartboxasia.control.dto;

import java.util.List;

public class DsIlluminant {

    public int id;
    public String ean;
    public List<DsDimmingCurve> curves;

    public DsIlluminant withId(int id) {
        this.id = id;
        return this;
    }

    public DsIlluminant widthEAN(String ean) {
        this.ean = ean;
        return this;
    }

    public DsIlluminant withDimmingCurves(List<DsDimmingCurve> curves) {
        this.curves = curves;
        return this;
    }

}
