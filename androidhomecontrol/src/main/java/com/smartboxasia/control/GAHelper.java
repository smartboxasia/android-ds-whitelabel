package com.smartboxasia.control;

import android.widget.Toast;

import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.testenabler.TestEnablerHelper;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.HitBuilders.EventBuilder;

public class GAHelper {

    private static App application;

    static void createTracker(final App app) {
        application = app;
        synchronized (application) {
            final GoogleAnalytics analytics = GoogleAnalytics.getInstance(app);

            int configResId;
            if (TestEnablerHelper.isTestEnablerServiceAvailable(app)) {
                Toast.makeText(application, "Google Analytics running on test tracker", Toast.LENGTH_SHORT).show();
                configResId = R.xml.test_tracker;
            } else {
                configResId = R.xml.production_tracker;
            }
            application.gmsTracker = analytics.newTracker(configResId);

            analytics.setAppOptOut(true);
        }
    }

    public static void setAppOptOut(final boolean optOut) {
        GoogleAnalytics.getInstance(application).setAppOptOut(optOut);
    }

    private static EventBuilder getEventBuilder(final String category, final String action) {
        return new HitBuilders.EventBuilder(category, action);
    }

    private static void sendUIActionEvent(final String action, final String label) {
        final EventBuilder builder = getEventBuilder("ui_action", action);
        if (label != null) {
            builder.setLabel(label);
        }
        application.gmsTracker.send(builder.build());
    }

    public static void sendScreenViewEvent(final String screenName) {
        application.gmsTracker.setScreenName(screenName);
        application.gmsTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public static void sendConnectionTypeEvent(final ConnectionData data) {
        String label;
        switch (data.getType()) {
            case ConnectionData.CONNECTION_TYPE_CLOUD: {
                label = "cloud";
                break;
            }
            case ConnectionData.CONNECTION_TYPE_CLOUD_ONLY: {
                label = "cloud_only";
                break;
            }
            case ConnectionData.CONNECTION_TYPE_LOCAL: {
                label = "local";
                break;
            }
            case ConnectionData.CONNECTION_TYPE_MANUAL: {
                label = "manual";
                break;
            }
            default: {
                label = "unknown";
                break;
            }
        }
        final EventBuilder builder = getEventBuilder("config", "connection_type");
        builder.setLabel(label);
        application.gmsTracker.send(builder.build());
    }

    public static void sendSceneCalledEvent(final String label) {
        sendUIActionEvent("scene_called", label);
    }

    public static void sendFavoriteCalledEvent(final String label) {
        sendUIActionEvent("favorite_called", label);
    }

    public static void sendTaggingButtonClicked(final String label) {
        sendUIActionEvent("tagging_button_clicked", label);
    }

    public static void sendBarcodeScanned() {
        sendUIActionEvent("barcode_scanned", null);
    }

    public static void sendSaveSceneEvent() {
        sendUIActionEvent("scene_saved", null);
    }

    public static void sendrefreshConsumptionClicked() {
        sendUIActionEvent("refresh_device_consumption_clicked", null);
    }
}
