package com.smartboxasia.control.intentservice;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import android.os.Build;

import com.smartboxasia.control.data.helper.SSLSocketFactoryCreator;
import com.turbomanage.httpclient.AsyncCallback;
import com.turbomanage.httpclient.AsyncHttpClient;
import com.turbomanage.httpclient.AsyncRequestExecutor;
import com.turbomanage.httpclient.AsyncRequestExecutorFactory;
import com.turbomanage.httpclient.ConsoleRequestLogger;
import com.turbomanage.httpclient.HttpMethod;
import com.turbomanage.httpclient.HttpRequest;
import com.turbomanage.httpclient.HttpResponse;

public class DsAndroidHttpClient extends AsyncHttpClient {

    /**
     * A task that executes the result on an executor thread instead of on the UI thread
     */
    public static class NonUiThreadDoHttpRequestTask implements AsyncRequestExecutor {

        private static final ExecutorService executor = Executors.newSingleThreadExecutor();
        private AsyncHttpClient client;
        private AsyncCallback callback;

        /**
         * Construct a new task with a callback to invoke when the request completes or fails.
         * 
         * @param callback
         */
        public NonUiThreadDoHttpRequestTask(final AsyncHttpClient httpClient, final AsyncCallback callback) {
            client = httpClient;
            this.callback = callback;

        }

        protected Runnable createRunnable(final HttpRequest... params) {
            return new Runnable() {
                public void run() {
                    try {
                        if (params != null && params.length > 0) {
                            final HttpRequest httpRequest = params[0];
                            final HttpResponse response = client.tryMany(httpRequest);
                            callback.onComplete(response);
                            return;
                        } else {
                            throw new IllegalArgumentException("DoHttpRequestTask takes exactly one argument of type HttpRequest");
                        }
                    } catch (final Exception e) {
                        callback.onError(e);
                        return;
                    }
                }
            };
        }

        /**
         * Needed in order for this Task class to implement the {@link AsyncRequestExecutor} interface.
         * 
         * @see com.turbomanage.httpclient.AsyncRequestExecutor#execute(com.turbomanage.httpclient.HttpRequest)
         */
        @Override
        public void execute(final HttpRequest httpRequest) {
            executor.execute(createRunnable(httpRequest));
        }
    }

    public static class NonUiThreadAsyncTaskFactory implements AsyncRequestExecutorFactory {

        @Override
        public AsyncRequestExecutor getAsyncRequestExecutor(final AsyncHttpClient client, final AsyncCallback callback) {
            return new NonUiThreadDoHttpRequestTask(client, callback);
        }
    }

    public static final boolean CONSOLE_LOGGING_ENABLED = false;

    static {
        ensureCookieManager();
    }

    public DsAndroidHttpClient(final String url) {
        super(new NonUiThreadAsyncTaskFactory(), url);
        setRequestLogger(new ConsoleRequestLogger() {

            @Override
            public boolean isLoggingEnabled() {
                return CONSOLE_LOGGING_ENABLED;
            }
        });
    }

    @Override
    protected void prepareConnection(final HttpURLConnection urlConnection, final HttpMethod httpMethod, final String contentType) throws IOException {
        super.prepareConnection(urlConnection, httpMethod, contentType);
        if (urlConnection instanceof HttpsURLConnection) {
            final HttpsURLConnection connection = (HttpsURLConnection) urlConnection;
            fixHangingConnectionIssue(connection);
            connection.setSSLSocketFactory(SSLSocketFactoryCreator.createSslSocketFactory());
            connection.setHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(final String hostname, final SSLSession session) {
                    return true;
                }
            });
        }
    }

    /*
     * Workaround for an issue were Samsung Devices kept a connection in state WAIT_CLOSE until there were no more connections available and the connection for all devices connected to the server
     * timed out. Following issue is the reason for this: https://code.google.com/p/google-http-java-client/issues/detail?id=213
     */
    private void fixHangingConnectionIssue(final URLConnection connection) {
        if (Build.VERSION.SDK_INT >= 14) { // Build.VERSION_CODES.ICE_CREAM_SANDWICH
            try {
                connection.setRequestProperty("Connection", "close");
            } catch (final Exception e) {
            }
        }
    }
}
