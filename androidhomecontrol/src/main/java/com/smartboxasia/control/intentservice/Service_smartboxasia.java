package com.smartboxasia.control.intentservice;

import java.util.StringTokenizer;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.smartboxasia.control.DssConstants;
import com.turbomanage.httpclient.AsyncCallback;
import com.turbomanage.httpclient.HttpResponse;

public class Service_smartboxasia extends IntentService {

    public static final String CALLING_PACKAGE = "com.smartboxasia.control.callingPackage";
    public static final String ACTION_IDENTIFIER = "com.smartboxasia.control.actionIdentifier";
    public static final String RESULT_ACTION = "com.smartboxasia.control.result.action";

    private static final String DEBUG_TAG = "DsService";

    private long sceneId = 0;
    private int roomId = 0;
    private int groupNumber = 0;
    private String serverUrl = "";
    // private int portNumber = 8080;
    private DsAndroidHttpClient client;
    private String appToken = "";
    private String sessionToken = "";

    public Service_smartboxasia() {
        super("Service_smartboxasia");
    }

    @Override
    protected void onHandleIntent(final Intent intent) {

        if (!intent.hasExtra(ACTION_IDENTIFIER)) {
            // TODO let the user know, that the app missed sending the necessary extras
            Log.e(DEBUG_TAG, "Necessary extra missing: " + ACTION_IDENTIFIER);
            return;
        }
        if (!intent.hasExtra(CALLING_PACKAGE)) {
            // TODO let the user know, that the app missed sending the necessary extras
            Log.e(DEBUG_TAG, "Necessary extra missing: " + CALLING_PACKAGE);
            return;
        }

        String apiCallingActionIdentifier = intent.getExtras().getString(ACTION_IDENTIFIER);
        final String apiCallerIdentifier = intent.getExtras().getString(CALLING_PACKAGE);
        // create a unique action id from calling package + their supplied id
        apiCallingActionIdentifier = apiCallerIdentifier + apiCallingActionIdentifier;

        // get the data for this action
        final SharedPreferences preferences = getSharedPreferences("ExternalAppActions", MODE_PRIVATE);
        final String tokenizedAction = preferences.getString(apiCallingActionIdentifier, "null");

        if (tokenizedAction.compareTo("null") != 0) {
            final StringTokenizer tokenizer = new StringTokenizer(tokenizedAction, "||");

            boolean isOldAction = false;

            // if there's 6 tokens, it's an old action with port number in it.
            if (tokenizer.countTokens() == 6) {
                isOldAction = true;
            }

            serverUrl = tokenizer.nextToken();
            Log.i("dSService", "server url: " + serverUrl);

            if (isOldAction) {
                final int portNumber = Integer.valueOf(tokenizer.nextToken());
                Log.i("dSService", "is old action, extracting port number: " + portNumber);
            }

            appToken = tokenizer.nextToken();
            Log.i("dSService", "apptoken: " + appToken);

            roomId = Integer.valueOf(tokenizer.nextToken());
            Log.i("dSService", "room ID: " + roomId);

            sceneId = Long.valueOf(tokenizer.nextToken());
            Log.i("dSService", "scene: " + sceneId);

            groupNumber = Integer.valueOf(tokenizer.nextToken());
            Log.i("dSService", "group: " + groupNumber);

            client = new DsAndroidHttpClient(serverUrl);
            client.setMaxRetries(5);

        } else {
            return;
        }

        // Do this in a new thread
        new Thread(new Runnable() {
            public void run() {

                getSessionTokenAndCallScene(Service_smartboxasia.this);

            }
        }).start();

        return;
    }

    private void getSessionTokenAndCallScene(final Context context) {
        final String request = "/json/system/loginApplication?loginToken=" + appToken;
        client.post(request, "application/json", null, new AsyncCallback() {
            @Override
            public void onComplete(final HttpResponse httpResponse) {
                try {
                    final JSONObject response = new JSONObject(httpResponse.getBodyAsString());
                    if (response.getBoolean("ok")) {
                        // get result and set application token in global app variable
                        final JSONObject result = response.getJSONObject("result");
                        sessionToken = result.getString("token");

                        Log.d(DEBUG_TAG, "Got session token: " + sessionToken);

                        String urlToCall;
                        // call scene
                        if (sceneId >= DssConstants.Scenes.USER_DEFINED_ACTIONS) {
                            urlToCall = "/json/event/raise?name=highlevelevent&parameter=id=" + sceneId + "&token=" + sessionToken;
                        } else {
                            urlToCall = "/json/zone/callScene?id=" + roomId + "&groupID=" + groupNumber + "&sceneNumber=" + sceneId + "&force=true" + "&token=" + sessionToken;
                        }

                        client.post(urlToCall, "application/json", null, new AsyncCallback() {
                            @Override
                            public void onComplete(final HttpResponse httpResponse) {
                                try {
                                    final JSONObject response = new JSONObject(httpResponse.getBodyAsString());
                                    if (response.getBoolean("ok")) {
                                        Log.d(DEBUG_TAG, "Called scene: " + sceneId + " in room:" + roomId + " with group:" + groupNumber + " from dSService");
                                    } else {
                                        Log.e(DEBUG_TAG, "Scene call not okay: " + response.getString("message"));
                                    }
                                } catch (final JSONException e) {
                                    Log.e(DEBUG_TAG, "Could not read response for scene call: " + e.getMessage());
                                }
                            }
                        });
                    } else {
                        Log.e(DEBUG_TAG, "Session token request not ok: " + response.getString("message"));
                    }

                } catch (final JSONException e) {
                    Log.e(DEBUG_TAG, "Could not read response for session token request: " + e.getMessage());
                }
            }

            @Override
            public void onError(final Exception e) {
                Log.e(DEBUG_TAG, "Didn't get session token, connection problem");
            }
        });
    }

}
