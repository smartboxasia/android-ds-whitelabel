package com.smartboxasia.control.data.config.app;

import java.util.Set;

import com.smartboxasia.control.dto.DsScene;
import com.google.common.collect.Sets;

public class UserDefinedActionsCache {

    static synchronized void clearCachedValues() {
        userDefinedActions.clear();
    }

    private static final Set<DsScene> userDefinedActions = Sets.newLinkedHashSet();

    public synchronized static void set_userDefinedActions(final Set<DsScene> newUserDefinedActions) {
        userDefinedActions.clear();
        userDefinedActions.addAll(newUserDefinedActions);
    }

    public synchronized static Set<DsScene> get_userDefinedActions() {
        return Sets.newLinkedHashSet(userDefinedActions);
    }
}
