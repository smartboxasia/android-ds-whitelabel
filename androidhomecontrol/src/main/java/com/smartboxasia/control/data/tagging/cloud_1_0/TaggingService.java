package com.smartboxasia.control.data.tagging.cloud_1_0;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.helper.CommandBuilder;
import com.smartboxasia.control.data.helper.DsServiceHelper;
import com.smartboxasia.control.data.helper.EncodingHelper;
import com.smartboxasia.control.data.helper.JSONCallback;
import com.smartboxasia.control.data.helper.Mappings;
import com.smartboxasia.control.data.tagging.cloud_1_0.json.JSONTags;
import com.smartboxasia.control.data.tagging.cloud_1_0.json.TaggingMappings;
import com.smartboxasia.control.domain.SocketType;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsTag;
import com.smartboxasia.control.events.GetAllTagsReceivedEvent;
import com.smartboxasia.control.events.SetTagMaxPowerEvent;
import com.smartboxasia.control.events.SetTagSocketTypeEvent;
import com.google.common.collect.Lists;

public class TaggingService {

    private static final String TAG = TaggingService.class.getSimpleName();

    // ///////////
    // Cloud Calls
    // ///////////

    public static void setTagMaxPower(final String tagValue, final DsDevice device) {
        final String cleanTagValue = EncodingHelper.getEncodedString(tagValue);
        final String cleanDeviceId = EncodingHelper.getEncodedString(device.get_dsid());

        final CommandBuilder builder = new CommandBuilder("public/service/v1_0/Tagging/SetTag", JSONCallback.PARAM_APP_TOKEN).addParameter("objectId", cleanDeviceId).addParameter("objectType", "DSDevice").addEmptyParameter("irdiPR").addParameter("PropertyShortName", "MAX_POWER")
                .addEmptyParameter("irdiVA").addParameter("valueShortName", "WATT").addParameter("value", cleanTagValue).addParameter("languageCode", "XX-xx");

        Connection.getConnectionService().sendAndParseCloudPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (checkSucceededCloud(root)) {
                    DsServiceHelper.signal(new SetTagMaxPowerEvent());
                } else {
                    // broadcast
                    DsServiceHelper.signal(new SetTagMaxPowerEvent(SetTagMaxPowerEvent.ERROR_CLOUD));
                    Log.e(TAG, "Could not set max power tag for device: " + getErrorMessageCloud(root));
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // broadcast
                DsServiceHelper.signal(new SetTagMaxPowerEvent(errorCode));
                Log.e(TAG, "Could not set max power tag for device, connection problem");
            }
        });
    }

    public static void setTagSocketType(final SocketType socketType, final DsDevice device) {
        final String tagValue = Mappings.findValue(TaggingMappings.SOCKET_TYPES, socketType);
        final String deviceId = device.get_dsid();

        final CommandBuilder builder = new CommandBuilder("public/service/v1_0/Tagging/SetTag", JSONCallback.PARAM_APP_TOKEN).addParameter("objectId", deviceId).addParameter("objectType", "DSDevice").addEmptyParameter("irdiPR").addParameter("PropertyShortName", "SOCKET_TYPE")
                .addEmptyParameter("irdiVA").addParameter("valueShortName", tagValue).addEmptyParameter("value").addParameter("languageCode", "XX-xx");

        Connection.getConnectionService().sendAndParseCloudPostCommand(builder, new JSONCallback() {
            @Override
            public Void onResult(final JSONObject root) {
                if (checkSucceededCloud(root)) {
                    DsServiceHelper.signal(new SetTagSocketTypeEvent());
                } else {
                    fail(SetTagSocketTypeEvent.ERROR_CLOUD, getErrorMessageCloud(root));
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                fail(SetTagSocketTypeEvent.ERROR_CLOUD, "connection problem: " + message);
            }

            private void fail(final int errorCode, final String message) {
                // broadcast
                Log.w(TAG, "Could not set socket type tag for device: " + message);
                DsServiceHelper.signal(new SetTagSocketTypeEvent(errorCode));
            }
        });
    }

    public static void getAllTags(final DsDevice device) {
        final String cleanDeviceId = EncodingHelper.getEncodedString(device.get_dsid());

        final CommandBuilder builder = new CommandBuilder("public/service/v1_0/Tagging/GetAllTags", JSONCallback.PARAM_APP_TOKEN).addParameter("objectId", cleanDeviceId).addParameter("languageCode", "XX-xx").addParameter("&includeMetadata", false).addParameter("objectType", "DSDevice");

        Connection.getConnectionService().sendAndParseCloudGetCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {

                final List<DsTag> dsTags = Lists.newArrayList();
                if (checkSucceededCloud(root)) {
                    parseAllTagValues(root, dsTags);
                }

                DsServiceHelper.signal(new GetAllTagsReceivedEvent(device, dsTags));

                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                Log.e(TAG, "Could not get all tags: " + message);
                DsServiceHelper.signal(new GetAllTagsReceivedEvent(errorCode, device));
            }
        });
    }

    private static void parseAllTagValues(final JSONObject jsonResponse, final List<DsTag> dsTags) {
        final JSONObject response = JSONCallback.getResponseCloud(jsonResponse);
        final JSONArray tags = DsServiceHelper.getJSONArray(response, JSONTags.TAGS);

        for (int i = 0; i < tags.length(); i++) {
            final JSONObject tag = DsServiceHelper.getJSONObject(tags, i);

            dsTags.add(parseTag(tag));
        }
    }

    private static DsTag parseTag(final JSONObject tag) {
        // Some fields are trailing with whitespace. this leads to errors if not trimmed as Strings
        // are not equal anymore when compared to the keyword constants.
        final String irdiPR = DsServiceHelper.getStringTrimmed(tag, JSONTags.IRDIPR, "");
        final String irdiUN = DsServiceHelper.getStringTrimmed(tag, JSONTags.IRDIUN, "");
        final String irdiVA = DsServiceHelper.getStringTrimmed(tag, JSONTags.IRDIVA, "");
        final String propertyPreferredName = DsServiceHelper.getStringTrimmed(tag, JSONTags.PROPERTY_PREFERRED_NAME, "");
        final String propertyShortName = DsServiceHelper.getStringTrimmed(tag, JSONTags.PROPERTY_SHORT_NAME, "");
        final String unitPreferredName = DsServiceHelper.getStringTrimmed(tag, JSONTags.UNIT_PREFERRED_NAME, "");
        final String unitShortName = DsServiceHelper.getStringTrimmed(tag, JSONTags.UNIT_SHORT_NAME, "");
        final String value = DsServiceHelper.getStringTrimmed(tag, JSONTags.VALUE, "");
        final String valuePreferredName = DsServiceHelper.getStringTrimmed(tag, JSONTags.VALUE_PREFERRED_NAME, "");
        final String valueShortName = DsServiceHelper.getStringTrimmed(tag, JSONTags.VALUE_SHORT_NAME, "");
        final String tagMetaData = DsServiceHelper.getStringTrimmed(tag, JSONTags.TAG_META_DATA, "");
        final DsTag dsTag = new DsTag().withIrdiPR(irdiPR).withIrdiUN(irdiUN).withIrdiVA(irdiVA).withPropertyPreferredName(propertyPreferredName).withPropertyShortName(propertyShortName).withUnitPreferredName(unitPreferredName).withUnitShortName(unitShortName).withValue(value)
                .withValuePreferredName(valuePreferredName).withValueShortName(valueShortName).withTagMetaData(tagMetaData);
        return dsTag;
    }

    public static void deleteTagSocketType(final DsDevice device, final DsTag tag) {

        final String deviceId = device.get_dsid();

        final CommandBuilder builder = new CommandBuilder("public/service/v1_0/Tagging/DeleteTags", JSONCallback.PARAM_APP_TOKEN).addParameter("objectId", deviceId).addParameter("objectType", "DSDevice");
        final String bodyString = buildBody(tag);

        Connection.getConnectionService().sendAndParseCloudPostCommandWithBody(builder, bodyString, new JSONCallback() {
            @Override
            public Void onResult(final JSONObject root) {
                if (checkSucceededCloud(root)) {
                    DsServiceHelper.signal(new SetTagSocketTypeEvent());
                } else {
                    fail(SetTagSocketTypeEvent.ERROR_CLOUD, getErrorMessageCloud(root));
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                fail(SetTagSocketTypeEvent.ERROR_CLOUD, "connection problem: " + message);
            }

            private void fail(final int errorCode, final String message) {
                // broadcast
                Log.w(TAG, "Could not set socket type tag for device: " + message);
                DsServiceHelper.signal(new SetTagSocketTypeEvent(errorCode));
            }
        });
    }

    private static String buildBody(final DsTag tag) {
        String bodyString = null;
        try {
            final JSONObject jsonTag = new JSONObject();
            jsonTag.put(JSONTags.IRDIPR, tag.irdiPR);
            jsonTag.put(JSONTags.PROPERTY_SHORT_NAME, tag.propertyShortName);
            final JSONArray tagsArray = new JSONArray();
            tagsArray.put(jsonTag);
            final JSONObject body = new JSONObject();
            body.put(JSONTags.TAGS_TO_DELETE, tagsArray);
            bodyString = body.toString();
        } catch (final JSONException e) {
            // no body
        }
        return bodyString;
    }

    public static void deleteTagMaxPower(final DsDevice device, final DsTag tag) {
        final String deviceId = device.get_dsid();

        final CommandBuilder builder = new CommandBuilder("public/service/v1_0/Tagging/DeleteTags", JSONCallback.PARAM_APP_TOKEN).addParameter("objectId", deviceId).addParameter("objectType", "DSDevice");
        final String bodyString = buildBody(tag);

        Connection.getConnectionService().sendAndParseCloudPostCommandWithBody(builder, bodyString, new JSONCallback() {
            @Override
            public Void onResult(final JSONObject root) {
                if (checkSucceededCloud(root)) {
                    DsServiceHelper.signal(new SetTagMaxPowerEvent());
                } else {
                    fail(SetTagMaxPowerEvent.ERROR_CLOUD, getErrorMessageCloud(root));
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                fail(SetTagMaxPowerEvent.ERROR_CLOUD, "connection problem: " + message);
            }

            private void fail(final int errorCode, final String message) {
                // broadcast
                Log.w(TAG, "Could not set socket type tag for device: " + message);
                DsServiceHelper.signal(new SetTagMaxPowerEvent(errorCode));
            }
        });
    }
}
