package com.smartboxasia.control.data.config.app;

import java.util.List;

import com.smartboxasia.control.domain.BusMemberType;
import com.smartboxasia.control.domain.Capability;
import com.smartboxasia.control.dto.DsMeter;
import com.google.common.collect.Lists;

public class MetersCache {

    static synchronized void clearCachedValues() {
        meters.clear();
    }

    private static final List<DsMeter> meters = Lists.newArrayList();
    private static final List<BusMemberType> validBusMemberTypes = Lists.newArrayList(BusMemberType.DSM11, BusMemberType.DSM12, BusMemberType.VDC);

    public synchronized static void set_meters(final List<DsMeter> newMeters) {
        meters.clear();
        meters.addAll(newMeters);
    }

    public synchronized static List<DsMeter> get_meters(final boolean includeVdcs) {
        final List<DsMeter> result = Lists.newArrayList();
        for (final DsMeter meter : meters) {
            final BusMemberType type = meter.get_busMemberType();
            if (meter.hasCapability(Capability.METERING) || (includeVdcs && validBusMemberTypes.contains(type))) {
                result.add(meter);
            }
        }
        return result;
    }

    public synchronized static DsMeter get_meter_for_id(final String id) {
        for (final DsMeter meter : meters) {
            if (meter.get_dsid().equals(id) || meter.get_dsuid().equals(id)) {
                return meter;
            }
        }
        return null;
    }
}
