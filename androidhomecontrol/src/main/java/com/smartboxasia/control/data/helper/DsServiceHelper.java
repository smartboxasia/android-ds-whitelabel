package com.smartboxasia.control.data.helper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.smartboxasia.control.App;
import com.smartboxasia.control.BuildConfig;

public class DsServiceHelper {

    private static final String TAG = DsServiceHelper.class.getSimpleName();

    private static Handler handler = new Handler(Looper.getMainLooper());

    public static void signal(final Object event) {
        // post the event on the UI thread.
        handler.post(new Runnable() {

            @Override
            public void run() {
                App.eventBus.post(event);
            }
        });
    }

    /**
     * This method unescapes escape strings that were specifically made on the server side. Should be used for small strings like names that the user has input into the system.
     * 
     * @param string the string to unescape
     * @return the string without escape characters
     */
    public static String unescape(final String string) {
        return string.replaceAll("&amp;", "&").replaceAll("&quot;", "\"").replaceAll("&#039;", "'").replaceAll("&lt;", "<").replaceAll("&gt;", ">");
    }

    // ///////////////////////////
    // JSON parsing helper methods
    // ///////////////////////////

    public static JSONObject extractResponse(final HttpResponse httpResponse) {
        JSONObject response = new JSONObject();
        if (httpResponse != null) {
            String responseString = "";
            try {
                responseString = new String(readStream(httpResponse.getEntity().getContent()));
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "Response: " + responseString);
                }
                response = new JSONObject(responseString);
            } catch (final JSONException e) {
                final int end = Math.min(20, responseString.length());
                Log.e(TAG, "Could not parse json: " + responseString.substring(0, end), e);
            } catch (final IllegalStateException e) {
                Log.e(TAG, "Tried to open stream twice", e);
            } catch (final IOException e) {
                Log.e(TAG, "Could not read response", e);
            }
        }
        return response;
    }

    public static byte[] readStream(final InputStream in) throws IOException {
        int nRead;
        final byte[] data = new byte[16384];
        final ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        while ((nRead = in.read(data)) != -1) {
            buffer.write(data, 0, nRead);
        }
        buffer.flush();
        return buffer.toByteArray();
    }

    public static JSONObject getJSONObject(final JSONObject jsonObject, final String key) {
        JSONObject result = new JSONObject();
        try {
            if (jsonObject != null) {
                result = jsonObject.getJSONObject(key);
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Could not read json object for key " + key, e);
        }
        return result;
    }

    public static JSONObject getJSONObject(final JSONArray jsonArray, final int index) {
        JSONObject result = new JSONObject();
        try {
            if (jsonArray != null) {
                result = (JSONObject) jsonArray.get(index);
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Could not read json object for index " + index, e);
        }
        return result;
    }

    public static JSONArray getJSONArray(final JSONObject jsonObject, final String key) {
        try {
            return jsonObject.getJSONArray(key);
        } catch (final JSONException e) {
            Log.e(TAG, "Could not read json object for key " + key, e);
        }
        return new JSONArray();
    }

    public static JSONArray optJSONArray(final JSONObject jsonObject, final String key) {
        JSONArray result = jsonObject.optJSONArray(key);
        if (result == null) {
            result = new JSONArray();
        }
        return result;
    }

    public static boolean getBoolean(final JSONObject response, final String key, final boolean defaultResult) {
        boolean result = defaultResult;
        if (response != null) {
            if (!response.isNull(key)) {
                try {
                    result = response.getBoolean(key);
                } catch (final JSONException e) {
                    Log.e(TAG, "Could not read json value for key " + key, e);
                }
            } else {
                Log.i(TAG, "json value for key " + key + " is null");
            }
        }
        return result;
    }

    public static String getString(final JSONObject response, final String key, final String defaultResult) {
        String result = defaultResult;
        if (response != null) {
            if (!response.isNull(key)) {
                try {
                    result = response.getString(key);
                } catch (final JSONException e) {
                    Log.e(TAG, "Could not read json value for key " + key, e);
                }
            } else {
                Log.i(TAG, "json value for key " + key + " is null");
            }
        }
        return result;
    }

    public static int getInt(final JSONObject response, final String key, final int defaultResult) {
        int result = defaultResult;
        if (response != null) {
            if (!response.isNull(key)) {
                try {
                    result = response.getInt(key);
                } catch (final JSONException e) {
                    Log.e(TAG, "Could not read json value for key " + key, e);
                }
            } else {
                Log.i(TAG, "json value for key " + key + " is null");
            }
        }
        return result;
    }

    public static long getLong(final JSONObject response, final String key, final long defaultResult) {
        long result = defaultResult;
        if (response != null) {
            if (!response.isNull(key)) {
                try {
                    result = response.getLong(key);
                } catch (final JSONException e) {
                    Log.e(TAG, "Could not read json value for key " + key, e);
                }
            } else {
                Log.i(TAG, "json value for key " + key + " is null");
            }
        }
        return result;
    }

    public static String getStringTrimmed(final JSONObject response, final String key, final String defaultResult) {
        final String untrimmed = getString(response, key, defaultResult);
        return untrimmed.trim();
    }

    public static JSONObject createJSONObject(final String responseString) {
        JSONObject result = new JSONObject();
        try {
            result = new JSONObject(responseString);
        } catch (final JSONException e) {
            final int end = Math.min(20, responseString.length());
            Log.e(TAG, "Could not create json object for String: " + responseString.substring(0, end), e);
        }
        return result;
    }

    public static String getStringFromArray(final JSONArray array, final int index, final String defaultResult) {
        String result = defaultResult;
        try {
            result = array.getString(index);
        } catch (final JSONException e) {
            Log.e(TAG, "json value at index " + index + " does not exist");
        }
        return result;
    }
}
