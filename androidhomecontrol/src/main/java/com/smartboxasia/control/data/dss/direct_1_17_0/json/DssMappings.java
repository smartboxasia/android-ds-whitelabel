package com.smartboxasia.control.data.dss.direct_1_17_0.json;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.smartboxasia.control.DssConstants.Scenes;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.dss.direct_1_17_0.json.JSONConstants.BusMemberTypes;
import com.smartboxasia.control.data.dss.direct_1_17_0.json.JSONConstants.Gtins;
import com.smartboxasia.control.data.dss.direct_1_17_0.json.JSONConstants.OutputModes;
import com.smartboxasia.control.data.helper.Mappings;
import com.smartboxasia.control.domain.BusMemberType;
import com.smartboxasia.control.domain.OutputMode;
import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class DssMappings extends Mappings {

    public static final Map<String, List<OutputMode>> DEVICE_OUTPUT_MODES;
    static {
        final Map<String, List<OutputMode>> map = Maps.newHashMap();
        map.put(Gtins.GE_KM200, getList(OutputMode.DISABLED, OutputMode.SWITCHED, OutputMode.DIMMABLE));
        map.put(Gtins.GE_TKM210, getList(OutputMode.DISABLED, OutputMode.SWITCHED, OutputMode.DIMMABLE));
        map.put(Gtins.GE_SDS200_CW, getList(OutputMode.DISABLED, OutputMode.SWITCHED, OutputMode.DIMMABLE)); // a.k.a. GE-SDS200
        map.put(Gtins.GE_SDS200_CS, getList(OutputMode.DISABLED, OutputMode.SWITCHED, OutputMode.DIMMABLE)); // a.k.a. GE-SDS201
        map.put(Gtins.GE_SDS220, getList(OutputMode.DISABLED, OutputMode.SWITCHED, OutputMode.DIMMABLE));
        map.put(Gtins.GE_SDM200, getList(OutputMode.DISABLED, OutputMode.SWITCHED, OutputMode.DIMMABLE));
        map.put(Gtins.GE_SDM200_JS, getList(OutputMode.DISABLED, OutputMode.SWITCHED, OutputMode.DIMMABLE)); // a.k.a. GE-SDM201
        map.put(Gtins.GE_SDM200_CS, getList(OutputMode.DISABLED, OutputMode.SWITCHED, OutputMode.DIMMABLE)); // a.k.a. GE-SDM202
        map.put(Gtins.GE_KL200, getList(OutputMode.DISABLED, OutputMode.SWITCHED_SINGLE_POLARITY));
        map.put(Gtins.GE_UMV200, getList(OutputMode.DISABLED, OutputMode.SWITCHED, OutputMode.DIMMABLE_0_10V, OutputMode.DIMMABLE_1_10V));
        map.put(Gtins.SW_KL200, getList(OutputMode.DISABLED, OutputMode.RELAY_SWITCHED));
        map.put(Gtins.SW_KL210, getList(OutputMode.DISABLED, OutputMode.RELAY_SWITCHED));
        map.put(Gtins.SW_KL211, getList(OutputMode.DISABLED, OutputMode.RELAY_SWITCHED));
        map.put(Gtins.SW_KL212, getList(OutputMode.DISABLED, OutputMode.RELAY_SWITCHED));
        map.put(Gtins.SW_SSL200_JS, getList(OutputMode.DISABLED, OutputMode.RELAY_SWITCHED));
        map.put(Gtins.SW_SSL200_FS, getList(OutputMode.DISABLED, OutputMode.RELAY_SWITCHED));
        map.put(Gtins.SW_UMR200, getList()); // TODO Add correct output modes
        DEVICE_OUTPUT_MODES = ImmutableMap.copyOf(map);
    }

    private static List<OutputMode> getList(final OutputMode... modes) {
        final List<OutputMode> result = Lists.newArrayList(modes);
        return Collections.unmodifiableList(result);
    }

    public static final BiMap<BusMemberType, Integer> BUS_MEMBER_TYPES;
    static {
        final Map<BusMemberType, Integer> map = Maps.newHashMap();
        map.put(BusMemberType.UNKNOWN, BusMemberTypes.UNKNOWN);
        map.put(BusMemberType.DSM11, BusMemberTypes.DSM11);
        map.put(BusMemberType.DSM12, BusMemberTypes.DSM12);
        map.put(BusMemberType.VDSM, BusMemberTypes.VDSM);
        map.put(BusMemberType.VDC, BusMemberTypes.VDC);
        BUS_MEMBER_TYPES = ImmutableBiMap.copyOf(EnumHashBiMap.create(map));
    }

    public static final Map<OutputMode, Integer> OUTPUT_MODES_TO_NAME_RES;
    static {
        final Map<OutputMode, Integer> map = Maps.newHashMap();
        map.put(OutputMode.DISABLED, R.string.output_mode_disabled);
        map.put(OutputMode.SWITCHED, R.string.output_mode_switched);
        map.put(OutputMode.RMS_DIMMER, R.string.dash);
        map.put(OutputMode.RMS_DIMMER_WITH_CURVE, R.string.dash);
        map.put(OutputMode.PHASE_CONTROL_DIMMER, R.string.dash);
        map.put(OutputMode.PHASE_CONTROL_DIMMER_WITH_CURVE, R.string.dash);
        map.put(OutputMode.REVERSE_PHASE_CONTROL_DIMMER, R.string.dash);
        map.put(OutputMode.DIMMABLE, R.string.output_mode_dimmed);
        map.put(OutputMode.PWM_DIMMER, R.string.dash);
        map.put(OutputMode.PWM_DIMMER_WITH_CURVE, R.string.dash);
        map.put(OutputMode.POSITIONING_CONTROL, R.string.dash);
        map.put(OutputMode.SWITCH_TWO_STEP, R.string.output_mode_switch_two_step);
        map.put(OutputMode.SWITCHED_SINGLE_POLARITY, R.string.output_mode_switched_single_polarity);
        map.put(OutputMode.SWITCH_THREE_STEP, R.string.output_mode_switch_three_step);
        map.put(OutputMode.RELAY_SWITCHED, R.string.output_mode_relay_switched);
        map.put(OutputMode.RELAY_WIPED, R.string.dash);
        map.put(OutputMode.RELAY_SAVING, R.string.dash);
        map.put(OutputMode.POSITIONING_CONTROL_UNCALIBRATED, R.string.dash);
        map.put(OutputMode.SWITCHED_DOUBLE_POLARITY, R.string.output_mode_switched_double_polarity);
        map.put(OutputMode.DIMMABLE_0_10V, R.string.output_mode_dimmed_0_10V);
        map.put(OutputMode.DIMMABLE_1_10V, R.string.output_mode_dimmed_1_10V);
        OUTPUT_MODES_TO_NAME_RES = ImmutableMap.copyOf(map);
    }

    public static final BiMap<OutputMode, Integer> OUTPUT_MODES;
    static {
        final Map<OutputMode, Integer> map = Maps.newHashMap();
        map.put(OutputMode.DISABLED, OutputModes.DISABLED);
        map.put(OutputMode.SWITCHED, OutputModes.SWITCHED);
        map.put(OutputMode.RMS_DIMMER, OutputModes.RMS_DIMMER);
        map.put(OutputMode.RMS_DIMMER_WITH_CURVE, OutputModes.RMS_DIMMER_WITH_CURVE);
        map.put(OutputMode.PHASE_CONTROL_DIMMER, OutputModes.PHASE_CONTROL_DIMMER);
        map.put(OutputMode.PHASE_CONTROL_DIMMER_WITH_CURVE, OutputModes.PHASE_CONTROL_DIMMER_WITH_CURVE);
        map.put(OutputMode.REVERSE_PHASE_CONTROL_DIMMER, OutputModes.REVERSE_PHASE_CONTROL_DIMMER);
        map.put(OutputMode.DIMMABLE, OutputModes.DIMMABLE);
        map.put(OutputMode.PWM_DIMMER, OutputModes.PWM_DIMMER);
        map.put(OutputMode.PWM_DIMMER_WITH_CURVE, OutputModes.PWM_DIMMER_WITH_CURVE);
        map.put(OutputMode.POSITIONING_CONTROL, OutputModes.POSITIONING_CONTROL);
        map.put(OutputMode.SWITCHED_SINGLE_POLARITY, OutputModes.SWITCH_SINGLE_POLARITY);
        map.put(OutputMode.RELAY_SWITCHED, OutputModes.RELAY_SWITCHED);
        map.put(OutputMode.RELAY_WIPED, OutputModes.RELAY_WIPED);
        map.put(OutputMode.RELAY_SAVING, OutputModes.RELAY_SAVING);
        map.put(OutputMode.POSITIONING_CONTROL_UNCALIBRATED, OutputModes.POSITIONING_CONTROL_UNCALIBRATED);
        map.put(OutputMode.DIMMABLE_0_10V, OutputModes.DIMMABLE_0_10V);
        map.put(OutputMode.DIMMABLE_1_10V, OutputModes.DIMMABLE_1_10V);
        OUTPUT_MODES = ImmutableBiMap.copyOf(EnumHashBiMap.create(map));
    }

    public static final BiMap<Long, Integer> DEFAULT_SCENE_NAMES;
    static {
        final Map<Long, Integer> map = Maps.newLinkedHashMap();
        map.put(Scenes.OFF, R.string.scene_default_0);
        map.put(Scenes.PRESET_1, R.string.scene_default_1);
        map.put(Scenes.PRESET_2, R.string.scene_default_2);
        map.put(Scenes.PRESET_3, R.string.scene_default_3);
        map.put(Scenes.PRESET_4, R.string.scene_default_4);
        map.put(Scenes.AREA_1_ON, R.string.scene_area_1_on);
        map.put(Scenes.AREA_2_ON, R.string.scene_area_2_on);
        map.put(Scenes.AREA_3_ON, R.string.scene_area_3_on);
        map.put(Scenes.AREA_4_ON, R.string.scene_area_4_on);
        map.put(Scenes.AREA_1_OFF, R.string.scene_area_1_off);
        map.put(Scenes.AREA_2_OFF, R.string.scene_area_2_off);
        map.put(Scenes.AREA_3_OFF, R.string.scene_area_3_off);
        map.put(Scenes.AREA_4_OFF, R.string.scene_area_4_off);
        map.put(Scenes.PRESET_11, R.string.scene_extended_11);
        map.put(Scenes.PRESET_12, R.string.scene_extended_12);
        map.put(Scenes.PRESET_13, R.string.scene_extended_13);
        map.put(Scenes.PRESET_14, R.string.scene_extended_14);
        map.put(Scenes.PRESET_21, R.string.scene_extended_21);
        map.put(Scenes.PRESET_22, R.string.scene_extended_22);
        map.put(Scenes.PRESET_23, R.string.scene_extended_23);
        map.put(Scenes.PRESET_24, R.string.scene_extended_24);
        map.put(Scenes.PRESET_31, R.string.scene_extended_31);
        map.put(Scenes.PRESET_32, R.string.scene_extended_32);
        map.put(Scenes.PRESET_33, R.string.scene_extended_33);
        map.put(Scenes.PRESET_34, R.string.scene_extended_34);
        map.put(Scenes.PRESET_41, R.string.scene_extended_41);
        map.put(Scenes.PRESET_42, R.string.scene_extended_42);
        map.put(Scenes.PRESET_43, R.string.scene_extended_43);
        map.put(Scenes.PRESET_44, R.string.scene_extended_44);
        DEFAULT_SCENE_NAMES = ImmutableBiMap.copyOf(HashBiMap.create(map));
    }

    public static final BiMap<Long, Integer> DEFAULT_SCENE_NAMES_SHADE;
    static {
        final Map<Long, Integer> map = Maps.newLinkedHashMap();
        map.put(Scenes.OFF, R.string.scene_default_0_shade);
        map.put(Scenes.PRESET_1, R.string.scene_default_1_shade);
        map.put(Scenes.PRESET_2, R.string.scene_default_2);
        map.put(Scenes.PRESET_3, R.string.scene_default_3);
        map.put(Scenes.PRESET_4, R.string.scene_default_4);
        map.put(Scenes.AREA_1_ON, R.string.scene_area_1_on_shade);
        map.put(Scenes.AREA_2_ON, R.string.scene_area_2_on_shade);
        map.put(Scenes.AREA_3_ON, R.string.scene_area_3_on_shade);
        map.put(Scenes.AREA_4_ON, R.string.scene_area_4_on_shade);
        map.put(Scenes.AREA_1_OFF, R.string.scene_area_1_off_shade);
        map.put(Scenes.AREA_2_OFF, R.string.scene_area_2_off_shade);
        map.put(Scenes.AREA_3_OFF, R.string.scene_area_3_off_shade);
        map.put(Scenes.AREA_4_OFF, R.string.scene_area_4_off_shade);
        map.put(Scenes.PRESET_11, R.string.scene_extended_11);
        map.put(Scenes.PRESET_12, R.string.scene_extended_12);
        map.put(Scenes.PRESET_13, R.string.scene_extended_13);
        map.put(Scenes.PRESET_14, R.string.scene_extended_14);
        map.put(Scenes.PRESET_21, R.string.scene_extended_21);
        map.put(Scenes.PRESET_22, R.string.scene_extended_22);
        map.put(Scenes.PRESET_23, R.string.scene_extended_23);
        map.put(Scenes.PRESET_24, R.string.scene_extended_24);
        map.put(Scenes.PRESET_31, R.string.scene_extended_31);
        map.put(Scenes.PRESET_32, R.string.scene_extended_32);
        map.put(Scenes.PRESET_33, R.string.scene_extended_33);
        map.put(Scenes.PRESET_34, R.string.scene_extended_34);
        map.put(Scenes.PRESET_41, R.string.scene_extended_41);
        map.put(Scenes.PRESET_42, R.string.scene_extended_42);
        map.put(Scenes.PRESET_43, R.string.scene_extended_43);
        map.put(Scenes.PRESET_44, R.string.scene_extended_44);
        DEFAULT_SCENE_NAMES_SHADE = ImmutableBiMap.copyOf(HashBiMap.create(map));
    }
}
