package com.smartboxasia.control.data.dss.direct_1_17_0;

import java.util.List;

import com.smartboxasia.control.domain.OutputMode;

public interface DsDeviceBehavior {

    public List<OutputMode> getAvailableOutputModes();

    public boolean isOemDevice();

    public boolean canModifyOutputMode();
}
