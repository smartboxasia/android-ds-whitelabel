package com.smartboxasia.control.data.helper;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.smartboxasia.control.App;
import com.smartboxasia.control.events.BaseEvent;

public abstract class JSONCallback extends JSONCallbackBase<Void> {

    public static JSONCallback callbackWithoutEffect() {
        return new JSONCallback() {
            @Override
            public Void onResult(final JSONObject root) {
                // do nothing
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // do nothing
            }
        };
    }

    public static JSONCallback rcCheck(final String tag, final String successMsg, final String failMsgStub, final BaseEvent event) {
        return new JSONCallback() {

            private String returnCodeMessage = "";
            private int returnCode;

            @Override
            public Void onResult(final JSONObject root) {
                if (!checkReturnCode(root)) {
                    fail(getReturnCode(), getReturnCodeMessage());
                    return null;
                }
                Log.i(tag, successMsg);
                App.eventBus.post(event);
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                fail(errorCode, message);
            }

            private void fail(final int errorCode, final String message) {
                final StringBuilder sb = new StringBuilder(failMsgStub).append(": ").append(message);
                Log.w(tag, sb.toString());
                event.setErrorCode(errorCode);
                App.eventBus.post(event);
            }

            private boolean checkReturnCode(final JSONObject root) {
                try {
                    returnCode = root.getInt(RETURN_CODE);
                    if (returnCode != RETURN_CODE_OK) {
                        returnCodeMessage = root.getString(RETURN_MESSAGE);
                        return false;
                    }
                } catch (final JSONException e) {
                    returnCodeMessage = e.getLocalizedMessage();
                    return false;
                }
                return true;
            }

            private int getReturnCode() {
                return returnCode;
            }

            private String getReturnCodeMessage() {
                return returnCodeMessage;
            }
        };
    }
}
