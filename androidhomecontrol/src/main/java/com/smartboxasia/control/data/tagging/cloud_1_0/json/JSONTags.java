package com.smartboxasia.control.data.tagging.cloud_1_0.json;

public class JSONTags {

    public static final String TAGS = "Tags";
    public static final String IRDIPR = "IrdiPR";
    public static final String IRDIUN = "IrdiUN";
    public static final String IRDIVA = "IrdiVA";
    public static final String PROPERTY_PREFERRED_NAME = "PropertyPreferredName";
    public static final String PROPERTY_SHORT_NAME = "PropertyShortName";
    public static final String UNIT_PREFERRED_NAME = "UnitPreferredName";
    public static final String UNIT_SHORT_NAME = "UnitShortName";
    public static final String VALUE = "Value";
    public static final String VALUE_PREFERRED_NAME = "ValuePreferredName";
    public static final String VALUE_SHORT_NAME = "ValueShortName";
    public static final String TAG_META_DATA = "TagMetaData";
    public static final String TAGS_TO_DELETE = "TagsToDelete";

}
