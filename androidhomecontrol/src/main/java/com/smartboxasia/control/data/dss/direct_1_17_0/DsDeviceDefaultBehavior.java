package com.smartboxasia.control.data.dss.direct_1_17_0;

import java.util.List;

import com.smartboxasia.control.data.dss.direct_1_17_0.json.DssMappings;
import com.smartboxasia.control.domain.OutputMode;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

public class DsDeviceDefaultBehavior implements DsDeviceBehavior {

    private String gtin;

    public DsDeviceDefaultBehavior(final String gtin) {
        this.gtin = gtin;
    }

    @Override
    public List<OutputMode> getAvailableOutputModes() {
        final List<OutputMode> result = Lists.newArrayList();
        final List<OutputMode> modes = DssMappings.DEVICE_OUTPUT_MODES.get(gtin);
        if (modes != null) {
            result.addAll(modes);
        }
        return result;
    }

    @Override
    public boolean isOemDevice() {
        return Strings.nullToEmpty(gtin).length() < 2;
    }

    @Override
    public boolean canModifyOutputMode() {
        return true;
    }
}
