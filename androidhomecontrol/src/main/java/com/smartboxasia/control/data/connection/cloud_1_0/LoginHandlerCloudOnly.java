package com.smartboxasia.control.data.connection.cloud_1_0;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.smartboxasia.control.App;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.connection.ConnectionHandler;
import com.smartboxasia.control.data.connection.LoginHandler;
import com.smartboxasia.control.data.connection.cloud_1_0.json.JSONTags;
import com.smartboxasia.control.data.connection.exception.NeedsSessionRefreshException;
import com.smartboxasia.control.data.helper.CommandBuilder;
import com.smartboxasia.control.data.helper.EncodingHelper;
import com.smartboxasia.control.data.helper.JSONCallback;
import com.smartboxasia.control.data.helper.UUIDHelper;
import com.smartboxasia.control.domain.LoginResult;
import com.google.common.base.Strings;

public class LoginHandlerCloudOnly extends LoginHandler {

    private static final String TAG = LoginHandlerCloudOnly.class.getSimpleName();

    public LoginHandlerCloudOnly(final ConnectionHandler connectionHandler) {
        super(connectionHandler);
    }

    @Override
    public LoginResult login(final String user, final String pass, final boolean silent) {
        Log.d("LOGIN-TRACE", "Cloud Only LoginHandler start logging in");
        if (!Strings.isNullOrEmpty(user) && !Strings.isNullOrEmpty(pass)) {
            Log.d("LOGIN-TRACE", "-- credentials were passed in, use them to get a new enabled application token from the cloud");
            if (getEnabledAppToken(user, pass)) {
                if (updateServerIdCloudOnly()) {
                    Log.d("LOGIN-TRACE", "-- cloud only login done");
                    return LoginResult.LOGIN_RESULT_OK;
                } else {
                    Log.d("LOGIN-TRACE", "-- updating the server id failed");
                    return LoginResult.LOGIN_RESULT_NOT_OK;
                }
            } else {
                Log.d("LOGIN-TRACE", "  -- could not get an enabled app token from the cloud");
                return LoginResult.LOGIN_RESULT_NOT_OK;
            }
        }

        // if we don't have an app token yet, we need to get the credentials from the user
        if (Strings.isNullOrEmpty(connectionData.getApplicationToken())) {
            Log.d("LOGIN-TRACE", "-- we don't have an application token. we need to ask the user to give us the credentials to retrieve one");
            return LoginResult.LOGIN_RESULT_NEED_CREDENTIALS;
        } else if (connectionData.hasValidAppToken()) {
            Log.d("LOGIN-TRACE", "-- already have an enabled app token");
            return LoginResult.LOGIN_RESULT_OK;
        }

        return LoginResult.LOGIN_RESULT_NEED_CREDENTIALS;
    }

    private boolean getEnabledAppToken(final String userName, final String password) {
        if (Strings.isNullOrEmpty(userName)) {
            return false;
        }

        final Context context = App.getInstance();

        final CommandBuilder builder = new CommandBuilder("public/accessmanagement/v1_0/RemoteConnectivity/GetRelayLinkAndToken").addParameterEncoded("user", userName).addParameterEncoded("password", password).addParameterEncoded("appName", context.getString(context.getApplicationInfo().labelRes))
                .addParameterEncoded("mobileName", android.os.Build.MODEL).addParameter("mobileAppUuid", UUIDHelper.getUuid());

        try {
            final String resultString = connectionHandler.sendCommandUnauthenticated(builder, false, false);
            final JSONObject result = new JSONObject(resultString);
            final int returnCode = result.getInt(JSONTags.RETURN_CODE);
            if (returnCode == JSONCallback.RETURN_CODE_OK) {
                final JSONObject response = result.getJSONObject(JSONTags.RESPONSE);

                String relayLink = response.getString(JSONTags.RELAY_LINK);
                if (!relayLink.endsWith("/")) {
                    relayLink += "/";
                }

                final String token = response.getString(JSONTags.TOKEN);

                // update the used url to the received relay link url. This will also be persisted.
                // also save the received application token. At this point it has already been enabled
                connectionData.setUrl(relayLink);
                connectionData.setApplicationToken(token);
                connectionData.setHasValidAppToken(true);

                return true;
            } else {
                // could not get an authenticated token, stop for now and tell the user
                showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_could_not_verify_cloud_message);
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Could not parse result", e);
            showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_communication_error);
        } catch (final IOException e) {
            Log.e(TAG, "Could not communicate with server", e);
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_cloud_message);
        } catch (final NeedsSessionRefreshException e) {
            // Should usually not happen for a cloud connection, but it can happen in case of a 403. Handle it like a normal FileNotFoundException.
            Log.e(TAG, "Could not communicate with server", e.getCause());
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_cloud_message);
        }
        return false;
    }

    public boolean updateServerIdCloudOnly() {

        final CommandBuilder builder = new CommandBuilder("public/accessmanagement/v1_0/RemoteConnectivity/GetApartmentInformationToUser").addParameter("user", EncodingHelper.getEncodedString(connectionData.getUser()));

        try {
            final String resultString = connectionHandler.sendCommandAuthenticated(builder, null, false, true);
            final JSONObject result = new JSONObject(resultString);
            final int returnCode = result.getInt(JSONTags.RETURN_CODE);
            if (returnCode == JSONCallback.RETURN_CODE_OK) {
                final JSONObject response = result.getJSONObject(JSONTags.RESPONSE);
                final String serverId = response.getString(JSONTags.DSID);
                final String apartmentId = response.getString(JSONTags.APARTMENT_ID);
                connectionData.setServerId(serverId);
                connectionData.setApartmentId(apartmentId);
                return true;
            } else {
                // could not get dssid
                showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_communication_error);
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Could not parse result", e);
            showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_communication_error);
        } catch (final IOException e) {
            Log.e(TAG, "Could not communicate with server", e);
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_cloud_message);
        } catch (final NeedsSessionRefreshException e) {
            // Should usually not happen for a cloud connection, but it can happen in case of a 403. Handle it like a normal FileNotFoundException.
            Log.e(TAG, "Could not communicate with server", e.getCause());
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_cloud_message);
        }
        return false;
    }
}
