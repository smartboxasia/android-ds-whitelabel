package com.smartboxasia.control.data.connection;

import java.util.List;

import android.content.Context;
import android.util.Log;

import com.google.common.collect.Lists;

public abstract class ConnectionServiceProviderCreator {

    /**
     * The minimal version for which a ConnecitonServiceProvider is available
     */
    public static final String MIN_DSS_VERSION_AVAILABLE = ConnectionServiceProviderImpl.MIN_DSS_VERSION;

    private static final List<Class<? extends ConnectionServiceProvider>> providers = Lists.newArrayList();

    private static final String TAG = null;
    static {
        // Fill with newest providers first
        providers.add(ConnectionServiceProviderImpl.class); // 1.17.0
    }

    public static ConnectionServiceProvider getProvider(final Context context, final String version, final int connectionType) {
        if (version == null) {
            for (final Class<? extends ConnectionServiceProvider> providerClass : providers) {
                ConnectionServiceProvider provider;
                provider = getProvider(providerClass);
                if (provider != null && provider.supportsConnectionType(connectionType)) {
                    return provider;
                }
            }
            // TODO warn user that there is no connection service for this type
            return null;
        }

        // Check from the newest to the oldest supported ConnectionServiceProvider
        for (final Class<? extends ConnectionServiceProvider> providerClass : providers) {
            final ConnectionServiceProvider provider = getProvider(providerClass);
            if (provider != null && provider.supportsVersion(version) && provider.supportsConnectionType(connectionType)) {
                return provider;
            }
        }
        // TODO warn user that the server software version is too old
        return null;
    }

    private static ConnectionServiceProvider getProvider(final Class<? extends ConnectionServiceProvider> providerClass) {
        try {
            return providerClass.newInstance();
        } catch (final InstantiationException e) {
            Log.w(TAG, "Could not instantiate ConnectionServiceProvider " + providerClass.getSimpleName() + " beacuse of an InstantiationException: " + e.getMessage());
        } catch (final IllegalAccessException e) {
            Log.w(TAG, "Could not instantiate ConnectionServiceProvider " + providerClass.getSimpleName() + " beacuse of an IllegalAccessException: " + e.getMessage());
        }
        return null;
    }

    /**
     * For testing
     *
     * @return the providers list
     */
    public static List<Class<? extends ConnectionServiceProvider>> getProviders() {
        return providers;
    }
}
