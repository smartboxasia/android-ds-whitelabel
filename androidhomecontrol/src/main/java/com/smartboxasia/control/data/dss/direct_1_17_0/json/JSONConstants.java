package com.smartboxasia.control.data.dss.direct_1_17_0.json;

import java.util.List;

import com.google.common.collect.Lists;

public class JSONConstants {

    public static class OutputModes {
        public static final int DISABLED = 0; // No output or output disabled
        public static final int SWITCHED = 16; // Switched
        public static final int RMS_DIMMER = 17; // RMS (root mean square) dimmer
        public static final int RMS_DIMMER_WITH_CURVE = 18; // RMS dimmer with characteristic curve
        public static final int PHASE_CONTROL_DIMMER = 19; // Phase control dimmer
        public static final int PHASE_CONTROL_DIMMER_WITH_CURVE = 20; // Phase control dimmer with characteristic curve
        public static final int REVERSE_PHASE_CONTROL_DIMMER = 21; // Reverse phase control dimmer
        public static final int DIMMABLE = 22; // Reverse phase control dimmer with characteristic curve
        public static final int PWM_DIMMER = 23; // PWM (pulse width modulation)
        public static final int PWM_DIMMER_WITH_CURVE = 24; // PWM with characteristic curve
        public static final int POSITIONING_CONTROL = 33; // Positioning control
        public static final int SWITCH_TWO_STEP = 34; // only double polarity
        public static final int SWITCH_SINGLE_POLARITY = 35;
        public static final int SWITCH_THREE_STEP = 38; // only double polarity
        public static final int RELAY_SWITCHED = 39; // Relay with switched mode scene table configuration
        public static final int RELAY_WIPED = 40; // Relay with wiped mode scene table configuration
        public static final int RELAY_SAVING = 41; // Relay with saving mode scene table configuration
        public static final int POSITIONING_CONTROL_UNCALIBRATED = 42; // Positioning control for uncalibrated shutter
        public static final int SWITCH_DOUBLE_POLARITY = 43;
        public static final int DIMMABLE_0_10V = 49; // UMV Dimmed mode
        public static final int DIMMABLE_1_10V = 51; // UMV Dimmed mode
    }

    public static class DeviceTypes {
        public static final int JALOUSIE = 3292;
        public static final int MARQUISE = 3282;
    }

    public static class Gtins {
        public static final String GE_KM200 = "4290046000010";//
        public static final String GE_TKM210 = "4290046000027";//
        public static final String GE_SDM200 = "4290046000034";
        public static final String GE_SDS200_CW = "7640156790221";
        public static final String GE_SDS200_CS = "7640156790238";
        public static final String GE_SDM200_JS = "7640156791655";
        public static final String GE_SDM200_CS = "7640156791662";
        public static final String GE_SDS220 = "7640156790214";
        public static final String GE_TKM220 = "4290046000201";
        public static final String GE_TKM230 = "4290046000218";
        public static final String GE_KL200 = "4290046000195";
        public static final String GE_UMV200 = "7640156790603";
        public static final String SW_KL200 = "4290046000959";
        public static final String SW_KL210 = "4290046000935"; // built into SW-ZWS200-J
        public static final String SW_KL211 = "4290046000942"; // built into SW-ZWS200-F
        public static final String SW_KL212 = "7640156790481"; // built into SW-ZWS200-E+F
        public static final String SW_UMR200 = "7640156790597";
        public static final String SW_SSL200_JS = "7640156791679";
        public static final String SW_SSL200_FS = "7640156791686";
    }

    public static final List<String> GE_SDS_OEM_EAN_NUMBERS = Lists.newArrayList("7640156790436", "7640156790214");

    public static class BusMemberTypes {
        public static final int UNKNOWN = -1;
        public static final int DSM11 = 16;
        public static final int DSM12 = 17;
        public static final int VDSM = 32;
        public static final int VDC = 33;
    }

    public static class SocketTypes {
        public static final String E14 = "E14";
        public static final String E27 = "E27";
        public static final String G4 = "G4";
        public static final String G53 = "G5,3";
        public static final String G9 = "G9";
        public static final String GU10 = "GU10";
        public static final String B15d = "B15d";
        public static final String B22d = "B22d";
        public static final String E10 = "E10";
        public static final String E12 = "E12";
        public static final String E40 = "E40";
        public static final String Fa4 = "Fa4";
        public static final String G635 = "G6.35";
        public static final String GU53 = "GU5.3";
        public static final String GX53 = "GX5.3";
        public static final String GY635 = "GY6.35";
        public static final String S14d = "S14d";
        public static final String S14s = "S14s";
        public static final String R7s = "R7s";
    }
}
