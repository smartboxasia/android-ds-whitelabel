package com.smartboxasia.control.data.connection.cloud_1_0.json;

public class JSONTags {

    public static final String RESPONSE = "Response";
    public static final String RETURN_CODE = "ReturnCode";
    public static final String RETURN_MESSAGE = "ReturnMessage";
    public static final String TOKEN = "Token";
    public static final String RELAY_LINK = "RelayLink";
    public static final String DSID = "DssId";
    public static final String APARTMENT_ID = "ApartmentId";

}
