package com.smartboxasia.control.data.config.app;

import java.util.List;
import java.util.Set;

import android.content.Context;
import android.util.Log;

import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.R;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.dto.DsScene;
import com.smartboxasia.control.ui.helper.SceneHelper;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;

public class ActivitiesCache {

    protected static final String PREFS_LIST_DELIMITER = "||";

    /**
     * Load shown scenes from shared preferences in following format: [roomId, sceneId, groupNumber, roomId2, sceneId, groupNumber2, etc....]
     */
    protected static Set<DsScene> parseSerializedScenes(final Context context, final String allScenes) {
        final Set<DsScene> workingSceneList = Sets.newLinkedHashSet();

        final List<String> tokens = Splitter.on(PREFS_LIST_DELIMITER).splitToList(allScenes);

        if (tokens.size() % 3 != 0) {
            // saved scenes corrupted
            Log.e("shown Scenes: ", "Saved activity prefs empty or corrupted: " + allScenes);
            return workingSceneList;
        }
        for (int j = 0; j < tokens.size(); j = j + 3) {

            final Integer roomId = Ints.tryParse(tokens.get(j));
            final Long sceneId = Longs.tryParse(tokens.get(j + 1));
            final Integer groupNumber = Ints.tryParse(tokens.get(j + 2));

            if (roomId == null || sceneId == null || groupNumber == null) {
                continue; // skip if a value is not parsable
            }

            // check room id
            // special case: if the scene is a global activity, the room id is 0 or smaller.
            if (DssConstants.Rooms.isGlobal(roomId)) {
                createGlobalActivityFavorite(context, workingSceneList, sceneId);
            } else {
                createNormalFavorite(context, workingSceneList, roomId, sceneId, groupNumber);
            }
        }

        return workingSceneList;
    }

    private static void createGlobalActivityFavorite(final Context context, final Set<DsScene> workingSceneList, final long sceneId) {
        String sceneName = null;
        int roomId = DssConstants.Rooms.GLOBAL;
        if (sceneId == DssConstants.Scenes.PANIC) {
            sceneName = context.getString(R.string.panic);
        } else if (sceneId == DssConstants.Scenes.ABSENT) {
            sceneName = context.getString(R.string.leave_home);
        } else if (sceneId == DssConstants.Scenes.PRESENT) {
            sceneName = context.getString(R.string.come_home);
        } else if (sceneId == DssConstants.Scenes.BELL_SINGAL) {
            sceneName = context.getString(R.string.door_bell);
        } else if (sceneId >= DssConstants.Scenes.USER_DEFINED_ACTIONS) {
            for (final DsScene userDefinedAction : UserDefinedActionsCache.get_userDefinedActions()) {
                // check scene id
                if (userDefinedAction.get_sceneId() == sceneId) {
                    sceneName = userDefinedAction.get_name();
                    roomId = DssConstants.Rooms.USER_ACTION;
                }
            }
        }
        if (!Strings.isNullOrEmpty(sceneName)) {
            workingSceneList.add(new DsScene(sceneName, sceneId, DssConstants.Groups.ACTION, roomId));
        }
    }

    private static void createNormalFavorite(final Context context, final Set<DsScene> workingSceneList, final int roomId, final long sceneId, final int groupNumber) {
        final DsRoom room = RoomsStore.get_room_by_id(roomId);
        if (room != null) {
            final List<DsScene> customScenes = room.get_scenes();
            final String defaultSceneName = SceneHelper.getDefaultSceneName(context, sceneId, groupNumber);
            final DsScene defaultScene = new DsScene(defaultSceneName, sceneId, groupNumber, roomId);
            if (customScenes.contains(defaultScene)) {
                final int customSceneIndex = customScenes.indexOf(defaultScene);
                workingSceneList.add(customScenes.get(customSceneIndex));
            } else {
                workingSceneList.add(defaultScene);
            }
        }
    }
}
