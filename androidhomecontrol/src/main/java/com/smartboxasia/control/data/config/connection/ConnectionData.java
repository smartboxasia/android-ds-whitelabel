package com.smartboxasia.control.data.config.connection;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.common.base.Strings;

public class ConnectionData implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Manually added connection configuration
     */
    public static final int CONNECTION_TYPE_MANUAL = 0;
    /**
     * Cloud connection configuration with an url and application token validated by SMARTBOX
     */
    public static final int CONNECTION_TYPE_CLOUD = 1;
    /**
     * A Locally found server has been used to create this connection configuration
     */
    public static final int CONNECTION_TYPE_LOCAL = 2;
    /**
     * Cloud only connection configuration with an url and application token validated by SMARTBOX
     */
    public static final int CONNECTION_TYPE_CLOUD_ONLY = 3;

    public static final long NO_CONNECTION = -1;

    private final int type;

    long connectionId = NO_CONNECTION;
    String name = "";
    String user = "";
    String url = "";
    String cloudUrl = "";
    String applicationToken = "";
    String serverId = "";
    String apartmentId = "";
    boolean hasValidAppToken = false;

    public ConnectionData(final int type) {
        this.type = type;
    }

    public void saveConfig() {
        if (connectionId == NO_CONNECTION) {
            ConnectionConfigService.addConnectionData(this);
        }
        updateConfigIfPersisted();
    }

    protected void updateConfigIfPersisted() {
        if (connectionId != NO_CONNECTION) {
            ConnectionConfigService.updateConnectionData(this);
        }
    }

    public long getConnectionId() {
        return connectionId;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
        updateConfigIfPersisted();
    }

    public String getUrl() {
        return url;
    }

    public String getHost() {
        try {
            return new URL(url).getHost();
        } catch (final MalformedURLException e) {
            // ignore
        }
        return "";
    }

    public String getPort() {
        try {
            return "" + new URL(url).getPort();
        } catch (final MalformedURLException e) {
            // ignore
        }
        return "";
    }

    public void setUrl(String url) {
        // direct connection urls end without slash
        url = Strings.nullToEmpty(url);
        if (url.endsWith("/")) {
            url = url.substring(0, url.length() - 1);
        }
        this.url = url;
        updateConfigIfPersisted();
    }

    public void setCloudUrl(String cloudUrl) {
        // cloud connection urls end with slash
        cloudUrl = Strings.nullToEmpty(cloudUrl);
        if (!cloudUrl.endsWith("/")) {
            cloudUrl = cloudUrl + "/";
        }
        this.cloudUrl = cloudUrl;
        updateConfigIfPersisted();
    }

    public String getCloudUrl() {
        return cloudUrl;
    }

    public String getApplicationToken() {
        return applicationToken;
    }

    public void setApplicationToken(final String applicationToken) {
        this.applicationToken = applicationToken;
        updateConfigIfPersisted();
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(final String serverId) {
        this.serverId = serverId;
        updateConfigIfPersisted();
    }

    public String getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(final String apartmentId) {
        this.apartmentId = apartmentId;
    }

    public boolean hasValidAppToken() {
        return hasValidAppToken;
    }

    public void setHasValidAppToken(final boolean validity) {
        hasValidAppToken = validity;
        updateConfigIfPersisted();
    }

    public boolean isCloudLogin() {
        return isCloudLogin(url);
    }

    public static boolean isCloudLogin(final String url) {
        return url.contains("smartboxasia.net");
    }

    public int getType() {
        return type;
    }

    public String getUser() {
        return user;
    }

    public void setUser(final String user) {
        this.user = user;
        updateConfigIfPersisted();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((applicationToken == null) ? 0 : applicationToken.hashCode());
        result = prime * result + ((cloudUrl == null) ? 0 : cloudUrl.hashCode());
        result = prime * result + (int) (connectionId ^ (connectionId >>> 32));
        result = prime * result + (hasValidAppToken ? 1231 : 1237);
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((user == null) ? 0 : user.hashCode());
        result = prime * result + ((serverId == null) ? 0 : serverId.hashCode());
        result = prime * result + ((apartmentId == null) ? 0 : apartmentId.hashCode());
        result = prime * result + type;
        result = prime * result + ((url == null) ? 0 : url.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ConnectionData)) {
            return false;
        }
        final ConnectionData other = (ConnectionData) obj;
        if (applicationToken == null) {
            if (other.applicationToken != null) {
                return false;
            }
        } else if (!applicationToken.equals(other.applicationToken)) {
            return false;
        }
        if (cloudUrl == null) {
            if (other.cloudUrl != null) {
                return false;
            }
        } else if (!cloudUrl.equals(other.cloudUrl)) {
            return false;
        }
        if (connectionId != other.connectionId) {
            return false;
        }
        if (hasValidAppToken != other.hasValidAppToken) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (user == null) {
            if (other.user != null) {
                return false;
            }
        } else if (!user.equals(other.user)) {
            return false;
        }
        if (serverId == null) {
            if (other.serverId != null) {
                return false;
            }
        } else if (!serverId.equals(other.serverId)) {
            return false;
        }
        if (apartmentId == null) {
            if (other.apartmentId != null) {
                return false;
            }
        } else if (!apartmentId.equals(other.apartmentId)) {
            return false;
        }
        if (type != other.type) {
            return false;
        }
        if (url == null) {
            if (other.url != null) {
                return false;
            }
        } else if (!url.equals(other.url)) {
            return false;
        }
        return true;
    }

    public void update(final ConnectionData fullData) {
        name = fullData.name;
        user = fullData.user;
        url = fullData.url;
        cloudUrl = fullData.cloudUrl;
        applicationToken = fullData.applicationToken;
        serverId = fullData.serverId;
        apartmentId = fullData.apartmentId;
        hasValidAppToken = fullData.hasValidAppToken;
    }
}
