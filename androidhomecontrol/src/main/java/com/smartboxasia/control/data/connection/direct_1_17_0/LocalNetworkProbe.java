package com.smartboxasia.control.data.connection.direct_1_17_0;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.MulticastLock;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;

import com.smartboxasia.control.data.config.connection.ConnectionData;

public class LocalNetworkProbe implements ServiceListener {

    private static final String TAG = LocalNetworkProbe.class.getSimpleName();

    private static final String DSS_SERVICE = "_dssweb._tcp.local.";
    private static final int DELAY = 200;
    private static final String HOSTNAME = "smartboxasia";

    private static MulticastLock mLock = null;

    private JmDNS zeroConf = null;

    private DiscoverServerHandler resultsUpdateHandler;

    private Context context;

    public static interface UpdateListener<T> {
        void updateReceived(T result);
    }

    public LocalNetworkProbe(final Context context, final UpdateListener<ConnectionData> serverDiscoveredListener) {
        this.context = context;
        resultsUpdateHandler = new DiscoverServerHandler(serverDiscoveredListener);
    }

    public void startProbe() throws IOException {

        if (zeroConf != null) {
            this.stopProbe();
        }

        // http://stackoverflow.com/a/9289190/1150427
        // not encouraged to do so. FIXME: 26/4/16
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Log.i(TAG, "Starting Multicast Lock...");
        final WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        // get the device ip address
        final InetAddress deviceIpAddress = getDeviceIpAddress(wifi);
        mLock = wifi.createMulticastLock(getClass().getName());
        mLock.setReferenceCounted(true);
        mLock.acquire();
        Log.i(TAG, "Starting ZeroConf probe....");
        zeroConf = JmDNS.create(deviceIpAddress, HOSTNAME);
        zeroConf.addServiceListener(DSS_SERVICE, this);
        resultsUpdateHandler.setZeroConf(zeroConf);

        Log.i(TAG, "Started ZeroConf probe....");
    }

    public void stopProbeAsync() {
        final Thread backgroundThread = new Thread() {
            @Override
            public void run() {
                stopProbe();
            }
        };
        try {
            backgroundThread.start();
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    private void stopProbe() {
        Log.i(TAG, "Stopping ZeroConf Probe...");
        if (zeroConf != null) {
            zeroConf.removeServiceListener(DSS_SERVICE, LocalNetworkProbe.this);
            zeroConf.unregisterAllServices();
            try {
                zeroConf.close();
            } catch (final IOException e) {
                e.printStackTrace();
            }
            zeroConf = null;
        }
        if (mLock != null) {
            mLock.release();
            mLock = null;
        }
    }

    private static class DiscoverServerHandler extends Handler {

        private UpdateListener<ConnectionData> listener;
        private JmDNS zeroConf;

        public DiscoverServerHandler(final UpdateListener<ConnectionData> listener) {
            this.listener = listener;
        }

        public void setZeroConf(final JmDNS zeroConf) {
            this.zeroConf = zeroConf;
        }

        @Override
        public void handleMessage(final Message msg) {
            if (msg.obj != null && zeroConf != null) {
                final String library = (String) msg.obj;
                final ServiceInfo serviceInfo = zeroConf.getServiceInfo(DSS_SERVICE, library);
                final ConnectionData data = new ConnectionData(ConnectionData.CONNECTION_TYPE_LOCAL);
                data.setName(serviceInfo.getName());
                final String url = serviceInfo.getHostAddresses()[0]; // just get the first (same as the reference impl)
                data.setUrl("https://" + url + ":8080"); // and also complete the host addr like the reference

                Log.d(TAG, "Found Service: name: " + data.getName() + ", url: " + data.getUrl());

                listener.updateReceived(data);
            }
        }
    }

    /**
     * Gets the current Android device IP address or return 10.0.0.2 which is localhost on Android.
     * <p>
     * 
     * @return the InetAddress of this Android device
     */
    private InetAddress getDeviceIpAddress(final WifiManager wifi) {
        InetAddress result = null;
        try {
            // default to Android localhost
            result = InetAddress.getByName("10.0.0.2");

            // figure out our wifi address, otherwise bail
            final WifiInfo wifiInfo = wifi.getConnectionInfo();
            final int intaddr = wifiInfo.getIpAddress();
            final byte[] byteaddr = new byte[] {
                    (byte) (intaddr & 0xff), (byte) (intaddr >> 8 & 0xff), (byte) (intaddr >> 16 & 0xff), (byte) (intaddr >> 24 & 0xff)
            };
            result = InetAddress.getByAddress(byteaddr);
        } catch (final UnknownHostException ex) {
            Log.w(TAG, String.format("getDeviceIpAddress Error: %s", ex.getMessage()));
        }

        return result;
    }

    @Override
    public void serviceAdded(final ServiceEvent event) {
        // go figure out what their ip address is
        Log.w(TAG, String.format("serviceAdded(event=\n%s\n)", event.toString()));
        zeroConf.requestServiceInfo(event.getType(), event.getName(), 1);
        final String name = event.getName();

        // trigger delayed gui event
        // needs to be delayed because jmdns hasnt parsed txt info yet
        resultsUpdateHandler.sendMessageDelayed(Message.obtain(resultsUpdateHandler, -1, name), DELAY);
    }

    @Override
    public void serviceRemoved(final ServiceEvent event) {
        Log.w(TAG, String.format("serviceRemoved(event=\n%s\n)", event.toString()));
    }

    @Override
    public void serviceResolved(final ServiceEvent event) {
        Log.w(TAG, String.format("serviceResolved(event=\n%s\n)", event.toString()));
    }
}
