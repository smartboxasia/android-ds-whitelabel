package com.smartboxasia.control.data.dss.direct_1_17_0;

import java.util.List;

import com.smartboxasia.control.domain.OutputMode;
import com.google.common.collect.Lists;

class DsDeviceGeSds200JokerBehavior implements DsDeviceBehavior {

    @Override
    public List<OutputMode> getAvailableOutputModes() {
        return Lists.newArrayList();
    }

    @Override
    public boolean isOemDevice() {
        return false;
    }

    @Override
    public boolean canModifyOutputMode() {
        return true;
    }

}
