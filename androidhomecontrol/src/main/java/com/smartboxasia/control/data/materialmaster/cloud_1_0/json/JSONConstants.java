package com.smartboxasia.control.data.materialmaster.cloud_1_0.json;

public class JSONConstants {

    public static final String FITTING_CHECK_FITS = "fits";
    public static final String FITTING_CHECK_NO_FIT_SOCKET_TYPE = "no_fit_socket_type_no_match";
    public static final String FITTING_CHECK_NO_FIT_MAX_POWER = "no_fit_max_power_no_match";
    public static final String FITTING_CHECK_NO_FIT_MAX_POWER_SOCKET_TYPE = "no_fit_socket_type_max_power";
    public static final String FITTING_CHECK_NO_CHECK_MISSING_DATA = "no_check_missing_device_data";
    public static final String FITTING_CHECK_NO_CHECK_MISSING_ILLUMINANT_DATA = "no_check_missing_illuminant_data";
    public static final String ILLUMINANT_DIMMING_RESULT_GREEN = "Green";
    public static final String ILLUMINANT_DIMMING_RESULT_YELLOW = "Yellow";
    public static final String ILLUMINANT_DIMMING_RESULT_RED = "Red";
}
