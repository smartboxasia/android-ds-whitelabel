package com.smartboxasia.control.data.helper;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.util.Log;

import com.smartboxasia.control.TZ;
import com.smartboxasia.control.data.connection.Connection;

public class EncodingHelper {

    private static final String TAG = EncodingHelper.class.getSimpleName();

    /**
     * This method unescapes escaped strings that were specifically made on the server side. Should be used for small strings like names that the user has input into the system.
     *
     * @param string the string to unescape
     * @return the string without escape characters
     */
    public static String unescape(final String string) {
        return string.replaceAll("&amp;", "&").replaceAll("&quot;", "\"").replaceAll("&#039;", "'").replaceAll("&lt;", "<").replaceAll("&gt;", ">");
    }

    public static String getEncodedUser() {
        return getEncodedString(Connection.getCurrentConnectionUser());
    }

    public static String getEncodedString(final String string) {
        final String encoding = "UTF-8";
        try {
            return URLEncoder.encode(string, encoding);
        } catch (final UnsupportedEncodingException e) {
            Log.e(TAG, "Unknown encoding UTF-8. This means trouble!");
        }
        return "";
    }

    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss"; // Dates are always in UTC, skip the timezone
    private static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DATE_TIME_PATTERN, Locale.getDefault());
    static {
        dateTimeFormat.setTimeZone(TZ.UTC);
    }

    public static synchronized String formatDateTimeString(final Date date) {
        return dateTimeFormat.format(date) + "Z"; // manually append Z as UTC timezone indicator
    }

    public static synchronized Date parseDateTimeString(final String dateTimeString) {
        return dateTimeFormat.parse(dateTimeString, new ParsePosition(0));
    }

    private static final String TIME_PATTERN = "HH:mm";
    private static final SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_PATTERN, Locale.getDefault());

    public static synchronized String formatTimeString(final Date date) {
        return timeFormat.format(date);
    }

    public static synchronized Date parseTimeString(final String timeString) {
        return timeFormat.parse(timeString, new ParsePosition(0));
    }
}
