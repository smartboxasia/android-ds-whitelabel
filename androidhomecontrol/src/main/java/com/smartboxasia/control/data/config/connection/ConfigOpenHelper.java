package com.smartboxasia.control.data.config.connection;

import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;

import com.smartboxasia.control.R;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

public class ConfigOpenHelper {

    // list of all configured servers
    private static final String PREFS_CUSTOM_SERVERS = "customServers";
    private static final String KEY_SERVERS = "servers";

    // List of all application tokens
    private static final String PREFS_APPLICATION_TOKENS = "applicationTokens";
    private static final String KEY_IS_CLOUD_POSTFIX = "isCloud";

    // Last connection used
    private static final String SERVER_PREFS = "serverURL";
    private static final String KEY_CLOUD_URL = "cloudUrl";
    private static final String KEY_SERVER_NAME = "serverName";
    private static final String KEY_SERVER_URL = "url";

    private Context context;

    public ConfigOpenHelper(final Context context) {
        this.context = context;
    }

    // //////////////////
    // Connection Methods
    // //////////////////

    private void persistCustomServers(final List<String> customServers) {
        // persist it in sharedPreferences
        final SharedPreferences preferences = context.getSharedPreferences(PREFS_CUSTOM_SERVERS, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        if (customServers.size() > 0) {
            // Create one string with all the custom servers
            final String customServersString = Joiner.on("||").join(customServers);
            editor.putString(KEY_SERVERS, customServersString);
        } else {
            editor.clear();
        }
        editor.commit();
    }

    private List<String> restoreCustomServers() {
        // retrieve custom servers from shared preferences
        final SharedPreferences urlPreferences = context.getSharedPreferences(PREFS_CUSTOM_SERVERS, Context.MODE_PRIVATE);
        final String allCustomServers = urlPreferences.getString(KEY_SERVERS, "null");

        final List<String> customServers = Lists.newArrayList();
        if (allCustomServers.compareTo("null") != 0) {
            customServers.addAll(Splitter.on("||").splitToList(allCustomServers));
        }
        return customServers;
    }

    public synchronized void addConnectionData(final ConnectionData data) {
        // set or replace app token
        setAppTokenForUrl(data);
        setCloudUrl(data);

        // add new entry if it is a new server
        final List<String> customServers = restoreCustomServers();
        if (!customServers.contains(data.getUrl())) {
            customServers.add(data.getName());
            customServers.add(data.getUrl());
            persistCustomServers(customServers);
        }

        data.connectionId = 0l; // make sure we mark the connection data as saved
    }

    public synchronized void deleteConnectionData(final String connectionId) {
        final List<String> servers = restoreCustomServers();
        for (int i = 1; i < servers.size() + 1; i += 2) {
            if (connectionId.equals(servers.get(i))) {
                servers.remove(i);
                servers.remove(i - 1);
            }
        }
        persistCustomServers(servers);

        deleteCloudUrl(connectionId);
        deleteAppTokenForUrl(connectionId);
    }

    public synchronized void updateConnectionData(final ConnectionData data) {

        // update app token
        setAppTokenForUrl(data);
        setCloudUrl(data);

        // update connection name
        final List<String> customServers = restoreCustomServers();
        for (int i = 1; i < customServers.size() + 1; i += 2) {
            if (data.getUrl().equals(customServers.get(i))) {
                customServers.set(i - 1, data.getName());
            }
        }
        persistCustomServers(customServers);
    }

    public synchronized ConnectionData getConnectionData(final String connectionId) {
        final List<String> customServers = restoreCustomServers();
        for (int i = 1; i < customServers.size() + 1; i += 2) {
            if (connectionId.equals(customServers.get(i))) {
                return readConnectionData(customServers, i - 1);
            }
        }
        return null;
    }

    public synchronized List<ConnectionData> getAllConnections() {
        final List<ConnectionData> result = Lists.newArrayList();
        final List<String> customServers = restoreCustomServers();
        for (int i = 0; i < customServers.size(); i += 2) {
            result.add(readConnectionData(customServers, i));
        }
        return result;
    }

    private ConnectionData readConnectionData(final List<String> customServers, final int i) {
        final String name = customServers.get(i);
        final String url = customServers.get(i + 1);
        final String appToken = getAppTokenForUrl(url);
        final String cloudUrl = getCloudUrl(url);
        final ConnectionData data = new ConnectionData(getIsCloudForUrl(url) ? ConnectionData.CONNECTION_TYPE_CLOUD : ConnectionData.CONNECTION_TYPE_MANUAL);
        data.name = name;
        data.url = url;
        data.applicationToken = appToken;
        data.hasValidAppToken = true;
        data.cloudUrl = cloudUrl;
        data.connectionId = 0l; // saved connections are always marked stored
        return data;
    }

    private String getAppTokenForUrl(final String url) {
        final SharedPreferences tokenPreferences = context.getSharedPreferences(PREFS_APPLICATION_TOKENS, Context.MODE_PRIVATE);
        return tokenPreferences.getString(url, "");
    }

    public synchronized boolean getIsCloudForUrl(final String url) {
        final SharedPreferences tokenPreferences = context.getSharedPreferences(PREFS_APPLICATION_TOKENS, Context.MODE_PRIVATE);
        return tokenPreferences.getBoolean(url + KEY_IS_CLOUD_POSTFIX, false);
    }

    private void setAppTokenForUrl(final ConnectionData data) {
        final SharedPreferences tokenPreferences = context.getSharedPreferences(PREFS_APPLICATION_TOKENS, Context.MODE_PRIVATE);
        tokenPreferences.edit().putString(data.getUrl(), data.getApplicationToken()).putBoolean(data.getUrl() + KEY_IS_CLOUD_POSTFIX, data.isCloudLogin()).commit();
    }

    public synchronized void deleteAppTokenForUrl(final String url) {
        final SharedPreferences tokenPreferences = context.getSharedPreferences(PREFS_APPLICATION_TOKENS, Context.MODE_PRIVATE);
        tokenPreferences.edit().remove(url).remove(url + KEY_IS_CLOUD_POSTFIX).commit();
    }

    //
    // Last Used Connection Info
    //

    public synchronized void deleteLastUsedConnection() {
        if (getLastUsedConnectionData() == null) {
            return;
        }

        final SharedPreferences.Editor urlPreferences = context.getSharedPreferences(SERVER_PREFS, Context.MODE_PRIVATE).edit();
        urlPreferences.remove(KEY_SERVER_URL);
        urlPreferences.remove(KEY_SERVER_NAME);
        urlPreferences.remove(KEY_CLOUD_URL);
        urlPreferences.commit();
    }

    public synchronized void updateLastUsedConnectionData(final String connectionId) {
        final ConnectionData data = getConnectionData(connectionId);
        // persist the serverURL and name
        final SharedPreferences serverPreferences = context.getSharedPreferences(SERVER_PREFS, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = serverPreferences.edit();
        editor.putString(KEY_SERVER_URL, data.getUrl());
        editor.putString(KEY_SERVER_NAME, data.getName());
        editor.putString(KEY_CLOUD_URL, data.getCloudUrl());
        editor.commit();
    }

    public synchronized ConnectionData getLastUsedConnectionData() {
        final SharedPreferences urlPreferences = context.getSharedPreferences(SERVER_PREFS, Context.MODE_PRIVATE);
        final String url = urlPreferences.getString(KEY_SERVER_URL, "null");
        if (url.compareTo("null") != 0) {
            return getConnectionData(url);
        }
        return null;
    }

    private synchronized String getCloudUrl(final String serverUrl) {
        final SharedPreferences urlPreferences = context.getSharedPreferences(SERVER_PREFS, Context.MODE_PRIVATE);
        return urlPreferences.getString(serverUrl + KEY_CLOUD_URL, context.getString(R.string.cloud_url_production));
    }

    private synchronized void setCloudUrl(final ConnectionData data) {
        final SharedPreferences urlPreferences = context.getSharedPreferences(SERVER_PREFS, Context.MODE_PRIVATE);
        urlPreferences.edit().putString(data.getUrl() + KEY_CLOUD_URL, data.getCloudUrl()).commit();
    }

    private synchronized void deleteCloudUrl(final String serverUrl) {
        final SharedPreferences urlPreferences = context.getSharedPreferences(SERVER_PREFS, Context.MODE_PRIVATE);
        urlPreferences.edit().remove(serverUrl + KEY_CLOUD_URL).commit();
    }

    public synchronized String getGlobalCloudUrl() {
        final SharedPreferences urlPreferences = context.getSharedPreferences(SERVER_PREFS, Context.MODE_PRIVATE);
        return urlPreferences.getString(KEY_CLOUD_URL, context.getString(R.string.cloud_url_production));
    }

    public synchronized void setDebugCloudLoginUrl(final String cloudUrl) {
        final SharedPreferences urlPreferences = context.getSharedPreferences(SERVER_PREFS, Context.MODE_PRIVATE);
        urlPreferences.edit().putString(KEY_CLOUD_URL, cloudUrl).commit();
    }
}
