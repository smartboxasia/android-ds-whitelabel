package com.smartboxasia.control.data.connection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.smartboxasia.control.App;

public class ConnectivityChangeReceiver extends BroadcastReceiver {

    public enum ConnectivityState {
        UP,
        DOWN;
    }

    public static class ConnectivityChangedEvent {

        private final ConnectivityState state;

        public ConnectivityChangedEvent(final ConnectivityState state) {
            this.state = state;
        }

        public ConnectivityState getState() {
            return state;
        }
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        final ConnectivityManager connectivityManager = (ConnectivityManager) App.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        final boolean isConnected = networkInfo != null && networkInfo.isConnected();
        if (!isConnected) {
            App.eventBus.post(new ConnectivityChangedEvent(ConnectivityState.DOWN));
        }
    }
}
