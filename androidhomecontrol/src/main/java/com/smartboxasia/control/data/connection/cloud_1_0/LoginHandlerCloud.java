package com.smartboxasia.control.data.connection.cloud_1_0;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.smartboxasia.control.App;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.connection.ConnectionHandler;
import com.smartboxasia.control.data.connection.LoginHandler;
import com.smartboxasia.control.data.connection.cloud_1_0.json.JSONTags;
import com.smartboxasia.control.data.connection.exception.NeedsSessionRefreshException;
import com.smartboxasia.control.data.helper.CommandBuilder;
import com.smartboxasia.control.data.helper.EncodingHelper;
import com.smartboxasia.control.data.helper.JSONCallback;
import com.smartboxasia.control.data.helper.UUIDHelper;
import com.smartboxasia.control.domain.LoginResult;
import com.google.common.base.Strings;

public class LoginHandlerCloud extends LoginHandler {

    private static final String TAG = LoginHandlerCloud.class.getSimpleName();

    public LoginHandlerCloud(final ConnectionHandler connectionHandler) {
        super(connectionHandler);
    }

    @Override
    public LoginResult login(final String user, final String pass, final boolean silent) {
        boolean gotAFreshAppToken = false;
        Log.d("LOGIN-TRACE", "Cloud LoginHandler start logging in");
        if (!Strings.isNullOrEmpty(user) && !Strings.isNullOrEmpty(pass)) {
            Log.d("LOGIN-TRACE", "-- credentials were passed in, use them to get a new enabled application token from the cloud");
            final LoginResult result = getEnabledAppToken(user, pass);
            if (result == LoginResult.LOGIN_RESULT_OK) {
                // remember that we just got a new app token.
                gotAFreshAppToken = true;
            } else {
                Log.d("LOGIN-TRACE", "  -- could not get an enabled app token from the cloud");
                return result;
            }
        }

        // if we don't have an app token yet, we need to get the credentials from the user
        if (Strings.isNullOrEmpty(connectionData.getApplicationToken())) {
            Log.d("LOGIN-TRACE", "-- we don't have an application token. we need to ask the user to give us the credentials to retrieve one");
            return LoginResult.LOGIN_RESULT_NEED_CREDENTIALS;
        }

        // This will be the first contact with the relay linked server
        if (!checkVersion(silent)) {
            Log.d("LOGIN-TRACE", "-- version check failed");
            return LoginResult.LOGIN_RESULT_NOT_OK;
        }

        if (!updateServerId(silent)) {
            Log.d("LOGIN-TRACE", "-- updating the server id failed");
            return LoginResult.LOGIN_RESULT_NOT_OK;
        }

        Log.d("LOGIN-TRACE", "-- we're set for getting a session token");

        // Ugly workaround for an error in the dss that gives out a new app token before it was enabled
        RefreshSessionResult refreshSessionResult = refreshSession(silent);
        if (gotAFreshAppToken) {
            int counter = 10;
            while (counter-- > 0 && refreshSessionResult == RefreshSessionResult.INVALID_APP_TOKEN) {
                Log.d("LOGIN-TRACE", "-- token not yet valid, waiting 2 seconds");
                try {
                    Thread.sleep(2000);
                    refreshSessionResult = refreshSession(silent);
                } catch (final InterruptedException e) {
                    refreshSessionResult = RefreshSessionResult.NOT_OK;
                    Log.d("LOGIN-TRACE", "-- waiting for a fresh session token failed");
                }
            }
        }

        // we've got an app token and the server version has been checked
        switch (refreshSessionResult) {
            case OK: {
                Log.d("LOGIN-TRACE", "-- refreshing the session token succeeded");
                return LoginResult.LOGIN_RESULT_OK;
            }
            case INVALID_APP_TOKEN: {
                Log.d("LOGIN-TRACE", "-- app token was not enabled, try to relogin with new credentials");
                connectionData.setApplicationToken(null);
                connectionData.setHasValidAppToken(false);
                return LoginResult.LOGIN_RESULT_NEED_CREDENTIALS;
            }
            case NOT_OK:
            default: {
                Log.d("LOGIN-TRACE", "-- could not refresh the session");
                return LoginResult.LOGIN_RESULT_NOT_OK;
            }
        }
    }

    private LoginResult getEnabledAppToken(final String userName, final String password) {
        if (Strings.isNullOrEmpty(userName)) {
            return LoginResult.LOGIN_RESULT_NOT_OK;
        }

        final String userEncoded = EncodingHelper.getEncodedString(userName);
        final Context context = App.getInstance();
        final CommandBuilder builder = new CommandBuilder("public/accessmanagement/v1_0/RemoteConnectivity/GetRelayLinkAndToken").addParameter("user", userEncoded).addParameterEncoded("password", password).addParameterEncoded("appName", context.getString(context.getApplicationInfo().labelRes))
                .addParameterEncoded("mobileName", android.os.Build.MODEL).addParameter("mobileAppUuid", UUIDHelper.getUuid());

        try {
            final String resultString = connectionHandler.sendCommandUnauthenticated(builder, false, false);
            final JSONObject result = new JSONObject(resultString);
            final int returnCode = result.getInt(JSONTags.RETURN_CODE);
            if (returnCode == 0) {
                final JSONObject response = result.getJSONObject(JSONTags.RESPONSE);

                String relayLink = response.getString(JSONTags.RELAY_LINK);
                if (!relayLink.endsWith("/")) {
                    relayLink += "/";
                }

                final String token = response.getString(JSONTags.TOKEN);

                // update the used url to the received relay link url. This will also be persisted.
                // also save the received application token. At this point it has already been enabled
                connectionData.setUrl(relayLink);
                connectionData.setApplicationToken(token);
                connectionData.setUser(userName);
                if (connectionData.getName().isEmpty()) {
                    connectionData.setName(userName);
                }

                return LoginResult.LOGIN_RESULT_OK;
            } else if (returnCode == JSONCallback.RETURN_CODE_INVALID_CREDENTIALS) {
                // could not get an authenticated token due to wrong user/pass
                showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_could_not_verify_cloud_message);
                return LoginResult.LOGIN_RESULT_NEED_CREDENTIALS;
            } else {
                // could not get an authenticated token, stop for now and tell the user
                showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_could_not_verify_cloud_message);
                return LoginResult.LOGIN_RESULT_NOT_OK;
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Could not parse result", e);
            showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_communication_error);
        } catch (final IOException e) {
            Log.e(TAG, "Could not communicate with server", e);
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_cloud_message);
        } catch (final NeedsSessionRefreshException e) {
            // Should usually not happen for a cloud connection, but it can happen in case of a 403. Handle it like a normal FileNotFoundException.
            Log.e(TAG, "Could not communicate with server", e.getCause());
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_cloud_message);
        }
        return LoginResult.LOGIN_RESULT_NOT_OK;
    }
}
