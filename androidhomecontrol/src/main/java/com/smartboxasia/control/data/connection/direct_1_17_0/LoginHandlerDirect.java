package com.smartboxasia.control.data.connection.direct_1_17_0;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.smartboxasia.control.App;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.connection.ConnectionHandler;
import com.smartboxasia.control.data.connection.LoginHandler;
import com.smartboxasia.control.data.connection.exception.NeedsSessionRefreshException;
import com.smartboxasia.control.data.connection.json.JSONTags;
import com.smartboxasia.control.data.helper.CommandBuilder;
import com.smartboxasia.control.data.helper.EncodingHelper;
import com.smartboxasia.control.domain.LoginResult;
import com.google.common.base.Strings;

public class LoginHandlerDirect extends LoginHandler {

    private static final String TAG = LoginHandlerDirect.class.getSimpleName();

    public LoginHandlerDirect(final ConnectionHandler connectionHandler) {
        super(connectionHandler);
    }

    @Override
    public LoginResult login(final String user, final String pass, final boolean silent) {
        Log.d("LOGIN-TRACE", "Direct LoginHandler start logging in");
        // only the first contact could lead to an UnknownCertificateException.
        // if the user doesn't accept it, we don't want to succeed afterwards.
        if (!checkVersion(silent)) {
            Log.d("LOGIN-TRACE", "-- version check failed");
            return LoginResult.LOGIN_RESULT_NOT_OK;
        }

        if (!updateServerId(silent)) {
            Log.d("LOGIN-TRACE", "-- updating the server id failed");
            return LoginResult.LOGIN_RESULT_NOT_OK;
        }

        if (!Strings.isNullOrEmpty(user) && !Strings.isNullOrEmpty(pass)) {
            Log.d("LOGIN-TRACE", "-- credentials were passed in, use them to get a new application token");

            // first try to log in
            String tempToken = requestTempToken(user, pass);
            if (tempToken == null) {
                Log.d("LOGIN-TRACE", "  -- could not log in with credentials");
                return LoginResult.LOGIN_RESULT_NEED_CREDENTIALS;
            }

            Log.d("LOGIN-TRACE", "  -- credentials verified.");

            // logging in succeeded. try to get an eneabled app token
            String applicationToken = connectionData.getApplicationToken();
            if (!Strings.isNullOrEmpty(applicationToken)) {
                Log.d("LOGIN-TRACE", "  -- we already have an app token. trying to enable it.");
                if (!enableAppToken(applicationToken, tempToken, false)) {
                    Log.d("LOGIN-TRACE", "  -- could not enable old application token. trying to get a new one");
                }
            }

            if (!connectionData.hasValidAppToken()) {
                // to get a new app token we have to log out first
                if (!logout()) {
                    Log.d("LOGIN-TRACE", "  -- could not log out");
                    return LoginResult.LOGIN_RESULT_NOT_OK;
                }

                // now we get a new app token
                applicationToken = requestAppToken();
                if (applicationToken == null) {
                    Log.d("LOGIN-TRACE", "  -- could not get a new app token");
                    return LoginResult.LOGIN_RESULT_NOT_OK;
                }

                // now we need to log in again
                tempToken = requestTempToken(user, pass);
                if (tempToken == null) {
                    Log.d("LOGIN-TRACE", "  -- no temp token returned");
                    return LoginResult.LOGIN_RESULT_NOT_OK;
                }
                // if we have both, we can try to enable the app token
                if (!enableAppToken(applicationToken, tempToken, true)) {
                    Log.d("LOGIN-TRACE", "  -- could not enable application token");
                    return LoginResult.LOGIN_RESULT_NOT_OK;
                }
            }
        }

        // if we don't have a valid app token yet, we need to get the credentials
        if (!connectionData.hasValidAppToken()) {
            Log.d("LOGIN-TRACE", "-- we don't have an application token. we need to ask the user to give us the credentials to retrieve one");
            return LoginResult.LOGIN_RESULT_NEED_CREDENTIALS;
        }

        Log.d("LOGIN-TRACE", "-- we're set for getting a session token");
        // we have an application token. Use it to refresh the session token
        switch (refreshSession(silent)) {
            case OK: {
                Log.d("LOGIN-TRACE", "-- refreshing the session token succeeded");
                return LoginResult.LOGIN_RESULT_OK;
            }
            case INVALID_APP_TOKEN: {
                return LoginResult.LOGIN_RESULT_NEED_CREDENTIALS;
            }
            case NOT_OK:
            default: {
                return LoginResult.LOGIN_RESULT_NOT_OK;
            }
        }

    }

    private boolean logout() {
        final CommandBuilder builder = new CommandBuilder("/json/system/logout");
        try {
            final String resultString = connectionHandler.sendCommandUnauthenticated(builder, true, true);
            final JSONObject result = new JSONObject(resultString);
            final boolean ok = result.getBoolean(JSONTags.OK);
            if (ok) {
                return true;
            } else {
                // could not log out
                showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_could_not_logout_message);
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Could not parse result", e);
            showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_communication_error);
        } catch (final IOException e) {
            Log.e(TAG, "Could not communicate with server", e);
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_direct_message);
        } catch (final NeedsSessionRefreshException e) {
            Log.i(TAG, "Session was already timed out when logout was called");
        }
        return false;
    }

    private String requestAppToken() {
        final Context context = App.getInstance();
        final String encodedAppName = EncodingHelper.getEncodedString(context.getString(context.getApplicationInfo().labelRes));
        final String encodedModelName = EncodingHelper.getEncodedString(android.os.Build.MODEL);
        final CommandBuilder builder = new CommandBuilder("/json/system/requestApplicationToken").addParameter("applicationName", encodedModelName + ":+" + encodedAppName);

        try {
            final String resultString = connectionHandler.sendCommandUnauthenticated(builder, true, true);
            final JSONObject result = new JSONObject(resultString);
            final boolean ok = result.getBoolean(JSONTags.OK);
            if (ok) {
                final String appToken = result.getJSONObject(JSONTags.RESULT).getString(JSONTags.APPLICATION_TOKEN);
                connectionData.setApplicationToken(appToken); // save the new app token
                return appToken;
            } else {
                // could not get a new application token
                showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_could_not_get_application_tokenverify_direct_message);
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Could not parse result", e);
            showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_communication_error);
        } catch (final IOException e) {
            Log.e(TAG, "Could not communicate with server", e);
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_direct_message);
        } catch (final NeedsSessionRefreshException e) {
            // No session necessary for this call. A 403 should be handled like a normal FileNotFoundException
            Log.e(TAG, "Could not communicate with server", e);
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_direct_message);
        }
        return null;
    }

    private String requestTempToken(final String userName, final String password) {
        final String user = EncodingHelper.getEncodedString(userName);
        final String pass = EncodingHelper.getEncodedString(password);

        if (Strings.isNullOrEmpty(userName)) {
            return null;
        }

        final CommandBuilder builder = new CommandBuilder("/json/system/login").addParameter("user", user).addParameter("password", pass);

        try {
            final String resultString = connectionHandler.sendCommandUnauthenticated(builder, true, true);
            final JSONObject result = new JSONObject(resultString);
            final boolean ok = result.getBoolean(JSONTags.OK);
            if (ok) {
                return result.getJSONObject(JSONTags.RESULT).getString(JSONTags.TOKEN);
            } else {
                // could not get an authentication token, stop for now and tell the user
                showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_could_not_verify_direct_message);
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Could not parse result", e);
            showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_communication_error);
        } catch (final IOException e) {
            Log.e(TAG, "Could not communicate with server", e);
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_direct_message);
        } catch (final NeedsSessionRefreshException e) {
            // No session necessary for this call. A 403 should be handled like a normal FileNotFoundException
            Log.e(TAG, "Could not communicate with server", e);
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_direct_message);
        }
        return null;
    }

    private boolean enableAppToken(final String appToken, final String tempToken, final boolean showError) {

        final CommandBuilder builder = new CommandBuilder("/json/system/enableToken").addParameter("applicationToken", appToken).addParameter("token", tempToken);

        try {
            final String resultString = connectionHandler.sendCommandUnauthenticated(builder, true, true);
            final JSONObject result = new JSONObject(resultString);
            final boolean ok = result.getBoolean(JSONTags.OK);
            if (ok) {
                // the app token is now valid
                connectionData.setHasValidAppToken(true);
                return true;
            } else {
                connectionData.setApplicationToken(null);
                if (showError) {
                    showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_could_not_enable_token);
                }
                return false;
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Could not parse result", e);
            showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_communication_error);
        } catch (final IOException e) {
            Log.e(TAG, "Could not communicate with server", e);
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_direct_message);
        } catch (final NeedsSessionRefreshException e) {
            // Freshly created session is already invalid? Handle it like a normal FileNotFoundException
            Log.e(TAG, "Could not communicate with server", e);
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_direct_message);
        }
        return false;
    }
}
