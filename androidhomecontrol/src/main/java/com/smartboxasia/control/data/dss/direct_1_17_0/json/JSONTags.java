package com.smartboxasia.control.data.dss.direct_1_17_0.json;

public class JSONTags {

    public static final String ZONES = "zones";
    public static final String GROUPS = "groups";
    public static final String SCENES = "scenes";
    public static final String DEVICES = "devices";
    public static final String SCENE = "scene";
    public static final String GROUP = "group";
    public static final String NAME = "name";
    public static final String LAST_CALLED_SCENE = "lastCalledScene";
    public static final String APARTMENT = "apartment";
    public static final String ZONE_ID = "ZoneID";
    public static final String IS_PRESENT = "isPresent";
    public static final String PRESENT = "present";
    public static final String ON = "on";
    public static final String OUTPUT_MODE = "outputMode";
    public static final String METER_DSUID = "meterDSUID";
    public static final String METER_DSID = "meterDSID";
    public static final String PRODUCT_ID = "productID";
    public static final String GTIN = "GTIN";
    public static final String ENERGY_METER_VALUE = "energyMeterValue";
    public static final String POWER_CONSUMPTION = "powerConsumption";
    public static final String CAPABILITIES = "capabilities";
    public static final String BUS_MEMBER_TYPE = "busMemberType";
    public static final String DS_METERS = "dSMeters";
    public static final String METERING = "metering";
    public static final String TEMPERATURE_CONTROL = "temperature-control";
    public static final String CONSUMPTION = "consumption";
    public static final String EVENTS = "events";
    public static final String OEM_EAN_NUMBER = "OemEanNumber";

    public static final String ID = "id";
    public static final String DSID1 = "dsid";
    public static final String DSID2 = "dSID";
    public static final String DSUID1 = "dsuid";
    public static final String DSUID2 = "dSUID";
    public static final String DEVICE_ID = "deviceID";
    public static final String DISPLAY_ID = "DisplayID";

}
