package com.smartboxasia.control.data.helper;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;

import android.content.Context;
import android.content.SharedPreferences;

import com.smartboxasia.control.App;
import com.google.common.base.Strings;

public class UUIDHelper {

    private static final String PREFS_UUID = "UUID";
    private static final String PREFS_UUID_KEY = "UUID";

    private static String uuid = "";

    public static String getUuid() {
        if (Strings.isNullOrEmpty(uuid)) {
            uuid = id(App.getInstance()); // Backward compatibility
            if (Strings.isNullOrEmpty(uuid)) {
                final SharedPreferences prefs = App.getInstance().getSharedPreferences(PREFS_UUID, Context.MODE_PRIVATE);
                if (!prefs.contains(PREFS_UUID_KEY)) {
                    final String newUuid = UUID.randomUUID().toString();
                    prefs.edit().putString(PREFS_UUID_KEY, newUuid).commit();
                }
                uuid = prefs.getString(PREFS_UUID_KEY, "");
            }
        }
        return uuid;
    }

    /**
     * Backward compatibility code
     */

    private synchronized static String id(final Context context) {
        String result = "";
        final File installation = new File(context.getFilesDir(), "INSTALLATION");
        if (installation.exists()) {
            try {
                result = readInstallationFile(installation);
            } catch (final Exception e) {
                // ignore
            } finally {
                installation.delete();
            }
        }
        return result;
    }

    private static String readInstallationFile(final File installation) throws IOException {
        final RandomAccessFile f = new RandomAccessFile(installation, "r");
        final byte[] bytes = new byte[(int) f.length()];
        f.readFully(bytes);
        f.close();
        return new String(bytes);
    }
}
