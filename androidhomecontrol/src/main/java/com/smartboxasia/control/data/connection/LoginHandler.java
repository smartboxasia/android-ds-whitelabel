package com.smartboxasia.control.data.connection;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.smartboxasia.control.App;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.connection.exception.NeedsSessionRefreshException;
import com.smartboxasia.control.data.connection.json.JSONTags;
import com.smartboxasia.control.data.helper.CommandBuilder;
import com.smartboxasia.control.data.helper.VersionHelper;
import com.smartboxasia.control.domain.LoginResult;
import com.smartboxasia.control.events.ShowLoginErrorDialogEvent;

public abstract class LoginHandler {

    private static final String TAG = LoginHandler.class.getSimpleName();

    public static enum RefreshSessionResult {
        OK,
        NOT_OK,
        INVALID_APP_TOKEN
    }

    protected ConnectionHandler connectionHandler;
    protected ConnectionData connectionData;

    public LoginHandler(final ConnectionHandler handler) {
        connectionHandler = handler;
        connectionData = connectionHandler.getConnectionData();
    }

    /**
     * Logs into the server configured in the currently loaded ConnectionConfig. If userName and password are null a Login dialog will pop up if no enabled application token can be found.
     * 
     * @param userName the user name if known
     * @param password the password if known
     * @param silent if true no errors dialogs will be shown
     * @return LoginResult.LOGIN_RESULT_OK if login was successful, LOGIN_RESULT_NOT_OK if it could not connect, or LOGIN_RESULT_NEED_CREDENDIALS if credentials need to be passed to login
     */
    public abstract LoginResult login(final String userName, final String password, final boolean silent);

    public boolean checkVersion(final boolean silent) {

        final CommandBuilder builder = new CommandBuilder("/json/system/version");

        try {
            final String resultString = connectionHandler.sendCommandUnauthenticated(builder, true, true);
            final JSONObject rootObject = new JSONObject(resultString);
            if (rootObject.getBoolean(JSONTags.OK)) {
                final JSONObject resultObject = rootObject.getJSONObject(JSONTags.RESULT);
                final String version = resultObject.getString(JSONTags.VERSION);
                final String versionNumber = VersionHelper.extractVersion(version);
                final boolean result = VersionHelper.dssVersionIsNewerThanMinVersion(versionNumber, ConnectionServiceProviderImpl.MIN_DSS_VERSION);
                if (!result) {
                    final String title = App.getInstance().getString(R.string.min_version_not_met_title);
                    final String message = App.getInstance().getString(R.string.min_version_not_met_msg, ConnectionServiceProviderImpl.MIN_DSS_VERSION);
                    if (!silent) {
                        showLoginErrorDialog(title, message);
                    }
                }
                return result;
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Could not parse result", e);
            if (!silent) {
                showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_communication_error);
            }
        } catch (final IOException e) {
            Log.e(TAG, "Could not communicate with server", e);
            if (!silent) {
                showLoginConnectionErrorDialog();
            }
        } catch (final NeedsSessionRefreshException e) {
            // No session necessary for this call. A 403 should be handled like a normal FileNotFoundException
            Log.e(TAG, "Could not communicate with server", e);
            if (!silent) {
                showLoginConnectionErrorDialog();
            }
        }
        return false;
    }

    public boolean updateServerId(final boolean silent) {

        final CommandBuilder builder = new CommandBuilder("/json/system/getDSID");

        try {
            final String resultString = connectionHandler.sendCommandUnauthenticated(builder, true, true);
            final JSONObject rootObject = new JSONObject(resultString);
            if (rootObject.getBoolean(JSONTags.OK)) {
                final JSONObject resultObject = rootObject.getJSONObject(JSONTags.RESULT);
                final String serverId = resultObject.getString(JSONTags.DSID);
                connectionData.setServerId(serverId);
                return true;
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Could not parse result", e);
            if (!silent) {
                showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_communication_error);
            }
        } catch (final IOException e) {
            Log.e(TAG, "Could not communicate with server", e);
            if (!silent) {
                showLoginConnectionErrorDialog();
            }
        } catch (final NeedsSessionRefreshException e) {
            // No session necessary for this call. A 403 should be handled like a normal FileNotFoundException
            Log.e(TAG, "Could not communicate with server", e);
            if (!silent) {
                showLoginConnectionErrorDialog();
            }
        }
        return false;
    }

    private void showLoginConnectionErrorDialog() {
        if (connectionData.isCloudLogin()) {
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_cloud_message);
        } else {
            showLoginErrorDialog(R.string.login_failed_no_connection_title, R.string.login_failed_no_connection_direct_message);
        }
    }

    /**
     * Presents an error dialog to the user
     * 
     * @param titleResId the dialog title resource id
     * @param messageResId the message resource id
     */
    protected void showLoginErrorDialog(final int titleResId, final int messageResId) {
        final Context context = App.getInstance();
        final String title = context.getString(titleResId);
        final String message = context.getString(messageResId);
        showLoginErrorDialog(title, message);
    }

    /**
     * Presents an error dialog to the user
     * 
     * @param title the dialog title
     * @param message the message
     */
    protected synchronized void showLoginErrorDialog(final String title, final String message) {
        App.eventBus.post(new ShowLoginErrorDialogEvent(title, message, true));
    }

    /**
     * Refreshes the session using the app token
     * 
     * @param silent
     * @return true on success
     */
    public RefreshSessionResult refreshSession(final boolean silent) {

        final CommandBuilder builder = new CommandBuilder("/json/system/loginApplication").addParameter("loginToken", connectionData.getApplicationToken());

        try {
            final String resultString = connectionHandler.sendCommandUnauthenticated(builder, true, true);
            final JSONObject result = new JSONObject(resultString);
            final boolean ok = result.getBoolean(JSONTags.OK);
            if (ok) {
                connectionHandler.setSessionToken(result.getJSONObject(JSONTags.RESULT).getString(JSONTags.TOKEN));
                connectionData.setHasValidAppToken(true);
                return RefreshSessionResult.OK;
            } else {
                // resetting the connection data's session validity
                connectionHandler.setSessionToken(null);
                connectionData.setHasValidAppToken(false);
                return RefreshSessionResult.INVALID_APP_TOKEN;
            }
        } catch (final JSONException e) {
            Log.e(TAG, "Could not parse result");
            if (silent) {
                showLoginErrorDialog(R.string.login_failed_title, R.string.login_failed_communication_error);
            }
        } catch (final IOException e) {
            Log.e(TAG, "Could not communicate with server");
            if (silent) {
                showLoginConnectionErrorDialog();
            }
        } catch (final NeedsSessionRefreshException e) {
            // A 403 during session refresh is not good. Handle it like a normal FileNotfoundException
            Log.e(TAG, "Could not communicate with server");
            if (silent) {
                showLoginConnectionErrorDialog();
            }
        }
        return RefreshSessionResult.NOT_OK;
    }
}
