package com.smartboxasia.control.data.connection;

import android.util.Log;

import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.connection.cloud_1_0.LoginHandlerCloud;
import com.smartboxasia.control.data.connection.cloud_1_0.LoginHandlerCloudOnly;
import com.smartboxasia.control.data.connection.direct_1_17_0.LoginHandlerDirect;
import com.smartboxasia.control.data.helper.VersionHelper;

class ConnectionServiceProviderImpl implements ConnectionServiceProvider {

    public static final String MIN_DSS_VERSION = "1.17.0";

    @Override
    public ConnectionService createConnectionService(final ConnectionData connectionData) {
        Log.d("LOGIN-TRACE", "\ngetConnectionService");
        final ConnectionHandler connectionHandler = createNewConnectionHandler(connectionData);
        if (connectionHandler == null) {
            // no connection handler, no connection service
            Log.d("LOGIN-TRACE", "-- could not create connection handler, returning null");
            return null;
        }
        final LoginHandler loginHandler = createNewLoginHandler(connectionHandler);
        if (loginHandler == null) {
            // no login handler, no connection service
            Log.d("LOGIN-TRACE", "-- could not create login handler, returning null");
            return null;
        }

        final ConnectionServiceImpl connectionService = new ConnectionServiceImpl();
        connectionService.injectConnectionHandler(connectionHandler);
        connectionService.injectLoginHandler(loginHandler);
        Log.d("LOGIN-TRACE", "-- connection service created");
        return connectionService;
    }

    @Override
    public boolean supportsVersion(final String version) {
        return VersionHelper.dssVersionIsNewerThanMinVersion(version, MIN_DSS_VERSION);
    }

    @Override
    public boolean supportsConnectionType(final int connectionType) {
        return ConnectionData.CONNECTION_TYPE_MANUAL == connectionType
                || ConnectionData.CONNECTION_TYPE_LOCAL == connectionType
                || ConnectionData.CONNECTION_TYPE_CLOUD == connectionType
                || ConnectionData.CONNECTION_TYPE_CLOUD_ONLY == connectionType;
    }

    private static ConnectionHandler createNewConnectionHandler(final ConnectionData connectionData) {
        Log.d("LOGIN-TRACE", "-- createNewConnectionHandler");
        if (connectionData == null) {
            return null;
        }
        final ConnectionHandler newConnectionHandler = new ConnectionHandler();
        newConnectionHandler.injectConnectionData(connectionData);
        return newConnectionHandler;
    }

    private static LoginHandler createNewLoginHandler(final ConnectionHandler connectionHandler) {
        Log.d("LOGIN-TRACE", "-- createNewLoginHandler");
        if (connectionHandler == null) {
            return null;
        }
        final LoginHandler newLoginHandler;
        switch (connectionHandler.getConnectionData().getType()) {
            case ConnectionData.CONNECTION_TYPE_LOCAL:
            case ConnectionData.CONNECTION_TYPE_MANUAL: {
                Log.d("LOGIN-TRACE", "  -- for type direct");
                newLoginHandler = new LoginHandlerDirect(connectionHandler);
                break;
            }
            case ConnectionData.CONNECTION_TYPE_CLOUD: {
                Log.d("LOGIN-TRACE", "  -- for type cloud");
                newLoginHandler = new LoginHandlerCloud(connectionHandler);
                break;
            }
            case ConnectionData.CONNECTION_TYPE_CLOUD_ONLY: {
                Log.d("LOGIN-TRACE", "  -- for type cloud only");
                newLoginHandler = new LoginHandlerCloudOnly(connectionHandler);
                break;
            }
            default: {
                Log.d("LOGIN-TRACE", "  -- could not create login handler..returning null");
                // We found no login handler for this type. So don't return a
                // connection service.
                return null;
            }
        }
        return newLoginHandler;
    }
}
