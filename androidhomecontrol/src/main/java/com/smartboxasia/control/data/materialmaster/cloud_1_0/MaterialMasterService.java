package com.smartboxasia.control.data.materialmaster.cloud_1_0;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.connection.ConnectionHandler;
import com.smartboxasia.control.data.helper.CommandBuilder;
import com.smartboxasia.control.data.helper.DsServiceHelper;
import com.smartboxasia.control.data.helper.EncodingHelper;
import com.smartboxasia.control.data.helper.JSONCallback;
import com.smartboxasia.control.data.helper.JSONCallbackBase;
import com.smartboxasia.control.data.materialmaster.cloud_1_0.json.JSONTags;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsDimmingCurve;
import com.smartboxasia.control.dto.DsIlluminant;
import com.smartboxasia.control.events.CheckIlluminantResultEvent;
import com.smartboxasia.control.events.TransferingDimmingCurveFinishedEvent;
import com.google.common.collect.Lists;

public class MaterialMasterService {

    private static final String TAG = MaterialMasterService.class.getSimpleName();

    // ///////////
    // Cloud Calls
    // ///////////

    public static boolean getAllIlluminantsSync(final ConnectionHandler handler, final DsDevice device, final List<DsIlluminant> illuminants) {
        final String cleanDeviceId = EncodingHelper.getEncodedString(device.get_dsid());

        final CommandBuilder builder = new CommandBuilder("public/MaterialMaster/v1_0/IlluminantService/GetIlluminants").addParameter("dSid", cleanDeviceId).addParameter("languageCode", "XX_xx");

        final Boolean result = handler.sendAndParseCloudGetCommand(builder, new JSONCallbackBase<Boolean>() {

            @Override
            public Boolean onResult(final JSONObject root) {

                if (checkSucceededCloud(root)) {

                    final JSONObject response = getResponseCloud(root);
                    final JSONArray jsonIlluminants = DsServiceHelper.getJSONArray(response, JSONTags.ILLUMINANTS);

                    // parse illuminants
                    for (int i = 0; i < jsonIlluminants.length(); i++) {
                        final JSONObject jsonIlluminant = DsServiceHelper.getJSONObject(jsonIlluminants, i);
                        illuminants.add(parseIlluminant(jsonIlluminant));
                    }
                    return true;
                } else {
                    DsServiceHelper.signal(new TransferingDimmingCurveFinishedEvent(getReturnCodeCloud(root)));
                    return false;
                }
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                Log.e(TAG, "Could not get all illuminants: " + message);
                DsServiceHelper.signal(new TransferingDimmingCurveFinishedEvent(errorCode));
            }
        });

        return (result != null && result);
    }

    private static DsIlluminant parseIlluminant(final JSONObject jsonIlluminant) {
        final int id = DsServiceHelper.getInt(jsonIlluminant, JSONTags.ID, -1);
        final String ean = DsServiceHelper.getStringTrimmed(jsonIlluminant, JSONTags.EAN, "");
        final JSONArray jsonCurves = DsServiceHelper.getJSONArray(jsonIlluminant, JSONTags.DIMMING_CURVE);
        final List<DsDimmingCurve> curves = parseDimmingCurves(jsonCurves);
        final DsIlluminant illuminant = new DsIlluminant().withId(id).widthEAN(ean).withDimmingCurves(curves);
        return illuminant;
    }

    private static List<DsDimmingCurve> parseDimmingCurves(final JSONArray jsonCurves) {
        final List<DsDimmingCurve> result = Lists.newArrayList();
        for (int i = 0; i < jsonCurves.length(); i++) {
            final JSONObject jsonCurve = DsServiceHelper.getJSONObject(jsonCurves, i);
            final int id = DsServiceHelper.getInt(jsonCurve, JSONTags.ID, -1);
            if (id > 0) {
                result.add(new DsDimmingCurve().widthId(id));
            }
        }
        return result;
    }

    public static void checkIlluminant(final DsDevice device, final String ean, final boolean isTransferDimmingCurveDialog) {
        final String cleanDeviceId = EncodingHelper.getEncodedString(device.get_dsid());
        final String cleanEan = EncodingHelper.getEncodedString(ean);

        final CommandBuilder builder = new CommandBuilder("public/MaterialMaster/v1_0/IlluminantService/CheckIlluminant").addParameter("dsid", cleanDeviceId).addParameter("gtin", cleanEan);

        Connection.getConnectionService().sendAndParseCloudGetCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (checkSucceededCloud(root)) {
                    final JSONObject response = getResponseCloud(root);
                    final String deviceDimmableResult = response.optString(JSONTags.DEVICE_DIMMABLE_RESULT);
                    final String fittingCheckResult = response.optString(JSONTags.FITTING_CHECK_RESULT);
                    final String illuminantDimmingResult = response.optString(JSONTags.ILLUMINANT_DIMMING_RESULT);
                    DsServiceHelper.signal(new CheckIlluminantResultEvent(deviceDimmableResult, fittingCheckResult, illuminantDimmingResult, isTransferDimmingCurveDialog));
                } else {
                    DsServiceHelper.signal(new CheckIlluminantResultEvent(CheckIlluminantResultEvent.ERROR));
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                Log.e(TAG, "Could not check illuminant: " + message);
                DsServiceHelper.signal(new CheckIlluminantResultEvent(errorCode));
            }
        });

    }
}
