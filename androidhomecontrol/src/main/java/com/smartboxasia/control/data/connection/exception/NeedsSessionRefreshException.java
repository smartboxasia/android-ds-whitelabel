package com.smartboxasia.control.data.connection.exception;

import java.io.FileNotFoundException;

public class NeedsSessionRefreshException extends Exception {

    private static final long serialVersionUID = 1L;

    public NeedsSessionRefreshException(FileNotFoundException e) {
        super(e);
    }

}
