package com.smartboxasia.control.data.helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VersionHelper {

    private static Pattern PATTERN = Pattern.compile("([0-9]+\\.[0-9]+\\.[0-9]+)");

    /**
     * Parses a Version String of the type:
     *
     * <pre>
     * dSS v1.22.6 (1.22.6) (oebuild@aizobuilder)
     * </pre>
     *
     * and returns the version string after the 'v'. In this case "1.22.6".
     *
     * @param versionString the String to parse
     * @return the version number as string
     */
    public static String extractVersion(final String versionString) {
        final Matcher matcher = PATTERN.matcher(versionString);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return "";
    }

    /**
     * Compares two version strings of the form "1.22.6"
     *
     * @param dssVersion the version of the server
     * @param minDssVersion the min Version of the current ConnectionHandler
     * @return true if the server version is higher than the min version
     */
    public static boolean dssVersionIsNewerThanMinVersion(final String dssVersion, final String minDssVersion) {
        final String[] vals1 = dssVersion.split("\\.");
        final String[] vals2 = minDssVersion.split("\\.");
        final int count = Math.min(vals1.length, vals2.length);
        try {
            for (int i = 0; i < count; i++) {
                final int ver1 = Integer.valueOf(vals1[i]);
                final int ver2 = Integer.valueOf(vals2[i]);
                if (ver1 < ver2) {
                    return false;
                }
            }
        } catch (final NumberFormatException e) {
            return false;
        }
        return true;
    }
}
