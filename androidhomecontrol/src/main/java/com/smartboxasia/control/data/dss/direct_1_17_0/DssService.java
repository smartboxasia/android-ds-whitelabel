package com.smartboxasia.control.data.dss.direct_1_17_0;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.DssConstants.Groups;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.app.ConsumptionCache;
import com.smartboxasia.control.data.config.app.MetersCache;
import com.smartboxasia.control.data.config.app.RoomsStore;
import com.smartboxasia.control.data.config.app.UserDefinedActionsCache;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.connection.exception.NeedsSessionRefreshException;
import com.smartboxasia.control.data.dss.direct_1_17_0.json.DssMappings;
import com.smartboxasia.control.data.dss.direct_1_17_0.json.JSONConstants.BusMemberTypes;
import com.smartboxasia.control.data.dss.direct_1_17_0.json.JSONConstants.DeviceTypes;
import com.smartboxasia.control.data.dss.direct_1_17_0.json.JSONConstants.OutputModes;
import com.smartboxasia.control.data.dss.direct_1_17_0.json.JSONTags;
import com.smartboxasia.control.data.helper.CommandBuilder;
import com.smartboxasia.control.data.helper.DsServiceHelper;
import com.smartboxasia.control.data.helper.EncodingHelper;
import com.smartboxasia.control.data.helper.JSONCallback;
import com.smartboxasia.control.data.helper.JSONCallbackBase;
import com.smartboxasia.control.data.helper.Mappings;
import com.smartboxasia.control.domain.BusMemberType;
import com.smartboxasia.control.domain.Capability;
import com.smartboxasia.control.domain.LoginResult;
import com.smartboxasia.control.domain.OutputMode;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsMeter;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.dto.DsScene;
import com.smartboxasia.control.events.ActivitiesChangedEvent;
import com.smartboxasia.control.events.AllMetersConsumptionsChangedEvent;
import com.smartboxasia.control.events.ConsumptionChangedEvent;
import com.smartboxasia.control.events.DeviceBlinkEvent;
import com.smartboxasia.control.events.DeviceReasignedEvent;
import com.smartboxasia.control.events.DeviceRenamedEvent;
import com.smartboxasia.control.events.DeviceStateChangedEvent;
import com.smartboxasia.control.events.DeviceValueChangedEvent;
import com.smartboxasia.control.events.GotUserDefinedActionsEvent;
import com.smartboxasia.control.events.MeterChangedEvent;
import com.smartboxasia.control.events.MeterRenamedEvent;
import com.smartboxasia.control.events.OutputModeChangedEvent;
import com.smartboxasia.control.events.RoomRenamedEvent;
import com.smartboxasia.control.events.RoomsChangedEvent;
import com.smartboxasia.control.events.SceneActivatedEvent;
import com.smartboxasia.control.events.SceneRenamedEvent;
import com.smartboxasia.control.events.SceneSavedEvent;
import com.smartboxasia.control.events.SpecificMeterChangedEvent;
import com.smartboxasia.control.ui.helper.SceneHelper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * SMARTBOX JSON Services
 */
public class DssService {

    private static final String TAG = DssService.class.getSimpleName();

    private static JSONArray getRooms(final JSONObject response) {
        return DsServiceHelper.getJSONArray(response, JSONTags.ZONES);
    }

    private static JSONObject getApartment(final JSONObject response) {
        return DsServiceHelper.getJSONObject(response, JSONTags.APARTMENT);
    }

    private static void addDeviceIdParam(final CommandBuilder builder, final DsDevice device) {
        if (device.get_hasDsuid()) {
            builder.addParameter(JSONTags.DSUID1, device.get_dsuid());
        } else {
            builder.addParameter(JSONTags.DSID1, device.get_dsid());
        }
    }

    private static void addDeviceIdParam2(final CommandBuilder builder, final DsDevice device) {
        if (device.get_hasDsuid()) {
            builder.addParameter(JSONTags.DSUID1, device.get_dsuid());
        } else {
            builder.addParameter(JSONTags.DEVICE_ID, device.get_dsid());
        }
    }

    private static void addMeterIdParam(final CommandBuilder builder, final DsMeter meter) {
        if (meter.get_hasDsuid()) {
            builder.addParameter(JSONTags.DSUID1, meter.get_dsuid());
        } else {
            builder.addParameter(JSONTags.ID, meter.get_dsid());
        }
    }

    // /////////////////////////////////////
    // Structure manipulation helper methods
    // /////////////////////////////////////

    private static void setDeviceState(final int roomId, final String deviceId, final boolean on) {
        final DsRoom room = RoomsStore.get_room_by_id(roomId);
        if (room != null) {
            final DsDevice device = room.get_device_by_id(deviceId);
            if (device != null) {
                device.set_on(on);
            }
        }
    }

    private static void setLastCalledSceneToUndefined(final int roomId, final int groupNumber, final long sceneId) {
        final DsRoom room = RoomsStore.get_room_by_id(roomId);
        if (room != null && room.get_lastCalledScene(groupNumber) == sceneId) {
            room.set_lastCalledScene(groupNumber, -1);
        }
    }

    // ///////////////
    // Server Commands
    // ///////////////

    public static void callActivity(final Context context, final int roomId, final int groupNumber, final long sceneId, final boolean force) {
        callActivity(context, roomId, groupNumber, sceneId, force, true);
    }

    private static void callActivity(final Context context, final int roomId, final int groupNumber, final long sceneId, final boolean force, final boolean retry) {

        CommandBuilder builder;
        // check if the activity is a user defined action
        if (sceneId >= 1024) {
            builder = new CommandBuilder("/json/event/raise").addParameter("name", "highlevelevent").addParameter("parameter=id", sceneId);
        } else {
            builder = new CommandBuilder("/json/zone/callScene").addParameter("id", roomId).addParameter("groupID", groupNumber).addParameter("sceneNumber", sceneId).addParameter("force", force);
        }
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    final List<DsRoom> rooms = RoomsStore.get_roomsSorted();

                    if (rooms != null) {
                        // don't update last called scene if it's increment/decrement scene call
                        if (sceneId != DssConstants.Scenes.STEP_DOWN && sceneId != DssConstants.Scenes.STEP_UP) {
                            for (final DsRoom room : rooms) {
                                if (room.get_id() == roomId) {
                                    room.set_lastCalledScene(groupNumber, sceneId);
                                }
                            }
                        }

                        // reset last called scene if Leave Home is called
                        if (roomId == DssConstants.Rooms.GLOBAL && sceneId == DssConstants.Scenes.ABSENT) {
                            for (final DsRoom room : rooms) {
                                room.set_lastCalledScene(DssConstants.Groups.LIGHT, sceneId);
                                room.set_lastCalledScene(DssConstants.Groups.SHADE, sceneId);
                                room.set_lastCalledScene(DssConstants.Groups.CLIMATE, sceneId);
                            }
                        }
                    }

                    // broadcast completion
                    DsServiceHelper.signal(new SceneActivatedEvent());
                    Log.i(TAG, "Scene number " + sceneId + ", groupID " + groupNumber + " called on roomId " + roomId + " forced: " + force);

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        callActivity(context, roomId, groupNumber, sceneId, force, false);
                    } else {
                        Log.e(TAG, "Could not refresh Session token");
                        DsServiceHelper.signal(new SceneActivatedEvent(ERROR_SERVER));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                DsServiceHelper.signal(new SceneActivatedEvent(errorCode));
                Log.e(TAG, "Scene number " + sceneId + " on roomId " + roomId + " not called, connection problem: " + message);
            }
        });
    }

    public static void turnOnDevice(final Context context, final int roomId, final DsDevice device) {
        turnOnDevice(context, roomId, device, true);
    }

    private static void turnOnDevice(final Context context, final int roomId, final DsDevice device, final boolean retry) {
        final CommandBuilder builder = new CommandBuilder("/json/device/turnOn");
        addDeviceIdParam(builder, device);
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    // update device status
                    setDeviceState(roomId, device.get_id(), true);

                    // broadcast completion
                    DsServiceHelper.signal(new DeviceStateChangedEvent());
                    Log.i(TAG, "Device " + device.get_id() + " turned on");

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        turnOnDevice(context, roomId, device, false);
                    } else {
                        // broadcast
                        DsServiceHelper.signal(new DeviceStateChangedEvent(ERROR_SERVER));
                        Log.e(TAG, "Device " + device.get_id() + " not turned on: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // broadcast
                DsServiceHelper.signal(new DeviceStateChangedEvent(errorCode));
                Log.e(TAG, "Device " + device.get_id() + " not turned on, connection problem: " + message);
            }
        });
    }

    public static void blinkDevice(final Context context, final DsDevice device) {
        blinkDevice(context, device, true);
    }

    private static void blinkDevice(final Context context, final DsDevice device, final boolean retry) {
        final CommandBuilder builder = new CommandBuilder("/json/device/blink");
        addDeviceIdParam(builder, device);
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    // broadcast completion
                    DsServiceHelper.signal(new DeviceBlinkEvent());
                    Log.i(TAG, "Device " + device.get_id() + " blinked");

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        blinkDevice(context, device, false);
                    } else {
                        // broadcast
                        DsServiceHelper.signal(new DeviceBlinkEvent(ERROR_SERVER));
                        Log.e(TAG, "Device " + device.get_id() + " not blinked: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // broadcast
                DsServiceHelper.signal(new DeviceBlinkEvent(errorCode));
                Log.e(TAG, "Device " + device.get_id() + " not blinked, connection problem: " + message);
            }
        });
    }

    public static void callSceneOnDevice(final Context context, final DsDevice device, final long sceneId) {
        callSceneOnDevice(context, device, sceneId, true);
    }

    private static void callSceneOnDevice(final Context context, final DsDevice device, final long sceneId, final boolean retry) {
        final CommandBuilder builder = new CommandBuilder("/json/device/callScene").addParameter("sceneNumber", sceneId);
        addDeviceIdParam(builder, device);
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (!JSONCallbackBase.checkSucceededDirect(root)) {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        callSceneOnDevice(context, device, sceneId, false);
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // no callback on scene calls
            }
        });
        Log.i(TAG, "called scene " + sceneId + " on deviceId " + device.get_id());
    }

    public static void undoScene(final Context context, final int roomId, final int groupNumber, final long sceneId) {
        undoScene(context, roomId, groupNumber, sceneId, true);
    }

    private static void undoScene(final Context context, final int roomId, final int groupNumber, final long sceneId, final boolean retry) {
        final CommandBuilder builder = new CommandBuilder("/json/zone/undoScene").addParameter("id", roomId).addParameter("groupID", groupNumber).addParameter("sceneNumber", sceneId);
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    // update the last called scene in the affected room. As it's undone, the current scene is undefined
                    setLastCalledSceneToUndefined(roomId, groupNumber, sceneId);

                    // broadcast completion
                    DsServiceHelper.signal(new SceneActivatedEvent());
                    Log.i(TAG, "Scene " + sceneId + " undone successfully");

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        undoScene(context, roomId, groupNumber, sceneId, false);
                    } else {
                        // broadcast
                        DsServiceHelper.signal(new SceneActivatedEvent(ERROR_SERVER));
                        Log.e(TAG, "Scene " + sceneId + " not undone: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                DsServiceHelper.signal(new SceneActivatedEvent());
                Log.e(TAG, "Scene " + sceneId + " not undone: " + message);
            }
        });
    }

    public static void turnOffDevice(final Context context, final int roomId, final DsDevice device) {
        turnOffDevice(context, roomId, device, true);
    }

    private static void turnOffDevice(final Context context, final int roomId, final DsDevice device, final boolean retry) {
        final CommandBuilder builder = new CommandBuilder("/json/device/turnOff");
        addDeviceIdParam(builder, device);
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    // update device status
                    setDeviceState(roomId, device.get_id(), false);

                    // broadcast completion
                    DsServiceHelper.signal(new DeviceStateChangedEvent());
                    Log.i(TAG, "Device " + device.get_id() + " turned off");

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        turnOffDevice(context, roomId, device, true);
                    } else {
                        // broadcast
                        DsServiceHelper.signal(new DeviceStateChangedEvent(ERROR_SERVER));
                        Log.e(TAG, "Device " + device.get_id() + " not turned off: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // broadcast
                DsServiceHelper.signal(new DeviceStateChangedEvent(errorCode));
                Log.e(TAG, "Device " + device.get_id() + " not turned off, connection problem: " + message);
            }
        });
    }

    public static int getDeviceConsumptionValueSync(final DsDevice device) {
        return getSensorValueSync(device, 2, true);
    }

    public static int getDeviceMeteringValueSync(final DsDevice device) {
        return getSensorValueSync(device, 4, true);
    }

    private static int getSensorValueSync(final DsDevice device, final int sensorIndex, final boolean retry) {
        int result = -1;

        final CommandBuilder builder = new CommandBuilder("/json/device/getSensorValue").addParameter("sensorIndex", sensorIndex);
        addDeviceIdParam(builder, device);

        String responseString = "";
        try {
            responseString = Connection.getConnectionService().sendCommandAuthenticated(builder, null, true, false);
        } catch (final IOException e) {
            Log.e(TAG, "Didn't get sensor value", e);
        } catch (final NeedsSessionRefreshException e) {
            if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                Log.d(TAG, "Session token refreshed, retrying call");
                result = getSensorValueSync(device, sensorIndex, false);
            } else {
                Log.e(TAG, "Didn't get sensor value", e.getCause());
            }
        }

        final JSONObject root = DsServiceHelper.createJSONObject(responseString);
        if (JSONCallbackBase.checkSucceededDirect(root)) {

            final JSONObject resultObj = JSONCallbackBase.getResultDirect(root);
            result = DsServiceHelper.getInt(resultObj, "sensorValue", -1);

        } else {
            if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                Log.d(TAG, "Session token refreshed, retrying call");
                result = getSensorValueSync(device, sensorIndex, false);
            }
        }

        return result;
    }

    /**
     * Gets the device value synchronously
     */
    public static int getDeviceOuputValueSync(final DsDevice device) {
        int offset = 0;
        if (device.get_groupNumber() == Groups.SHADE) {
            offset = 2;
        }
        return getDeviceOuputValueSync(device, offset, true);
    }

    /**
     * Gets the shade angle synchronously
     */
    public static int getShadeAngleSync(final DsDevice device) {
        return getDeviceOuputValueSync(device, 4, true);
    }

    private static int getDeviceOuputValueSync(final DsDevice device, final int offset, final boolean retry) {
        int result = -1;

        final CommandBuilder builder = new CommandBuilder("/json/device/getOutputValue").addParameter("offset", offset);
        addDeviceIdParam(builder, device);

        String responseString = "";
        try {
            responseString = Connection.getConnectionService().sendCommandAuthenticated(builder, null, true, false);
        } catch (final IOException e) {
            Log.e(TAG, "Didn't get device output value", e);
        } catch (final NeedsSessionRefreshException e) {
            if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                Log.d(TAG, "Session token refreshed, retrying call");
                result = getDeviceOuputValueSync(device, offset, false);
            } else {
                Log.e(TAG, "Didn't get device output value", e.getCause());
            }
        }

        final JSONObject root = DsServiceHelper.createJSONObject(responseString);
        if (JSONCallbackBase.checkSucceededDirect(root)) {

            final JSONObject resultObj = JSONCallbackBase.getResultDirect(root);
            result = DsServiceHelper.getInt(resultObj, "value", -1);

        } else {
            if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                Log.d(TAG, "Session token refreshed, retrying call");
                result = getDeviceOuputValueSync(device, offset, false);
            }
        }

        return result;
    }

    public static void setDeviceOutputValue(final DsDevice device, final int value) {
        CommandBuilder builder = new CommandBuilder("/json/device/setOutputValue");
        if (device.get_groupNumber() == Groups.LIGHT) {
            // since older light klemme firmware might not understand the offset parameter,
            // we use the older function setValue instead of setOutputValue. Else we use the new command
            builder = new CommandBuilder("/json/device/setValue").addParameter("value", value);
        } else {
            int offset = 0;
            if (device.get_groupNumber() == Groups.SHADE) {
                offset = 2;
            }
            builder = new CommandBuilder("/json/device/setOutputValue").addParameter("value", value).addParameter("offset", offset);
        }
        addDeviceIdParam(builder, device);
        changeDeviceValue(builder, true);
    }

    public static void setShadeAngle(final DsDevice device, final int value) {
        if (device.get_type() != DeviceTypes.JALOUSIE) {
            return; // only for jalousies
        }
        final CommandBuilder builder = new CommandBuilder("/json/device/setOutputValue").addParameter("value", value).addParameter("offset", 4);
        addDeviceIdParam(builder, device);
        changeDeviceValue(builder, true);
    }

    private static void changeDeviceValue(final CommandBuilder request, final boolean retry) {
        Connection.getConnectionService().sendAndParseDirectPostCommand(request, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    DsServiceHelper.signal(new DeviceValueChangedEvent());
                    Log.i(TAG, "device value changed");

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        changeDeviceValue(request, false);
                    } else {
                        // broadcast
                        DsServiceHelper.signal(new DeviceValueChangedEvent(ERROR_SERVER));
                        Log.e(TAG, "Couldn't set device value");
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // Broadcast
                DsServiceHelper.signal(new DeviceValueChangedEvent(errorCode));
                Log.e(TAG, "Couldn't set device value, connection problem: " + message);
            }
        });
    }

    public static void saveScene(final long sceneId, final int groupNumber) {
        saveScene(sceneId, groupNumber, true);
    }

    private static void saveScene(final long sceneId, final int groupNumber, final boolean retry) {
        if (groupNumber == 0) { // redmine bug 8370
            Log.e(TAG, "Tried to save scene in group 0. That's not allowed: " + sceneId + ", " + groupNumber + ", " + retry, new IllegalArgumentException());
            return;
        }
        final CommandBuilder builder = new CommandBuilder("/json/apartment/saveScene").addParameter("sceneNumber", sceneId).addParameter("groupID", groupNumber);
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    DsServiceHelper.signal(new SceneSavedEvent());
                    Log.i(TAG, "Scene saved");

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        saveScene(sceneId, groupNumber, false);
                    } else {
                        // broadcast
                        DsServiceHelper.signal(new SceneSavedEvent(ERROR_SERVER));
                        Log.e(TAG, "Could not save scene");
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // Broadcast
                DsServiceHelper.signal(new SceneSavedEvent(errorCode));
                Log.e(TAG, "Could not save scene, connection problem: " + message);
            }
        });
    }

    public static void setDeviceOutputMode(final DsDevice device, final OutputMode newMode) {
        setDeviceOutputMode(device, newMode, true);
    }

    private static void setDeviceOutputMode(final DsDevice device, final OutputMode newMode, final boolean retry) {
        final Integer newModeId = Mappings.findValue(DssMappings.OUTPUT_MODES, newMode, OutputModes.DISABLED);
        final CommandBuilder builder = new CommandBuilder("/json/device/setOutputMode").addParameter("modeID", newModeId);
        addDeviceIdParam(builder, device);
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    // broadcast completion
                    DsServiceHelper.signal(new OutputModeChangedEvent(newMode));
                    Log.i(TAG, "Device " + device.get_id() + " set to mode " + newModeId);

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        setDeviceOutputMode(device, newMode, false);
                    } else {
                        // broadcast
                        DsServiceHelper.signal(new OutputModeChangedEvent(ERROR_SERVER, device.get_outputMode()));
                        Log.e(TAG, "Device " + device.get_id() + " set to new mode: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // broadcast
                DsServiceHelper.signal(new OutputModeChangedEvent(errorCode, device.get_outputMode()));
                Log.e(TAG, "Device " + device.get_id() + " set to new mode, connection problem: " + message);
            }
        });
    }

    // ///////////////////////////////
    // Structure manipulation commands
    // ///////////////////////////////

    public static void changeRoomName(final Context context, final int roomId, final String newName) {
        changeRoomName(context, roomId, newName, true);
    }

    private static void changeRoomName(final Context context, final int roomId, final String newName, final boolean retry) {
        final String cleanRoomname = EncodingHelper.getEncodedString(newName);
        final CommandBuilder builder = new CommandBuilder("/json/zone/setName").addParameter("id", roomId).addParameter("newName", cleanRoomname);
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    // broadcast completion
                    DsServiceHelper.signal(new RoomRenamedEvent());
                    Log.i(TAG, "Room " + roomId + " got new name: " + newName);

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        changeRoomName(context, roomId, newName, false);
                    } else {
                        // broadcast
                        DsServiceHelper.signal(new RoomRenamedEvent(ERROR_SERVER));
                        Log.e(TAG, "Room " + roomId + " didn't get renamed: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // broadcast
                DsServiceHelper.signal(new RoomRenamedEvent(errorCode));
                Log.e(TAG, "Room " + roomId + " didn't get renamed, connection problems: " + message);
            }
        });
    }

    public static void changeActivityName(final Context context, final int roomId, final int groupNumber, final long sceneId, final String newName) {
        changeActivityName(context, roomId, groupNumber, sceneId, newName, true);
    }

    private static void changeActivityName(final Context context, final int roomId, final int groupNumber, final long sceneId, final String newName, final boolean retry) {
        final String cleanActivityName = EncodingHelper.getEncodedString(newName);
        final CommandBuilder builder = new CommandBuilder("/json/zone/sceneSetName").addParameter("id", roomId).addParameter("sceneNumber", sceneId).addParameter("groupID", groupNumber).addParameter("newName", cleanActivityName);
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    // broadcast completion
                    DsServiceHelper.signal(new SceneRenamedEvent(newName));
                    Log.i(TAG, "Activity got new name: " + newName);

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        changeActivityName(context, roomId, groupNumber, sceneId, newName, false);
                    } else {
                        // broadcast
                        DsServiceHelper.signal(new SceneRenamedEvent(ERROR_SERVER, newName));
                        Log.e(TAG, "Activity didn't get renamed: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // broadcast
                DsServiceHelper.signal(new SceneRenamedEvent(errorCode, newName));
                Log.e(TAG, "Room " + roomId + " didn't get renamed, connection problems: " + message);
            }
        });
    }

    public static void changeDeviceName(final Context context, final DsDevice device, final String newName) {
        changeDeviceName(context, device, newName, true);
    }

    private static void changeDeviceName(final Context context, final DsDevice device, final String newName, final boolean retry) {
        final String cleanDeviceName = EncodingHelper.getEncodedString(newName);

        final CommandBuilder builder = new CommandBuilder("/json/device/setName").addParameter("newName", cleanDeviceName);
        addDeviceIdParam(builder, device);
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    // broadcast completion
                    DsServiceHelper.signal(new DeviceRenamedEvent(newName));
                    Log.i(TAG, "Device got new name: " + newName);

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        changeDeviceName(context, device, newName, false);
                    } else {
                        // broadcast
                        DsServiceHelper.signal(new DeviceRenamedEvent(ERROR_SERVER, newName));
                        Log.e(TAG, "Device didn't get renamed: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // broadcast
                DsServiceHelper.signal(new DeviceRenamedEvent(errorCode, newName));
                Log.e(TAG, "Device didn't get renamed, connection problems: " + message);
            }
        });
    }

    public static void changeMeterName(final Context context, final DsMeter meter, final String newName) {
        changeMeterName(context, meter, newName, true);
    }

    private static void changeMeterName(final Context context, final DsMeter meter, final String newName, final boolean retry) {

        final String cleanMeterName = EncodingHelper.getEncodedString(newName);

        final CommandBuilder builder = new CommandBuilder("/json/circuit/setName").addParameter("newName", cleanMeterName);
        addMeterIdParam(builder, meter);
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    // broadcast completion
                    DsServiceHelper.signal(new MeterRenamedEvent(newName));
                    Log.i(TAG, "Meter got new name: " + newName);

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        changeMeterName(context, meter, newName, false);
                    } else {
                        // broadcast
                        DsServiceHelper.signal(new MeterRenamedEvent(ERROR_SERVER, newName));
                        Log.e(TAG, "Meter didn't get renamed: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // broadcast
                DsServiceHelper.signal(new MeterRenamedEvent(errorCode, newName));
                Log.e(TAG, "Meter didn't get renamed, connection problems: " + message);
            }

        });
    }

    public static void assignDeviceToZone(final Context context, final DsDevice device, final int roomId) {
        assignDeviceToZone(context, device, roomId, true);
    }

    private static void assignDeviceToZone(final Context context, final DsDevice device, final int roomId, final boolean retry) {
        final CommandBuilder builder = new CommandBuilder("/json/structure/zoneAddDevice").addParameter("zone", roomId);
        addDeviceIdParam2(builder, device);
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    // broadcast completion
                    DsServiceHelper.signal(new DeviceReasignedEvent(roomId));
                    Log.i(TAG, "Device asigned to new room.");

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        assignDeviceToZone(context, device, roomId, false);
                    } else {
                        // broadcast
                        DsServiceHelper.signal(new DeviceReasignedEvent(ERROR_SERVER, roomId));
                        Log.e(TAG, "Device didn't get asigned to new room: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // broadcast
                DsServiceHelper.signal(new DeviceReasignedEvent(errorCode, roomId));
                Log.e(TAG, "Device didn't get asigned to new room, connection problems: " + message);
            }
        });
    }

    // ///////////////
    // Data collection
    // ///////////////

    public static void updateActivties(final Context context) {
        updateActivties(context, true);
    }

    private static void updateActivties(final Context context, final boolean retry) {
        final CommandBuilder builder = new CommandBuilder("/json/property/query").addParameter("query", "/apartment/zones/*(ZoneID)/groups/*(group,lastCalledScene)/scenes/*(scene,name)");
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    final JSONObject result = JSONCallbackBase.getResultDirect(root);
                    final JSONArray roomsJsonArray = getRooms(result);

                    // for each room, except virtual room 0
                    for (int i = 1; i < roomsJsonArray.length(); i++) {

                        // start by checking what groups are available in the room

                        final JSONObject roomJsonObject = DsServiceHelper.getJSONObject(roomsJsonArray, i);

                        final int roomId = DsServiceHelper.getInt(roomJsonObject, JSONTags.ZONE_ID, -1);
                        final DsRoom room = RoomsStore.get_room_by_id(roomId);

                        if (room == null) {
                            Log.w(TAG, "skipping activities for unknown room with id " + roomId);
                            continue;
                        }

                        // check what color groups are available in this room
                        final Set<Integer> presentGroups = Sets.newHashSet();
                        for (final DsDevice device : room.get_devices()) {
                            presentGroups.add(device.get_groupNumber());
                        }

                        final JSONArray groupsJSONArray = DsServiceHelper.getJSONArray(roomJsonObject, JSONTags.GROUPS);

                        // for each group color needed
                        final List<DsScene> allScenesInRoom = Lists.newArrayList();

                        for (int j = 0; j < groupsJSONArray.length(); j++) {
                            final JSONObject groupJSONObject = DsServiceHelper.getJSONObject(groupsJSONArray, j);
                            final int group = DsServiceHelper.getInt(groupJSONObject, JSONTags.GROUP, -1);
                            if (presentGroups.contains(group)) {
                                parseScenesForGroup(context, allScenesInRoom, groupJSONObject, group, room.get_id());
                            }
                        }

                        room.set_scenes(allScenesInRoom);
                    }

                    // Broadcast completion
                    DsServiceHelper.signal(new ActivitiesChangedEvent());
                    Log.d(TAG, "Got activities");

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        updateActivties(context, false);
                    } else {
                        // Broadcast completion
                        DsServiceHelper.signal(new ActivitiesChangedEvent(ERROR_SERVER));
                        Log.e(TAG, "getActivities request not ok: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            private void parseScenesForGroup(final Context context, final List<DsScene> allScenesInRoom, final JSONObject groupJSONObject, final int groupNumber, final int roomId) {
                // get all activities
                final Map<Long, String> customSceneNames = Maps.newHashMap();
                final JSONArray scenes = DsServiceHelper.optJSONArray(groupJSONObject, JSONTags.SCENES);
                for (int j = 0; j < scenes.length(); j++) {
                    final JSONObject scene = DsServiceHelper.getJSONObject(scenes, j);
                    final long sceneId = DsServiceHelper.getLong(scene, JSONTags.SCENE, -1);
                    final String tempName = DsServiceHelper.getString(scene, JSONTags.NAME, "");

                    customSceneNames.put(sceneId, DsServiceHelper.unescape(tempName));
                }

                // replace with standard name if name is empty or old default
                for (final long sceneId : SceneHelper.defaultSceneIds) {

                    final String customSceneName = customSceneNames.get(sceneId);

                    String sceneName;
                    if (Strings.isNullOrEmpty(customSceneName) || SceneHelper.oldDefaultSceneNames.contains(customSceneName)) {
                        sceneName = SceneHelper.getDefaultSceneName(context, sceneId, groupNumber);
                    } else {
                        sceneName = customSceneName;
                    }
                    allScenesInRoom.add(new DsScene(sceneName, sceneId, groupNumber, roomId));
                }

                // set lastCalledScene for group
                final int lastCalledSceneId = DsServiceHelper.getInt(groupJSONObject, JSONTags.LAST_CALLED_SCENE, -1);
                RoomsStore.get_room_by_id(roomId).set_lastCalledScene(groupNumber, lastCalledSceneId);
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // Broadcast
                DsServiceHelper.signal(new ActivitiesChangedEvent(errorCode));
                Log.e(TAG, "Didn't get activities, connection problem: " + message);
            }
        });
    }

    public static void updateRooms(final Context context) {
        updateRooms(context, true);
    }

    private static void updateRooms(final Context context, final boolean retry) {
        final CommandBuilder builder = new CommandBuilder("/json/apartment/getStructure");
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {

                if (JSONCallbackBase.checkSucceededDirect(root)) {
                    // get result and set id's + names in global app variables
                    final JSONObject jsonResult = JSONCallbackBase.getResultDirect(root);
                    final JSONObject jsonApartment = getApartment(jsonResult);
                    final JSONArray jsonRooms = getRooms(jsonApartment);

                    final List<DsRoom> rooms = extractRooms(jsonRooms);

                    if (rooms != null) {
                        RoomsStore.set_roomsUnsorted(rooms);

                        // Broadcast completion
                        DsServiceHelper.signal(new RoomsChangedEvent());
                        Log.d(TAG, "Got structure");
                    } else {
                        DsServiceHelper.signal(new RoomsChangedEvent(ERROR_INVALID_JSON));
                    }

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        updateRooms(context, false);
                    } else {
                        // Broadcast completion
                        DsServiceHelper.signal(new RoomsChangedEvent(ERROR_SERVER));
                        Log.e(TAG, "Structure request not ok: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            private List<DsRoom> extractRooms(final JSONArray jsonRooms) {
                List<DsRoom> result = null;
                if (jsonRooms.length() < 1) {
                    // nothing to do, only the virtual room
                    result = Lists.newArrayList();
                    return result;
                }

                final List<DsRoom> rooms = Lists.newArrayList();
                // skip room 0 (virtual room)
                for (int i = 1; i < jsonRooms.length(); i++) {
                    final JSONObject room = DsServiceHelper.getJSONObject(jsonRooms, i);
                    final int id = DsServiceHelper.getInt(room, JSONTags.ID, -1);
                    if (i < 0) {
                        Log.w(TAG, " Could not parse room at index " + i);
                        continue;
                    }
                    String name = DsServiceHelper.unescape(DsServiceHelper.getString(room, JSONTags.NAME, ""));
                    final JSONArray jsonDevices = DsServiceHelper.getJSONArray(room, JSONTags.DEVICES);

                    final List<DsDevice> devices = Lists.newArrayList();

                    for (int j = 0; j < jsonDevices.length(); j++) {
                        final JSONObject device = DsServiceHelper.getJSONObject(jsonDevices, j);
                        if (DsServiceHelper.getBoolean(device, JSONTags.IS_PRESENT, false)) {
                            final boolean hasDsUid = device.has(JSONTags.DSUID2);
                            final String deviceUid = device.optString(JSONTags.DSUID2);
                            final String displayId = device.optString(JSONTags.DISPLAY_ID);
                            final String deviceId = device.optString(JSONTags.ID);
                            final String deviceName = DsServiceHelper.unescape(DsServiceHelper.getString(device, JSONTags.NAME, ""));
                            final boolean deviceIsOn = DsServiceHelper.getBoolean(device, JSONTags.ON, false);
                            final int outputModeId = DsServiceHelper.getInt(device, JSONTags.OUTPUT_MODE, OutputModes.DISABLED);
                            final OutputMode deviceOutputMode = Mappings.findKey(DssMappings.OUTPUT_MODES, outputModeId, OutputMode.DISABLED);
                            final String deviceMeterUid = device.optString(JSONTags.METER_DSUID);
                            final String deviceMeterId = device.optString(JSONTags.METER_DSID);
                            final JSONArray jsonGroups = DsServiceHelper.getJSONArray(device, JSONTags.GROUPS);
                            final int deviceGroupNumber = Integer.valueOf(DsServiceHelper.getStringFromArray(jsonGroups, 0, "-1"));
                            final int deviceType = DsServiceHelper.getInt(device, JSONTags.PRODUCT_ID, -1);

                            final String gtin = DsServiceHelper.getString(device, JSONTags.GTIN, "");
                            final String oemEanNumber = DsServiceHelper.getString(device, JSONTags.OEM_EAN_NUMBER, "");
                            final int serialNumber = extractSerialNumber(deviceId);

                            final DsDevice newDevice = DsDeviceFactory.create(gtin, oemEanNumber, serialNumber);
                            newDevice.set_hasDsuid(hasDsUid);
                            newDevice.set_dsuid(deviceUid);
                            newDevice.set_dsid(deviceId);
                            newDevice.set_displayId(displayId);
                            newDevice.set_name(deviceName);
                            newDevice.set_on(deviceIsOn);
                            newDevice.set_outputMode(deviceOutputMode);
                            newDevice.set_dsmUid(deviceMeterUid);
                            newDevice.set_dsmId(deviceMeterId);
                            newDevice.set_groupNumber(deviceGroupNumber);
                            newDevice.set_type(deviceType);

                            devices.add(newDevice);
                        } else {
                            // nothing, skip non-present devices
                        }
                    }

                    if (Strings.isNullOrEmpty(name)) {
                        name = context.getString(R.string.room_number, id);
                    }
                    // insert the room directly at the sorted position
                    rooms.add(new DsRoom(id, name, devices));
                }
                result = Lists.newArrayList(rooms);

                return result;
            }

            private int extractSerialNumber(final String deviceId) {
                if (deviceId.length() < 8) {
                    return 0;
                }
                final String serial = deviceId.substring(deviceId.length() - 8, deviceId.length());
                Integer serialNumber = null;
                try {
                    serialNumber = Integer.parseInt(serial, 16);
                } catch (final NumberFormatException e) {
                    // ignore
                }
                if (serialNumber == null) {
                    return 0;
                }
                return serialNumber;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // Broadcast
                DsServiceHelper.signal(new RoomsChangedEvent(errorCode));
                Log.e(TAG, "Didn't get structure, connection problem: " + message);
            }
        });
    }

    public static void updateMeters(final Context context) {
        updateMeters(context, true);
    }

    private static void updateMeters(final Context context, final boolean retry) {
        final CommandBuilder builder = new CommandBuilder("/json/property/query").addParameter("query", "/apartment/dSMeters/*(dSID,dSUID,DisplayID,energyMeterValue,present,name,powerConsumption,busMemberType)/capabilities(*)");
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                // return the result
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    final JSONObject result = JSONCallbackBase.getResultDirect(root);
                    final JSONArray jsonMeters = DsServiceHelper.getJSONArray(result, JSONTags.DS_METERS);
                    final List<DsMeter> meters = Lists.newArrayList();
                    for (int i = 0; i < jsonMeters.length(); i++) {
                        final JSONObject jsonMeter = DsServiceHelper.getJSONObject(jsonMeters, i);
                        if (jsonMeter != null && DsServiceHelper.getBoolean(jsonMeter, JSONTags.PRESENT, false)) {

                            final boolean hasDsUid = jsonMeter.has(JSONTags.DSUID2);
                            final String dSUID = jsonMeter.optString(JSONTags.DSUID2);
                            final String dSID = jsonMeter.optString(JSONTags.DSID2);
                            final String displayId = jsonMeter.optString(JSONTags.DISPLAY_ID);
                            final String name = DsServiceHelper.unescape(DsServiceHelper.getString(jsonMeter, JSONTags.NAME, ""));
                            final int energyMeterValue = DsServiceHelper.getInt(jsonMeter, JSONTags.ENERGY_METER_VALUE, -1);
                            final int powerConsumption = DsServiceHelper.getInt(jsonMeter, JSONTags.POWER_CONSUMPTION, -1);
                            final JSONArray jsonCapabilities = jsonMeter.optJSONArray(JSONTags.CAPABILITIES);
                            final List<Capability> capabilities = Lists.newArrayList();
                            if (hasDsUid) {
                                parseCapabilities(capabilities, jsonCapabilities);
                            } else {
                                capabilities.add(Capability.METERING); // default capability for dSM before capabilities were available
                            }
                            final int busMemberType = jsonMeter.optInt(JSONTags.BUS_MEMBER_TYPE, BusMemberTypes.UNKNOWN);

                            final DsMeter meter = new DsMeter();
                            meter.set_dsuid(dSUID);
                            meter.set_dsid(dSID);
                            meter.set_displayId(displayId);
                            meter.set_hasDsuid(hasDsUid);
                            meter.set_name(name);
                            meter.set_meterReading(energyMeterValue);
                            meter.set_consumption(powerConsumption);
                            meter.set_capabilities(capabilities);
                            meter.set_busMemberType(Mappings.findKey(DssMappings.BUS_MEMBER_TYPES, busMemberType, BusMemberType.UNKNOWN));

                            meters.add(meter);

                        } else {
                            // skip if not present
                        }
                    }
                    MetersCache.set_meters(meters);

                    // broadcast completion
                    DsServiceHelper.signal(new MeterChangedEvent());
                    Log.d(TAG, "Got meters");

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        updateMeters(context, false);
                    } else {
                        // Broadcast
                        DsServiceHelper.signal(new MeterChangedEvent(ERROR_SERVER));
                        Log.e(TAG, "Meters not retrieved: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }

                return null;
            }

            private void parseCapabilities(final List<Capability> capabilities, final JSONArray jsonCapabilities) {
                if (jsonCapabilities == null) {
                    return;
                }
                // the capabilities array consists of only one object containing all capabilities...
                for (int i = 0; i < jsonCapabilities.length(); i++) {
                    final JSONObject jsonCapability = DsServiceHelper.getJSONObject(jsonCapabilities, i);
                    if (jsonCapability.optBoolean(JSONTags.DEVICES, false)) {
                        capabilities.add(Capability.DEVICES);
                    }
                    if (jsonCapability.optBoolean(JSONTags.METERING, false)) {
                        capabilities.add(Capability.METERING);
                    }
                    if (jsonCapability.optBoolean(JSONTags.TEMPERATURE_CONTROL, false)) {
                        capabilities.add(Capability.TEMPERATURE_CONTROL);
                    }
                }
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // Broadcast
                DsServiceHelper.signal(new MeterChangedEvent(errorCode));
                Log.e(TAG, "Didn't get meters, connection problems: " + message);
            }
        });
    }

    // Gets a specific meter consumption
    public static void updateMeterConsumption(final Context context, final DsMeter meter) {
        updateMeterConsumption(context, meter, true);
    }

    private static void updateMeterConsumption(final Context context, final DsMeter meter, final boolean retry) {
        final CommandBuilder builder = new CommandBuilder("/json/circuit/getConsumption");
        addMeterIdParam(builder, meter);

        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {

                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    // get result and set current meter consumption in global app variable
                    final JSONObject result = JSONCallbackBase.getResultDirect(root);

                    final int consumption = DsServiceHelper.getInt(result, JSONTags.CONSUMPTION, -1);

                    // Broadcast completion
                    DsServiceHelper.signal(new SpecificMeterChangedEvent(meter.get_id(), consumption));
                    // Log.d(DEBUG_TAG, "Got specific meter consumption"); // this log is being received often

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        updateMeterConsumption(context, meter, false);
                    } else {
                        // Broadcast
                        DsServiceHelper.signal(new SpecificMeterChangedEvent(ERROR_SERVER, meter.get_id(), -1));
                        Log.e(TAG, "Specific meter consumption request not ok: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // Broadcast
                DsServiceHelper.signal(new SpecificMeterChangedEvent(errorCode, meter.get_id(), -1));
                Log.e(TAG, "Didn't get specific meter consumption, connection problem: " + message);
            }
        });
    }

    // Gets apartment consumption with the getConsumption function
    public static void updateConsumption() {
        updateConsumption(true);
    }

    private static void updateConsumption(final boolean retry) {
        final CommandBuilder builder = new CommandBuilder("/json/apartment/getConsumption");
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    // get result and set current consumption in global app variable
                    final JSONObject result = JSONCallbackBase.getResultDirect(root);

                    final int consumption = DsServiceHelper.getInt(result, JSONTags.CONSUMPTION, -1);

                    // cache the consumption
                    ConsumptionCache.set_consumption(consumption);

                    // broadcast completion
                    DsServiceHelper.signal(new ConsumptionChangedEvent());

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        updateConsumption(false);
                    } else {
                        // broadcast
                        DsServiceHelper.signal(new ConsumptionChangedEvent(ERROR_SERVER));
                        ConsumptionCache.set_consumption(-1);
                        Log.e(TAG, "Consumption request not ok: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {

                // Broadcast
                DsServiceHelper.signal(new ConsumptionChangedEvent(errorCode));
                ConsumptionCache.set_consumption(-1);
                Log.e(TAG, "Didn't get consumption, connection problem: " + message);
            }
        });
    }

    // update the consumption of all meters
    public static void updateAllMeterConsumptions() {
        updateAllMeterConsumptions(true);
    }

    private static void updateAllMeterConsumptions(final boolean retry) {
        final CommandBuilder builder = new CommandBuilder("/json/property/query").addParameter("query", "/apartment/dSMeters/*(powerConsumption,dSUID,dSID)");
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                // return result
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    int consumption = 0;
                    String dsid = "";
                    String dsuid = "";

                    // get result and set current consumption in global app variable
                    final JSONObject result = JSONCallbackBase.getResultDirect(root);
                    final JSONArray jsonMeters = DsServiceHelper.getJSONArray(result, JSONTags.DS_METERS);
                    for (int i = 0; i < jsonMeters.length(); i++) {
                        final JSONObject jsonMeter = DsServiceHelper.getJSONObject(jsonMeters, i);
                        consumption = DsServiceHelper.getInt(jsonMeter, JSONTags.POWER_CONSUMPTION, -1);
                        dsuid = DsServiceHelper.getString(jsonMeter, JSONTags.DSUID2, "");
                        dsid = DsServiceHelper.getString(jsonMeter, JSONTags.DSID2, "");
                        for (final DsMeter meter : MetersCache.get_meters(false)) {
                            if (meter.get_dsid().compareTo(dsid) == 0 || meter.get_dsuid().compareTo(dsuid) == 0) {
                                meter.set_consumption(consumption);
                            }
                        }
                    }

                    // Broadcast completion
                    DsServiceHelper.signal(new AllMetersConsumptionsChangedEvent());
                    // Log.d(DEBUG_TAG, "Got all meters consumption");

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        updateAllMeterConsumptions(false);
                    } else {
                        DsServiceHelper.signal(new AllMetersConsumptionsChangedEvent(ERROR_SERVER));
                        Log.w(TAG, "Didn't get consumption: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // Broadcast
                DsServiceHelper.signal(new AllMetersConsumptionsChangedEvent(errorCode));
                Log.e(TAG, "Didn't get consumption, connection problem");
            }
        });
    }

    // Get user defined events
    public static void updateUserDefinedActions(final Context context) {
        updateUserDefinedActions(context, true);
    }

    private static void updateUserDefinedActions(final Context context, final boolean retry) {
        final CommandBuilder builder = new CommandBuilder("/json/property/query").addParameter("query", "/usr/events/*(name,id)");
        Connection.getConnectionService().sendAndParseDirectPostCommand(builder, new JSONCallback() {

            @Override
            public Void onResult(final JSONObject root) {
                if (JSONCallbackBase.checkSucceededDirect(root)) {

                    final JSONObject result = JSONCallbackBase.getResultDirect(root);
                    if (!result.has(JSONTags.EVENTS)) {
                        Log.d(TAG, "No user defined events defined.");
                        DsServiceHelper.signal(new GotUserDefinedActionsEvent(GotUserDefinedActionsEvent.ERROR_NO_UDA_DEFINED));
                        return null;
                    }

                    final JSONArray events = DsServiceHelper.getJSONArray(result, JSONTags.EVENTS);

                    final Set<DsScene> workingArray = Sets.newLinkedHashSet();
                    for (int i = 0; i < events.length(); i++) {
                        final JSONObject jsonEvent = DsServiceHelper.getJSONObject(events, i);
                        final String name = DsServiceHelper.unescape(DsServiceHelper.getString(jsonEvent, JSONTags.NAME, ""));
                        final long sceneId = DsServiceHelper.getLong(jsonEvent, JSONTags.ID, -1);
                        workingArray.add(new DsScene(name, sceneId, DssConstants.Groups.ACTION, DssConstants.Rooms.USER_ACTION));
                    }

                    UserDefinedActionsCache.set_userDefinedActions(workingArray);

                    // Broadcast completion
                    DsServiceHelper.signal(new GotUserDefinedActionsEvent());
                    Log.d(TAG, "Got user defined actions");

                } else {
                    if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                        Log.d(TAG, "Session token refreshed, retrying call");
                        updateUserDefinedActions(context, false);
                    } else {
                        // Broadcast
                        DsServiceHelper.signal(new GotUserDefinedActionsEvent(ERROR_SERVER));
                        Log.e(TAG, "Didn't get user defined actions: " + JSONCallbackBase.getErrorMessageDirect(root));
                    }
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                // Broadcast
                DsServiceHelper.signal(new GotUserDefinedActionsEvent(errorCode));
                Log.e(TAG, "Didn't get user defined actions, connection problem: " + message);
            }

        });
    }
}
