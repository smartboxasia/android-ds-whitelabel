package com.smartboxasia.control.data.dimwizard.cloud_1_1;

import java.util.List;

import org.json.JSONObject;

import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.connection.ConnectionHandler;
import com.smartboxasia.control.data.connection.ConnectionHandler.ConnectionTask;
import com.smartboxasia.control.data.helper.CommandBuilder;
import com.smartboxasia.control.data.helper.DsServiceHelper;
import com.smartboxasia.control.data.helper.EncodingHelper;
import com.smartboxasia.control.data.helper.JSONCallback;
import com.smartboxasia.control.data.materialmaster.cloud_1_0.MaterialMasterService;
import com.smartboxasia.control.dto.DsDevice;
import com.smartboxasia.control.dto.DsIlluminant;
import com.smartboxasia.control.events.TransferingDimmingCurveFinishedEvent;
import com.google.common.collect.Lists;

public class DimWizardService {

    // ///////////
    // Cloud Calls
    // ///////////

    private static void transferDimmingCurveSync(final ConnectionHandler handler, final DsDevice device, final int illuminantId, final int curveId) {
        final String cleanDeviceId = EncodingHelper.getEncodedString(device.get_dsid());

        final CommandBuilder builder = new CommandBuilder("public/dss/v1_1/DimWizard/TransferDimmingCurve").addParameter("dsid", cleanDeviceId).addParameter("IlluminantID", illuminantId).addParameter("DimmingCurveID", curveId);

        handler.sendAndParseCloudPostCommand(builder, new JSONCallback() {
            @Override
            public Void onResult(final JSONObject root) {
                if (checkSucceededCloud(root)) {
                    DsServiceHelper.signal(new TransferingDimmingCurveFinishedEvent());
                } else {
                    DsServiceHelper.signal(new TransferingDimmingCurveFinishedEvent(getReturnCodeCloud(root)));
                }
                return null;
            }

            @Override
            public void onFailure(final int errorCode, final String message) {
                DsServiceHelper.signal(new TransferingDimmingCurveFinishedEvent(errorCode));
            }
        });
    }

    public static void transferDimmingCurve(final DsDevice device, final String ean) {
        Connection.getConnectionService().executeTask(new ConnectionTask() {

            @Override
            public void executeTask(final ConnectionHandler handler) {
                final List<DsIlluminant> illuminants = Lists.newArrayList();
                if (MaterialMasterService.getAllIlluminantsSync(handler, device, illuminants)) {
                    for (final DsIlluminant illuminant : illuminants) {
                        if (ean.equals(illuminant.ean)) {
                            final int illuminantId = illuminant.id;
                            if (illuminant.curves != null && !illuminant.curves.isEmpty()) {
                                final int curveId = illuminant.curves.get(0).id;
                                transferDimmingCurveSync(handler, device, illuminantId, curveId);
                            } else {
                                DsServiceHelper.signal(new TransferingDimmingCurveFinishedEvent(TransferingDimmingCurveFinishedEvent.ERROR_NO_CURVES));
                            }
                            break;
                        }
                    }
                }
            }
        });
    }
}
