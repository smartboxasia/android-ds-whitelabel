package com.smartboxasia.control.data.helper;

public class CommandBuilder {

    private final StringBuilder contentBuilder;
    private boolean firstParameter = true;
    private String tokenParamName = JSONCallback.PARAM_TOKEN;

    public CommandBuilder(final String baseUrl) {
        contentBuilder = new StringBuilder(baseUrl);
    }

    public CommandBuilder(final String baseUrl, final String tokenParamName) {
        contentBuilder = new StringBuilder(baseUrl);
        this.tokenParamName = tokenParamName;
    }

    public CommandBuilder addApartment(final int id) {
        return addParameter("apartmentid", id);
    }

    public CommandBuilder addEmptyApartment() {
        return addEmptyParameter("apartmentid");
    }

    public CommandBuilder addEmptyParameter(final String key) {
        return addParameter(key, "");
    }

    public CommandBuilder addParameter(final String key, final int value) {
        return addParameter(key, String.valueOf(value));
    }

    public CommandBuilder addParameter(final String key, final Object value) {
        return addParameter(key, value.toString());
    }

    public CommandBuilder addParameter(final String key, final String value) {
        contentBuilder.append(firstParameter ? "?" : "&");
        contentBuilder.append(key).append("=").append(value);
        firstParameter = false;
        return this;
    }

    public CommandBuilder addParameterEncoded(final String key, final String value) {
        return addParameter(key, EncodingHelper.getEncodedString(value));
    }

    public CommandBuilder addUser() {
        return addParameter("user", EncodingHelper.getEncodedUser());
    }

    public CommandBuilder addZoneId(final int zoneId) {
        return addParameter("zoneId", zoneId);
    }

    public String build() {
        return contentBuilder.toString();
    }

    public String getTokenParamName() {
        return tokenParamName;
    }
}
