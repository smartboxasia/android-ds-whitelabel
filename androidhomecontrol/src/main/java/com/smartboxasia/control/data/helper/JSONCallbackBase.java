package com.smartboxasia.control.data.helper;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.smartboxasia.control.data.connection.Connection;
import com.google.common.collect.BiMap;

public abstract class JSONCallbackBase<T> {

    private static final String TAG = JSONCallbackBase.class.getSimpleName();

    public static final int ERROR_INVALID_JSON = 1;
    public static final int ERROR_CLOUD = 2;
    public static final int ERROR_APP = 3;
    public static final int ERROR_SERVER = 4;

    public static final int ERROR_NONE = 0;

    public abstract T onResult(final JSONObject root);

    public abstract void onFailure(final int errorCode, final String message);

    public static float extractFloatValue(final JSONObject obj, final String tagName) {

        if (obj.isNull(tagName)) {
            return Float.NaN;
        }

        try {
            return (float) obj.getDouble(tagName);
        } catch (final JSONException e) {
            Log.w(TAG, "Problems extracting tag " + tagName + ": " + e.getMessage());
        }

        Log.w(TAG, "Returning default value: " + Float.NaN);
        return Float.NaN;
    }

    public static <V> V extractEnumForBoolean(final JSONObject obj, final String tagName, final BiMap<V, Boolean> valueMapping, final V defaultValue) {

        if (obj == null || !obj.has(tagName)) {
            Log.w(TAG, "Can't find tag " + tagName + " in JSON object " + obj);
            Log.w(TAG, "Returning default value: " + defaultValue);
            return defaultValue;
        }

        if (obj.isNull(tagName)) {
            return defaultValue;
        }

        try {
            final boolean elem = obj.getBoolean(tagName);
            return Mappings.findKey(valueMapping, elem, defaultValue);
        } catch (final JSONException e) {
            Log.w(TAG, "Problems extracting tag " + tagName + ": " + e.getMessage());
        }

        Log.w(TAG, "Returning default value: " + defaultValue);
        return defaultValue;
    }

    public static <V> V extractEnumForInteger(final JSONObject obj, final String tagName, final BiMap<V, Integer> valueMapping, final V defaultValue) {

        if (obj == null || !obj.has(tagName)) {
            Log.w(TAG, "Can't find tag " + tagName + " in JSON object " + obj);
            Log.w(TAG, "Returning default value: " + defaultValue);
            return defaultValue;
        }

        if (obj.isNull(tagName)) {
            return defaultValue;
        }

        try {
            final int elem = obj.getInt(tagName);
            return Mappings.findKey(valueMapping, elem, defaultValue);
        } catch (final JSONException e) {
            Log.w(TAG, "Problems extracting tag " + tagName + ": " + e.getMessage());
        }

        Log.w(TAG, "Returning default value: " + defaultValue);
        return defaultValue;
    }

    public static <V> V extractEnumForString(final JSONObject obj, final String tagName, final BiMap<V, String> valueMapping, final V defaultValue) {

        if (obj == null || !obj.has(tagName)) {
            Log.w(TAG, "Can't find tag " + tagName + " in JSON object " + obj);
            Log.w(TAG, "Returning default value: " + defaultValue);
            return defaultValue;
        }

        if (obj.isNull(tagName)) {
            return defaultValue;
        }

        try {
            final String elem = obj.getString(tagName);
            return Mappings.findKey(valueMapping, elem, defaultValue);
        } catch (final JSONException e) {
            Log.w(TAG, "Problems extracting tag " + tagName + ": " + e.getMessage());
        }

        Log.w(TAG, "Returning default value: " + defaultValue);
        return defaultValue;
    }

    public static final String RESPONSE = "Response";
    public static final String RETURN_CODE = "ReturnCode";
    public static final String RETURN_MESSAGE = "ReturnMessage";

    public static final String PARAM_TOKEN = "token";
    public static final String PARAM_APP_TOKEN = "appToken";

    public static final int RETURN_CODE_OK = 0;
    public static final int RETURN_CODE_INVALID_TOKEN = 98;

    public static boolean checkSucceededCloud(final JSONObject response) {
        final int returnCode = getReturnCodeCloud(response);
        if (returnCode == RETURN_CODE_INVALID_TOKEN) {
            Connection.getActiveConnectionData().setHasValidAppToken(false);
            Connection.refreshSession();
        }
        return returnCode == RETURN_CODE_OK;
    }

    public static String getErrorMessageCloud(final JSONObject response) {
        return DsServiceHelper.getString(response, RETURN_MESSAGE, "");
    }

    public static int getReturnCodeCloud(final JSONObject response) {
        return DsServiceHelper.getInt(response, RETURN_CODE, -1);
    }

    public static JSONObject getResponseCloud(final JSONObject response) {
        return DsServiceHelper.getJSONObject(response, RESPONSE);
    }

    public static final String OK = "ok";
    public static final String MESSAGE = "message";
    public static final String RESULT = "result";

    public static final int RETURN_CODE_INVALID_CREDENTIALS = 4;

    public static boolean checkSucceededDirect(final JSONObject response) {
        return DsServiceHelper.getBoolean(response, OK, false);
    }

    public static String getErrorMessageDirect(final JSONObject response) {
        return DsServiceHelper.getString(response, MESSAGE, "");
    }

    public static JSONObject getResultDirect(final JSONObject response) {
        return DsServiceHelper.getJSONObject(response, RESULT);
    }
}
