package com.smartboxasia.control.data.config.app;

import java.util.List;
import java.util.Set;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.smartboxasia.control.App;
import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.dto.DsScene;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class FavoriteActivitiesStore extends ActivitiesCache {

    private static final String PREFS_FAVORITE_ACTIVITES = "FavoriteActivitesComplete";

    private static final Set<DsScene> favoriteActivities = Sets.newLinkedHashSet();

    private static boolean isInitialized = false;

    static synchronized void clearCachedValues() {
        isInitialized = false;
        favoriteActivities.clear();
    }

    public synchronized static void set_favoriteActivities(final Set<DsScene> newFavoriteActivities) {
        favoriteActivities.clear();
        favoriteActivities.addAll(newFavoriteActivities);
        persistFavoritesInPrefs();
        isInitialized = true;
    }

    public synchronized static Set<DsScene> get_favoriteActivities() {
        if (!isInitialized) {
            return Sets.newHashSet();
        }
        return Sets.newLinkedHashSet(favoriteActivities);
    }

    public static synchronized void loadFavoritesFromPrefs() {
        final Context context = App.getInstance();

        // get and set the favorite scenes
        final SharedPreferences favoritePreferences = context.getSharedPreferences(PREFS_FAVORITE_ACTIVITES, Application.MODE_PRIVATE);
        final String allFavoriteIds = favoritePreferences.getString(Connection.getActiveConnectionData().getUrl(), null);

        Set<DsScene> workingSceneList = Sets.newLinkedHashSet();

        if (allFavoriteIds != null) {
            Log.i("favorites: ", allFavoriteIds);
            workingSceneList = parseSerializedScenes(context, allFavoriteIds);
        } else {
            // no favorites saved (fresh install), add Leave Home as default
            Log.i("favorites: ", "No saved favorites");
            workingSceneList.add(new DsScene(context.getString(R.string.leave_home), DssConstants.Scenes.ABSENT, DssConstants.Groups.ACTION, DssConstants.Rooms.GLOBAL));
        }

        // set the found favorites
        favoriteActivities.clear();
        favoriteActivities.addAll(workingSceneList);

        isInitialized = true;
    }

    private static void persistFavoritesInPrefs() {
        final Context context = App.getInstance();

        // Create one string with all the favorites
        final List<String> parts = Lists.newArrayList();
        final StringBuilder part = new StringBuilder();
        for (final DsScene scene : favoriteActivities) {

            part.setLength(0);
            part.append(scene.get_roomId());
            part.append(PREFS_LIST_DELIMITER);
            part.append(scene.get_sceneId());
            part.append(PREFS_LIST_DELIMITER);
            part.append(scene.get_groupNumber());

            parts.add(part.toString());
        }
        final String allFavorites = Joiner.on(PREFS_LIST_DELIMITER).join(parts);
        Log.i("all favorites: ", allFavorites);

        // persist it in sharedPreferences
        final SharedPreferences preferences = context.getSharedPreferences(PREFS_FAVORITE_ACTIVITES, Application.MODE_PRIVATE);
        final ConnectionData activeConnectionData = Connection.getActiveConnectionData();
        if (activeConnectionData != null) {
            final SharedPreferences.Editor editor = preferences.edit();
            editor.putString(activeConnectionData.getUrl(), allFavorites);
            editor.commit();
        }
    }
}
