package com.smartboxasia.control.data.helper;

import java.io.IOException;

import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.connection.ConnectionHandler.ConnectionTask;
import com.smartboxasia.control.data.connection.ConnectionService;
import com.smartboxasia.control.data.connection.exception.NeedsSessionRefreshException;
import com.smartboxasia.control.domain.LoginResult;

public class NopConnectionService implements ConnectionService {

    @Override
    public ConnectionData getActiveConnectionData() {
        return null;
    }

    @Override
    public void login(final ConnectionData connectionData, final String user, final String pass) {
        // If a login is requested we might have a new ConnectionData. So give
        // it a try by refreshing the ConnectionService
        Connection.reconnectConnectionService(connectionData, null, user, pass, true);
    }

    @Override
    public void refreshSession(final ConnectionData data, final boolean silent) {

    }

    @Override
    public LoginResult refreshSessionSync(final ConnectionData data) {
        return LoginResult.LOGIN_RESULT_OK;
    }

    @Override
    public void shutdown() {

    }

    @Override
    public long getActiveConnectionId() {
        return ConnectionData.NO_CONNECTION;
    }

    @Override
    public String getActiveServerId() {
        return "";
    }

    @Override
    public <T> void sendAndParseCloudGetCommand(final CommandBuilder command, final JSONCallbackBase<T> callback) {

    }

    @Override
    public <T> void sendAndParseCloudPostCommand(final CommandBuilder command, final JSONCallbackBase<T> callback) {

    }

    @Override
    public <T> void sendAndParseDirectGetCommand(final CommandBuilder command, final JSONCallbackBase<T> callback) {

    }

    @Override
    public <T> void sendAndParseDirectPostCommand(final CommandBuilder command, final JSONCallbackBase<T> callback) {

    }

    @Override
    public void executeTask(final ConnectionTask task) {

    }

    @Override
    public <T> void sendAndParseDirectPostCommandWithBody(final CommandBuilder command, final String body, final JSONCallbackBase<T> callback) {

    }

    @Override
    public <T> void sendAndParseCloudPostCommandWithBody(final CommandBuilder command, final String body, final JSONCallbackBase<T> callback) {

    }

    @Override
    public String sendCommandAuthenticated(final CommandBuilder commandUrl, final String body, final boolean direct, final boolean useGetMethod) throws IOException, NeedsSessionRefreshException {
        return "";
    }

    @Override
    public void sendRawCommand(final String url, final String body, final JSONCallback parser, final boolean useGetMethod) {

    }
}
