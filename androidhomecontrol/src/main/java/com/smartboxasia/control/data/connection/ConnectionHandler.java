package com.smartboxasia.control.data.connection;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

import com.smartboxasia.control.App;
import com.smartboxasia.control.BuildConfig;
import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.connection.exception.NeedsSessionRefreshException;
import com.smartboxasia.control.data.helper.CommandBuilder;
import com.smartboxasia.control.data.helper.JSONCallback;
import com.smartboxasia.control.data.helper.JSONCallbackBase;
import com.smartboxasia.control.data.helper.SSLSocketFactoryCreator;
import com.smartboxasia.control.domain.LoginResult;
import com.smartboxasia.control.events.NoConnectivityEvent;
import com.google.common.base.Strings;

public class ConnectionHandler {

    private static final String TAG = ConnectionHandler.class.getSimpleName();

    public static interface ConnectionTask {
        void executeTask(ConnectionHandler handler);
    }

    private String sessionToken;
    private ConnectionData connectionData;
    private ConnectivityManager connectivityManager;

    public ConnectionHandler() {
        connectivityManager = (ConnectivityManager) App.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public void injectConnectionData(final ConnectionData connectionData) {
        this.connectionData = connectionData;
    }

    /**
     * Sends HTTP Get request to a dSS and passes the result over to the parser.
     * 
     * @param command the file part of the url to request
     * @param responseParser the parser that will parse the JSON response
     */
    public <T> T sendAndParseDirectGetRequest(final CommandBuilder command, final JSONCallbackBase<T> responseParser) {
        return sendAndParseCommand(command, null, responseParser, true, true, true);
    }

    /**
     * Sends HTTP Get request to the cloud and passes the result over to the parser.
     * 
     * @param command the file part of the url to request
     * @param appendSessionToken if true the session token will be appended, if false it will not be appended
     */
    public <T> T sendAndParseCloudGetCommand(final CommandBuilder command, final JSONCallbackBase<T> responseParser) {
        return sendAndParseCommand(command, null, responseParser, false, true, true);
    }

    /**
     * Sends HTTP Post request to a dSS and passes the result over to the parser.
     * 
     * @param command the file part of the url to request
     * @param responseParser the parser that will parse the JSON response
     */
    public <T> T sendAndParseDirectPostRequest(final CommandBuilder command, final JSONCallbackBase<T> responseParser) {
        return sendAndParseCommand(command, null, responseParser, true, false, true);
    }

    /**
     * Sends HTTP Post request with body to a dSS and passes the result over to the parser.
     * 
     * @param command the file part of the url to request
     * @param body the body to send with the post
     * @param appendSessionToken if true the session token will be appended, if false it will not be appended
     */
    public <T> T sendAndParseDirectPostRequestWithBody(final CommandBuilder command, final String body, final JSONCallbackBase<T> responseParser) {
        return sendAndParseCommand(command, body, responseParser, true, false, true);
    }

    /**
     * Sends HTTP Post request to the cloud and passes the result over to the parser.
     * 
     * @param command the file part of the url to request
     * @param appendSessionToken if true the session token will be appended, if false it will not be appended
     */
    public <T> T sendAndParseCloudPostCommand(final CommandBuilder command, final JSONCallbackBase<T> responseParser) {
        return sendAndParseCommand(command, null, responseParser, false, false, true);
    }

    /**
     * Sends HTTP Post request with body to the cloud and passes the result over to the parser.
     * 
     * @param command the file part of the url to request
     * @param body the body to send with the post
     * @param appendSessionToken if true the session token will be appended, if false it will not be appended
     */
    public <T> T sendAndParseCloudPostRequestWithBody(final CommandBuilder command, final String body, final JSONCallbackBase<T> responseParser) {
        return sendAndParseCommand(command, body, responseParser, false, false, true);
    }

    private <T> T sendAndParseCommand(final CommandBuilder command, final String body, final JSONCallbackBase<T> callback, final boolean direct, final boolean useGetMethod, final boolean retry) {
        try {
            final String resultString = sendCommandAuthenticated(command, body, direct, useGetMethod);

            if (!Strings.isNullOrEmpty(resultString)) {
                final JSONObject root = new JSONObject(resultString);
                return callback.onResult(root);
            }
        } catch (final MalformedURLException e) {
            // this is a programming error in the app or
            callback.onFailure(JSONCallback.ERROR_APP, e.getLocalizedMessage());
        } catch (final JSONException e) {
            // this is a potential error on server / version compatibility
            callback.onFailure(JSONCallback.ERROR_INVALID_JSON, e.getLocalizedMessage());
        } catch (final FileNotFoundException e) {
            // this is a potential error on server / version compatibility
            callback.onFailure(JSONCallback.ERROR_SERVER, e.getLocalizedMessage());
        } catch (final IOException e) {
            // Cloud not operational
            callback.onFailure(JSONCallback.ERROR_CLOUD, e.getLocalizedMessage());
        } catch (final NeedsSessionRefreshException e) {
            // retry after session refresh
            if (retry && Connection.refreshSessionSync() == LoginResult.LOGIN_RESULT_OK) {
                sendAndParseCommand(command, body, callback, direct, useGetMethod, false);
            } else {
                // Session not refreshable, Handle the same as the FileNotFoundException
                callback.onFailure(JSONCallback.ERROR_SERVER, e.getCause().getLocalizedMessage());
            }
        }
        return null;
    }

    /**
     * Sends HTTP Get request and returns result or empty string.
     * 
     * @param command the file part of the url to request
     * @param direct use dSS connection or cloud url
     * @param useGetMethod if true, send a get request, else send a post request
     * @return the result string or the empty string
     * @throws IOException in case of connection trouble or malformed url
     * @throws NeedsSessionRefreshException in case the session expired
     * @throws NoNetworkException
     */
    public String sendCommandUnauthenticated(final CommandBuilder command, final boolean direct, final boolean useGetMethod) throws IOException, NeedsSessionRefreshException {
        return sendCommand(command, null, false, direct, useGetMethod);
    }

    /**
     * Sends HTTP Get request and returns result or empty string.
     * 
     * @param command the file part of the url to request
     * @param direct use dSS connection or cloud url
     * @param useGetMethod if true, send a get request, else send a post request
     * @return the result string or the empty string
     * @throws IOException in case of connection trouble or malformed url
     * @throws NeedsSessionRefreshException in case the session expired
     * @throws NoNetworkException
     */
    public String sendCommandAuthenticated(final CommandBuilder command, final String body, final boolean direct, final boolean useGetMethod) throws IOException, NeedsSessionRefreshException {
        return sendCommand(command, body, true, direct, useGetMethod);
    }

    private String sendCommand(final CommandBuilder command, final String body, final boolean appendToken, final boolean direct, final boolean useGetMethod) throws MalformedURLException, IOException, NeedsSessionRefreshException {

        final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
            App.eventBus.post(new NoConnectivityEvent());
            throw new IOException();
        }

        if (connectionData == null) {
            throw new NullPointerException("no connection data loaded");
        }

        final String commandUrl = command.build();
        String token = "";
        String url = "";

        if (appendToken) {
            if (commandUrl.contains("?")) {
                token += "&" + command.getTokenParamName() + "=";
            } else {
                token += "?" + command.getTokenParamName() + "=";
            }
            if (direct) {
                token += sessionToken;
            } else {
                token += connectionData.getApplicationToken();
            }
        }

        if (direct) {
            url += connectionData.getUrl();
        } else {
            url += connectionData.getCloudUrl();
        }

        return getRequestResult(url + commandUrl + token, body, direct, useGetMethod);
    }

    /**
     * Sends HTTP Get request and returns result or empty string.
     *
     * @param commandUrl the complete request url including the host
     * @param body the body to send if it is a post
     * @param appendSessionToken if true the session token will be appended, if false it will not be appended
     * @return the result string or the empty string
     * @throws IOException in case of connection trouble or malformed url
     * @throws UnknownCertificateExcpetion
     */
    <T> T sendAndParseRawCommand(final String url, final String body, final JSONCallbackBase<T> parser, final boolean useGetMethod) {

        final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
            App.eventBus.post(new NoConnectivityEvent());
            return null;
        }

        String response;
        try {
            response = getRequestResult(url, null, false, useGetMethod);
            if (!Strings.isNullOrEmpty(response)) {
                final JSONObject responseObject = new JSONObject(response);
                return parser.onResult(responseObject);
            }
        } catch (final MalformedURLException e) {
            // we called the wrong service
            // potential error on server / version compatibility
            Log.e(TAG, "Problems on connection: " + e.getLocalizedMessage());
        } catch (final JSONException e) {
            // response does not correspond to JSON
            // potential error on server / version compatibility
            Log.e(TAG, "Problems on connection: " + e.getLocalizedMessage());
        } catch (final IOException e) {
            Log.e(TAG, e.getMessage());
        } catch (final NeedsSessionRefreshException e) {
            // no session needed
            Log.e(TAG, "Problems with session: " + e.getLocalizedMessage());
        }
        return null;
    }

    private String getRequestResult(final String urlString, final String body, final boolean direct, final boolean useGetMethod) throws MalformedURLException, IOException, NeedsSessionRefreshException {
        final URL url = new URL(urlString);
        if (BuildConfig.DEBUG) {
            Log.v(TAG, url.toString());
        }
        final URLConnection connection = url.openConnection();
        connection.setDoOutput(false);
        if (connection instanceof HttpURLConnection) {
            if (useGetMethod) {
                ((HttpURLConnection) connection).setRequestMethod("GET");
            } else {
                ((HttpURLConnection) connection).setRequestMethod("POST");
            }
        }
        connection.setConnectTimeout(15000);
        connection.setReadTimeout(30000);
        connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        if (body != null && !useGetMethod) {
            connection.setRequestProperty("Content-Length", "" + body.length());
        } else {
            connection.setRequestProperty("Content-Length", "0");
        }
        fixHangingConnectionIssue(connection);
        if (connection instanceof HttpsURLConnection) {
            ((HttpsURLConnection) connection).setSSLSocketFactory(SSLSocketFactoryCreator.createSslSocketFactory());
            ((HttpsURLConnection) connection).setHostnameVerifier(new AllowAllHostnameVerifier());
        }

        connection.connect();

        if (body != null && !useGetMethod) {
            if (BuildConfig.DEBUG) {
                Log.v(TAG, body);
            }
            final byte[] outputInBytes = body.getBytes("UTF-8");
            final OutputStream os = connection.getOutputStream();
            os.write(outputInBytes);
            os.close();
        }

        final StringBuilder result = new StringBuilder();
        try {
            final InputStream content = connection.getInputStream();
            readStreamContent(result, content);
        } catch (final FileNotFoundException e) {
            if (direct && connection instanceof HttpURLConnection) {
                // in case we're calling a dss directly, we should check session invalidity here (403)
                final int responseCode = ((HttpURLConnection) connection).getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_FORBIDDEN) {
                    // the session is not valid anymore. Handle this farther outside
                    throw new NeedsSessionRefreshException(e);
                }
            }
            throw e;
        }

        return result.toString();
    }

    private void readStreamContent(final StringBuilder result, final InputStream content) throws IOException {
        try {
            final BufferedReader reader = new BufferedReader(new InputStreamReader(content));
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
        } finally {
            content.close();
        }
    }

    /*
     * Workaround for an issue were Samsung Devices kept a connection in state WAIT_CLOSE until there were no more connections available and the connection for all devices connected to the server
     * timed out. Following issue is the reason for this: https://code.google.com/p/google-http-java-client/issues/detail?id=213
     */
    private void fixHangingConnectionIssue(final URLConnection connection) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            try {
                connection.setRequestProperty("Connection", "close");
            } catch (final Exception e) {
            }
        }
    }

    public void setSessionToken(final String token) {
        sessionToken = token;
    }

    public ConnectionData getConnectionData() {
        return connectionData;
    }

    public boolean isConnectionInitialized() {
        return connectionData != null && connectionData.hasValidAppToken();
    }
}
