package com.smartboxasia.control.data.materialmaster.cloud_1_0.json;

public class JSONTags {

    public static final String ILLUMINANTS = "Illuminants";
    public static final String ID = "ID";
    public static final String EAN = "EAN";
    public static final String DIMMING_CURVE = "DimmingCurves";
    public static final String DEVICE_DIMMABLE_RESULT = "DeviceDimmableResult";
    public static final String FITTING_CHECK_RESULT = "FittingCheckResult";
    public static final String ILLUMINANT_DIMMING_RESULT = "IlluminantDimmingResult";

}
