package com.smartboxasia.control.data.connection.json;

public class JSONTags {

    public static final String RESULT = "result";
    public static final String OK = "ok";
    public static final String TOKEN = "token";
    public static final String VERSION = "version";
    public static final String DSID = "dSID";
    public static final String APPLICATION_TOKEN = "applicationToken";
}
