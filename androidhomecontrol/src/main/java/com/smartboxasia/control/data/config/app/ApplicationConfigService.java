package com.smartboxasia.control.data.config.app;

public class ApplicationConfigService {

    public static void clearAllCaches() {
        ConsumptionCache.clearCachedValues();
        FavoriteActivitiesStore.clearCachedValues();
        MetersCache.clearCachedValues();
        RoomsStore.clearCachedValues();
        ShownScenesStore.clearCachedValues();
        UserDefinedActionsCache.clearCachedValues();
    }

}
