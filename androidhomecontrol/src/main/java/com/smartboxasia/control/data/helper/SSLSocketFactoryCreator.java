package com.smartboxasia.control.data.helper;

import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import android.util.Log;

public class SSLSocketFactoryCreator {

    private static final String TAG = SSLSocketFactoryCreator.class.getSimpleName();

    public static SSLSocketFactory createSslSocketFactory() {
        try {
            // Create an SSLContext that uses our TrustManager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            final AcceptAllTrustManager trustManager = new AcceptAllTrustManager();
            sslContext.init(null, new TrustManager[] {
                    trustManager
            }, new java.security.SecureRandom());
            return sslContext.getSocketFactory();
        } catch (final GeneralSecurityException e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }

    private static class AcceptAllTrustManager implements X509TrustManager {

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        @Override
        public void checkClientTrusted(final X509Certificate[] chain, final String authType) throws CertificateException {

        }

        @Override
        public void checkServerTrusted(final X509Certificate[] chain, final String authType) throws CertificateException {

        }
    }
}
