package com.smartboxasia.control.data.config.app;

import java.util.List;
import java.util.Map;
import java.util.Set;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.smartboxasia.control.App;
import com.smartboxasia.control.DssConstants;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.data.dss.direct_1_17_0.json.DssMappings;
import com.smartboxasia.control.dto.DsRoom;
import com.smartboxasia.control.dto.DsScene;
import com.smartboxasia.control.ui.helper.SceneHelper;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;

public class ShownScenesStore extends ActivitiesCache {

    private static final String PREFS_LIST_DELIMITER = "||";
    private static final String PREFS_SUBLIST_DELIMITER = "#";

    private static final String PREFS_SHOWN_ACTIVITES = "ShownActivites";
    private static final String KEY_KNOWN_ROOMS_POSTFIX = "KnownRooms";
    private static final String KEY_ALL_SCENES_POSTFIX = "AllScenes";

    private static boolean isInitialized = false;
    private static final Map<Integer, Set<Integer>> knownRooms = Maps.newHashMap();
    private static final Set<DsScene> shownScenes = Sets.newLinkedHashSet();

    synchronized static void clearCachedValues() {
        isInitialized = false;
        shownScenes.clear();
        knownRooms.clear();
    }

    public synchronized static void set_shownScenes(final Set<DsScene> scenes) {
        shownScenes.clear();
        shownScenes.addAll(scenes);
        persistShownScenes();
    }

    public synchronized static Set<DsScene> get_shownScenes() {
        if (!isInitialized) {
            return Sets.newHashSet();
        }
        return Sets.newLinkedHashSet(shownScenes);
    }

    public synchronized static List<DsScene> get_shownScenesForRoomAndGroup(final int roomId, final int groupNumber) {
        if (!isInitialized) {
            return Lists.newArrayList();
        }

        if (DssConstants.Rooms.isGlobal(roomId)) {
            return Lists.newArrayList(); // no scenes "shown" for global rooms
        }

        // check if this is a known room and group
        final DsRoom room = RoomsStore.get_room_by_id(roomId);
        final Set<Integer> knownGroups = knownRooms.get(roomId);
        if (knownGroups == null || !knownGroups.contains(groupNumber)) {
            // if not we set the default scenes to be shown and mark the group and room as known

            add_knownRoomWithGroup(roomId, groupNumber);

            final List<Long> defaultShownSceneIds = SceneHelper.defaultShownSceneIds;
            for (final long sceneId : defaultShownSceneIds) {
                final DsScene customScene = room.get_scene(sceneId, groupNumber);
                if (customScene != null) {
                    shownScenes.add(customScene);
                } else {
                    final String sceneName = SceneHelper.getDefaultSceneName(App.getInstance(), sceneId, groupNumber);
                    shownScenes.add(new DsScene(sceneName, sceneId, groupNumber, roomId));
                }
            }
            persistShownScenes();
        }

        final List<DsScene> result = Lists.newArrayList();
        final List<Long> defaultShownSceneIds = Lists.newArrayList(DssMappings.DEFAULT_SCENE_NAMES.keySet());
        for (final long sceneId : defaultShownSceneIds) {
            DsScene scene;
            scene = room.get_scene(sceneId, groupNumber);
            if (scene == null) {
                final String sceneName = SceneHelper.getDefaultSceneName(App.getInstance(), sceneId, groupNumber);
                scene = new DsScene(sceneName, sceneId, groupNumber, roomId);
            }
            if (shownScenes.contains(scene)) {
                result.add(scene);
            }
        }
        return result;
    }

    private static void add_knownRoomWithGroup(final int roomId, final int groupNumber) {
        if (!knownRooms.containsKey(roomId)) {
            final Set<Integer> groupSet = Sets.newHashSet();
            knownRooms.put(roomId, groupSet);
        }
        knownRooms.get(roomId).add(groupNumber);
        persistKnownRoomsAndGroups();
    }

    public static void loadShownScenesFromPrefs() {
        final Context context = App.getInstance();

        // get and set the shown scenes
        final SharedPreferences shownScenesPreferences = context.getSharedPreferences(PREFS_SHOWN_ACTIVITES, Application.MODE_PRIVATE);
        final String allShownScenes = shownScenesPreferences.getString(Connection.getActiveConnectionData().getUrl() + KEY_ALL_SCENES_POSTFIX, null);
        final String allKnownRoomIdsAndGroupNumbers = shownScenesPreferences.getString(Connection.getActiveConnectionData().getUrl() + KEY_KNOWN_ROOMS_POSTFIX, null);

        knownRooms.clear();
        if (allKnownRoomIdsAndGroupNumbers != null) {
            parseKnownRoomIds(allKnownRoomIdsAndGroupNumbers);
            Log.i("knwon rooms and groups restored: ", allKnownRoomIdsAndGroupNumbers);
        }

        shownScenes.clear();
        if (allShownScenes != null) {
            shownScenes.addAll(parseSerializedScenes(context, allShownScenes));
            Log.i("shown Scenes restored: ", allShownScenes);
        }

        isInitialized = true;
    }

    private static void parseKnownRoomIds(final String allKnownRoomIds) {
        final List<String> knownRoomIdsWithGroups = Splitter.on(PREFS_LIST_DELIMITER).splitToList(allKnownRoomIds);
        for (final String roomString : knownRoomIdsWithGroups) {
            parseRoomString(roomString);
        }
    }

    private static void parseRoomString(final String roomString) {
        List<String> knownGroups = Splitter.on(PREFS_SUBLIST_DELIMITER).splitToList(roomString);

        knownGroups = Lists.newArrayList(knownGroups); // make the list modifiable
        final String roomId = knownGroups.remove(0); // remove the roomId which is in front of the list
        final Integer parsedRoomId = Ints.tryParse(roomId);

        if (parsedRoomId == null || RoomsStore.get_room_by_id(parsedRoomId) == null) {
            return; // skip configuration for nonexistent rooms
        }

        // now go through all the known groups and restore the info
        for (final String groupNumber : knownGroups) {
            final Integer parsedGroupNumber = Ints.tryParse(groupNumber);
            if (parsedGroupNumber != null) {
                add_knownRoomWithGroup(parsedRoomId, parsedGroupNumber);
            }
        }
    }

    private static void persistShownScenes() {
        final Context context = App.getInstance();

        // Create one string with all the favorites
        final List<String> parts = Lists.newArrayList();

        // flatten the scenes
        final StringBuilder part = new StringBuilder();
        for (final DsScene scene : get_shownScenes()) {
            if (DssConstants.Rooms.isGlobal(scene.get_roomId())) {
                continue; // ignore global scenes
            }

            part.setLength(0);
            part.append(scene.get_roomId());
            part.append(PREFS_LIST_DELIMITER);
            part.append(scene.get_sceneId());
            part.append(PREFS_LIST_DELIMITER);
            part.append(scene.get_groupNumber());

            parts.add(part.toString());
        }
        final String allShownScenes = Joiner.on(PREFS_LIST_DELIMITER).join(parts);
        Log.i("shown scenes to persist: ", allShownScenes);

        // persist it in sharedPreferences
        final SharedPreferences preferences = context.getSharedPreferences(PREFS_SHOWN_ACTIVITES, Application.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Connection.getActiveConnectionData().getUrl() + KEY_ALL_SCENES_POSTFIX, allShownScenes);
        editor.commit();
    }

    private static void persistKnownRoomsAndGroups() {
        final Context context = App.getInstance();

        // Create one string with all the favorites
        final List<String> parts = Lists.newArrayList();

        // now flatten the known rooms
        for (final int knownRoomId : knownRooms.keySet()) {
            final String goupsString = Joiner.on(PREFS_SUBLIST_DELIMITER).join(knownRooms.get(knownRoomId));
            parts.add("" + knownRoomId + PREFS_SUBLIST_DELIMITER + goupsString);

        }
        final String allKnownRooms = Joiner.on(PREFS_LIST_DELIMITER).join(parts);
        Log.i("known rooms: ", allKnownRooms);

        // persist it in sharedPreferences
        final SharedPreferences preferences = context.getSharedPreferences(PREFS_SHOWN_ACTIVITES, Application.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Connection.getActiveConnectionData().getUrl() + KEY_KNOWN_ROOMS_POSTFIX, allKnownRooms);
        editor.commit();
    }

}
