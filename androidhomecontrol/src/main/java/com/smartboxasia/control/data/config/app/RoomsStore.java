package com.smartboxasia.control.data.config.app;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.smartboxasia.control.App;
import com.smartboxasia.control.data.connection.Connection;
import com.smartboxasia.control.dto.DsRoom;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;

public class RoomsStore extends ActivitiesCache {

    private static final String PREFS_LIST_DELIMITER = "||";
    private static final String PREFS_SUBLIST_DELIMITER = "#";

    private static final String PREFS_ROOM_SEQUENCES = "roomSequences";

    private static final List<DsRoom> roomsSorted = Lists.newArrayList();

    private static boolean isInitialized = false;
    private static final Map<Integer, Integer> roomSequence = Maps.newHashMap();

    synchronized static void clearCachedValues() {
        isInitialized = false;
        roomsSorted.clear();
        roomSequence.clear();

    }

    public synchronized static void set_roomsUnsorted(final List<DsRoom> newRooms) {
        updateSequenceForNewRoomList(newRooms);
        sortRoomListWithSequence(newRooms);
        persistRoomSequence();
        isInitialized = true;
        // now that the rooms are available we can load the shown scenes configuration
        ShownScenesStore.loadShownScenesFromPrefs();
        FavoriteActivitiesStore.loadFavoritesFromPrefs();
    }

    public synchronized static void set_roomsSorted(final List<DsRoom> newRoomsSorted) {
        roomsSorted.clear();
        roomsSorted.addAll(newRoomsSorted);
        updateRoomSequenceFromSortedRooms();
        persistRoomSequence();
        isInitialized = true;
    }

    public synchronized static List<DsRoom> get_roomsSorted() {
        if (!isInitialized) {
            return Lists.newArrayList();
        }
        return Lists.newArrayList(roomsSorted);
    }

    public synchronized static DsRoom get_room_by_id(final int roomId) {
        if (!isInitialized) {
            return null;
        }
        for (final DsRoom room : roomsSorted) {
            if (room.get_id() == roomId) {
                return room;
            }
        }
        return null;
    }

    private static void updateRoomSequenceFromSortedRooms() {
        roomSequence.clear();
        for (int i = 0; i < roomsSorted.size(); i++) {
            roomSequence.put(roomsSorted.get(i).get_id(), i);
        }
    }

    private static void sortRoomListWithSequence(final List<DsRoom> newRooms) {
        roomsSorted.clear();
        // init list with empty elements for direct insertion at sorted position
        for (int i = 0; i < newRooms.size(); i++) {
            roomsSorted.add(null);

        }
        for (final DsRoom room : newRooms) {
            final Integer index = roomSequence.get(room.get_id());
            if (index != null) {
                roomsSorted.set(index, room);
            }
        }
    }

    /*
     * Updates the room sequence to match the rooms loaded
     */
    private static void updateSequenceForNewRoomList(final List<DsRoom> newRooms) {
        final Map<Integer, Integer> newSequence = Maps.newLinkedHashMap();
        final List<Integer> indices = Lists.newArrayList();

        for (final DsRoom room : newRooms) {
            final int id = room.get_id();
            if (roomSequence.containsKey(id)) {
                final int index = roomSequence.get(id);
                newSequence.put(id, index);
                indices.add(index);
            }
        }

        Collections.sort(indices);

        // normalize the sequence
        for (int i = 0; i < newSequence.size(); i++) {
            for (final Entry<Integer, Integer> entry : newSequence.entrySet()) {
                if (entry.getValue() == indices.get(i)) {
                    entry.setValue(i);
                    break;
                }
            }
        }

        // add new rooms at the end of the sequence, sorted alphabetically
        Collections.sort(newRooms, DsRoom.COMPARATOR_ALPHABETIC);
        for (final DsRoom room : newRooms) {
            if (!newSequence.containsKey(room.get_id())) {
                newSequence.put(room.get_id(), newSequence.size()); // add unknown rooms at the end
            }
        }

        roomSequence.clear();
        roomSequence.putAll(newSequence);
    }

    public static synchronized void loadRoomSequenceFromPrefs() {
        final Context context = App.getInstance();

        // get the url-associated sequence of rooms
        final SharedPreferences sequencePreferences = context.getSharedPreferences(PREFS_ROOM_SEQUENCES, Application.MODE_PRIVATE);
        final String stringSequence = sequencePreferences.getString(Connection.getActiveConnectionData().getUrl(), null);
        Log.i("sequence", "loading this sequence in onCreate: " + stringSequence);

        roomSequence.clear();
        if (stringSequence != null) {
            parseSequence(stringSequence);
        }
    }

    private static void parseSequence(final String stringSequence) {
        final List<String> flatMap = Lists.newArrayList(Splitter.on(PREFS_LIST_DELIMITER).split(stringSequence));
        for (final String flatEntry : flatMap) {
            final List<String> splitEntry = Splitter.on(PREFS_SUBLIST_DELIMITER).splitToList(flatEntry);
            if (splitEntry.size() == 2) {
                roomSequence.put(Ints.tryParse(splitEntry.get(0)), Ints.tryParse(splitEntry.get(1)));
            }
        }
    }

    private static void persistRoomSequence() {
        final Context context = App.getInstance();

        final List<String> flatMap = Lists.newArrayList();
        for (final Entry<Integer, Integer> entry : roomSequence.entrySet()) {
            flatMap.add(entry.getKey() + PREFS_SUBLIST_DELIMITER + entry.getValue());
        }

        // Create one string with the sequence
        final String saveSequence = Joiner.on(PREFS_LIST_DELIMITER).join(flatMap);
        Log.i("sequence", "Persisting this sequence: " + saveSequence);

        final SharedPreferences sequencePreferences = context.getSharedPreferences(PREFS_ROOM_SEQUENCES, Application.MODE_PRIVATE);
        final SharedPreferences.Editor editor3 = sequencePreferences.edit();
        editor3.putString(Connection.getActiveConnectionData().getUrl(), saveSequence);
        editor3.commit();
    }
}
