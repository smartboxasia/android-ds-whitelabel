package com.smartboxasia.control.data.connection;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import com.smartboxasia.control.App;
import com.smartboxasia.control.data.config.connection.ConnectionConfigService;
import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.connection.ConnectionHandler.ConnectionTask;
import com.smartboxasia.control.data.connection.exception.NeedsSessionRefreshException;
import com.smartboxasia.control.data.helper.CommandBuilder;
import com.smartboxasia.control.data.helper.JSONCallback;
import com.smartboxasia.control.data.helper.JSONCallbackBase;
import com.smartboxasia.control.domain.LoginResult;
import com.smartboxasia.control.events.LoginResultEvent;

class ConnectionServiceImpl implements ConnectionService {

    private static final int NUM_OF_PARALLEL_THREADS = 3;

    private ScheduledExecutorService executor = Executors.newScheduledThreadPool(NUM_OF_PARALLEL_THREADS);

    private ConnectionHandler connectionHandler;
    private LoginHandler loginHandler;

    public boolean isConnectionInitialized() {
        return connectionHandler != null && connectionHandler.isConnectionInitialized();
    }

    void injectConnectionHandler(final ConnectionHandler handler) {
        connectionHandler = handler;
    }

    void injectLoginHandler(final LoginHandler handler) {
        loginHandler = handler;
    }

    @Override
    public ConnectionData getActiveConnectionData() {
        if (connectionHandler == null) {
            return null;
        }
        final ConnectionData connectionData = connectionHandler.getConnectionData();
        // To be sure, load complete data from persistent memory
        if (connectionData != null) {
            final ConnectionData fullData = ConnectionConfigService.getConnectionData(connectionData.getUrl());
            if (fullData != null) {
                connectionData.update(fullData);
            }
            return connectionData;
        }
        return null;
    }

    @Override
    public long getActiveConnectionId() {
        if (connectionHandler == null || connectionHandler.getConnectionData() == null) {
            return ConnectionData.NO_CONNECTION;
        }
        return connectionHandler.getConnectionData().getConnectionId();
    }

    @Override
    public String getActiveServerId() {
        if (connectionHandler == null || connectionHandler.getConnectionData() == null) {
            return "";
        }
        return connectionHandler.getConnectionData().getServerId();
    }

    @Override
    public void shutdown() {
        executor.shutdownNow();
    }

    @Override
    public void refreshSession(final ConnectionData data, final boolean silent) {
        executor.execute(new Runnable() {

            @Override
            public void run() {
                doLogin(data, null, null, silent);
            }
        });
    }

    @Override
    public LoginResult refreshSessionSync(final ConnectionData data) {
        return doLogin(data, null, null, false);
    }

    @Override
    public void login(final ConnectionData connectionData, final String email, final String pass) {
        executor.execute(new Runnable() {

            @Override
            public void run() {
                doLogin(connectionData, email, pass, false);
            }
        });
    }

    @Override
    public <T> void sendAndParseCloudGetCommand(final CommandBuilder command, final JSONCallbackBase<T> parser) {
        executor.execute(new Runnable() {

            @Override
            public void run() {
                connectionHandler.sendAndParseCloudGetCommand(command, parser);
            }
        });
    }

    @Override
    public <T> void sendAndParseCloudPostCommand(final CommandBuilder command, final JSONCallbackBase<T> parser) {
        executor.execute(new Runnable() {

            @Override
            public void run() {
                connectionHandler.sendAndParseCloudPostCommand(command, parser);
            }
        });
    }

    @Override
    public <T> void sendAndParseDirectGetCommand(final CommandBuilder command, final JSONCallbackBase<T> parser) {
        executor.execute(new Runnable() {

            @Override
            public void run() {
                connectionHandler.sendAndParseDirectGetRequest(command, parser);
            }
        });
    }

    @Override
    public <T> void sendAndParseDirectPostCommand(final CommandBuilder command, final JSONCallbackBase<T> parser) {
        executor.execute(new Runnable() {

            @Override
            public void run() {
                connectionHandler.sendAndParseDirectPostRequest(command, parser);
            }
        });
    }

    @Override
    public void sendRawCommand(final String url, final String body, final JSONCallback parser, final boolean useGetMethod) {
        executor.execute(new Runnable() {

            @Override
            public void run() {
                connectionHandler.sendAndParseRawCommand(url, body, parser, useGetMethod);
            }
        });
    }

    @Override
    public void executeTask(final ConnectionTask task) {
        executor.execute(new Runnable() {

            @Override
            public void run() {
                task.executeTask(connectionHandler);
            }
        });
    }

    @Override
    public String sendCommandAuthenticated(final CommandBuilder commandUrl, final String body, final boolean direct, final boolean useGetMethod) throws IOException, NeedsSessionRefreshException {
        return connectionHandler.sendCommandAuthenticated(commandUrl, body, direct, useGetMethod);
    }

    @Override
    public <T> void sendAndParseDirectPostCommandWithBody(final CommandBuilder command, final String body, final JSONCallbackBase<T> parser) {
        executor.execute(new Runnable() {

            @Override
            public void run() {
                connectionHandler.sendAndParseDirectPostRequestWithBody(command, body, parser);
            }
        });
    }

    @Override
    public <T> void sendAndParseCloudPostCommandWithBody(final CommandBuilder command, final String body, final JSONCallbackBase<T> parser) {
        executor.execute(new Runnable() {

            @Override
            public void run() {
                connectionHandler.sendAndParseCloudPostRequestWithBody(command, body, parser);
            }
        });
    }

    private LoginResult doLogin(final ConnectionData connectionData, final String email, final String pass, final boolean silent) {
        LoginResult result = LoginResult.LOGIN_RESULT_NOT_OK;
        if (loginHandler != null) {
            result = loginHandler.login(email, pass, silent);
        }
        if (result == LoginResult.LOGIN_RESULT_OK) {
            connectionData.saveConfig();
        }
        App.eventBus.post(new LoginResultEvent(result));
        return result;
    }
}
