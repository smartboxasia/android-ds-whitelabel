package com.smartboxasia.control.data.connection;

import com.smartboxasia.control.App;
import com.smartboxasia.control.R;
import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.helper.NopConnectionService;
import com.smartboxasia.control.domain.LoginResult;
import com.smartboxasia.control.events.LoginResultEvent;
import com.smartboxasia.control.events.ShowLoginErrorDialogEvent;
import com.google.common.eventbus.Subscribe;

public abstract class Connection {

    private static ConnectionService connectionService;

    public static boolean isLoggedIn;

    private static Object eventReceiver = new Object() {

        @Subscribe
        public void onEvent(final LoginResultEvent event) {
            if (event.getResult() == LoginResult.LOGIN_RESULT_OK) {
                isLoggedIn = true;
            }
        }
    };

    static {
        // we want to get every event concerning login results
        App.eventBus.register(eventReceiver);
    }

    /**
     * Use the supplied version string to determine which ConnectionService should get used to connect to this server. If null is passed the newest ConnectionService is used.
     *
     * @param data the ConnectionData to use for initializing the connection
     * @param version the min version of the server or null
     * @param user the user name or null
     * @param pass the password or null
     */
    public static void reconnectConnectionService(final ConnectionData connectionData, final String version, final String user, final String pass, final boolean refreshSession) {

        // reset the logged in state. If login succeeds it will beset to true again.
        isLoggedIn = false;

        if (connectionService != null) {
            connectionService.shutdown();
        }

        if (connectionData == null) {
            // no connection data available. Just start the app without connection.
            connectionService = new NopConnectionService();
            return;
        }

        final ConnectionServiceProvider provider = ConnectionServiceProviderCreator.getProvider(App.getInstance(), version, connectionData.getType());
        if (provider == null) {

            // no connection service for this version of the server/connection
            // type available. Just start the app without connecting.
            final String title = App.getInstance().getString(R.string.min_version_not_met_title);
            final String message = App.getInstance().getString(R.string.min_version_not_met_msg, ConnectionServiceProviderCreator.MIN_DSS_VERSION_AVAILABLE);
            App.eventBus.post(new ShowLoginErrorDialogEvent(title, message, true));

            if (refreshSession) {
                App.eventBus.post(new LoginResultEvent(LoginResult.LOGIN_RESULT_NOT_OK));
            }

            connectionService = new NopConnectionService();
            return;
        }

        connectionService = provider.createConnectionService(connectionData);

        if (refreshSession) {
            connectionService.login(connectionData, user, pass);
        }
    }

    public static ConnectionData getActiveConnectionData() {
        return connectionService.getActiveConnectionData();
    }

    public static ConnectionService getConnectionService() {
        return connectionService;
    }

    public static String getCurrentConnectionName() {
        final ConnectionData activeConnectionData = connectionService.getActiveConnectionData();
        if (activeConnectionData != null) {
            return activeConnectionData.getName();
        } else {
            return "";
        }
    }

    public static String getCurrentConnectionUser() {
        final ConnectionData activeConnectionData = connectionService.getActiveConnectionData();
        if (activeConnectionData != null) {
            return activeConnectionData.getUser();
        } else {
            return "";
        }
    }

    public static String getCurrentConnectionServerId() {
        final ConnectionData activeConnectionData = connectionService.getActiveConnectionData();
        if (activeConnectionData != null) {
            return activeConnectionData.getServerId();
        } else {
            return "";
        }
    }

    public static void refreshSession() {
        connectionService.refreshSession(getActiveConnectionData(), false);
    }

    public static void refreshSessionNoWarnings() {
        connectionService.refreshSession(getActiveConnectionData(), true);
    }

    public static LoginResult refreshSessionSync() {
        return connectionService.refreshSessionSync(getActiveConnectionData());
    }
}
