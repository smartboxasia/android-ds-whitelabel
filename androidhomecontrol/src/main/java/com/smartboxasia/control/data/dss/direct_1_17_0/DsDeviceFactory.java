package com.smartboxasia.control.data.dss.direct_1_17_0;

import com.smartboxasia.control.data.dss.direct_1_17_0.json.JSONConstants;
import com.smartboxasia.control.dto.DsDevice;
import com.google.common.base.Strings;

public abstract class DsDeviceFactory {

    static DsDevice create(final String gtin, final String oemEanNumber, final int serialNumber) {

        DsDevice res;
        if (isGESDS220WithOldFW(gtin, oemEanNumber)) {
            res = new DsDevice(new DsDeviceGeSds220OldFwBehavior());
        } else if (isGESDS200Joker(gtin, serialNumber)) {
            res = new DsDevice(new DsDeviceGeSds200JokerBehavior());
        } else if (isUMR200(gtin, serialNumber, 2)) { // serial number of output device is modulo 4 rest 2
            res = new DsDevice(new DsDeviceUmr200Behavior(true));
        } else if (isUMR200(gtin, serialNumber, 3)) { // serial number of output device is modulo 4 rest 3
            res = new DsDevice(new DsDeviceUmr200Behavior(false));
        } else {
            res = new DsDevice(new DsDeviceDefaultBehavior(gtin));
        }

        return res;
    }

    private static boolean isUMR200(final String gtin, final int serialNumber, final int subDeviceId) {
        return JSONConstants.Gtins.SW_UMR200.equals(gtin) && serialNumber % 4 == subDeviceId;
    }

    static boolean isGESDS200Joker(final String gtin, final int serialNumber) {
        return (JSONConstants.Gtins.GE_SDS200_CW.equals(gtin) || JSONConstants.Gtins.GE_SDS200_CS.equals(gtin)) && serialNumber % 2 != 0;
    }

    static boolean isGESDS220WithOldFW(final String gtin, final String oemEanNumber) {
        return Strings.isNullOrEmpty(gtin) && JSONConstants.GE_SDS_OEM_EAN_NUMBERS.contains(oemEanNumber);
    }
}
