package com.smartboxasia.control.data.config.app;

public class ConsumptionCache {

    private static int consumption;

    static void clearCachedValues() {
        consumption = -1;
    }

    public static void set_consumption(final int newConsumption) {
        consumption = newConsumption;
    }

    public static int get_consumption() {
        return consumption;
    }
}
