package com.smartboxasia.control.data.tagging.cloud_1_0.json;

import java.util.Map;

import com.smartboxasia.control.data.dss.direct_1_17_0.json.JSONConstants.SocketTypes;
import com.smartboxasia.control.domain.SocketType;
import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Maps;

public class TaggingMappings {

    public static final BiMap<SocketType, String> SOCKET_TYPES;
    static {
        final Map<SocketType, String> map = Maps.newHashMap();
        map.put(SocketType.E14, SocketTypes.E14);
        map.put(SocketType.E27, SocketTypes.E27);
        map.put(SocketType.G4, SocketTypes.G4);
        map.put(SocketType.G53, SocketTypes.G53);
        map.put(SocketType.G9, SocketTypes.G9);
        map.put(SocketType.GU10, SocketTypes.GU10);
        map.put(SocketType.B15d, SocketTypes.B15d);
        map.put(SocketType.B22d, SocketTypes.B22d);
        map.put(SocketType.E10, SocketTypes.E10);
        map.put(SocketType.E12, SocketTypes.E12);
        map.put(SocketType.E40, SocketTypes.E40);
        map.put(SocketType.Fa4, SocketTypes.Fa4);
        map.put(SocketType.G635, SocketTypes.G635);
        map.put(SocketType.GU53, SocketTypes.GU53);
        map.put(SocketType.GX53, SocketTypes.GX53);
        map.put(SocketType.GY635, SocketTypes.GY635);
        map.put(SocketType.S14d, SocketTypes.S14d);
        map.put(SocketType.S14s, SocketTypes.S14s);
        map.put(SocketType.R7s, SocketTypes.R7s);
        SOCKET_TYPES = ImmutableBiMap.copyOf(EnumHashBiMap.create(map));
    }
}
