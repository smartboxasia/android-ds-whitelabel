package com.smartboxasia.control.data.connection;

import com.smartboxasia.control.data.config.connection.ConnectionData;

public interface ConnectionServiceProvider {

    /**
     * Returns if this provider can create a connection service for the requested version
     *
     * @param version the version the connection service would have to support
     * @return true if this provider can create a connection service for the requested version
     */
    public abstract boolean supportsVersion(String version);

    /**
     * Returns if this provider can create a connection service for the requested service type (e.g. direct or cloud)
     *
     * @param connectionType the type of connection the connection service would have to support
     * @return true if this provider can create a connection service for the requested service type (e.g. direct or cloud)
     */
    public abstract boolean supportsConnectionType(int connectionType);

    /**
     * Creates and returns a connection service
     *
     * @param connectionData the connection data the service should use
     * @return the connection service
     */
    public abstract ConnectionService createConnectionService(final ConnectionData connectionData);

}
