package com.smartboxasia.control.data.connection;

import java.io.IOException;

import com.smartboxasia.control.data.config.connection.ConnectionData;
import com.smartboxasia.control.data.connection.ConnectionHandler.ConnectionTask;
import com.smartboxasia.control.data.connection.exception.NeedsSessionRefreshException;
import com.smartboxasia.control.data.helper.CommandBuilder;
import com.smartboxasia.control.data.helper.JSONCallback;
import com.smartboxasia.control.data.helper.JSONCallbackBase;
import com.smartboxasia.control.domain.LoginResult;

public interface ConnectionService {

    public static final long MIN_REQUEST_DEALY = 1500;

    /**
     * Returns the currently used connection data
     *
     * @return the currently used connection data or null if none is available
     */
    public ConnectionData getActiveConnectionData();

    /**
     * Returns the currently used connection id
     *
     * @return the currently used connection id or -1 if none is available
     */
    public long getActiveConnectionId();

    /**
     * Returns the currently connected server id
     *
     * @return the currently used server id or an empty string if none is available
     */
    public String getActiveServerId();

    /**
     * Shuts down the polling and releases all tasks
     */
    public void shutdown();

    /**
     * Trys to log into the server defined by the connectionData
     *
     * @param connectionData the connection data to use for the connection
     * @param user the username for the server, can be null if we already have an enabled application token
     * @param pass the password for the server, can be null if we already have an enabled application token
     */
    public void login(final ConnectionData connectionData, final String user, final String pass);

    /**
     * Renews the session token
     */
    public void refreshSession(final ConnectionData data, boolean silent);

    public LoginResult refreshSessionSync(ConnectionData data);

    <T> void sendAndParseCloudGetCommand(CommandBuilder command, JSONCallbackBase<T> parser);

    <T> void sendAndParseCloudPostCommand(CommandBuilder command, JSONCallbackBase<T> parser);

    <T> void sendAndParseDirectGetCommand(CommandBuilder command, JSONCallbackBase<T> parser);

    <T> void sendAndParseDirectPostCommand(CommandBuilder command, JSONCallbackBase<T> parser);

    void sendRawCommand(String url, String body, JSONCallback parser, boolean useGetMethod);

    void executeTask(ConnectionTask task);

    <T> void sendAndParseDirectPostCommandWithBody(CommandBuilder command, String body, JSONCallbackBase<T> parser);

    <T> void sendAndParseCloudPostCommandWithBody(CommandBuilder command, String body, JSONCallbackBase<T> parser);

    String sendCommandAuthenticated(CommandBuilder commandUrl, String body, boolean direct, boolean useGetMethod) throws IOException, NeedsSessionRefreshException;

}
