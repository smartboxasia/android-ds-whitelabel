package com.smartboxasia.control.data.dss.direct_1_17_0;

import java.util.List;

import com.smartboxasia.control.domain.OutputMode;
import com.google.common.collect.Lists;

class DsDeviceUmr200Behavior implements DsDeviceBehavior {

    private boolean isDevice3;

    public DsDeviceUmr200Behavior(final boolean isDevice3) {
        this.isDevice3 = isDevice3;
    }

    @Override
    public List<OutputMode> getAvailableOutputModes() {
        if (isDevice3) {
            return Lists.newArrayList(OutputMode.DISABLED, OutputMode.SWITCHED_SINGLE_POLARITY, OutputMode.SWITCHED_DOUBLE_POLARITY, OutputMode.SWITCH_TWO_STEP, OutputMode.SWITCH_THREE_STEP);
        } else { // isDevice4
            return Lists.newArrayList(OutputMode.DISABLED, OutputMode.SWITCHED_SINGLE_POLARITY);
        }
    }

    @Override
    public boolean isOemDevice() {
        return false;
    }

    @Override
    public boolean canModifyOutputMode() {
        return false;
    }

}
