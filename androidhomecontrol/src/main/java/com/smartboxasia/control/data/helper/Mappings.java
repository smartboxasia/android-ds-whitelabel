package com.smartboxasia.control.data.helper;

import com.google.common.collect.BiMap;

public abstract class Mappings {

    public static <K, V> V findValue(final BiMap<K, V> map, final K key, final V defaultValue) {
        final V value = findValue(map, key);
        if (value == null) {
            return defaultValue;
        } else {
            return value;
        }
    }

    public static <K, V> V findValue(final BiMap<K, V> map, final K key) {
        return map.get(key);
    }

    public static <K, V> K findKey(final BiMap<K, V> map, final V value, final K defaultKey) {
        final K key = findKey(map, value);
        if (key == null) {
            return defaultKey;
        } else {
            return key;
        }
    }

    public static <K, V> K findKey(final BiMap<K, V> map, final V value) {
        return map.inverse().get(value);
    }
}
