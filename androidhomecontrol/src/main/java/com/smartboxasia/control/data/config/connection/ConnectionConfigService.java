package com.smartboxasia.control.data.config.connection;

import java.util.List;

import com.smartboxasia.control.App;

public class ConnectionConfigService {

    private static ConfigOpenHelper connectionConfig = new ConfigOpenHelper(App.getInstance());

    public static void updateConnectionData(final ConnectionData data) {
        connectionConfig.updateConnectionData(data);
    }

    public static void addConnectionData(final ConnectionData data) {
        connectionConfig.addConnectionData(data);
    }

    public static ConnectionData getConnectionData(final String url) {
        return connectionConfig.getConnectionData(url);
    }

    public static void deleteConnectionData(final String url) {
        connectionConfig.deleteConnectionData(url);
    }

    public static List<ConnectionData> getAllConnectionData() {
        return connectionConfig.getAllConnections();
    }

    public static void updateLastUsedConnectionData(final String url) {
        connectionConfig.updateLastUsedConnectionData(url);
    }

    public static void deleteLastUsedConnectionData() {
        connectionConfig.deleteLastUsedConnection();
    }

    public static ConnectionData getLastUsedConnectionData() {
        return connectionConfig.getLastUsedConnectionData();
    }

    public static String getGlobalCloudUrl() {
        return connectionConfig.getGlobalCloudUrl();
    }

    public static void setDebugCloudLoginUrl(final String string) {
        connectionConfig.setDebugCloudLoginUrl(string);
    }
}
